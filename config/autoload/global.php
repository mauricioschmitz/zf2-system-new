<?php
/**
 * Local Configuration Override
 *
 * This configuration override file is for overriding environment-specific and
 * security-sensitive configuration information. Copy this file without the
 * .dist extension at the end and populate values as needed.
 *
 * @NOTE: This file is ignored from Git by default with the .gitignore included
 * in ZendSkeletonApplication. This is a good practice, as it prevents sensitive
 * credentials from accidentally being committed into version control.
 */

return array(
    'module_layouts' => array(
        'Login'        => 'layout/login',
        'Admin'        => 'layout/admin',
        'Affiliate'    => 'layout/affiliate',
        'Base'         => 'layout/base',
        'Jornal'       => 'layout/jornal',
        'Portfolio'    => 'layout/portfolio',
        'Political'    => 'layout/political',
        'Blog'         => 'layout/blog',
    ),
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => array(
                    'host' => 'localhost',
                    'port' => '3306',
                    'user' => 'u210603454_alter',//'root',
                    'password' => 'Hcv8ZeeXNbuS',//'hqJLX2QkhDmHTsaE',
                    'dbname' => 'u210603454_vale',//'valealternativo',
//                    'user' => 'u854692975_gt10',
//                    'password' => '44LBz2SiHx',
//                    'dbname' => 'u854692975_top10',
//                    'user' => 'u112741921_ms',
//                    'password' => '6bBwiaV5b6',
//                    'dbname' => 'u112741921_ms',
//                    'user' => 'u125875216_camp',
//                    'password' => 'nZsh7jVPZm',
//                    'dbname' => 'u125875216_camp',
//                    'user' => 'localhost',
//                    'password' => 'localhost',
//                    'dbname' => 'zf2_system_new',
//                    'dbname' => 'grupotop10',
//                    'dbname' => 'grupotop10teste',
                    'driverOptions' => array(
                        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"
                    )
                )
            )
        ),
        'configuration' => array(
            'orm_default' => array(
                'numeric_functions'=> array(
                    'Rand' => 'Doctrine\ORM\Extension\Rand'
                )
            )
        ),
        'authentication' => array(
            'orm_default' => array(
                'object_manager' => 'Doctrine\ORM\EntityManager',
                'identity_class' => 'Login\Entity\User',
                'identity_property' => 'email',
                'credential_property' => 'password',
                'credential_callable' => function(Login\Entity\User $user, $passwordGiven) {
                    if($user->getPassword() == $passwordGiven)
                        return (bool)$user->getActive();
                    return false;
                },
            ),
        ),
    ),
    'myConfiguration' => array(
        'firephp' => 0
    ),
    'session' => array(
        'use_cookies' => true,
        'cookie_httponly' => true,
    ),
    'mail_auth' => array(
        'name'              => 'GMAIL',
        'host'              => 'smtp.gmail.com',
        'port'              => 465,
        'connection_class'  => 'login',
        'connection_config' => array(
            'username' => 'auth@mauricioschmitz.com.br',
            'password' => '-AgE2re5UseW',
            'ssl'      => 'ssl',
        ),
    ),
    
);
