<?php
namespace Affiliate\Form\Filter;

use Admin\Form\Filter\PessoaFisicaFilter;
/**
 * Description of PessoaFisicaFilter
 *
 * @author mauricioschmitz
 */
class AfiliadoPessoaFisicaFilter extends PessoaFisicaFilter {
    
    public function __construct() {
        $isEmpty = \Zend\Validator\NotEmpty::IS_EMPTY;
       
        $array = array('email'=>'E-mail', 'password'=>'Senha');
        foreach ($array as $key=>$value){
            $this->add(array(
                'name' => $key,
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'StripTags'
                    ),
                    array(
                        'name' => 'StringTrim'
                    )
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                $isEmpty => ($key == 'cnpj' ? 'Informe um '. $value.' válido' : $value.' não deve estar vazio')
                            )
                        ),
                        'break_chain_on_failure' => true
                    )
                )
            ));
        }
    }
}
