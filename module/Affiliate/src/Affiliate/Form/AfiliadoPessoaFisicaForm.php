<?php

namespace Affiliate\Form;

use Admin\Form\PessoaFisicaForm;
use Affiliate\Form\Filter\AfiliadoPessoaFisicaFilter;
/**
 * Description of PessoaFisicaForm
 *
 * @author mauricioschmitz
 */
class AfiliadoPessoaFisicaForm extends PessoaFisicaForm {
    
    protected $em;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        
        $this->em = $em;
        
        parent::__construct($em);

        $this->setInputFilter(new AfiliadoPessoaFisicaFilter());
        
        //Input titulo
        $this->add(array(
            'name' => 'email',
            'type' => 'email',
            'attributes' => array(
                'id' => 'email',
                'required' => 'true',
                'class' => 'form-control',
                'placeholder' => 'example@gmail.com',
                'title' => 'Informe seu e-mail'
            ),
            'options' => array(
                'label' =>'E-mail',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input titulo
//        $this->add(array(
//            'name' => 'indicacao',
//            'type' => 'text',
//            'attributes' => array(
//                'id' => 'indicacao',
//                'class' => 'form-control',
//                'autofocus' => 'true',
//                'title' => 'Informe o código do usuário que indicou você'
//            ),
//            'options' => array(
//                'label' =>'Indicação',
//                'label_attributes' => array(
//                    'class'  => 'col-sm-2 control-label'
//                ),
//            )
//        ));
        $this->add(array(
            'name' => 'old_password',
            'type' => 'password',
            'attributes' => array(
                'id' => 'old_password',
                'required' => 'true',
                'class' => 'form-control',
                'placeholder' => 'Informe sua senha antiga',
                'title' => 'Informe sua senha antiga'
            ),
            'options' => array(
                'label' =>'Senha antiga',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        $this->add(array(
            'name' => 'password',
            'type' => 'password',
            'attributes' => array(
                'id' => 'password',
                'required' => 'true',
                'class' => 'form-control',
                'placeholder' => '*****',
                'title' => 'Informe sua senha'
            ),
            'options' => array(
                'label' =>'Senha',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        $this->add(array(
            'name' => 'password_confirm',
            'type' => 'password',
            'attributes' => array(
                'id' => 'password_confirm',
                'required' => 'true',
                'class' => 'form-control',
                'placeholder' => '*****',
                'data-rule-equalTo' => '#password',
                'title' => 'Repita sua senha'
            ),
            'options' => array(
                'label' =>'Repita a senha',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            ),
            'validators' => array(
                array(
                    'name' => 'Identical',
                    'options' => array(
                        'token' => 'password', // name of first password field
                    ),
                ),
            ),
        ));
        
        //Input titulo
        $this->add(array(
            'name' => 'plano_id',
            'type' => 'radio',
            'attributes' => array(
                'id' => 'plano_id',
                'required' => 'true',
                'class' => 'form-control',
            ),
            'options' => array(
                'value_options' => $this->getPlanosOptions(),
                'label' =>'Plano',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input titulo
        $this->add(array(
            'name' => 'approve',
            'type' => 'checkbox',
            'attributes' => array(
                'id' => 'approve',
                'required' => 'true',
                'class' => 'form-control',
            ),
            'options' => array(
                'label' =>'Concordo com os termos',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
    }
   
    public function getPlanosOptions(){
        $list = $this->em->getRepository('Admin\Entity\Plano')->findBy(array('ativo'=>1));
        foreach ($list as $plano) {
            $selectData[$plano->getId()] = $plano->getTitulo();
        }
        return $selectData;
    }
}
