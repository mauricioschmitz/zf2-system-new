<?php

namespace Affiliate\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Admin\Entity\PessoaFisica;
use Login\Entity\User;
use Login\Entity\UserRole;
use Login\Utility\UserPassword;
use Zend\Crypt\Password\Bcrypt;
use Base\Util\Mail;
use Base\Util\Util;

class CadastroController extends AbstractController
{
    public function __construct() {
        $this->form = 'Affiliate\Form\AfiliadoPessoaFisicaForm';
        $this->controller = 'cadastro';
        $this->route = 'cadastro/default';
        $this->service = 'Affiliate\Service\AfiliadoService';
        $this->entity = 'Admin\Entity\Afiliado';
        $this->scripts_styles = array(
            array('content'=>'Scripts.InitMasks();', 'type'=>'headscript', 'function'=>'appendScript'),
        );
        $this->layout()->menu  = false;
    }
    
    public function insertAction(){
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $this->layout()->tituloTela = $this->tituloTela . ' | Adicionar';
        
        if(is_string($this->form))
            $form = new $this->form($em);
        else 
            $form = $this->form($em);
        
        $request = $this->getRequest();
        if($request->isPost()){
            $form->setData($request->getPost());
            $params = $request->getPost();

            $em = $this->getEm();
            $pessoas = $em->getRepository('Admin\Entity\PessoaFisica')->findBy(array('cpf'=>$this->params()->fromPost('cpf', '')));
            $user = $em->getRepository('Login\Entity\User')->findBy(array('email'=>$this->params()->fromPost('email', '')));
            if(count($pessoas) > 0){
                $this->flashMessenger()->addErrorMessage('CPF Já cadastrado!');  
                
            }elseif(count($user) > 0){
                $this->flashMessenger()->addErrorMessage('E-mail Já cadastrado!');  
                
            }elseif(Util::calculaIdade($params['data_nascimento']) < 18){
                $this->flashMessenger()->addErrorMessage('Você deve ter mais de 18 anos para se cadastrar');  
                
            }elseif(!isset($params['approve']) || !$params['approve']){
                $this->flashMessenger()->addInfoMessage('Você deve ler e concordar com os termos');
            }else{
                if($form->isValid()){
                    $parametros = $params->toArray();
                    $dataNascimento = $parametros['data_nascimento'];
                    $pessoaFisica = new PessoaFisica($params->toArray());
                    $pessoaFisica->setDataNascimento(new \DateTime(implode('-', array_reverse(explode('/',$dataNascimento)))));
                    $pessoaFisica->setFones($params['telefone'].'<|>'.$params['celular']);
                    
                    $em->persist($pessoaFisica);
                    $em->flush();
                    
                    $user = new User($params->toArray());
                    $user->setName($params['nome']);
                    $util = new UserPassword();
                    $user->setPassword($util->create($params['password']));
                    
                    $em->persist($user);
                    $em->flush();
                    
                    $role = $em->getRepository('Login\Entity\Role')->findOneBy(array('roleName'=> 'Affiliate'));
                    $userRole = new UserRole(array('user'=>$user,'role'=>$role));
                    
                    $em->persist($userRole);
                    $em->flush();
                    
                    $service = $this->getServiceLocator()->get($this->service);
                    $data = $request->getPost()->toArray();
                    
                    $plano = $em->getRepository('Admin\Entity\Plano')->find($params['plano_id']);
                    $data['nome_plano'] = $plano->getTitulo();
                    $data['valor_investimento'] = $plano->getValor();
                    $data['cota'] = $plano->getCota();
                    $data['percentual_indicacao'] = $plano->getPercentualIndicacao();
                    $data['pontos'] = $plano->getPontos();
                    $data['valor_saque'] = $plano->getValorSaque();
                    $data['inicio_vigencia'] = new \DateTime('0000-00-00');
                    $data['fim_vigencia'] = new \DateTime('0000-00-00');
                    $data['indicacao'] = (isset($_SESSION['INDICACAO']['id']) ? $_SESSION['INDICACAO']['id'] : '');
                    $externalObjects = array(
                        'pessoaFisica'=>'Admin\Entity\PessoaFisica',
                        'user'=>'Login\Entity\User'
                    );
                    foreach($externalObjects as $campo=>$entity){
                        $busca = $this->getEm()->getRepository($entity)->find(${$campo}->getId());
                        $data[$campo] = $busca;
                    }
                    
                    
                    if($afiliado = $service->save($data)){
                        $this->flashMessenger()->addSuccessMessage('Cadastro realizado com sucesso!');
                        $config = $this->getServiceLocator()->get('config');
                        $config = $config['sendemail'];
                        $mail = $this->getServiceLocator()->get('Email');
                        $mail->sendMail($config['fromMail'], $config['fromName'], $user->getEmail(), $user->getName(), false, false, \Base\Util\Mail::$CONFIRMARCADASTRO['assunto'],  array('mensagem'=>\Base\Util\Mail::$CONFIRMARCADASTRO['mensagem'], 'variaveis'=>array('login'=>$user->getEmail(), 'link'=>$this->getEvent()->getRouter()->assemble(array(''), array('name'=>'adm', 'force_canonical'=>true)))), false, $em);
                        unset($_SESSION['INDICACAO']);
                        return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller, 'action'=>'informacoes', 'id'=>  $afiliado->getId()));

                    } else {
                        $em->remove($pessoaFisica);
                        $em->flush();
                        $em->remove($userRole);
                        $em->flush();
                        $em->remove($user);
                        $em->flush();
                        $this->flashMessenger()->addErrorMessage('Ocorreu um erro!');  
                    }
                    return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller));

                }else{
                    $this->flashMessenger()->addErrorMessage('Dados inseridos inválidos');
                }
            }
            
            
            
        }
        
        $this->scriptsStyles();
        
        return new ViewModel(array('form'=>$form, 'route'=>$this->route, 'controller'=>$this->controller ,'em'=>$em, 'params'=>$this->aditionalParameters(), 'plano'=>$this->params()->fromRoute('id'), 'indicacao'=>(isset($_SESSION['INDICACAO']) ? $_SESSION['INDICACAO'] : '')));
        
    }
    
    public function informacoesAction(){
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $this->layout()->tituloTela = $this->tituloTela;
        $afiliado = $em->getRepository('Admin\Entity\Afiliado')->find($this->params()->fromRoute('id', ''));
        $texto = $em->getRepository('Admin\Entity\Texto')->findOneBy(array('apelido'=>'contas_deposito'));
        return new ViewModel(array('afiliado'=>$afiliado, 'texto' => $texto));
    }
    
    public function aditionalParameters(){
        $em = $this->getEm();
        $repository = $em->getRepository('Admin\Entity\Plano');
        $planos = $repository->findBy(array('ativo'=>1), array('pontos'=>'ASC'));
        
        $texto = $em->getRepository('Admin\Entity\Texto')->findOneBy(array('apelido'=>'contrato'));
        
        return array('planos'=>$planos, 'planosselect'=>true, 'plano'=>$this->params()->fromRoute('plano'), 'texto'=>$texto);
    }
}
