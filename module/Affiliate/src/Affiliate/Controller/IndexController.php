<?php

namespace Affiliate\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractController
{
    public function __construct() {
        $this->form = 'Admin\Form\Admin';
        $this->controller = 'Admin';
        $this->route = 'adm/default';
        $this->service = 'Admin\Service\AdminService';
        $this->entity = 'Admin\Entity\Admin';
        
        
    }
    public function indexAction()
    {
        $this->layout()->menu  = true;
        $em = $this->getEm();
        
        $repository = $em->getRepository('Admin\Entity\Plano');
        $planos = $repository->findBy(array('ativo'=>1), array('pontos'=>'ASC'));
        
        $config = $em->getRepository('Admin\Entity\AfiliadoConfig')->findOneBy(array('ativo'=>1));
        
        if($this->params()->fromQuery('id', '') != ''){
            $afiliadoRep = $em->getRepository('Admin\Entity\Afiliado');
            $afiliado = $afiliadoRep->find($this->params()->fromQuery('id'));
            $_SESSION['INDICACAO'] = array('id'=>$afiliado->getId(), 'nome'=>$afiliado->getPessoaFisica()->getNome());
        }
        
        return new ViewModel(array('params'=>array('planos'=>$planos, 'config'=>$config)));
    }

    public function apresentacaoAction(){
        // Turn off the layout, i.e. only render the view script.
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        return $viewModel;
    }

    public function aditionalParameters(){
        return array();
    }
}
