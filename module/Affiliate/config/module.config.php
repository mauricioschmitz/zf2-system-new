<?php
namespace Affiliate;

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Affiliate\Controller\Index',
                        'action'     => 'index',
                    ),
                ),

            ),
            'apresentacao' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/apresentacao',
                    'defaults' => array(
                        'controller' => 'Affiliate\Controller\Index',
                        'action'     => 'apresentacao',
                    ),
                ),

            ),
            'cadastro' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/cadastro',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Affiliate\Controller',
                        'controller'    => 'Cadastro',
                        'action'        => 'insert',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action[/:id]]]',
                            'constraints' => array(
                                'id'         => '\d+',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    )
                ),
            )
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Affiliate\Controller\Index' => 'Affiliate\Controller\IndexController',
            'Affiliate\Controller\Cadastro' => 'Affiliate\Controller\CadastroController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/affiliate.phtml',
            'affiliate/index/index' =>    __DIR__ . '/../view/affiliate/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
            'partial/planoslist'      => __DIR__ . '/../view/affiliate/partial/planoslist.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'analytics' => array(
        'GA_ID' => 'UA-80618402-1',
    ),
    'sendemail' => array(
        'toMail' => 'odair.neckel@hotmail.com',
        'toName' => 'Grupo Top 10',
        'fromMail' => 'odair.neckel@hotmail.com',
        'fromName' => 'Grupo Top 10',
    ),
    'TpMinify' => array(
        'serveOptions' => array(
            'minApp' => array(
                'groups' => array(
                    'aff-js' => array(
                        getcwd() . '/public/libs/jquery/dist/jquery.min.js',
                        getcwd() . '/public/libs/jquery-ui/jquery-ui.min.js',
                        getcwd() . '/public/libs/slimScroll/jquery.slimscroll.min.js',
                        getcwd() . '/public/libs/bootstrap/dist/js/bootstrap.min.js',
                        getcwd() . '/public/libs/metisMenu/dist/metisMenu.min.js',
                        getcwd() . '/public/libs/iCheck/icheck.min.js',
                        getcwd() . '/public/libs/sparkline/index.js',
                        getcwd() . '/public/libs/moment/moment.js',
                        getcwd() . '/public/libs/moment/locale/pt-br.js',
                        getcwd() . '/public/libs/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js',
                        getcwd() . '/public/libs/bootstrap-datepicker-master/dist/locales/bootstrap-datepicker.pt-BR.min.js',
                        getcwd() . '/public/libs/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                        getcwd() . '/public/libs/maskedInput/jquery.maskedinput.js',
                        getcwd() . '/public/libs/maskmoney/jquery.maskMoney.js',
                        getcwd() . '/public/libs/jquery-validation/jquery.validate.min.js',
                        getcwd() . '/public/libs/jquery-validation/pt-BR.js',
                        getcwd() . '/public/libs/clipboard.js-master/dist/clipboard.min.js',
                        getcwd() . '/public/libs/summernote/summernote.js',
                        getcwd() . '/public/libs/summernote/lang/summernote-pt-BR.js',
                        getcwd() . '/public/js/homer.js',
                        getcwd() . '/public/js/scripts.js',
                    ),
                ),
            )
        ),
    ),
);
