<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Affiliate;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Zend\Session\Config\SessionConfig;
use Zend\Session\SessionManager;
use Zend\EventManager\EventInterface;
use Zend\Mvc\Service\ViewHelperManagerFactory;
use Affiliate\Service\AfiliadoService;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $config = $e->getApplication()
                  ->getServiceManager()
                  ->get('Configuration');

        $sessionConfig = new SessionConfig();
        $sessionConfig->setOptions($config['session']);
        $sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();
        
        $em = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($em);
//        $em->attach('route', array($this, 'doHttpsRedirect'));
        /**
         * Optional: If you later want to use namespaces, you can already store the 
         * Manager in the shared (static) Container (=namespace) field
         */
        Container::setDefaultManager($sessionManager);
        
        $config = $e->getApplication()->getServiceManager()->get('config');
        $layout = $e->getViewModel();
        $layout->GA_ID = $config['analytics']['GA_ID'];
    }

    public function doHttpsRedirect(MvcEvent $e){
        $sm = $e->getApplication()->getServiceManager();
        $uri = $e->getRequest()->getUri();
        $scheme = $uri->getScheme();
        if ($scheme != 'https'){
            $uri->setScheme('https');
            $response=$e->getResponse();
            $response->getHeaders()->addHeaderLine('Location', $uri);
            $response->setStatusCode(302);
            $response->sendHeaders();
            return $response;
        }
    }
    
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig(){
        
        return array(
            'factories' => array(
                'Affiliate\Service\AfiliadoService' => function($em){
                    return new AfiliadoService($em->get('Doctrine\ORM\EntityManager'));
                }
            ),
            'invokables' => array(
                //'FirePhpProfiler' => 'Admin\Profiler\FirePhpProfiler',
            ),
            
        );
    }
    
}
