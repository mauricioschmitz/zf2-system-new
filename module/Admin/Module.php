<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin;

use Admin\Entity\RastreamentoOcorrencia;
use Admin\Service\RastreamentoOcorrenciaService;
use Admin\Service\RastreamentoService;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Service\ViewHelperManagerFactory;
use Zend\Authentication\AuthenticationService;
use Admin\Service\CategoriaService;
use Admin\Service\NoticiaService;
use Admin\Service\PublicidadeService;
use Admin\Service\ImagemNoticiaService;
use Admin\Service\PerfilService;
use Admin\Service\PlanoService;
use Admin\Service\PessoaFisicaService;
use Admin\Service\PessoaJuridicaService;
use Admin\Service\TextoService;
use Admin\Service\UpgradeAfiliadoService;
use Admin\Service\AfiliadoConfigService;
use Admin\Service\SaqueAfiliadoService;
use Admin\Service\AfiliadoService;
use Admin\Service\VeiculoService;
use Admin\Service\GastoGasolinaService;
use Admin\Service\ContaService;
use Admin\Service\ContaPagarService;
use Admin\Service\CategoriaContaService;
use Admin\Service\ContaReceberService;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $eventManager->attach (
            MvcEvent::EVENT_ROUTE,
            function  (MvcEvent $e)
            {
                $serviceManager = $e->getApplication()->getServiceManager();
                $authenticationService = $serviceManager->get('Zend\Authentication\AuthenticationService');
                $loggedUser = $authenticationService->getIdentity();
                // update 
                $layout = $e->getViewModel();
                $layout->userinfo = $loggedUser;
                
                if(!is_null($loggedUser)){
                    $em = $serviceManager->get('Doctrine\ORM\EntityManager');
                    foreach($loggedUser->getRoles() as $role){
                        if($role->getRoleName() == 'Affiliate'){
                            $afiliado = $em->getRepository('Admin\Entity\Afiliado')->findOneBy(array('user'=> $loggedUser));
                            $layout->afiliado = $afiliado;
                        }
                        
                        if($role->getRoleName() == 'Affiliate-Admin'){
                            //Conta os novos afiliados
                            $layout->novosAfiliados = 0;
                            $qb = $em->createQueryBuilder();
                            $qb->select($qb->expr()->count('a.id'));
                            $qb->from('Admin\Entity\Afiliado','a');
                            $qb->andWhere('a.ativo = 1');
                            $qb->andWhere('a.pago <> 1');
                            $count = $qb->getQuery()->getSingleScalarResult();
                            $layout->novosAfiliados = $count;
                            
                            //Conta os novos afiliados
                            $layout->novosUpgrades = 0;
                            $qb = $em->createQueryBuilder();
                            $qb->select($qb->expr()->count('u.id'));
                            $qb->from('Admin\Entity\UpgradeAfiliado','u');
                            $qb->andWhere('u.ativo = 1');
                            $qb->andWhere('u.situacao <> 1');
                            $count = $qb->getQuery()->getSingleScalarResult();
                            $layout->novosUpgrades = $count;
                            
                            //Conta os novos afiliados
                            $layout->novosSaques = 0;
                            $qb = $em->createQueryBuilder();
                            $qb->select($qb->expr()->count('s.id'));
                            $qb->from('Admin\Entity\SaqueAfiliado','s');
                            $qb->andWhere('s.ativo = 1');
                            $qb->andWhere('s.situacao = 0');
                            $count = $qb->getQuery()->getSingleScalarResult();
                            $layout->novosSaques = $count;
                        }
                    }
                }
       });
       
       
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig(){
        
        return array(
            'factories' => array(
                'Admin\Service\CategoriaService' => function($em){
                    return new CategoriaService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\PublicidadeService' => function($em){
                    return new PublicidadeService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\NoticiaService' => function($em){
                    return new NoticiaService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\ImagemNoticiaService' => function($em){
                    return new ImagemNoticiaService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\PerfilService' => function($em){
                    return new PerfilService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\PlanoService' => function($em){
                    return new PlanoService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\PessoaFisicaService' => function($em){
                    return new PessoaFisicaService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\PessoaJuridicaService' => function($em){
                    return new PessoaJuridicaService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\TextoService' => function($em){
                    return new TextoService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\UpgradeAfiliadoService' => function($em){
                    return new UpgradeAfiliadoService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\AfiliadoConfigService' => function($em){
                    return new AfiliadoConfigService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\SaqueAfiliadoService' => function($em){
                    return new SaqueAfiliadoService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\AfiliadoService' => function($em){
                    return new AfiliadoService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\VeiculoService' => function($em){
                    return new VeiculoService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\GastoGasolinaService' => function($em){
                    return new GastoGasolinaService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\ContaPagarService' => function($em){
                    return new ContaPagarService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\CategoriaContaService' => function($em){
                    return new CategoriaContaService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\ContaReceberService' => function($em){
                    return new ContaReceberService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\ContaService' => function($em){
                    return new ContaService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\RastreamentoOcorrenciaService' => function($em){
                    return new RastreamentoOcorrenciaService($em->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Service\RastreamentoService' => function($em){
                    return new RastreamentoService($em->get('Doctrine\ORM\EntityManager'));
                }
            ),
        );
    }
    
}
