<?php
namespace Admin;

return array(
    'router' => array(
        'routes' => array(
            'adm' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/adm',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action[/:id[/:ativo]]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'         => '\d+',
                                'ativo'         => '\d+',
                            ),
                            'query' => array(
                                'type' => 'Query',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    )
                ),
            ),
            'uploadeditor' =>array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/uploadeditor',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'Index',
                        'action'        => 'uploadeditor',
                    ),
                ),
            ),
            'galeriaeditor' =>array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/galeriaeditor',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'Index',
                        'action'        => 'galeriaeditor',
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Admin\Controller\Index' => 'Admin\Controller\IndexController',
            'Admin\Controller\Publicidade' => 'Admin\Controller\PublicidadeController',
            'Admin\Controller\Categoria' => 'Admin\Controller\CategoriaController',
            'Admin\Controller\Noticia' => 'Admin\Controller\NoticiaController',
            'Admin\Controller\Imagemnoticia' => 'Admin\Controller\ImagemnoticiaController',
            'Admin\Controller\Perfil' => 'Admin\Controller\PerfilController',
            'Admin\Controller\Plano' => 'Admin\Controller\PlanoController',
            'Admin\Controller\Pessoafisica' => 'Admin\Controller\PessoafisicaController',
            'Admin\Controller\Pessoajuridica' => 'Admin\Controller\PessoajuridicaController',
            'Admin\Controller\Confirmarpagamento' => 'Admin\Controller\ConfirmarpagamentoController',
            'Admin\Controller\Texto' => 'Admin\Controller\TextoController',
            'Admin\Controller\Afiliado' => 'Admin\Controller\AfiliadoController',
            'Admin\Controller\Extratoconta' => 'Admin\Controller\ExtratocontaController',
            'Admin\Controller\Upgrade' => 'Admin\Controller\UpgradeController',
            'Admin\Controller\Afiliadoconfig' => 'Admin\Controller\AfiliadoconfigController',
            'Admin\Controller\Saqueafiliado' => 'Admin\Controller\SaqueafiliadoController',
            'Admin\Controller\Renovacao' => 'Admin\Controller\RenovacaoController',
            'Admin\Controller\Afiliadopagamento' => 'Admin\Controller\AfiliadopagamentoController',
            'Admin\Controller\Afiliadodistribuicao' => 'Admin\Controller\AfiliadodistribuicaoController',
            'Admin\Controller\Minhaequipe' => 'Admin\Controller\MinhaequipeController',
            'Admin\Controller\Veiculo' => 'Admin\Controller\VeiculoController',
            'Admin\Controller\Gastogasolina' => 'Admin\Controller\GastogasolinaController',
            'Admin\Controller\Conta' => 'Admin\Controller\ContaController',
            'Admin\Controller\Contapagar' => 'Admin\Controller\ContapagarController',
            'Admin\Controller\Contareceber' => 'Admin\Controller\ContareceberController',
            'Admin\Controller\Categoriaconta' => 'Admin\Controller\CategoriacontaController',
            'Admin\Controller\Rastreamento' => 'Admin\Controller\RastreamentoController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/admin.phtml',
//            'admin/index/index' =>    __DIR__ . '/../view/admin/index/index.phtml',
//            'admin/categoria/index' =>    __DIR__ . '/../view/admin/categoria/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
            
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
        
        
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'TpMinify' => array(
        'serveOptions' => array(
            'minApp' => array(
                'groups' => array(
                    'admin-css' => array(
                        getcwd() . '/public/libs/toastr/build/toastr.min.css',
                        getcwd() . '/public/libs/metisMenu/dist/metisMenu.min.css',
                        getcwd() . '/public/libs/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css',
                        getcwd() . '/public/libs/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                        getcwd() . '/public/libs/animate.css/animate.min.css',
                        getcwd() . '/public/fonts/pe-icon-7-stroke/css/helper.css',
                        getcwd() . '/public/libs/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css',
                        getcwd() . '/public/css/admin/styles.css',
                        getcwd() . '/public/css/styles.css',
                        getcwd() . '/public/css/static_custom.css',
                    ),
                    'admin-js' => array(
                        getcwd() . '/public/libs/jquery/dist/jquery.min.js',
                        getcwd() . '/public/libs/slimScroll/jquery.slimscroll.min.js',
                        getcwd() . '/public/libs/bootstrap/dist/js/bootstrap.min.js',
                        getcwd() . '/public/libs/metisMenu/dist/metisMenu.min.js',
                        getcwd() . '/public/libs/iCheck/icheck.min.js',
                        getcwd() . '/public/libs/sparkline/index.js',
                        getcwd() . '/public/libs/moment/moment.js',
                        getcwd() . '/public/libs/moment/locale/pt-br.js',
                        getcwd() . '/public/libs/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js',
                        getcwd() . '/public/libs/bootstrap-datepicker-master/dist/locales/bootstrap-datepicker.pt-BR.min.js',
                        getcwd() . '/public/libs/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                        getcwd() . '/public/libs/maskmoney/jquery.maskMoney.js',
                        getcwd() . '/public/libs/jquery-validation/jquery.validate.min.js',
                        getcwd() . '/public/libs/jquery-validation/pt-BR.js',
                        getcwd() . '/public/libs/summernote/summernote.js',
                        getcwd() . '/public/libs/summernote/lang/summernote-pt-BR.js',
                        getcwd() . '/public/libs/clipboard.js-master/dist/clipboard.min.js',
                        getcwd() . '/public/libs/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js',
                        getcwd() . '/public/libs/toastr/build/toastr.min.js',
                        getcwd() . '/public/js/homer.js',
                        getcwd() . '/public/js/scripts.js',
                    )
                )
            )
        ),
    ),
    'google' => array(
        'api_key' => 'AIzaSyAKXClpdO8uXsuz_0soA8WYmplRV0eBRpA',
    ),
    'sendemail' => array(
        'from' => 'contato@mauricioschmitz.com.br',
        'mail_auth' => array(
            'host'              => 'smtp.gmail.com',
            'connection_class'  => 'plain',
            'port' => 587,
            'connection_config' => array(
                'username' => 'contato@mauricioschmitz.com.br',
                'password' => 'brunaeuteamo',
                'ssl' => 'tls'
            )
        )
    ),
);
