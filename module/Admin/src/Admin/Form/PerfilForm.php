<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\PerfilFilter;
use Affiliate\Form\AfiliadoPessoaFisicaForm;
/**
 * Description of PerfilForm
 *
 * @author mauricioschmitz
 */
class PerfilForm extends AfiliadoPessoaFisicaForm {
   protected $em;
   
   public function __construct(\Doctrine\ORM\EntityManager $em) {
        $this->em = $em;
        
        parent::__construct($em);

        $this->setInputFilter(new PerfilFilter());
        $this->setAttribute('id', 'formPerfil');
        //Input nome
        $this->add(array(
            'name' => 'name',
            'type' => 'text',
            'attributes' => array(
                'id' => 'name',
                'required' => 'true',
                'class' => 'form-control',
                'placeholder' => 'Informe seu nome',
                'autofocus' => 'true',
                'title' => 'Informe seu nome'
            ),
            'options' => array(
                'label' =>'Nome',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input titulo
        $this->add(array(
            'name' => 'email',
            'type' => 'email',
            'attributes' => array(
                'id' => 'email',
                'required' => 'true',
                'class' => 'form-control',
                'placeholder' => 'example@gmail.com',
                'autofocus' => 'true',
                'title' => 'Informe seu e-mail'
            ),
            'options' => array(
                'label' =>'E-mail',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        $this->add(array(
            'name' => 'password',
            'type' => 'password',
            'attributes' => array(
                'id' => 'password',
                'required' => 'true',
                'class' => 'form-control',
                'placeholder' => '*****',
                'autofocus' => 'false',
                'title' => 'Informe sua senha'
            ),
            'options' => array(
                'label' =>'Password',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
    }
}
