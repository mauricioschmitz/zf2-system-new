<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\ContaReceberFilter;
/**
 * Description of ContaReceberForm
 *
 * @author mauricioschmitz
 */
class ContaReceberForm extends AbstractForm {
    
    protected $em;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        
        $this->em = $em;
        
        parent::__construct(null);

        $this->setInputFilter(new ContaReceberFilter());
        
        //Input categoria
        $this->add(array(
            'name' => 'conta',
            'type' => 'select',
            'attributes' => array(
                'id' => 'conta',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'true',
                'title' => 'Informe uma conta',
                'options' => $this->getOptionsForSelect(),
            ),
            'options' => array(
                'label' =>'Referência',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input data
        $this->add(array(
            'name' => 'data',
            'type' => 'text',
            'attributes' => array(
                'id' => 'data',
                'required' => 'true',
                'class' => 'form-control date',
                'autofocus' => 'false',
                'title' => 'Informe uma data',
            ),
            'options' => array(
                'label' =>'Data',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input titulo
        $this->add(array(
            'name' => 'titulo',
            'type' => 'text',
            'attributes' => array(
                'id' => 'titulo',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o título',
            ),
            'options' => array(
                'label' =>'Título',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input valor
        $this->add(array(
            'name' => 'valor_previsto',
            'type' => 'text',
            'attributes' => array(
                'id' => 'valor_previsto',
                'required' => 'true',
                'class' => 'form-control moeda',
                'autofocus' => 'false',
                'title' => 'Informe o valor',
            ),
            'options' => array(
                'label' =>'Valor previsto',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input valor
        $this->add(array(
            'name' => 'valor_recebido',
            'type' => 'text',
            'attributes' => array(
                'id' => 'valor_recebido',
                'required' => 'true',
                'class' => 'form-control moeda',
                'autofocus' => 'false',
                'title' => 'Informe o valor',
            ),
            'options' => array(
                'label' =>'Valor recebido',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
    }

   

    public function getOptionsForSelect() {
        $list = $this->em->getRepository('Admin\Entity\Conta')->getAtivos();
        foreach ($list as $conta) {
            $selectData[$conta->getId()] = $conta->getTitulo();
        }
        return $selectData;
    }
}
