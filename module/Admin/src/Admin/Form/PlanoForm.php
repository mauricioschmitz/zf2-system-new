<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\PlanoFilter;
/**
 * Description of PlanoForm
 *
 * @author mauricioschmitz
 */
class PlanoForm extends AbstractForm {
    
    protected $em;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        
        $this->em = $em;
        
        parent::__construct(null);

        $this->setInputFilter(new PlanoFilter());
        //Input titulo
        $this->add(array(
            'name' => 'titulo',
            'type' => 'text',
            'attributes' => array(
                'id' => 'titulo',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o título',
            ),
            'options' => array(
                'label' =>'Título',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input valor
        $this->add(array(
            'name' => 'valor',
            'type' => 'text',
            'attributes' => array(
                'id' => 'valor',
                'required' => 'true',
                'class' => 'form-control moeda',
                'autofocus' => 'false',
                'title' => 'Informe o valor',
            ),
            'options' => array(
                'label' =>'Valor',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input cotas
        $this->add(array(
            'name' => 'cota',
            'type' => 'text',
            'attributes' => array(
                'id' => 'cota',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe a moeda',
            ),
            'options' => array(
                'label' =>'Moedas',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input cotas
        $this->add(array(
            'name' => 'percentual_indicacao',
            'type' => 'text',
            'attributes' => array(
                'id' => 'percentual_indicacao',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o percentual por indicação',
            ),
            'options' => array(
                'label' =>'Percentual por indicação',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input cotas
        $this->add(array(
            'name' => 'pontos',
            'type' => 'text',
            'attributes' => array(
                'id' => 'pontos',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe os pontos',
            ),
            'options' => array(
                'label' =>'Pontos',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input valor saque
        $this->add(array(
            'name' => 'valor_saque',
            'type' => 'text',
            'attributes' => array(
                'id' => 'valor_saque',
                'required' => 'true',
                'class' => 'form-control moeda',
                'autofocus' => 'false',
                'title' => 'Informe o valor para saque',
            ),
            'options' => array(
                'label' =>'Valor saque',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        
        //Input titulo
        $this->add(array(
            'name' => 'hexadecimal',
            'type' => 'text',
            'attributes' => array(
                'id' => 'hexadecimal',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o hexadecimal',
                'maxlenght' => 6
            ),
            'options' => array(
                'label' =>'Hexadecimal #',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
    }

   
}
