<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\RastreamentoFilter;
/**
 * Description of RastreamentoForm
 *
 * @author mauricioschmitz
 */
class RastreamentoForm extends AbstractForm {

    public function __construct() {
        parent::__construct(null);

        $this->setInputFilter(new RastreamentoFilter());
        //Input situacao
        $this->add(array(
            'name' => 'situacao',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 'situacao',
                'value' => 'Postado',
            )
        ));

        //Input titulo
        $this->add(array(
            'name' => 'codigo',
            'type' => 'text',
            'attributes' => array(
                'id' => 'codigo',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o código',
            ),
            'options' => array(
                'label' =>'Código',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));

        //Input origem
        $this->add(array(
            'name' => 'origem',
            'type' => 'text',
            'attributes' => array(
                'id' => 'origem',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe a origem',
            ),
            'options' => array(
                'label' =>'Origem',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));

        //Input origem
        $this->add(array(
            'name' => 'destino',
            'type' => 'text',
            'attributes' => array(
                'id' => 'destino',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o destino',
            ),
            'options' => array(
                'label' =>'Destino',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));

        //Input titulo
        $this->add(array(
            'name' => 'email',
            'type' => 'email',
            'attributes' => array(
                'id' => 'email',
                'required' => 'true',
                'class' => 'form-control',
                'placeholder' => 'example@gmail.com',
                'autofocus' => 'true',
                'title' => 'Informe seu e-mail'
            ),
            'options' => array(
                'label' =>'E-mail',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));

        //Input origem
        $this->add(array(
            'name' => 'descricao',
            'type' => 'text',
            'attributes' => array(
                'id' => 'descricao',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe uma descrição',
            ),
            'options' => array(
                'label' =>'Descrição',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
    }

   
}
