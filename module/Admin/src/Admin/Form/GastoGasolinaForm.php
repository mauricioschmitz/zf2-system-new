<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\GastoGasolinaFilter;
/**
 * Description of GastoGasolinaForm
 *
 * @author mauricioschmitz
 */
class GastoGasolinaForm extends AbstractForm {
    
    protected $em;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        
        $this->em = $em;
        
        parent::__construct(null);

        $this->setInputFilter(new GastoGasolinaFilter());
        
        //Input categoria
        $this->add(array(
            'name' => 'veiculo',
            'type' => 'select',
            'attributes' => array(
                'id' => 'veiculo',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'true',
                'title' => 'Informe um veiculo',
                'options' => $this->getOptionsForSelect(),
            ),
            'options' => array(
                'label' =>'Veículo',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input data
        $this->add(array(
            'name' => 'data',
            'type' => 'text',
            'attributes' => array(
                'id' => 'data',
                'required' => 'true',
                'class' => 'form-control date',
                'autofocus' => 'false',
                'title' => 'Informe uma data',
            ),
            'options' => array(
                'label' =>'Data',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input titulo
        $this->add(array(
            'name' => 'km',
            'type' => 'text',
            'attributes' => array(
                'id' => 'km',
                'required' => 'true',
                'class' => 'form-control decimal3casas',
                'autofocus' => 'false',
                'title' => 'Informe a quilometragem',
            ),
            'options' => array(
                'label' =>'Quilometragem',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input cotas
        $this->add(array(
            'name' => 'litros',
            'type' => 'text',
            'attributes' => array(
                'id' => 'litros',
                'required' => 'true',
                'class' => 'form-control decimal',
                'autofocus' => 'false',
                'title' => 'Informe a moeda',
            ),
            'options' => array(
                'label' =>'Litros',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input valor
        $this->add(array(
            'name' => 'preco_combustivel',
            'type' => 'text',
            'attributes' => array(
                'id' => 'preco_combustivel',
                'required' => 'true',
                'class' => 'form-control moeda3casas',
                'autofocus' => 'false',
                'title' => 'Informe o valor',
            ),
            'options' => array(
                'label' =>'Preço Combustível',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
    }

   

    public function getOptionsForSelect() {
        $list = $this->em->getRepository('Admin\Entity\Veiculo')->findBy(array('ativo'=>1));
        foreach ($list as $veiculo) {
            $selectData[$veiculo->getId()] = $veiculo->getTitulo();
        }
        return $selectData;
    }
}
