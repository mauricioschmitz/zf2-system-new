<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Admin\Form\Filter\PessoaJuridicaFilter;
/**
 * Description of PessoaJuridicaForm
 *
 * @author mauricioschmitz
 */
class PessoaJuridicaForm extends AbstractForm {
    
    protected $em;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        
        $this->em = $em;
        
        parent::__construct(null);

        $this->setInputFilter(new PessoaJuridicaFilter());
        //Input noem
        $this->add(array(
            'name' => 'nome_fantasia',
            'type' => 'text',
            'attributes' => array(
                'id' => 'nome_fantasia',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o nome fantasia',
            ),
            'options' => array(
                'label' =>'Nome fantasia',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input noem
        $this->add(array(
            'name' => 'razao_social',
            'type' => 'text',
            'attributes' => array(
                'id' => 'razao_social',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe a razão social',
            ),
            'options' => array(
                'label' =>'Razão Social',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input cpf
        $this->add(array(
            'name' => 'cnpj',
            'type' => 'text',
            'attributes' => array(
                'id' => 'cnpj',
                'required' => 'true',
                'class' => 'form-control cnpj',
                'autofocus' => 'false',
                'title' => 'Informe um CNPJ válido',
            ),
            'options' => array(
                'label' =>'CNPJ',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input rg
        $this->add(array(
            'name' => 'ie',
            'type' => 'text',
            'attributes' => array(
                'id' => 'ie',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe a IE',
            ),
            'options' => array(
                'label' =>'IE',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input telefone
        $this->add(array(
            'name' => 'telefone',
            'type' => 'text',
            'attributes' => array(
                'id' => 'telefone',
                'required' => 'false',
                'class' => 'form-control telefone',
                'autofocus' => 'false',
                'title' => 'Informe um telefone',
            ),
            'options' => array(
                'label' =>'Telefone',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input telefone
        $this->add(array(
            'name' => 'celular',
            'type' => 'text',
            'attributes' => array(
                'id' => 'celular',
                'required' => 'false',
                'class' => 'form-control celular',
                'autofocus' => 'false',
                'title' => 'Informe um celular',
            ),
            'options' => array(
                'label' =>'Celular',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        $this->addEnderecoElement(array('cep','uf','cidade','endereco','numero','bairro','complemento'), array('CEP','UF','Cidade','Endereço','Número','Bairro','Complemento'));
        
    }
    
    public function getOptionsForSelect() {
        $list = $this->em->getRepository('Admin\Entity\Municipio')->findBy(array('ativo'=>1));
        
        foreach ($list as $municipio) {
            $selectData[$municipio->getId()] = $municipio->getNome();
        }
        return $selectData;
    }

   
}
