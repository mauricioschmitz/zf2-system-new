<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\VeiculoFilter;
/**
 * Description of VeiculoForm
 *
 * @author mauricioschmitz
 */
class VeiculoForm extends AbstractForm {
    
    protected $em;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        
        $this->em = $em;
        
        parent::__construct(null);

        $this->setInputFilter(new VeiculoFilter());
        //Input titulo
        $this->add(array(
            'name' => 'titulo',
            'type' => 'text',
            'attributes' => array(
                'id' => 'titulo',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o título',
            ),
            'options' => array(
                'label' =>'Título',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input placa
        $this->add(array(
            'name' => 'placa',
            'type' => 'text',
            'attributes' => array(
                'id' => 'placa',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe a placa',
            ),
            'options' => array(
                'label' =>'Placa',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input placa
        $this->add(array(
            'name' => 'chassi',
            'type' => 'text',
            'attributes' => array(
                'id' => 'chassi',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o chassi',
            ),
            'options' => array(
                'label' =>'Chassi',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input placa
        $this->add(array(
            'name' => 'link',
            'type' => 'text',
            'attributes' => array(
                'id' => 'link',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o link',
            ),
            'options' => array(
                'label' =>'Link',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
    }

   
}
