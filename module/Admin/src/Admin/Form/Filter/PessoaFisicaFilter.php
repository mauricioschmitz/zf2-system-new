<?php
namespace Admin\Form\Filter;

use Base\Form\Filter\AbstractFilter;

/**
 * Description of PessoaFisicaFilter
 *
 * @author mauricioschmitz
 */
class PessoaFisicaFilter extends AbstractFilter {
    
    public function __construct() {
        $isEmpty = \Zend\Validator\NotEmpty::IS_EMPTY;
        
        $array = array('nome'=>'Nome', 'cpf'=>'CPF', 'cep'=>'CEP', 'cidade'=>'Cidade', 'uf'=>'UF', 'endereco'=>'Endereço', 'numero'=>'Número', 'bairro'=>'Bairro');
        foreach ($array as $key=>$value){
            $this->add(array(
                'name' => $key,
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'StripTags'
                    ),
                    array(
                        'name' => 'StringTrim'
                    )
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                $isEmpty => ($key == 'cpf' ? 'Informe um '. $value.' válido' : $value.' não deve estar vazio')
                            )
                        ),
                        'break_chain_on_failure' => true
                    )
                )
            ));
        }
    }
}
