<?php
namespace Admin\Form\Filter;

use Base\Form\Filter\AbstractFilter;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\Validator\NotEmpty;

/**
 * Description of CategoroiaFilter
 *
 * @author mauricioschmitz
 */
class CategoriaContaFilter extends AbstractFilter {
    
    public function __construct() {
        $isEmpty = \Zend\Validator\NotEmpty::IS_EMPTY;
        
        $this->add(array(
            'name' => 'titulo',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            $isEmpty => 'Título não deve estar vazio'
                        )
                    ),
                    'break_chain_on_failure' => true
                )
            )
        ));
        
        $this->add(array(
            'name' => 'destaque',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            )
        ));
    }
}
