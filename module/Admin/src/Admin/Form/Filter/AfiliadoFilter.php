<?php
namespace Admin\Form\Filter;

use Base\Form\Filter\AbstractFilter;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\Validator\NotEmpty;

/**
 * Description of AfiliadoFilter
 *
 * @author mauricioschmitz
 */
class AfiliadoFilter extends AbstractFilter {
    
    public function __construct() {
        $isEmpty = \Zend\Validator\NotEmpty::IS_EMPTY;
        
        $array = array('numeroBanco'=>'Número banco', 'banco'=>'Banco', 'agencia'=>'Agência', 'conta'=>'Conta', 'titular'=>'Titular');
        foreach ($array as $key=>$value){
            $this->add(array(
                'name' => $key,
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'StripTags'
                    ),
                    array(
                        'name' => 'StringTrim'
                    )
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                $isEmpty => $value.' não deve estar vazio'
                            )
                        ),
                        'break_chain_on_failure' => true
                    )
                )
            ));
        }
    }
}
