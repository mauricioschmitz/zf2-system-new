<?php
namespace Admin\Form\Filter;

use Base\Form\Filter\AbstractFilter;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\Validator\NotEmpty;

/**
 * Description of AfiliadoConfigFilter
 *
 * @author mauricioschmitz
 */
class AfiliadoConfigFilter extends AbstractFilter {
    
    public function __construct() {
        $isEmpty = \Zend\Validator\NotEmpty::IS_EMPTY;
        
        $this->add(array(
            'name' => 'percentual_distribuicao',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            $isEmpty => 'Percentual não deve estar vazio'
                        )
                    ),
                    'break_chain_on_failure' => true
                )
            )
        ));
        
        $this->add(array(
            'name' => 'percentual_retorno',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            $isEmpty => 'Percentual não deve estar vazio'
                        )
                    ),
                    'break_chain_on_failure' => true
                )
            )
        ));
    }
}
