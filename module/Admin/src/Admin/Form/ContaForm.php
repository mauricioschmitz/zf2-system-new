<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\ContaFilter;
/**
 * Description of ContaForm
 *
 * @author mauricioschmitz
 */
class ContaForm extends AbstractForm {

    public function __construct() {
        parent::__construct(null);

        $this->setInputFilter(new ContaFilter());
        //Input titulo
        $this->add(array(
            'name' => 'titulo',
            'type' => 'text',
            'attributes' => array(
                'id' => 'titulo',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o título',
            ),
            'options' => array(
                'label' =>'Título',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));

        //Input placa
        $this->add(array(
            'name' => 'link',
            'type' => 'text',
            'attributes' => array(
                'id' => 'link',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o link',
            ),
            'options' => array(
                'label' =>'Link',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input data
        $this->add(array(
            'name' => 'data',
            'type' => 'text',
            'attributes' => array(
                'id' => 'data',
                'required' => 'true',
                'class' => 'form-control date',
                'autofocus' => 'false',
                'title' => 'Informe uma data',
            ),
            'options' => array(
                'label' =>'Data',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
    }

   
}
