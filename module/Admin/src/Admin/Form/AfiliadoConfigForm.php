<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\AfiliadoConfigFilter;
/**
 * Description of AfiliadoConfigForm
 *
 * @author mauricioschmitz
 */
class AfiliadoConfigForm extends AbstractForm {
    
    protected $em;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        
        $this->em = $em;
        
        parent::__construct(null);

        $this->setInputFilter(new AfiliadoConfigFilter());
        //Input percentual distribuição
        $this->add(array(
            'name' => 'percentual_distribuicao',
            'type' => 'text',
            'attributes' => array(
                'id' => 'percentual_distribuicao',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o percentual para distribuição',
            ),
            'options' => array(
                'label' =>'Percentual para distribuição',
                'label_attributes' => array(
                    'class'  => 'col-sm-4 control-label'
                ),
            )
        ));
        
        //Input percentual retorno
        $this->add(array(
            'name' => 'percentual_retorno',
            'type' => 'text',
            'attributes' => array(
                'id' => 'percentual_retorno',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o percentual para retorno',
            ),
            'options' => array(
                'label' =>'Percentual para retorno',
                'label_attributes' => array(
                    'class'  => 'col-sm-4 control-label'
                ),
            )
        ));
    }
}
