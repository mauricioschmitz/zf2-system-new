<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\SaqueAfiliadoFilter;
/**
 * Description of SaqueAfiliadoForm
 *
 * @author mauricioschmitz
 */
class SaqueAfiliadoForm extends AbstractForm {
    
    protected $em;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        
        $this->em = $em;
        
        parent::__construct(null);

        $this->setInputFilter(new SaqueAfiliadoFilter());
        //Input valor
        $this->add(array(
            'name' => 'valor',
            'type' => 'text',
            'attributes' => array(
                'id' => 'valor',
                'required' => 'true',
                'class' => 'form-control moeda',
                'title' => 'Informe o valor',
            ),
            'options' => array(
                'label' =>'Valor',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input data depósito
        $this->add(array(
            'name' => 'data_pagamento',
            'type' => 'text',
            'attributes' => array(
                'id' => 'data_pagamento',
                'required' => 'true',
                'class' => 'form-control date',
                'autofocus' => 'false',
                'title' => 'Informe a data do depósito',
            ),
            'options' => array(
                'label' =>'Data Depósito',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input descricao
        $this->add(array(
            'name' => 'descricao',
            'type' => 'textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'descricao',
                'rows' => 4
            ),
            'options' => array(
                'label' =>'Detalhes',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                )
            )
        ));
        
        //Input situacao
        $this->add(array(
            'name' => 'situacao',
            'type' => 'hidden',
            'attributes' => array(
                'value' => 1
            ),
            'options' => array(
                'label' =>'Situação'
            )
        ));
        
    }
    
}
