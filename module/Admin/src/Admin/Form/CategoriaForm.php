<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\CategoriaFilter;
/**
 * Description of CategoriaForm
 *
 * @author mauricioschmitz
 */
class CategoriaForm extends AbstractForm {

    public function __construct() {
        parent::__construct(null);

        $this->setInputFilter(new CategoriaFilter());
        //Input titulo
        $this->add(array(
            'name' => 'titulo',
            'type' => 'text',
            'attributes' => array(
                'id' => 'titulo',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o título',
            ),
            'options' => array(
                'label' =>'Título',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));

        //Input cor
        $this->add(array(
            'name' => 'cor',
            'type' => 'radio',
            'attributes' => array(
                'id' => 'cor',
                'options' => array(
                        '0' => '0',
                        '1' => '1',
                        '2' => '2',
                        '3' => '3',
                        '4' => '4',
                        '5' => '5',
                        '6' => '6',
                        '7' => '7',
                        '8' => '8',
                        '9' => '9',
                        '10' => '10',
                        
                ),
            ),
            'options' => array(
                'label' =>'Cor',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
//        $cor = new Element\Radio('cor');
//        $cor->setLabel('Cor')
//                ->setOptions(
//                array(
//                    'value_options' => array(
//                        '0' => '0',
//                        '1' => '1',
//                        '2' => '2',
//                        '3' => '3',
//                        '4' => '4',
//                        '5' => '5',
//                        '6' => '6',
//                        '7' => '7',
//                        '8' => '8',
//                        '9' => '9',
//                        '10' => '10'
//                    )
//                )
//        );
//        $this->add($cor);

        //Input destaque
        $this->add(array(
            'name' => 'destaque',
            'type' => 'checkbox',
            'attributes' => array(
                'value' => 1,
                'class' =>''
            ),
            'options' => array(
                'label' =>'Destaque',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0
            )
        ));
    }

   
}
