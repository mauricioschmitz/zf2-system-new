<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\TextoFilter;
/**
 * Description of TextoForm
 *
 * @author mauricioschmitz
 */
class TextoForm extends AbstractForm {
    
    protected $em;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        $this->em = $em;
        
        parent::__construct(null);

        $this->setInputFilter(new TextoFilter());

        //Input titulo
        $this->add(array(
            'name' => 'titulo',
            'type' => 'text',
            'attributes' => array(
                'id' => 'titulo',
                'required' => 'true',
                'class' => 'form-control disabled',
                'readonly' => 'readonly',
                'autofocus' => 'false',
                'title' => 'Informe o título',
            ),
            'options' => array(
                'label' =>'Título',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));

        //Input descricao
        $this->add(array(
            'name' => 'texto',
            'type' => 'textarea',
            'attributes' => array(
                'class' => 'summernote',
                'rows' => 4
            ),
            'options' => array(
                'label' =>'Texto',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                )
            )
        ));
        
        
    }

}
