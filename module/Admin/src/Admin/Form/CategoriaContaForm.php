<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\CategoriaContaFilter;
/**
 * Description of CategoriaContaForm
 *
 * @author mauricioschmitz
 */
class CategoriaContaForm extends AbstractForm {

    public function __construct() {
        parent::__construct(null);

        $this->setInputFilter(new CategoriaContaFilter());
        //Input titulo
        $this->add(array(
            'name' => 'titulo',
            'type' => 'text',
            'attributes' => array(
                'id' => 'titulo',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o título',
            ),
            'options' => array(
                'label' =>'Título',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));

        //Input destaque
        $this->add(array(
            'name' => 'destaque',
            'type' => 'checkbox',
            'attributes' => array(
                'value' => 1,
                'class' =>''
            ),
            'options' => array(
                'label' =>'Destaque',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0
            )
        ));
    }

   
}
