<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Admin\Form\Filter\PessoaFisicaFilter;
use Zend\Form\Form;
use Zend\Form\Element;
/**
 * Description of PessoaFisicaForm
 *
 * @author mauricioschmitz
 */
class PessoaFisicaForm extends AbstractForm {
    
    protected $em;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        
        $this->em = $em;
        
        parent::__construct(null);

        $this->setInputFilter(new PessoaFisicaFilter());
        //Input titulo
        $this->add(array(
            'name' => 'nome',
            'type' => 'text',
            'attributes' => array(
                'id' => 'nome',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'true',
                'title' => 'Informe o nome',
            ),
            'options' => array(
                'label' =>'Nome',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input cpf
        $this->add(array(
            'name' => 'cpf',
            'type' => 'text',
            'attributes' => array(
                'id' => 'cpf',
                'required' => 'true',
                'class' => 'form-control cpf',
                'title' => 'Informe um CPF válido',
            ),
            'options' => array(
                'label' =>'CPF',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input rg
        $this->add(array(
            'name' => 'rg',
            'type' => 'text',
            'attributes' => array(
                'id' => 'rg',
                'required' => 'false',
                'class' => 'form-control',
                'title' => 'Informe o RG',
            ),
            'options' => array(
                'label' =>'RG',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input telefone
        $this->add(array(
            'name' => 'telefone',
            'type' => 'text',
            'attributes' => array(
                'id' => 'telefone',
                'required' => 'false',
                'class' => 'form-control telefone',
                'title' => 'Informe um telefone',
            ),
            'options' => array(
                'label' =>'Telefone',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input telefone
        $this->add(array(
            'name' => 'celular',
            'type' => 'text',
            'attributes' => array(
                'id' => 'celular',
                'required' => 'false',
                'class' => 'form-control celular',
                'title' => 'Informe um celular',
            ),
            'options' => array(
                'label' =>'Celular',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input data publicacao
        $this->add(array(
            'name' => 'data_nascimento',
            'type' => 'text',
            'attributes' => array(
                'id' => 'data_nascimento',
                'required' => 'true',
                'class' => 'form-control date',
                'title' => 'Informe sua data de nascimento',
            ),
            'options' => array(
                'label' =>'Data nascimento',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input sexo
        $this->add(array(
            'name' => 'sexo',
            'type' => 'select',
            'attributes' => array(
                'id' => 'sexo',
                'required' => 'true',
                'class' => 'form-control',
                'title' => 'Informe seu sexo',
                'options' => array(''=>'Selecione', 'M'=>'Masculino', 'F'=>'Feminino'),
            ),
            'options' => array(
                'label' =>'Sexo',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input estado civil
        $this->add(array(
            'name' => 'estado_civil',
            'type' => 'select',
            'attributes' => array(
                'id' => 'estado_civil',
                'required' => 'true',
                'class' => 'form-control',
                'title' => 'Informe seu estado civil',
                'options' => array(
                    ''=>'Selecione',
                    'Solteiro(a)'=>'Solteiro(a)',
                    'Casado(a)'=>'Casado(a)',
                    'Divorciado(a)'=>'Divorciado(a)',
                    'Viúvo(a)'=>'Viúvo(a)',
                    'Separado(a)'=>'Separado(a)',
                    'União Estável'=>'União Estável',
                ),
            ), 
            'options' => array(
                'label' =>'Estado civil',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input nome pai
        $this->add(array(
            'name' => 'nome_pai',
            'type' => 'text',
            'attributes' => array(
                'id' => 'nome_pai',
                'class' => 'form-control',
                'title' => 'Informe o nome do seu pai',
            ),
            'options' => array(
                'label' =>'Nome Pai',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input nome mae
        $this->add(array(
            'name' => 'nome_mae',
            'type' => 'text',
            'attributes' => array(
                'id' => 'nome_mae',
                'class' => 'form-control',
                'title' => 'Informe o nome da sua mãe',
            ),
            'options' => array(
                'label' =>'Nome Mãe',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input valor
        $foto = $this->add(array(
            'name' => 'foto',
            'type' => 'file',
            'attributes' => array(
                'id' => 'foto',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Insira um arquivo',
            ),
            'options' => array(
                'label' =>'Foto',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            ),
            
        ));
        
        $this->addEnderecoElement(array('cep','uf','cidade','endereco','numero','bairro','complemento'), array('CEP','UF','Cidade','Endereço','Número','Bairro','Complemento'));
        
    }
    
    public function getOptionsForSelect() {
        $list = $this->em->getRepository('Admin\Entity\Municipio')->findBy(array('ativo'=>1));
        
        foreach ($list as $municipio) {
            $selectData[$municipio->getId()] = $municipio->getNome();
        }
        return $selectData;
    }

   
}
