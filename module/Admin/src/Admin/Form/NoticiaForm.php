<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Admin\Controller\NoticiaController;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\NoticiaFilter;
/**
 * Description of NoticiaForm
 *
 * @author mauricioschmitz
 */
class NoticiaForm extends AbstractForm {
    
    protected $em;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        $this->em = $em;
        
        parent::__construct(null);

        $this->setInputFilter(new NoticiaFilter());

        //Input categoria
        $this->add(array(
            'name' => 'categoria',
            'type' => 'select',
            'attributes' => array(
                'id' => 'categoria',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'true',
                'title' => 'Informe a categoria',
                'options' => $this->getOptionsForSelect(),
            ),
            'options' => array(
                'label' =>'Categoria',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input titulo
        $this->add(array(
            'name' => 'titulo',
            'type' => 'text',
            'attributes' => array(
                'id' => 'titulo',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o título',
            ),
            'options' => array(
                'label' =>'Título',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));

        //Input data publicacao
        $this->add(array(
            'name' => 'data_publicacao',
            'type' => 'text',
            'attributes' => array(
                'id' => 'data_publicacao',
                'required' => 'true',
                'class' => 'form-control datetime',
                'autofocus' => 'false',
                'title' => 'Informe uma data de início',
            ),
            'options' => array(
                'label' =>'Data Publicação',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));

        //Input status
        $this->add(array(
            'name' => 'status',
            'type' => 'select',
            'attributes' => array(
                'id' => 'status',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'true',
                'title' => 'Informe um status',
                'options' => array(
                    'rascunho' => 'Rascunho',
                    'publicado' => 'Publicado',
                    'suspenso' => 'Suspenso',
                )
            ),
            'options' => array(
                'label' =>'Status',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));

        //Input status
        $this->add(array(
            'name' => 'nivel',
            'type' => 'select',
            'attributes' => array(
                'id' => 'nivel',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'true',
                'title' => 'Informe um nível de destaque',
                'options' => array(
                    'A' => 'A',
                    'B' => 'B',
                    'C' => 'C',
                )
            ),
            'options' => array(
                'label' =>'Nível',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input descricao
        $this->add(array(
            'name' => 'descricao',
            'type' => 'textarea',
            'attributes' => array(
                'class' => 'summernote',
                'rows' => 4
            ),
            'options' => array(
                'label' =>'Descrição',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                )
            )
        ));
        
        //Input chamada
        $this->add(array(
            'name' => 'chamada',
            'type' => 'textarea',
            'attributes' => array(
                'class' => 'form-control',
                'rows' => 4
            ),
            'options' => array(
                'label' =>'Chamada',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                )
            )
        ));
        
        //Input tag
        $this->add(array(
            'name' => 'tag',
            'type' => 'textarea',
            'attributes' => array(
                'class' => 'form-control',
                'rows' => 4
            ),
            'options' => array(
                'label' =>'Tags',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                )
            )
        ));
        
        //Input exclusivo
        $this->add(array(
            'name' => 'exclusivo',
            'type' => 'checkbox',
            'attributes' => array(
                 'value' => 1
            ),
            'options' => array(
                'label' =>'Exclusivo',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0
            )
        ));
        
        //Input fonte
        $this->add(array(
            'name' => 'fonte',
            'type' => 'text',
            'attributes' => array(
                'id' => 'fonte',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe a fonte',
            ),
            'options' => array(
                'label' =>'Fonte',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
    }

    public function getOptionsForSelect() {
        $list = $this->em->getRepository('Admin\Entity\Categoria')->findBy(array('ativo'=>1));
        foreach ($list as $categoria) {
            $selectData[$categoria->getId()] = $categoria->getTitulo();
        }
        return $selectData;
    }

}
