<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\PublicidadeFilter;
/**
 * Description of PublicidadeForm
 *
 * @author mauricioschmitz
 */
class PublicidadeForm extends AbstractForm {
    
    protected $em;
    private $dias = array(
        'sun' => 'Domingo',
        'mon' => 'Segunda',
        'tue' => 'Terça',
        'wed' => 'Quarta',
        'thu' => 'Quinta',
        'fri' => 'Sexta',
        'sat' => 'Sábado');
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        
        $this->em = $em;
        
        parent::__construct(null);

        $this->setInputFilter(new PublicidadeFilter());
        //Input Espaco
        $this->add(array(
            'name' => 'espaco_publicidade',
            'type' => 'select',
            'attributes' => array(
                'id' => 'espaco_publicidade',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'true',
                'title' => 'Informe o espaço',
                'options' => $this->getOptionsForSelect(),
            ),
            'options' => array(
                'label' =>'Espaço',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input titulo
        $this->add(array(
            'name' => 'titulo',
            'type' => 'text',
            'attributes' => array(
                'id' => 'titulo',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o título',
            ),
            'options' => array(
                'label' =>'Título',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input link
        $this->add(array(
            'name' => 'link',
            'type' => 'url',
            'attributes' => array(
                'id' => 'link',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe um link',
            ),
            'options' => array(
                'label' =>'Link',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input data publicacao
        $this->add(array(
            'name' => 'data_publicacao',
            'type' => 'text',
            'attributes' => array(
                'id' => 'data_publicacao',
                'required' => 'true',
                'class' => 'form-control date',
                'autofocus' => 'false',
                'title' => 'Informe uma data de início',
            ),
            'options' => array(
                'label' =>'Data Publicação',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input data expiracao
        $this->add(array(
            'name' => 'data_expiracao',
            'type' => 'text',
            'attributes' => array(
                'id' => 'data_expiracao',
                'required' => 'true',
                'class' => 'form-control date',
                'autofocus' => 'false',
                'title' => 'Informe uma data de fim',
            ),
            'options' => array(
                'label' =>'Data Expiração',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input valor
        $this->add(array(
            'name' => 'valor',
            'type' => 'text',
            'attributes' => array(
                'id' => 'valor',
                'required' => 'true',
                'class' => 'form-control moeda',
                'autofocus' => 'false',
                'title' => 'Informe o valor',
            ),
            'options' => array(
                'label' =>'Valor',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));

        //Input exclusivo

        foreach ($this->dias as $value=>$dia) {
            $this->add(array(
                'name' => $value,
                'type' => 'checkbox',
                'attributes' => array(
                    'value' => '1'
                ),
                'options' => array(
                    'label' => $dia,
                    'label_attributes' => array(
                        'class' => 'col-sm-2 control-label'
                    ),
                    'use_hidden_element' => true,
                    'checked_value' => 1,
                    'unchecked_value' => 0
                )
            ));
        }

        
        //Input valor
        $this->add(array(
            'name' => 'arquivo',
            'type' => 'file',
            'attributes' => array(
                'id' => 'arquivo',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Insira um arquivo',
            ),
            'options' => array(
                'label' =>'Arquivo',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
    }
    
    public function  getOptionsForSelect() {
        $list = $this->em->getRepository('Admin\Entity\EspacoPublicidade')->findBy(array('ativo'=>1));
        
        foreach ($list as $espaco) {
            $selectData[$espaco->getId()] = $espaco->getTitulo().' -> '.$espaco->getTamanho();
        }
        return $selectData;
    }

    /**
     * @return array
     */
    public function getDias()
    {
        return $this->dias;
    }

    /**
     * @param array $dias
     */
    public function setDias($dias)
    {
        $this->dias = $dias;
    }


   
}
