<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\AfiliadoPagamentoFilter;
/**
 * Description of AfiliadoPagamentoForm
 *
 * @author mauricioschmitz
 */
class AfiliadoPagamentoForm extends AbstractForm {
    
    protected $em;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        
        $this->em = $em;
        
        parent::__construct(null);

        $this->setInputFilter(new AfiliadoPagamentoFilter());
        //Input valor
        $this->add(array(
            'name' => 'valor',
            'type' => 'text',
            'attributes' => array(
                'id' => 'valor',
                'required' => 'true',
                'class' => 'form-control decimal',
                'autofocus' => 'false',
                'title' => 'Informe o valor a distribuir',
            ),
            'options' => array(
                'label' =>'Distribuir',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
    }
    
}
