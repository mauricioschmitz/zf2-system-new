<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\ContaPagarFilter;
/**
 * Description of ContaPagarForm
 *
 * @author mauricioschmitz
 */
class ContaPagarForm extends AbstractForm {
    
    protected $em;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        
        $this->em = $em;
        
        parent::__construct(null);

        $this->setInputFilter(new ContaPagarFilter());
        
        //Input categoria
        $this->add(array(
            'name' => 'conta',
            'type' => 'select',
            'attributes' => array(
                'id' => 'conta',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'true',
                'title' => 'Informe uma conta',
                'options' => $this->getOptionsForSelect(),
            ),
            'options' => array(
                'label' =>'Referência',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input categoria
        $this->add(array(
            'name' => 'categoria',
            'type' => 'select',
            'attributes' => array(
                'id' => 'categoria',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'true',
                'title' => 'Informe uma categoria',
                'options' => $this->getOptionsForSelectCategoria(),
            ),
            'options' => array(
                'label' =>'Categoria',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input data
        $this->add(array(
            'name' => 'data',
            'type' => 'text',
            'attributes' => array(
                'id' => 'data',
                'required' => 'true',
                'class' => 'form-control date',
                'autofocus' => 'false',
                'title' => 'Informe uma data',
            ),
            'options' => array(
                'label' =>'Data',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input titulo
        $this->add(array(
            'name' => 'titulo',
            'type' => 'text',
            'attributes' => array(
                'id' => 'titulo',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o título',
            ),
            'options' => array(
                'label' =>'Título',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input titulo
        $this->add(array(
            'name' => 'parcela_atual',
            'type' => 'text',
            'attributes' => array(
                'id' => 'parcela_atual',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe o parcela atual',
            ),
            'options' => array(
                'label' =>'Parcela atual',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input titulo
        $this->add(array(
            'name' => 'qtd_parcelas',
            'type' => 'text',
            'attributes' => array(
                'id' => 'qtd_parcelas',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'false',
                'title' => 'Informe a qtd de parcelas',
            ),
            'options' => array(
                'label' =>'Parcelas',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input valor
        $this->add(array(
            'name' => 'valor_previsto',
            'type' => 'text',
            'attributes' => array(
                'id' => 'valor_previsto',
                'required' => 'true',
                'class' => 'form-control moeda',
                'autofocus' => 'false',
                'title' => 'Informe o valor',
            ),
            'options' => array(
                'label' =>'Valor previsto',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input valor
        $this->add(array(
            'name' => 'valor_pago',
            'type' => 'text',
            'attributes' => array(
                'id' => 'valor_pago',
                'required' => 'true',
                'class' => 'form-control moeda',
                'autofocus' => 'false',
                'title' => 'Informe o valor',
            ),
            'options' => array(
                'label' =>'Valor pago',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
    }

   

    public function getOptionsForSelect() {
        $list = $this->em->getRepository('Admin\Entity\Conta')->getAtivos();
        foreach ($list as $conta) {
            $selectData[$conta->getId()] = $conta->getTitulo();
        }
        return $selectData;
    }
    
    public function getOptionsForSelectCategoria() {
        $list = $this->em->getRepository('Admin\Entity\CategoriaConta')->getAtivos();
        foreach ($list as $categoria) {
            $selectData[$categoria->getId()] = $categoria->getTitulo();
        }
        return $selectData;
    }
}
