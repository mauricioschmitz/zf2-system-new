<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\BuscaEstatisticaFilter;
/**
 * Description of BuscaEstatisticaForm
 *
 * @author mauricioschmitz
 */
class BuscaEstatisticaForm extends AbstractForm {
    
    protected $em;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        
        $this->em = $em;
        
        parent::__construct(null);
        $this->setInputFilter(new BuscaEstatisticaFilter());
        //Input data publicacao
        $this->add(array(
            'name' => 'inicio',
            'type' => 'text',
            'attributes' => array(
                'id' => 'inicio',
                'class' => 'form-control date',
                'title' => 'Informe uma data de início',
            ),
            'options' => array(
                'label' =>'Início',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input data expiracao
        $this->add(array(
            'name' => 'fim',
            'type' => 'text',
            'attributes' => array(
                'id' => 'fim',
                'class' => 'form-control date',
                'title' => 'Informe uma data de fim',
            ),
            'options' => array(
                'label' =>'Fim',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        
    }
    
}
