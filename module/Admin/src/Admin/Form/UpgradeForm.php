<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\UpgradeFilter;
/**
 * Description of UpgradeForm
 *
 * @author mauricioschmitz
 */
class UpgradeForm extends AbstractForm {
    
    protected $em;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        
        $this->em = $em;
        
        parent::__construct(null);

        $this->setInputFilter(new UpgradeFilter());
        //Input titulo
        $this->add(array(
            'name' => 'plano_id',
            'type' => 'radio',
            'attributes' => array(
                'id' => 'plano_id',
                'required' => 'true',
                'class' => 'form-control',
            ),
            'options' => array(
                'value_options' => $this->getPlanosOptions(),
                'label' =>'Plano',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
    }
    
    public function getPlanosOptions(){
        $list = $this->em->getRepository('Admin\Entity\Plano')->findBy(array('ativo'=>1));
        foreach ($list as $plano) {
            $selectData[$plano->getId()] = $plano->getTitulo();
        }
        return $selectData;
    }
}
