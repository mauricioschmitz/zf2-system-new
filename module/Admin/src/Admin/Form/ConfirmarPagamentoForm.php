<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\ConfirmarPagamentoFilter;
/**
 * Description of ConfirmarPagamentoForm
 *
 * @author mauricioschmitz
 */
class ConfirmarPagamentoForm extends AbstractForm {
    
    protected $em;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        
        $this->em = $em;
        
        parent::__construct(null);

        $this->setInputFilter(new ConfirmarPagamentoFilter());
        
        //Input data publicacao
        $this->add(array(
            'name' => 'data_pagamento',
            'type' => 'text',
            'attributes' => array(
                'id' => 'data_pagamento',
                'required' => 'true',
                'class' => 'form-control date',
                'title' => 'Informe a data do pagamento',
            ),
            'options' => array(
                'label' =>'Data pagamento',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input valor
        $this->add(array(
            'name' => 'arquivo',
            'type' => 'file',
            'attributes' => array(
                'id' => 'arquivo',
                'class' => 'form-control',
                'title' => 'Insira uma imagem do comprovante',
            ),
            'options' => array(
                'label' =>'Arquivo',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
    }
   
}
