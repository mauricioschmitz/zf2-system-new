<?php

namespace Admin\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\AfiliadoFilter;
/**
 * Description of AfiliadoForm
 *
 * @author mauricioschmitz
 */
class AfiliadoForm extends AbstractForm {

    public function __construct() {
        parent::__construct(null);

        $this->setInputFilter(new AfiliadoFilter());
        //Input numero_banco
        $this->add(array(
            'name' => 'numeroBanco',
            'type' => 'text',
            'attributes' => array(
                'id' => 'numeroBanco',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'true',
                'title' => 'Informe o número do banco',
            ),
            'options' => array(
                'label' =>'Número banco',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input banco
        $this->add(array(
            'name' => 'banco',
            'type' => 'text',
            'attributes' => array(
                'id' => 'banco',
                'required' => 'true',
                'class' => 'form-control',
                'title' => 'Informe o banco',
            ),
            'options' => array(
                'label' =>'Banco',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input agencia
        $this->add(array(
            'name' => 'agencia',
            'type' => 'text',
            'attributes' => array(
                'id' => 'agencia',
                'required' => 'true',
                'class' => 'form-control',
                'title' => 'Informe a agência',
            ),
            'options' => array(
                'label' =>'Agência',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input conta
        $this->add(array(
            'name' => 'conta',
            'type' => 'text',
            'attributes' => array(
                'id' => 'conta',
                'required' => 'true',
                'class' => 'form-control',
                'title' => 'Informe a conta',
            ),
            'options' => array(
                'label' =>'Conta',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input titular
        $this->add(array(
            'name' => 'titular',
            'type' => 'text',
            'attributes' => array(
                'id' => 'titular',
                'required' => 'true',
                'class' => 'form-control',
                'title' => 'Informe o titular',
            ),
            'options' => array(
                'label' =>'Titular',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input titular
        $this->add(array(
            'name' => 'obs',
            'type' => 'textarea',
            'attributes' => array(
                'id' => 'obs',
                'class' => 'form-control',
                'title' => 'Observações',
            ),
            'options' => array(
                'label' =>'Observações',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));

        
    }

   
}
