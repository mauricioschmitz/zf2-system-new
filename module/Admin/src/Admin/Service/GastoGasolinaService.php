<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of GastoGasolina
 *
 * @author mauricioschmitz
 */
class GastoGasolinaService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\GastoGasolina';
        parent::__construct($em);
    }
    
    public function importXLS($veiculo, $array){
        $sucesso = false;
        $fieldsName = $array[0];
        foreach($array as $row){
            $data = array();
            if(is_numeric($row[0]) && $row[3] > 0){
                foreach($row as $key=>$field){
                    if($fieldsName[$key] == 'data'){
                        $field = new \DateTime(implode('-', array_reverse(explode('/',$field))));
                        
                    }elseif($fieldsName[$key] == 'litros' || $fieldsName[$key] == 'preco_combustivel'){
                        $field = str_replace(',', '.', $field);
                    }
                    $data[$fieldsName[$key]] = $field;
                }
                try{
                    $data['veiculo'] = $veiculo;
                    $entity = $this->em->getRepository('Admin\Entity\GastoGasolina')->find($data['id']);
                    if(!$entity)
                        unset($data['id']);
                    $this->save($data);
                    $sucesso = true;
                } catch (Exception $ex) {
                    $sucesso = false;
                }
            }
        }
        return $sucesso;
        
    }
}
