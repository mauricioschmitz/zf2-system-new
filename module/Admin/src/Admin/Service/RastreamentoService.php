<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of RastreamentoService
 *
 * @author mauricioschmitz
 */
class RastreamentoService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\Rastreamento';
        parent::__construct($em);
    }
}
