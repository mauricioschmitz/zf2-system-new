<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of TextoService
 *
 * @author mauricioschmitz
 */
class TextoService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\Texto';
        parent::__construct($em);
    }
}
