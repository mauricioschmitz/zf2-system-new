<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
use Admin\Entity\CategoriaConta;
/**
 * Description of ContaPagarService
 *
 * @author mauricioschmitz
 */
class ContaPagarService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\ContaPagar';
        parent::__construct($em);
    }
    
    public function importXLS($conta, $array){
        $sucesso = false;
        $fieldsName = $array[0];
        foreach($array as $row){
            $data = array();
//            var_dump($array);die();
            if(is_numeric($row[0])){
                foreach($row as $key=>$field){
                    
                    if($fieldsName[$key] == 'valor_previsto' || $fieldsName[$key] == 'valor_pago'){
                        $field = str_replace(',', '.', $field);
                        
                    }elseif(trim ($fieldsName[$key]) == 'categoria'){
                        $descricaoCategoria = $field;
                        $field = $this->em->getRepository('Admin\Entity\CategoriaConta')->findOneBy(array('titulo'=>$descricaoCategoria));
                        if(is_null($field)){
                            $oCategoria = new CategoriaConta();
                            $oCategoria->setTitulo($descricaoCategoria);
                            $this->em->persist($oCategoria);
                            $this->em->flush();
                            $field = $oCategoria;

                        }
                    }
                    $data[$fieldsName[$key]] = $field;
                }
                try{
                    $data['conta'] = $conta;
                    $data['data'] = $conta->getData();
                    $entity = $this->em->getRepository('Admin\Entity\ContaPagar')->find($data['id']);
                    if(!$entity)
                        unset($data['id']);

                    $this->save($data);
                    $sucesso = true;
                } catch (Exception $ex) {
                    $sucesso = false;
                }
            }
        }
        
        return $sucesso;
        
    }
}
