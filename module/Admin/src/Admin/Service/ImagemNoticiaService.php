<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of NoticiaService
 *
 * @author mauricioschmitz
 */
class ImagemNoticiaService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\ImagemNoticia';
        parent::__construct($em);
    }
}
