<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of RastreamentoOcorrenciaService
 *
 * @author mauricioschmitz
 */
class RastreamentoOcorrenciaService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\RastreamentoOcorrencia';
        parent::__construct($em);
    }
}
