<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of PerfilService
 *
 * @author mauricioschmitz
 */
class PerfilService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Login\Entity\User';
        parent::__construct($em);
    }
}
