<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of CategoriaContaService
 *
 * @author mauricioschmitz
 */
class CategoriaContaService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\CategoriaConta';
        parent::__construct($em);
    }
}
