<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of UpgradeAfiliadoService
 *
 * @author mauricioschmitz
 */
class UpgradeAfiliadoService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\UpgradeAfiliado';
        parent::__construct($em);
    }
}
