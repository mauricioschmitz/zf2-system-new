<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of AfiliadoService
 *
 * @author mauricioschmitz
 */
class AfiliadoService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\Afiliado';
        parent::__construct($em);
    }
}
