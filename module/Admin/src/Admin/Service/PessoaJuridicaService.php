<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of PessoaJuridicaService
 *
 * @author mauricioschmitz
 */
class PessoaJuridicaService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\PessoaJuridica';
        parent::__construct($em);
    }
}
