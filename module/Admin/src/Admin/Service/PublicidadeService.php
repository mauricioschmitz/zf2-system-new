<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of CategoriaService
 *
 * @author mauricioschmitz
 */
class PublicidadeService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\Publicidade';
        parent::__construct($em);
    }
}
