<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of SaqueAfiliadoService
 *
 * @author mauricioschmitz
 */
class SaqueAfiliadoService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\SaqueAfiliado';
        parent::__construct($em);
    }
}
