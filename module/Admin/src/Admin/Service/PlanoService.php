<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of PlanoService
 *
 * @author mauricioschmitz
 */
class PlanoService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\Plano';
        parent::__construct($em);
    }
}
