<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of ContaService
 *
 * @author mauricioschmitz
 */
class ContaService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\Conta';
        parent::__construct($em);
    }
}
