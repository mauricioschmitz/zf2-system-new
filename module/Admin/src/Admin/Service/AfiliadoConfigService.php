<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of AfiliadoConfigService
 *
 * @author mauricioschmitz
 */
class AfiliadoConfigService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\AfiliadoConfig';
        parent::__construct($em);
    }
}
