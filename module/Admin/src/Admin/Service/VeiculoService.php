<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of VeiculoService
 *
 * @author mauricioschmitz
 */
class VeiculoService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\Veiculo';
        parent::__construct($em);
    }
}
