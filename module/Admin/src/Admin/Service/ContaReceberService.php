<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of ContaReceberService
 *
 * @author mauricioschmitz
 */
class ContaReceberService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\ContaReceber';
        parent::__construct($em);
    }
    
    public function importXLS($conta, $array){
        $sucesso = false;
        try{
            //Mauricio
            $data['id'] = $array[2][0];
            $data['conta'] = $conta;
            $data['data'] = $conta->getData();
            $data['titulo'] = 'Salário - Mauricio';
            $data['valorPrevisto'] = $array[4][9];
            $data['valorRecebido'] = $array[4][9];

            $entity = $this->em->getRepository('Admin\Entity\ContaReceber')->find($data['id']);
            if(!$entity)
                unset($data['id']);


            $this->save($data);

            //Bruna
            $data['id'] = $array[3][0];
            $data['conta'] = $conta;
            $data['data'] = $conta->getData();
            $data['titulo'] = 'Salário - Bruna';
            $data['valorPrevisto'] = $array[4][10];
            $data['valorRecebido'] = $array[4][10];

            $entity = $this->em->getRepository('Admin\Entity\ContaReceber')->find($data['id']);
            if(!$entity)
                unset($data['id']);


            $this->save($data);
            
            $sucesso = true;
        } catch (Exception $ex) {
            $sucesso = false;
        }
        return $sucesso;
    }
}
