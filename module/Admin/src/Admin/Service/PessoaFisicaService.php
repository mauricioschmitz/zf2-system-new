<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of PessoaFisicaService
 *
 * @author mauricioschmitz
 */
class PessoaFisicaService extends AbstractService{
    private $fotoSize = 76;
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\PessoaFisica';
        
        parent::__construct($em);
    }
    
    public function foto($File,$data){
        $extension = pathinfo($File['name'], PATHINFO_EXTENSION);
        $newName = time().'.'.$extension;
        
        $medida = getimagesize($File['tmp_name']);

        if($medida[0]>$medida[1]) {
            if($this->fotoSize > $medida[0]){
                $x = $medida[0];
                if($this->fotoSize > $medida[1]){
                    $y = $medida[1];
                }
            }else{
                $x = $this->fotoSize;
                $y = $medida[1]/($medida[0]/$this->fotoSize);
            }


        }else{
            if($this->fotoSize  > $medida[1]){
                $y = $medida[1];
                if($this->fotoSize  > $medida[0]){
                    $x = $medida[0];
                }
            }else{
                $y = $this->fotoSize ;
                $x = $medida[0]/($medida[1]/$this->fotoSize );
            }
        }

        if(strtolower($extension) == 'png' || strtolower($extension) == 'pneg'){
            $imagem = imagecreatefrompng($File['tmp_name']);
            $isTrueColor = imageistruecolor($imagem);

            if($isTrueColor){ // Verifica se é truecolor
                $final  = imagecreatetruecolor( $x, $y );
                imagealphablending($final, false);
                imagesavealpha($final  , true );

            }else{
                $final  = imagecreate( $x, $y );
                imagealphablending( $final, false );
                $transparent = imagecolorallocatealpha( $final, 0, 0, 0, 127 );
                imagefill( $final, 0, 0, $transparent );
                imagesavealpha( $final,true );
                imagealphablending( $final, true );
            }

            imagecopyresampled($final, $imagem, 0, 0, 0, 0, $x, $y, $medida[0], $medida[1]);
            imagepng($final, getcwd().'/public/uploads/pessoaFisica/'.$newName);
            imagedestroy($final);

        }elseif(strtolower($extension) == 'jpg' || strtolower($extension) == 'jpeg'){
            //cria imagem temporarioa
            $imagem = imagecreatefromjpeg($File['tmp_name']);

            //imagem
            $final = imagecreatetruecolor($x, $y);
            imagecopyresampled($final, $imagem,0, 0, 0, 0, $x, $y, $medida[0], $medida[1]);
            imagejpeg($final, getcwd().'/public/uploads/pessoaFisica/'.$newName, 80);
            imagedestroy($final);
        }
        $data['foto'] = $newName;
        return $data;
    }
}
