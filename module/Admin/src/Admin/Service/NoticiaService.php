<?php
namespace Admin\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;
/**
 * Description of NoticiaService
 *
 * @author mauricioschmitz
 */
class NoticiaService extends AbstractService{
    public function __construct(EntityManager $em) {
        $this->entity = 'Admin\Entity\Noticia';
        parent::__construct($em);
    }
}
