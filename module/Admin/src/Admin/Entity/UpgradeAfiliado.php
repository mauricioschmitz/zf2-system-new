<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\AbstractEntity;

/**
 * UpgradeAfiliado
 *
 * @ORM\Table(name="upgrade_afiliado", indexes={@ORM\Index(name="fk_afiliado_upgrade_afiliado_idx", columns={"afiliado_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Base\Entity\GlobalRepository")
 */
class UpgradeAfiliado Extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valor;

    /**
     * @var int
     *
     * @ORM\Column(name="cota", type="integer", nullable=false)
     */
    private $cota;

    /**
     * @var float
     *
     * @ORM\Column(name="percentual_indicacao", type="float", precision=10, scale=0, nullable=false)
     */
    private $percentualIndicacao;

    /**
     * @var int
     *
     * @ORM\Column(name="pontos", type="integer", nullable=false)
     */
    private $pontos;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_saque", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorSaque;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inicio_vigencia", type="date", nullable=false)
     */
    private $inicioVigencia = '0000-00-00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fim_vigencia", type="date", nullable=false)
     */
    private $fimVigencia = '0000-00-00';

    /**
     * @var int
     *
     * @ORM\Column(name="situacao", type="integer", nullable=false)
     */
    private $situacao = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \Admin\Entity\Afiliado
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Afiliado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="afiliado_id", referencedColumnName="id")
     * })
     */
    private $afiliado;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return UpgradeAfiliado
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set valor
     *
     * @param string $valor
     *
     * @return UpgradeAfiliado
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set cota
     *
     * @param int $cota
     *
     * @return UpgradeAfiliado
     */
    public function setCota($cota)
    {
        $this->cota = $cota;

        return $this;
    }

    /**
     * Get cota
     *
     * @return int
     */
    public function getCota()
    {
        return $this->cota;
    }

    /**
     * Set percentualIndicacao
     *
     * @param float $percentualIndicacao
     *
     * @return UpgradeAfiliado
     */
    public function setPercentualIndicacao($percentualIndicacao)
    {
        $this->percentualIndicacao = $percentualIndicacao;

        return $this;
    }

    /**
     * Get percentualIndicacao
     *
     * @return float
     */
    public function getPercentualIndicacao()
    {
        return $this->percentualIndicacao;
    }

    /**
     * Set pontos
     *
     * @param int $pontos
     *
     * @return UpgradeAfiliado
     */
    public function setPontos($pontos)
    {
        $this->pontos = $pontos;

        return $this;
    }

    /**
     * Get pontos
     *
     * @return int
     */
    public function getPontos()
    {
        return $this->pontos;
    }

    /**
     * Set valorSaque
     *
     * @param string $valorSaque
     *
     * @return UpgradeAfiliado
     */
    public function setValorSaque($valorSaque)
    {
        $this->valorSaque = $valorSaque;

        return $this;
    }

    /**
     * Get valorSaque
     *
     * @return string
     */
    public function getValorSaque()
    {
        return $this->valorSaque;
    }

    /**
     * Set inicioVigencia
     *
     * @param \DateTime $inicioVigencia
     *
     * @return UpgradeAfiliado
     */
    public function setInicioVigencia($inicioVigencia)
    {
        $this->inicioVigencia = $inicioVigencia;

        return $this;
    }

    /**
     * Get inicioVigencia
     *
     * @return \DateTime
     */
    public function getInicioVigencia()
    {
        return $this->inicioVigencia;
    }

    /**
     * Set fimVigencia
     *
     * @param \DateTime $fimVigencia
     *
     * @return UpgradeAfiliado
     */
    public function setFimVigencia($fimVigencia)
    {
        $this->fimVigencia = $fimVigencia;

        return $this;
    }

    /**
     * Get fimVigencia
     *
     * @return \DateTime
     */
    public function getFimVigencia()
    {
        return $this->fimVigencia;
    }

    /**
     * Set situacao
     *
     * @param int $situacao
     *
     * @return UpgradeAfiliado
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;

        return $this;
    }

    /**
     * Get situacao
     *
     * @return int
     */
    public function getSituacao()
    {
        return $this->situacao;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return UpgradeAfiliado
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set afiliado
     *
     * @param \Admin\Entity\Afiliado $afiliado
     *
     * @return UpgradeAfiliado
     */
    public function setAfiliado(\Admin\Entity\Afiliado $afiliado = null)
    {
        $this->afiliado = $afiliado;

        return $this;
    }

    /**
     * Get afiliado
     *
     * @return \Admin\Entity\Afiliado
     */
    public function getAfiliado()
    {
        return $this->afiliado;
    }
}
