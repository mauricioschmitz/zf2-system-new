<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EspacoPublicidade
 *
 * @ORM\Table(name="espaco_publicidade", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Base\Entity\GlobalRepository")
 */
class EspacoPublicidade
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="tamanho", type="string", length=255, nullable=false)
     */
    private $tamanho;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text", length=65535, nullable=true)
     */
    private $descricao;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", precision=10, scale=0, nullable=false)
     */
    private $valor;

    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return EspacoPublicidade
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set tamanho
     *
     * @param string $tamanho
     *
     * @return EspacoPublicidade
     */
    public function setTamanho($tamanho)
    {
        $this->tamanho = $tamanho;

        return $this;
    }

    /**
     * Get tamanho
     *
     * @return string
     */
    public function getTamanho()
    {
        return $this->tamanho;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return EspacoPublicidade
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set valor
     *
     * @param float $valor
     *
     * @return EspacoPublicidade
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return EspacoPublicidade
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set dataCriacao
     *
     * @param \DateTime $dataCriacao
     *
     * @return EspacoPublicidade
     */
    public function setDataCriacao($dataCriacao)
    {
        $this->dataCriacao = $dataCriacao;

        return $this;
    }

    /**
     * Get dataCriacao
     *
     * @return \DateTime
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }

    /**
     * Set dataModificacao
     *
     * @param \DateTime $dataModificacao
     *
     * @return EspacoPublicidade
     */
    public function setDataModificacao($dataModificacao)
    {
        $this->dataModificacao = $dataModificacao;

        return $this;
    }

    /**
     * Get dataModificacao
     *
     * @return \DateTime
     */
    public function getDataModificacao()
    {
        return $this->dataModificacao;
    }
}
