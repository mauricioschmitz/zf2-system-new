<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\AbstractEntity;
/**
 * AfiliadoDistribuicao
 *
 * @ORM\Table(name="afiliado_distribuicao")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Base\Entity\GlobalRepository")
 */
class AfiliadoDistribuicao extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=255, nullable=false)
     */
    private $descricao;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_arrecadado", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorArrecadado = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_cotas", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorCotas = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_comissao", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorComissao = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_sobra", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $valorSobra = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_pagamento", type="date", nullable=false)
     */
    private $dataPagamento;

    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return AfiliadoDistribuicao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set valorArrecadado
     *
     * @param string $valorArrecadado
     *
     * @return AfiliadoDistribuicao
     */
    public function setValorArrecadado($valorArrecadado)
    {
        $this->valorArrecadado = $valorArrecadado;

        return $this;
    }

    /**
     * Get valorArrecadado
     *
     * @return string
     */
    public function getValorArrecadado()
    {
        return $this->valorArrecadado;
    }

    /**
     * Set valorCotas
     *
     * @param string $valorCotas
     *
     * @return AfiliadoDistribuicao
     */
    public function setValorCotas($valorCotas)
    {
        $this->valorCotas = $valorCotas;

        return $this;
    }

    /**
     * Get valorCotas
     *
     * @return string
     */
    public function getValorCotas()
    {
        return $this->valorCotas;
    }

    /**
     * Set valorComissao
     *
     * @param string $valorComissao
     *
     * @return AfiliadoDistribuicao
     */
    public function setValorComissao($valorComissao)
    {
        $this->valorComissao = $valorComissao;

        return $this;
    }

    /**
     * Get valorComissao
     *
     * @return string
     */
    public function getValorComissao()
    {
        return $this->valorComissao;
    }

    /**
     * Set valorSobra
     *
     * @param string $valorSobra
     *
     * @return AfiliadoDistribuicao
     */
    public function setValorSobra($valorSobra)
    {
        $this->valorSobra = $valorSobra;

        return $this;
    }

    /**
     * Get valorSobra
     *
     * @return string
     */
    public function getValorSobra()
    {
        return $this->valorSobra;
    }

    /**
     * Set dataPagamento
     *
     * @param \DateTime $dataPagamento
     *
     * @return AfiliadoDistribuicao
     */
    public function setDataPagamento($dataPagamento)
    {
        $this->dataPagamento = $dataPagamento;

        return $this;
    }

    /**
     * Get dataPagamento
     *
     * @return \DateTime
     */
    public function getDataPagamento()
    {
        return $this->dataPagamento;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return AfiliadoDistribuicao
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

}
