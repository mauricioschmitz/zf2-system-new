<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\AbstractEntity;

/**
 * AfiliadoConfig
 *
 * @ORM\Table(name="afiliado_config")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Base\Entity\GlobalRepository")
 */
class AfiliadoConfig extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="percentual_distribuicao", type="float", precision=10, scale=0, nullable=false)
     */
    private $percentualDistribuicao;
    
    /**
     * @var float
     *
     * @ORM\Column(name="percentual_retorno", type="float", precision=10, scale=0, nullable=false)
     */
    private $percentualRetorno;
    
    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=false)
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_modificacao", type="datetime", nullable=false)
     */
    private $dataModificacao = 'CURRENT_TIMESTAMP';



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set percentualDistribuicao
     *
     * @param float $percentualDistribuicao
     *
     * @return AfiliadoConfig
     */
    public function setPercentualDistribuicao($percentualDistribuicao)
    {
        $this->percentualDistribuicao = $percentualDistribuicao;

        return $this;
    }

    /**
     * Get percentualDistribuicao
     *
     * @return float
     */
    public function getPercentualDistribuicao()
    {
        return $this->percentualDistribuicao;
    }

    /**
     * Set percentualDistribuicao
     *
     * @param float $percentualDistribuicao
     *
     * @return AfiliadoConfig
     */
    public function setPercentualRetorno($percentualRetorno)
    {
        $this->percentualRetorno = $percentualRetorno;

        return $this;
    }

    /**
     * Get percentualDistribuicao
     *
     * @return float
     */
    public function getPercentualRetorno()
    {
        return $this->percentualRetorno;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return AfiliadoConfig
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set dataCriacao
     *
     * @param \DateTime $dataCriacao
     *
     * @return AfiliadoConfig
     */
    public function setDataCriacao($dataCriacao)
    {
        $this->dataCriacao = $dataCriacao;

        return $this;
    }

    /**
     * Get dataCriacao
     *
     * @return \DateTime
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }

    /**
     * Set dataModificacao
     *
     * @param \DateTime $dataModificacao
     *
     * @return AfiliadoConfig
     */
    public function setDataModificacao($dataModificacao)
    {
        $this->dataModificacao = $dataModificacao;

        return $this;
    }

    /**
     * Get dataModificacao
     *
     * @return \DateTime
     */
    public function getDataModificacao()
    {
        return $this->dataModificacao;
    }
}
