<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\AbstractEntity;

/**
 * AfiliadoExtrato
 *
 * @ORM\Table(name="afiliado_extrato", indexes={@ORM\Index(name="fk_afiliado_extrato_afiliado_idx", columns={"afiliado_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Admin\Entity\AfiliadoExtratoRepository")
 */
class AfiliadoExtrato extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=255, nullable=false)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valor;

    /**
     * @var int
     *
     * @ORM\Column(name="situacao", type="integer", nullable=false)
     */
    private $situacao = '0';
    
    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=255, nullable=false)
     */
    private $descricao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_pagamento", type="date", nullable=false)
     */
    private $data_pagamento;

    
    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \Admin\Entity\Afiliado
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Afiliado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="afiliado_id", referencedColumnName="id")
     * })
     */
    private $afiliado;

    public static $_tipo = array(
        'cota' => 'cota',
        'comissão' => 'comissão',
        'saque' => 'saque'
    );
    
    public static $_situacao = array(
        'pendente' => '0',
        'calculada' => '1',
        'paga' => '2'
    );
    
    public static $_descricao = array(
        'cota' => 'DLG entre ',
        'comissão' => 'Comissão por indicação - ',
        'saque' => 'Saque',
        'renovação' => 'Renovação'
    );

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return AfiliadoExtrato
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set valor
     *
     * @param string $valor
     *
     * @return AfiliadoExtrato
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set situacao
     *
     * @param int $situacao
     *
     * @return AfiliadoExtrato
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;

        return $this;
    }

    /**
     * Get situacao
     *
     * @return int
     */
    public function getSituacao()
    {
        return $this->situacao;
    }
    
    /**
     * Set descricao
     *
     * @param int $descricao
     *
     * @return AfiliadoExtrato
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return int
     */
    public function getDescricao()
    {
        return $this->descricao;
    }
    
    /**
     * Set DataPagamento
     *
     * @param \DateTime $dataPagamento
     *
     * @return AfiliadoExtrato
     */
    public function setDataPagamento($dataPagamento)
    {
        $this->data_pagamento = $dataPagamento;

        return $this;
    }

    /**
     * Get dataPagamento
     *
     * @return \DateTime
     */
    public function getDataPagamento()
    {
        return $this->data_pagamento;
    }
    
    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return AfiliadoExtrato
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set afiliado
     *
     * @param \Admin\Entity\Afiliado $afiliado
     *
     * @return AfiliadoExtrato
     */
    public function setAfiliado(\Admin\Entity\Afiliado $afiliado = null)
    {
        $this->afiliado = $afiliado;

        return $this;
    }

    /**
     * Get afiliado
     *
     * @return \Admin\Entity\Afiliado
     */
    public function getAfiliado()
    {
        return $this->afiliado;
    }
}
