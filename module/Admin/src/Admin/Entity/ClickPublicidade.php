<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\AbstractEntity;
/**
 * VisualizacaoPublicidade
 *
 * @ORM\Table(name="click_publicidade", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_click_publicidade_publicidade_idx", columns={"publicidade_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Admin\Entity\ClickPublicidadeRepository")
 */
class ClickPublicidade extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=255, nullable=false)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text", length=65535, nullable=true)
     */
    private $descricao;

    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=false)
     */
    private $dataCriacao;

    /**
     * @var \Admin\Entity\Publicidade
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Publicidade")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="publicidade_id", referencedColumnName="id")
     * })
     */
    private $publicidade;


    public function __construct(array $options = array()) {
        parent::__construct($options);
        $this->dataCriacao = new \DateTime(date('Y-m-d H:i:s'));
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return ClickPublicidade
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return ClickPublicidade
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return ClickPublicidade
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Get dataCriacao
     *
     * @return \DateTime
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }

    /**
     * Set dataModificacao
     *
     * @param \DateTime $dataModificacao
     *
     * @return ClickPublicidade
     */
    public function setDataModificacao($dataModificacao)
    {
        $this->dataModificacao = $dataModificacao;

        return $this;
    }

    /**
     * Get dataModificacao
     *
     * @return \DateTime
     */
    public function getDataModificacao()
    {
        return $this->dataModificacao;
    }

    /**
     * Set publicidade
     *
     * @param \Admin\Entity\Publicidade $publicidade
     *
     * @return ClickPublicidade
     */
    public function setPublicidade(\Admin\Entity\Publicidade $publicidade = null)
    {
        $this->publicidade = $publicidade;

        return $this;
    }

    /**
     * Get publicidade
     *
     * @return \Admin\Entity\Publicidade
     */
    public function getPublicidade()
    {
        return $this->publicidade;
    }
}
