<?php

namespace Admin\Entity;

use Base\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * VisualizacaoNoticia
 *
 * @ORM\Table(name="visualizacao_noticia", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_imagem_noticia_noticia1_idx", columns={"noticia_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Admin\Entity\VisualizacaoRepository")
 */
class VisualizacaoNoticia extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=255, nullable=false)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text", length=65535, nullable=true)
     */
    private $descricao;

    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \Admin\Entity\Noticia
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Noticia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="noticia_id", referencedColumnName="id")
     * })
     */
    private $noticia;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return VisualizacaoNoticia
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return VisualizacaoNoticia
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return VisualizacaoNoticia
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set noticia
     *
     * @param \Admin\Entity\Noticia $noticia
     *
     * @return VisualizacaoNoticia
     */
    public function setNoticia(\Admin\Entity\Noticia $noticia = null)
    {
        $this->noticia = $noticia;

        return $this;
    }

    /**
     * Get noticia
     *
     * @return \Admin\Entity\Noticia
     */
    public function getNoticia()
    {
        return $this->noticia;
    }
}
