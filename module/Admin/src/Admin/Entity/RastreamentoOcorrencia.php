<?php

namespace Admin\Entity;

use Base\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * RastreamentoOcorrencia
 *
 * @ORM\Table(name="rastreamento_ocorrencia", indexes={@ORM\Index(name="fk_rastreamento_ocorrencia$rastreamento$restreamento_id_idx", columns={"rastreamento_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Admin\Entity\RastreamentoOcorrenciaRepository")
 */
class RastreamentoOcorrencia extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="detalhes", type="string", length=255, nullable=true)
     */
    private $detalhes;

    /**
     * @var string
     *
     * @ORM\Column(name="local", type="string", length=255, nullable=true)
     */
    private $local;

    /**
     * @var string
     *
     * @ORM\Column(name="situacao", type="string", length=255, nullable=true)
     */
    private $situacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="datetime", nullable=false)
     */
    private $data;

    /**
     * @var \Admin\Entity\Rastreamento
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Rastreamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rastreamento_id", referencedColumnName="id")
     * })
     */
    private $rastreamento;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set detalhes
     *
     * @param string $detalhes
     *
     * @return RastreamentoOcorrencia
     */
    public function setDetalhes($detalhes)
    {
        $this->detalhes = $detalhes;

        return $this;
    }

    /**
     * Get detalhes
     *
     * @return string
     */
    public function getDetalhes()
    {
        return $this->detalhes;
    }

    /**
     * Set local
     *
     * @param string $local
     *
     * @return RastreamentoOcorrencia
     */
    public function setLocal($local)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Get local
     *
     * @return string
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * Set situacao
     *
     * @param string $situacao
     *
     * @return RastreamentoOcorrencia
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;

        return $this;
    }

    /**
     * Get situacao
     *
     * @return string
     */
    public function getSituacao()
    {
        return $this->situacao;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return RastreamentoOcorrencia
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set rastreamento
     *
     * @param \Admin\Entity\Rastreamento $rastreamento
     *
     * @return RastreamentoOcorrencia
     */
    public function setRastreamento(\Admin\Entity\Rastreamento $rastreamento = null)
    {
        $this->rastreamento = $rastreamento;

        return $this;
    }

    /**
     * Get rastreamento
     *
     * @return \Admin\Entity\Rastreamento
     */
    public function getRastreamento()
    {
        return $this->rastreamento;
    }
}
