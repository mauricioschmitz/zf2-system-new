<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VisualizacaoPublicidade
 *
 * @ORM\Table(name="visualizacao_publicidade", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_visualizacao_publicidade_publicidade1_idx", columns={"publicidade_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Admin\Entity\GlobalRepository")
 */
class VisualizacaoPublicidade
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=255, nullable=false)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text", length=65535, nullable=true)
     */
    private $descricao;

    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \Admin\Entity\Publicidade
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Publicidade")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="publicidade_id", referencedColumnName="id")
     * })
     */
    private $publicidade;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return VisualizacaoPublicidade
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return VisualizacaoPublicidade
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return VisualizacaoPublicidade
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set dataCriacao
     *
     * @param \DateTime $dataCriacao
     *
     * @return VisualizacaoPublicidade
     */
    public function setDataCriacao($dataCriacao)
    {
        $this->dataCriacao = $dataCriacao;

        return $this;
    }

    /**
     * Get dataCriacao
     *
     * @return \DateTime
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }

    /**
     * Set dataModificacao
     *
     * @param \DateTime $dataModificacao
     *
     * @return VisualizacaoPublicidade
     */
    public function setDataModificacao($dataModificacao)
    {
        $this->dataModificacao = $dataModificacao;

        return $this;
    }

    /**
     * Get dataModificacao
     *
     * @return \DateTime
     */
    public function getDataModificacao()
    {
        return $this->dataModificacao;
    }

    /**
     * Set publicidade
     *
     * @param \Admin\Entity\Publicidade $publicidade
     *
     * @return VisualizacaoPublicidade
     */
    public function setPublicidade(\Admin\Entity\Publicidade $publicidade = null)
    {
        $this->publicidade = $publicidade;

        return $this;
    }

    /**
     * Get publicidade
     *
     * @return \Admin\Entity\Publicidade
     */
    public function getPublicidade()
    {
        return $this->publicidade;
    }
}
