<?php

namespace Admin\Entity;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Tools\Pagination\Paginator;
/**
 * NoticiaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class NoticiaRepository extends \Doctrine\ORM\EntityRepository{
    
    public $exibidos = array();
    
    public function findByNivel($nivel = '', $categoria=null, $limit = 4){
        $qb = $this->_em->createQueryBuilder();
        $qb->select('n')
            ->from('Admin\Entity\Noticia', 'n')
            ->andWhere('n.ativo = 1')
            ->orderBy('n.data_publicacao','DESC')
            ->setMaxResults($limit);
        
        $expr = Criteria::expr();
        $criteria = Criteria::create();
        $criteria->andWhere($expr->eq('n.status', 'publicado'));
        $criteria->andWhere($expr->lte('n.data_publicacao', date('Y-m-d H:i:s')));
        $qb->addCriteria ($criteria);
            
        if($nivel != ''){
            $expr = Criteria::expr();
            $criteria = Criteria::create();
            $criteria->andWhere($expr->eq('n.nivel', $nivel));
            $qb->addCriteria ($criteria);
        }
        
        if(!is_null($categoria)){
            $expr = Criteria::expr();
            $criteria = Criteria::create();
            $criteria->andWhere($expr->eq('n.categoria', $categoria));
            $qb->addCriteria ($criteria);
        }
        
        if(count($this->exibidos)){
            $expr = Criteria::expr();
            $criteria = Criteria::create();
            $criteria->where($expr->notIn('id', $this->exibidos));
            $qb->addCriteria ($criteria);
        }
        
        $result = $qb->getQuery()->getResult();
        foreach($result as $row){
            array_push($this->exibidos, $row->getId());
        }
        return $result;
    }
    
    public function getPagedNoticia($categoria=null,$offset =1, $limit = 1){
        $offset = $offset*$limit;
        $qb = $this->_em->createQueryBuilder();
        $qb->select('n')
            ->from('\Admin\Entity\Noticia', 'n')
            ->andWhere('n.ativo = 1')
            ->orderBy('n.data_publicacao', 'desc')
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        $expr = Criteria::expr();
        $criteria = Criteria::create();
        $criteria->andWhere($expr->eq('n.status', 'publicado'));
        $criteria->andWhere($expr->lte('n.data_publicacao', date('Y-m-d H:i:s')));
        $qb->addCriteria ($criteria);
            
        if(!is_null($categoria)){
            $expr = Criteria::expr();
            $criteria = Criteria::create();
            $criteria->andWhere($expr->eq('n.categoria', $categoria));
            $qb->addCriteria ($criteria);
        }
        $query = $qb->getQuery();
        $paginator = new Paginator( $query );

        return $paginator;
    }
}
