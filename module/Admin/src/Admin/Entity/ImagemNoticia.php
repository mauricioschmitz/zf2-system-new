<?php

namespace Admin\Entity;

use Base\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * ImagemNoticia
 *
 * @ORM\Table(name="imagem_noticia", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_imagem_noticia_noticia1_idx", columns={"noticia_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Base\Entity\GlobalRepository")
 */
class ImagemNoticia extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="arquivo", type="string", length=255, nullable=false)
     */
    private $arquivo;

    /**
     * @var int
     *
     * @ORM\Column(name="ordem", type="integer", nullable=false)
     */
    private $ordem = '1';
    
    /**
     * @var int
     *
     * @ORM\Column(name="exportada", type="integer", nullable=false)
     */
    private $exportada = '0';
    
    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \Admin\Entity\Noticia
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Noticia", inversedBy="imagens")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="noticia_id", referencedColumnName="id")
     * })
     */
    private $noticia;
    
     /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return ImagemNoticia
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set arquivo
     *
     * @param string $arquivo
     *
     * @return ImagemNoticia
     */
    public function setArquivo($arquivo)
    {
        $this->arquivo = $arquivo;

        return $this;
    }

    /**
     * Get arquivo
     *
     * @return string
     */
    public function getArquivo()
    {
        return $this->arquivo;
    }

    /**
     * Set ordem
     *
     * @param int $ordem
     *
     * @return ImagemNoticia
     */
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;

        return $this;
    }

    /**
     * Get ordem
     *
     * @return int
     */
    public function getOrdem()
    {
        return $this->ordem;
    }
    
    /**
     * Set exportada
     *
     * @param int $exportada
     *
     * @return ImagemNoticia
     */
    public function setExportada($exportada)
    {
        $this->exportada = $exportada;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getExportada()
    {
        return $this->exportada;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return ImagemNoticia
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set dataCriacao
     *
     * @param \DateTime $dataCriacao
     *
     * @return ImagemNoticia
     */
    public function setDataCriacao($dataCriacao)
    {
        $this->dataCriacao = $dataCriacao;

        return $this;
    }

    /**
     * Get dataCriacao
     *
     * @return \DateTime
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }

    /**
     * Set dataModificacao
     *
     * @param \DateTime $dataModificacao
     *
     * @return ImagemNoticia
     */
    public function setDataModificacao($dataModificacao)
    {
        $this->dataModificacao = $dataModificacao;

        return $this;
    }

    /**
     * Get dataModificacao
     *
     * @return \DateTime
     */
    public function getDataModificacao()
    {
        return $this->dataModificacao;
    }
    
    /**
     * Set noticia
     *
     * @param \Admin\Entity\Noticia $noticia
     *
     * @return Municipio
     */
    public function setNoticia(Noticia $noticia = null)
    {
        $this->noticia = $noticia;

        return $this;
    }

    /**
     * Get estado
     *
     * @return \Admin\Entity\Noticia
     */
    public function getNoticia()
    {
        return $this->noticia;
    }
}
