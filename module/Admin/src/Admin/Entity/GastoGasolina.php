<?php

namespace Admin\Entity;

use Base\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * GastoGasolina
 *
 * @ORM\Table(name="gasto_gasolina", indexes={@ORM\Index(name="fk_gasto_gasolina$veiculo$veiculo_id_idx", columns={"veiculo_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Admin\Entity\GastoGasolinaRepository")
 */
class GastoGasolina extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="date", nullable=false)
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(name="km", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $km;

    /**
     * @var string
     *
     * @ORM\Column(name="litros", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $litros;

    /**
     * @var string
     *
     * @ORM\Column(name="preco_combustivel", type="decimal", precision=10, scale=3, nullable=false)
     */
    private $preco_combustivel;

    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \Admin\Entity\Veiculo
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Veiculo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="veiculo_id", referencedColumnName="id")
     * })
     */
    private $veiculo;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return GastoGasolina
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set km
     *
     * @param string $km
     *
     * @return GastoGasolina
     */
    public function setKm($km)
    {
        $this->km = $km;

        return $this;
    }

    /**
     * Get km
     *
     * @return string
     */
    public function getKm()
    {
        return $this->km;
    }

    /**
     * Set litros
     *
     * @param string $litros
     *
     * @return GastoGasolina
     */
    public function setLitros($litros)
    {
        $this->litros = $litros;

        return $this;
    }

    /**
     * Get litros
     *
     * @return string
     */
    public function getLitros()
    {
        return $this->litros;
    }

    /**
     * Set precoCombustivel
     *
     * @param string $precoCombustivel
     *
     * @return GastoGasolina
     */
    public function setPrecoCombustivel($precoCombustivel)
    {
        $this->preco_combustivel = $precoCombustivel;

        return $this;
    }

    /**
     * Get precoCombustivel
     *
     * @return string
     */
    public function getPrecoCombustivel()
    {
        return $this->preco_combustivel;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return GastoGasolina
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set veiculo
     *
     * @param \Admin\Entity\Veiculo $veiculo
     *
     * @return GastoGasolina
     */
    public function setVeiculo(\Admin\Entity\Veiculo $veiculo = null)
    {
        $this->veiculo = $veiculo;

        return $this;
    }

    /**
     * Get veiculo
     *
     * @return \Admin\Entity\Veiculo
     */
    public function getVeiculo()
    {
        return $this->veiculo;
    }
}
