<?php

namespace Admin\Entity;

use Base\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * ContaPagar
 *
 * @ORM\Table(name="conta_pagar", indexes={@ORM\Index(name="fk_conta_pagar$conta$conta_id_idx", columns={"conta_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Admin\Entity\ContaPagarRepository")
 */
class ContaPagar extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="date", nullable=false)
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_previsto", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorPrevisto;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_pago", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $valorPago;

    /**
     * @var int
     *
     * @ORM\Column(name="parcela_atual", type="integer", nullable=false)
     */
    private $parcelaAtual = '1';
    
    /**
     * @var int
     *
     * @ORM\Column(name="qtd_parcelas", type="integer", nullable=false)
     */
    private $qtdParcelas = '1';
    
    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \Admin\Entity\Conta
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Conta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="conta_id", referencedColumnName="id")
     * })
     */
    private $conta;

    /**
     * @var \Admin\Entity\CategoriaConta
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\CategoriaConta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoria_conta_id", referencedColumnName="id")
     * })
     */
    private $categoria;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return ContaPagar
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return ContaPagar
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set valorPrevisto
     *
     * @param string $valorPrevisto
     *
     * @return ContaPagar
     */
    public function setValorPrevisto($valorPrevisto)
    {
        $this->valorPrevisto = $valorPrevisto;

        return $this;
    }

    /**
     * Get valorPrevisto
     *
     * @return string
     */
    public function getValorPrevisto()
    {
        return $this->valorPrevisto;
    }

    /**
     * Set valorPago
     *
     * @param string $valorPago
     *
     * @return ContaPagar
     */
    public function setValorPago($valorPago)
    {
        $this->valorPago = $valorPago;

        return $this;
    }

    /**
     * Get valorPago
     *
     * @return string
     */
    public function getValorPago()
    {
        return $this->valorPago;
    }

    /**
     * Set parcelaAtual
     *
     * @param int $parcelaAtual
     *
     * @return ContaPagar
     */
    public function setParcelaAtual($parcelaAtual)
    {
        $this->parcelaAtual = $parcelaAtual;

        return $this;
    }

    /**
     * Get parcelaAtual
     *
     * @return int
     */
    public function getParcelaAtual()
    {
        return $this->parcelaAtual;
    }
    
    /**
     * Set qtdParcelas
     *
     * @param int $qtdParcelas
     *
     * @return ContaPagar
     */
    public function setQtdParcelas($qtdParcelas)
    {
        $this->qtdParcelas = $qtdParcelas;

        return $this;
    }

    /**
     * Get qtdParcelas
     *
     * @return int
     */
    public function getQtdParcelas()
    {
        return $this->qtdParcelas;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return ContaPagar
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set conta
     *
     * @param \Admin\Entity\Conta $conta
     *
     * @return ContaPagar
     */
    public function setConta(\Admin\Entity\Conta $conta = null)
    {
        $this->conta = $conta;

        return $this;
    }

    /**
     * Get conta
     *
     * @return \Admin\Entity\Conta
     */
    public function getConta()
    {
        return $this->conta;
    }

    /**
     * Set categoria
     *
     * @param \Admin\Entity\CategoriaConta $categoria
     *
     * @return CategoriaConta
     */
    public function setCategoria(\Admin\Entity\CategoriaConta $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \Admin\Entity\CategoriaConta
     */
    public function getCategoria()
    {
        return $this->categoria;
    }
}
