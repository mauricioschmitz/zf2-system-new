<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Afiliado
 *
 * @ORM\Table(name="afiliado", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_pessoa_fisica_afiliado_idx", columns={"pessoa_fisica_id"}), @ORM\Index(name="fk_pessoa_juridica_afiliado_idx", columns={"pessoa_juridica_id"}), @ORM\Index(name="fk_user_afiliado_idx", columns={"user_id"})})
 * @ORM\Entity
 */
class Afiliado
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_plano", type="string", length=255, nullable=false)
     */
    private $nomePlano;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_investimento", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorInvestimento;

    /**
     * @var int
     *
     * @ORM\Column(name="cota", type="integer", nullable=false)
     */
    private $cota;

    /**
     * @var float
     *
     * @ORM\Column(name="percentual_indicacao", type="float", precision=10, scale=0, nullable=false)
     */
    private $percentualIndicacao;

    /**
     * @var int
     *
     * @ORM\Column(name="pontos", type="integer", nullable=false)
     */
    private $pontos;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_saque", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorSaque;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inicio_vigencia", type="date", nullable=false)
     */
    private $inicioVigencia = '0000-00-00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fim_vigencia", type="date", nullable=false)
     */
    private $fimVigencia = '0000-00-00';

    /**
     * @var string
     *
     * @ORM\Column(name="indicacao", type="string", length=255, nullable=true)
     */
    private $indicacao;

    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=false)
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_modificacao", type="datetime", nullable=false)
     */
    private $dataModificacao = 'CURRENT_TIMESTAMP';

    /**
     * @var \Admin\Entity\PessoaFisica
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\PessoaFisica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pessoa_fisica_id", referencedColumnName="id")
     * })
     */
    private $pessoaFisica;

    /**
     * @var \Admin\Entity\PessoaJuridica
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\PessoaJuridica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pessoa_juridica_id", referencedColumnName="id")
     * })
     */
    private $pessoaJuridica;

    /**
     * @var \Admin\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


}

