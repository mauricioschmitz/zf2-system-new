<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\AbstractEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Afiliado
 *
 * @ORM\Table(name="afiliado", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_pessoa_fisica_afiliado_idx", columns={"pessoa_fisica_id"}), @ORM\Index(name="fk_pessoa_juridica_afiliado_idx", columns={"pessoa_juridica_id"}), @ORM\Index(name="fk_user_afiliado_idx", columns={"user_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Admin\Entity\AfiliadoRepository")
 */
class Afiliado extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_plano", type="string", length=255, nullable=false)
     */
    private $nomePlano;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_investimento", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorInvestimento;

    /**
     * @var int
     *
     * @ORM\Column(name="cota", type="integer", nullable=false)
     */
    private $cota;

    /**
     * @var float
     *
     * @ORM\Column(name="percentual_indicacao", type="float", precision=10, scale=0, nullable=false)
     */
    private $percentualIndicacao;

    /**
     * @var int
     *
     * @ORM\Column(name="pontos", type="integer", nullable=false)
     */
    private $pontos;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_saque", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorSaque;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_sacado", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorSacado = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inicio_vigencia", type="date", nullable=false)
     */
    private $inicioVigencia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fim_vigencia", type="date", nullable=false)
     */
    private $fimVigencia;

    /**
     * @var string
     *
     * @ORM\Column(name="indicacao", type="string", length=255, nullable=true)
     */
    private $indicacao;

    /**
     * @var int
     *
     * @ORM\Column(name="pago", type="integer", nullable=false)
     */
    private $pago = '0';
    
    /**
     * @var string
     *
     * @ORM\Column(name="numero_banco", type="string", length=45, nullable=true)
     */
    private $numeroBanco;
    
    /**
     * @var string
     *
     * @ORM\Column(name="banco", type="string", length=255, nullable=true)
     */
    private $banco;
    
    /**
     * @var string
     *
     * @ORM\Column(name="agencia", type="string", length=45, nullable=true)
     */
    private $agencia;
    
    /**
     * @var string
     *
     * @ORM\Column(name="conta", type="string", length=45, nullable=true)
     */
    private $conta;
    
    /**
     * @var string
     *
     * @ORM\Column(name="titular", type="string", length=255, nullable=true)
     */
    private $titular;
    
    /**
     * @var string
     *
     * @ORM\Column(name="obs", type="text", length=65535, nullable=true)
     */
    private $obs;
    
    /**
     * @var int
     *
     * @ORM\Column(name="pontos_pessoais", type="integer", nullable=false)
     */
    private $pontosPessoais = '0';
    
    /**
     * @var int
     *
     * @ORM\Column(name="pontos_rede", type="integer", nullable=false)
     */
    private $pontosRede = '0';
    
    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \Admin\Entity\PessoaFisica
     *
     * @ORM\OneToOne(targetEntity="Admin\Entity\PessoaFisica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pessoa_fisica_id", referencedColumnName="id")
     * })
     */
    private $pessoaFisica;

    /**
     * @var \Admin\Entity\PessoaJuridica
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\PessoaJuridica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pessoa_juridica_id", referencedColumnName="id")
     * })
     */
    private $pessoaJuridica;

    /**
     * @var \Login\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Login\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomePlano
     *
     * @param string $nomePlano
     *
     * @return Afiliado
     */
    public function setNomePlano($nomePlano)
    {
        $this->nomePlano = $nomePlano;

        return $this;
    }

    /**
     * Get nomePlano
     *
     * @return string
     */
    public function getNomePlano()
    {
        return $this->nomePlano;
    }

    /**
     * Set valorInvestimento
     *
     * @param string $valorInvestimento
     *
     * @return Afiliado
     */
    public function setValorInvestimento($valorInvestimento)
    {
        $this->valorInvestimento = $valorInvestimento;

        return $this;
    }

    /**
     * Get valorInvestimento
     *
     * @return string
     */
    public function getValorInvestimento()
    {
        return $this->valorInvestimento;
    }

    /**
     * Set cota
     *
     * @param int $cota
     *
     * @return Afiliado
     */
    public function setCota($cota)
    {
        $this->cota = $cota;

        return $this;
    }

    /**
     * Get cota
     *
     * @return int
     */
    public function getCota()
    {
        return $this->cota;
    }

    /**
     * Set percentualIndicacao
     *
     * @param float $percentualIndicacao
     *
     * @return Afiliado
     */
    public function setPercentualIndicacao($percentualIndicacao)
    {
        $this->percentualIndicacao = $percentualIndicacao;

        return $this;
    }

    /**
     * Get percentualIndicacao
     *
     * @return float
     */
    public function getPercentualIndicacao()
    {
        return $this->percentualIndicacao;
    }

    /**
     * Set pontos
     *
     * @param int $pontos
     *
     * @return Afiliado
     */
    public function setPontos($pontos)
    {
        $this->pontos = $pontos;

        return $this;
    }

    /**
     * Get pontos
     *
     * @return int
     */
    public function getPontos()
    {
        return $this->pontos;
    }

    /**
     * Set valorSaque
     *
     * @param string $valorSaque
     *
     * @return Afiliado
     */
    public function setValorSaque($valorSaque)
    {
        $this->valorSaque = $valorSaque;

        return $this;
    }

    /**
     * Get valorSaque
     *
     * @return string
     */
    public function getValorSaque()
    {
        return $this->valorSaque;
    }

    /**
     * Set valorSacado
     *
     * @param string $valorSacado
     *
     * @return Afiliado
     */
    public function setValorSacado($valorSacado)
    {
        $this->valorSacado = $valorSacado;

        return $this;
    }

    /**
     * Get valorSaque
     *
     * @return string
     */
    public function getValorSacado()
    {
        return $this->valorSacado;
    }

    /**
     * Set inicioVigencia
     *
     * @param \DateTime $inicioVigencia
     *
     * @return Afiliado
     */
    public function setInicioVigencia($inicioVigencia)
    {
        $this->inicioVigencia = $inicioVigencia;

        return $this;
    }

    /**
     * Get inicioVigencia
     *
     * @return \DateTime
     */
    public function getInicioVigencia()
    {
        return $this->inicioVigencia;
    }

    /**
     * Set fimVigencia
     *
     * @param \DateTime $fimVigencia
     *
     * @return Afiliado
     */
    public function setFimVigencia($fimVigencia)
    {
        $this->fimVigencia = $fimVigencia;

        return $this;
    }

    /**
     * Get fimVigencia
     *
     * @return \DateTime
     */
    public function getFimVigencia()
    {
        return $this->fimVigencia;
    }

    /**
     * Set indicacao
     *
     * @param string $indicacao
     *
     * @return Afiliado
     */
    public function setIndicacao($indicacao)
    {
        $this->indicacao = $indicacao;

        return $this;
    }

    /**
     * Get indicacao
     *
     * @return string
     */
    public function getIndicacao()
    {
        return $this->indicacao;
    }

    /**
     * Set pago
     *
     * @param int $pago
     *
     * @return Afiliado
     */
    public function setPago($pago)
    {
        $this->pago = $pago;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getPago()
    {
        return $this->pago;
    }

    /**
     * Set numeroBanco
     *
     * @param string $numeroBanco
     *
     * @return Afiliado
     */
    public function setNumeroBanco($numeroBanco)
    {
        $this->numeroBanco = $numeroBanco;

        return $this;
    }

    /**
     * Get numeroBanco
     *
     * @return string
     */
    public function getNumeroBanco()
    {
        return $this->numeroBanco;
    }

    /**
     * Set banco
     *
     * @param string $banco
     *
     * @return Afiliado
     */
    public function setBanco($banco)
    {
        $this->banco = $banco;

        return $this;
    }

    /**
     * Get Banco
     *
     * @return string
     */
    public function getBanco()
    {
        return $this->banco;
    }

    /**
     * Set agencia
     *
     * @param string $agencia
     *
     * @return Afiliado
     */
    public function setAgencia($agencia)
    {
        $this->agencia = $agencia;

        return $this;
    }

    /**
     * Get agencia
     *
     * @return string
     */
    public function getAgencia()
    {
        return $this->agencia;
    }

    /**
     * Set conta
     *
     * @param string $conta
     *
     * @return Afiliado
     */
    public function setConta($conta)
    {
        $this->conta = $conta;

        return $this;
    }

    /**
     * Get conta
     *
     * @return string
     */
    public function getConta()
    {
        return $this->conta;
    }

    /**
     * Set titular
     *
     * @param string $titular
     *
     * @return Afiliado
     */
    public function setTitular($titular)
    {
        $this->titular = $titular;

        return $this;
    }

    /**
     * Get titular
     *
     * @return string
     */
    public function getTitular()
    {
        return $this->titular;
    }

    /**
     * Set obs
     *
     * @param string $obs
     *
     * @return Afiliado
     */
    public function setObs($obs)
    {
        $this->obs = $obs;

        return $this;
    }

    /**
     * Get obs
     *
     * @return string
     */
    public function getObs()
    {
        return $this->obs;
    }

    /**
     * Set pontosPessoais
     *
     * @param int $pontosPessoais
     *
     * @return Afiliado
     */
    public function setPontosPessoais($pontosPessoais)
    {
        $this->pontosPessoais = $pontosPessoais;

        return $this;
    }

    /**
     * Get pontosPessoais
     *
     * @return int
     */
    public function getPontosPessoais()
    {
        return $this->pontosPessoais;
    }

    /**
     * Set pontosRede
     *
     * @param int $pontosRede
     *
     * @return Afiliado
     */
    public function setpontosRede($pontosRede)
    {
        $this->pontosRede = $pontosRede;

        return $this;
    }

    /**
     * Get pontosRede
     *
     * @return int
     */
    public function getPontosRede()
    {
        return $this->pontosRede;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return Afiliado
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set pessoaFisica
     *
     * @param \Admin\Entity\PessoaFisica $pessoaFisica
     *
     * @return Afiliado
     */
    public function setPessoaFisica(\Admin\Entity\PessoaFisica $pessoaFisica = null)
    {
        $this->pessoaFisica = $pessoaFisica;

        return $this;
    }

    /**
     * Get pessoaFisica
     *
     * @return \Admin\Entity\PessoaFisica
     */
    public function getPessoaFisica()
    {
        return $this->pessoaFisica;
    }

    /**
     * Set pessoaJuridica
     *
     * @param \Admin\Entity\PessoaJuridica $pessoaJuridica
     *
     * @return Afiliado
     */
    public function setPessoaJuridica(\Admin\Entity\PessoaJuridica $pessoaJuridica = null)
    {
        $this->pessoaJuridica = $pessoaJuridica;

        return $this;
    }

    /**
     * Get pessoaJuridica
     *
     * @return \Admin\Entity\PessoaJuridica
     */
    public function getPessoaJuridica()
    {
        return $this->pessoaJuridica;
    }

    /**
     * Set user
     *
     * @param \Login\Entity\User $user
     *
     * @return Afiliado
     */
    public function setUser(\Login\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Login\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
