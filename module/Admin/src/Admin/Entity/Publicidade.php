<?php

namespace Admin\Entity;

use Base\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Publicidade
 *
 * @ORM\Table(name="publicidade", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_publicidade_espaco_publicidade1_idx", columns={"espaco_publicidade_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Admin\Entity\PublicidadeRepository")
 */
class Publicidade extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var int
     *
     * @ORM\Column(name="espaco_publicidade_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $espaco_publicidade_id;
    
    /**
     * @var int
     *
     * @ORM\Column(name="categoria_id", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $categoriaId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="arquivo", type="string", length=255, nullable=false)
     */
    private $arquivo;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=false)
     */
    private $link;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_publicacao", type="date", nullable=false)
     */
    private $data_publicacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_expiracao", type="date", nullable=true)
     */
    private $data_expiracao;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", precision=10, scale=0, nullable=false)
     */
    private $valor;

    /**
     * @var int
     *
     * @ORM\Column(name="sun", type="integer", nullable=true)
     */
    private $sun = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="mon", type="integer", nullable=false)
     */
    private $mon = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="tue", type="integer", nullable=false)
     */
    private $tue = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="wed", type="integer", nullable=false)
     */
    private $wed = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="thu", type="integer", nullable=false)
     */
    private $thu = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="fri", type="integer", nullable=false)
     */
    private $fri = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="sat", type="integer", nullable=false)
     */
    private $sat = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \Admin\Entity\EspacoPublicidade
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\EspacoPublicidade", inversedBy="publicidades")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="espaco_publicidade_id", referencedColumnName="id")
     * })
     */
    private $espaco_publicidade;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set espacoPublicidadeId
     *
     * @param int $espacoPublicidadeId
     *
     * @return Publicidade
     */
    public function setEspacoPublicidadeId($espacoPublicidadeId)
    {
        $this->espaco_publicidade_id = $espacoPublicidadeId;

        return $this;
    }

    /**
     * Get categoriaId
     *
     * @return int
     */
    public function getEspacoPublicidadeId()
    {
        return $this->espaco_publicidade_id;
    }
    
    /**
     * Set categoriaId
     *
     * @param int $categoriaId
     *
     * @return Publicidade
     */
    public function setCategoriaId($categoriaId)
    {
        $this->categoriaId = $categoriaId;

        return $this;
    }

    /**
     * Get categoriaId
     *
     * @return int
     */
    public function getCategoriaId()
    {
        return $this->categoriaId;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Publicidade
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set arquivo
     *
     * @param string $arquivo
     *
     * @return Publicidade
     */
    public function setArquivo($arquivo)
    {
        $this->arquivo = $arquivo;

        return $this;
    }

    /**
     * Get arquivo
     *
     * @return string
     */
    public function getArquivo()
    {
        return $this->arquivo;
    }
    
    /**
     * Set link
     *
     * @param string $link
     *
     * @return Publicidade
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set dataPublicacao
     *
     * @param \DateTime $dataPublicacao
     *
     * @return Publicidade
     */
    public function setDataPublicacao($dataPublicacao)
    {
        $this->data_publicacao = $dataPublicacao;

        return $this;
    }

    /**
     * Get dataPublicacao
     *
     * @return \DateTime
     */
    public function getDataPublicacao()
    {
        return $this->data_publicacao;
    }

    /**
     * Set dataExpiracao
     *
     * @param \DateTime $dataExpiracao
     *
     * @return Publicidade
     */
    public function setDataExpiracao($dataExpiracao)
    {
        $this->data_expiracao = $dataExpiracao;

        return $this;
    }

    /**
     * Get dataExpiracao
     *
     * @return \DateTime
     */
    public function getDataExpiracao()
    {
        return $this->data_expiracao;
    }

    /**
     * Set valor
     *
     * @param float $valor
     *
     * @return Publicidade
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set sun
     *
     * @param int $sun
     *
     * @return Publicidade
     */
    public function setSun($sun)
    {
        $this->sun = $sun;

        return $this;
    }


    /**
     * Get sun
     *
     * @return int
     */
    public function getSun()
    {
        return $this->sun;
    }

    /**
     * Set mon
     *
     * @param int $mon
     *
     * @return Publicidade
     */
    public function setMon($mon)
    {
        $this->mon = $mon;

        return $this;
    }


    /**
     * Get mon
     *
     * @return int
     */
    public function getMon()
    {
        return $this->mon;
    }

    /**
     * Set tue
     *
     * @param int $tue
     *
     * @return Publicidade
     */
    public function setTue($tue)
    {
        $this->tue = $tue;

        return $this;
    }


    /**
     * Get tue
     *
     * @return int
     */
    public function getTue()
    {
        return $this->tue;
    }

    /**
     * Set wed
     *
     * @param int $wed
     *
     * @return Publicidade
     */
    public function setWed($wed)
    {
        $this->wed = $wed;

        return $this;
    }


    /**
     * Get wed
     *
     * @return int
     */
    public function getWed()
    {
        return $this->wed;
    }

    /**
     * Set thu
     *
     * @param int $thu
     *
     * @return Publicidade
     */
    public function setThu($thu)
    {
        $this->thu = $thu;

        return $this;
    }

    /**
     * Get thu
     *
     * @return int
     */
    public function getThu()
    {
        return $this->thu;
    }

    /**
     * Get fri
     *
     * @return int
     */
    public function getFri()
    {
        return $this->fri;
    }

    /**
     * Set fri
     *
     * @param int $fri
     *
     * @return Publicidade
     */
    public function setFri($fri)
    {
        $this->fri = $fri;

        return $this;
    }


    /**
     * Get sat
     *
     * @return sat
     */
    public function getSat()
    {
        return $this->sat;
    }

    /**
     * Set sat
     *
     * @param int $sat
     *
     * @return Publicidade
     */
    public function setSat($sat)
    {
        $this->sat = $sat;

        return $this;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return Publicidade
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }


    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set espacoPublicidade
     *
     * @param \Admin\Entity\EspacoPublicidade $espacoPublicidade
     *
     * @return Publicidade
     */
    public function setEspacoPublicidade(\Admin\Entity\EspacoPublicidade $espacoPublicidade = null)
    {
        $this->espaco_publicidade = $espacoPublicidade;

        return $this;
    }

    /**
     * Get espacoPublicidade
     *
     * @return \Admin\Entity\EspacoPublicidade
     */
    public function getEspacoPublicidade()
    {
        return $this->espaco_publicidade;
    }
}
