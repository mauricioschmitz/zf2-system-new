<?php

namespace Admin\Entity;

use Base\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Categoria
 *
 * @ORM\Table(name="categoria", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Admin\Entity\CategoriaRepository")
 */
class Categoria extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="cor", type="string", length=255, nullable=false)
     */
    private $cor;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text", length=65535, nullable=true)
     */
    private $descricao;

    /**
     * @var string
     *
     * @ORM\Column(name="apelido", type="text", length=255, nullable=true)
     */
    private $apelido;
    
    /**
     * @var int
     *
     * @ORM\Column(name="destaque", type="integer", nullable=false)
     */
    private $destaque = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
    * @var ArrayCollection Coleção de noticias
    *
    * @ORM\OneToMany(targetEntity="Admin\Entity\Noticia", mappedBy="categoria")
    * @ORM\OrderBy({"data_publicacao" = "DESC"})
    */
    private $noticias;

    public function __construct(array $options = array()) {
        parent::__construct($options);
        $this->noticias = new ArrayCollection();
    }
    
    public function getNoticias()
    {
        return $this->noticias;
    }

    public function setNoticias(ArrayCollection $noticias)
    {
        foreach ($noticias as $noticia) {
            $noticia->setCategoria($this);
        }
        $this->noticias = $noticias;
        return $this;
    }
    
    /**
     * Get id
     *
     * @return int
     */
    function getId() {
        return $this->id;
    }
    
    /**
     * Get titulo
     *
     * @return string
     */
    
    function getTitulo() {
        return $this->titulo;
    }

    /**
     * Get COR
     *
     * @return string
     */
    
    function getCor() {
        return $this->cor;
    }
    
    /**
     * Get descricao
     *
     * @return string
     */
    function getDescricao() {
        return $this->descricao;
    }
    
    /**
     * Get destaque
     *
     * @return int
     */
    function getDestaque() {
        return $this->destaque;
    }
    
    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Categoria
     */
    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }
    
    /**
     * Set cor
     *
     * @param string $cor
     *
     * @return Categoria
     */
    function setCor($cor) {
        $this->cor = $cor;
        
        return $this;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return Categoria
     */
    function setDescricao($descricao) {
        $this->descricao = $descricao;
        return $this;
    }
    
    /**
     * Set destaque
     *
     * @param string $destaque
     *
     * @return Categoria
     */
    function setDestaque($destaque) {
        $this->destaque = $destaque;
        return $this;
    }
    
    /**
     * Set apelido
     *
     * @param string $apelido
     *
     * @return Noticia
     */
    public function setApelido($apelido)
    {
        $this->apelido = $apelido;

        return $this;
    }

    /**
     * Get apelido
     *
     * @return string
     */
    public function getApelido()
    {
        return $this->apelido;
    }

}

