<?php

namespace Admin\Entity;

use Base\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Noticia
 *
 * @ORM\Table(name="noticia", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_noticia_categoria1_idx", columns={"categoria_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Admin\Entity\NoticiaRepository")
 */
class Noticia extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="usuario_aprovacao_id", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $usuario_aprovacao_id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo = 'Sem título';

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text", nullable=true)
     */
    private $descricao;

    /**
     * @var string
     *
     * @ORM\Column(name="chamada", type="text", length=65535, nullable=true)
     */
    private $chamada;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="text", length=65535, nullable=false)
     */
    private $tag;

    /**
     * @var string
     *
     * @ORM\Column(name="nivel", type="string", length=1, nullable=false)
     */
    private $nivel = 'A';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_publicacao", type="datetime", nullable=true)
     */
    private $data_publicacao = null;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=45, nullable=false)
     */
    private $status = 'rascunho';

    /**
     * @var int
     *
     * @ORM\Column(name="exclusivo", type="integer", nullable=false)
     */
    private $exclusivo = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="fonte", type="string", length=255, nullable=false)
     */
    private $fonte;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_post_id", type="string", length=255, nullable=false)
     */
    private $facebookPostId;

    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \Admin\Entity\Categoria
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Categoria", inversedBy="noticias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     * })
     */
    private $categoria;
    
    
    /**
    * @var ArrayCollection Coleção de imagens
    *
    * @ORM\OneToMany(targetEntity="Admin\Entity\ImagemNoticia", mappedBy="noticia")
    * @ORM\OrderBy({"ordem" = "ASC","id"="ASC"})
    */
    private $imagens;

    public function __construct(array $options = array()) {
        parent::__construct($options);
        $this->imagens = new ArrayCollection();
    }
    
    public function getImagens()
    {
        return $this->imagens;
    }

    public function setImagens(ArrayCollection $imagens)
    {
        foreach ($imagens as $imagem) {
            $imagem->setNoticia($this);
        }
        $this->imagens = $imagens;
        return $this;
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usuarioAprovacaoId
     *
     * @param int $usuarioAprovacaoId
     *
     * @return Noticia
     */
    public function setUsuarioAprovacaoId($usuarioAprovacaoId)
    {
        $this->usuario_aprovacao_id = $usuarioAprovacaoId;

        return $this;
    }

    /**
     * Get usuarioAprovacaoId
     *
     * @return int
     */
    public function getUsuarioAprovacaoId()
    {
        return $this->usuario_aprovacao_id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Noticia
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return Noticia
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set chamada
     *
     * @param string $chamada
     *
     * @return Noticia
     */
    public function setChamada($chamada)
    {
        $this->chamada = $chamada;

        return $this;
    }

    /**
     * Get chamada
     *
     * @return string
     */
    public function getChamada()
    {
        return $this->chamada;
    }

    /**
     * Set tag
     *
     * @param string $tag
     *
     * @return Noticia
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set nivel
     *
     * @param string $nivel
     *
     * @return Noticia
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * Get nivel
     *
     * @return string
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * Set dataPublicacao
     *
     * @param \DateTime $dataPublicacao
     *
     * @return Noticia
     */
    public function setDataPublicacao($dataPublicacao = null)
    {
        $this->data_publicacao = $dataPublicacao;

        return $this;
    }

    /**
     * Get dataPublicacao
     *
     * @return \DateTime
     */
    public function getDataPublicacao()
    {
        return $this->data_publicacao;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Noticia
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set exclusivo
     *
     * @param int $exclusivo
     *
     * @return Noticia
     */
    public function setExclusivo($exclusivo)
    {
        $this->exclusivo = $exclusivo;

        return $this;
    }

    /**
     * Get exclusivo
     *
     * @return int
     */
    public function getExclusivo()
    {
        return $this->exclusivo;
    }
    
    /**
     * Set fonte
     *
     * @param string $fonte
     *
     * @return Noticia
     */
    public function setFonte($fonte)
    {
        $this->fonte = $fonte;

        return $this;
    }

    /**
     * Get fonte
     *
     * @return string
     */
    public function getFonte()
    {
        return $this->fonte;
    }
    
    /**
     * Set facebook_post_id
     *
     * @param string $facebook_post_id
     *
     * @return Noticia
     */
    public function setFacebookPostId($facebook_post_id)
    {
        $this->facebookPostId = $facebook_post_id;

        return $this;
    }

    /**
     * Get facebook_post_id
     *
     * @return string
     */
    public function getFacebookPostId()
    {
        return $this->facebookPostId;
    }
    
    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return Noticia
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set categoria
     *
     * @param \Admin\Entity\Categoria $categoria
     *
     * @return Noticia
     */
    public function setCategoria(\Admin\Entity\Categoria $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \Admin\Entity\Categoria
     */
    public function getCategoria(){
        return $this->categoria;
    }
}
