<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AfiliadoSaldo
 *
 * @ORM\Table(name="afiliado_saldo", indexes={@ORM\Index(name="fk_afiliado_saldo$afiliado$afiliado_id_idx", columns={"afiliado_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Base\Entity\GlobalRepository")
 */
class AfiliadoSaldo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=255, nullable=false)
     */
    private $descricao;

    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valor;

    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=false)
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_modificacao", type="datetime", nullable=false)
     */
    private $dataModificacao = 'CURRENT_TIMESTAMP';

    /**
     * @var \Admin\Entity\Afiliado
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Afiliado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="afiliado_id", referencedColumnName="id")
     * })
     */
    private $afiliado;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return AfiliadoSaldo
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set valor
     *
     * @param string $valor
     *
     * @return AfiliadoSaldo
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return AfiliadoSaldo
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set dataCriacao
     *
     * @param \DateTime $dataCriacao
     *
     * @return AfiliadoSaldo
     */
    public function setDataCriacao($dataCriacao)
    {
        $this->dataCriacao = $dataCriacao;

        return $this;
    }

    /**
     * Get dataCriacao
     *
     * @return \DateTime
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }

    /**
     * Set dataModificacao
     *
     * @param \DateTime $dataModificacao
     *
     * @return AfiliadoSaldo
     */
    public function setDataModificacao($dataModificacao)
    {
        $this->dataModificacao = $dataModificacao;

        return $this;
    }

    /**
     * Get dataModificacao
     *
     * @return \DateTime
     */
    public function getDataModificacao()
    {
        return $this->dataModificacao;
    }

    /**
     * Set afiliado
     *
     * @param \Admin\Entity\Afiliado $afiliado
     *
     * @return AfiliadoSaldo
     */
    public function setAfiliado(\Admin\Entity\Afiliado $afiliado = null)
    {
        $this->afiliado = $afiliado;

        return $this;
    }

    /**
     * Get afiliado
     *
     * @return \Admin\Entity\Afiliado
     */
    public function getAfiliado()
    {
        return $this->afiliado;
    }
}
