<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\AbstractEntity;

/**
 * SaqueAfiliado
 *
 * @ORM\Table(name="saque_afiliado")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Base\Entity\GlobalRepository")
 */
class SaqueAfiliado Extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valor;

    /**
     * @var int
     *
     * @ORM\Column(name="situacao", type="integer", nullable=false)
     */
    private $situacao = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_pagamento", type="date", nullable=true)
     */
    private $dataPagamento;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=255, nullable=true)
     */
    private $descricao;

    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=false)
     */
    private $dataCriacao;

    /**
     * @var \Admin\Entity\Afiliado
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Afiliado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="afiliado_id", referencedColumnName="id")
     * })
     */
    private $afiliado;
    
    
    /**
     * @var \Admin\Entity\AfiliadoExtrato
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\AfiliadoExtrato")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="extrato_id", referencedColumnName="id")
     * })
     */
    private $extrato;
    
    public function __construct(array $options = array()) {
        parent::__construct($options);
        $this->dataCriacao = new \DateTime(date('Y-m-d H:i:s'));
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valor
     *
     * @param string $valor
     *
     * @return SaqueAfiliado
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set situacao
     *
     * @param int $situacao
     *
     * @return SaqueAfiliado
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;

        return $this;
    }

    /**
     * Get situacao
     *
     * @return int
     */
    public function getSituacao()
    {
        return $this->situacao;
    }

    /**
     * Set dataPagamento
     *
     * @param \DateTime $dataPagamento
     *
     * @return SaqueAfiliado
     */
    public function setDataPagamento($dataPagamento)
    {
        $this->dataPagamento = $dataPagamento;

        return $this;
    }

    /**
     * Get dataPagamento
     *
     * @return \DateTime
     */
    public function getDataPagamento()
    {
        return $this->dataPagamento;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return SaqueAfiliado
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return SaqueAfiliado
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set dataCriacao
     *
     * @param \DateTime $dataCriacao
     *
     * @return SaqueAfiliado
     */
    public function setDataCriacao($dataCriacao)
    {
        $this->dataCriacao = $dataCriacao;

        return $this;
    }

    /**
     * Get dataCriacao
     *
     * @return \DateTime
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }
    
    /**
     * Set afiliado
     *
     * @param \Admin\Entity\Afiliado $afiliado
     *
     * @return SaqueAfiliado
     */
    public function setAfiliado(\Admin\Entity\Afiliado $afiliado = null)
    {
        $this->afiliado = $afiliado;

        return $this;
    }

    /**
     * Get afiliado
     *
     * @return \Admin\Entity\Afiliado
     */
    public function getAfiliado()
    {
        return $this->afiliado;
    }
    
    /**
     * Set extrato
     *
     * @param \Admin\Entity\AfiliadoExtrato $extrato
     *
     * @return SaqueAfiliado
     */
    public function setExtrato(\Admin\Entity\AfiliadoExtrato $extrato = null)
    {
        $this->extrato = $extrato;

        return $this;
    }

    /**
     * Get extrato
     *
     * @return \Admin\Entity\AfiliadoExtrato
     */
    public function getExtrato()
    {
        return $this->extrato;
    }
}
