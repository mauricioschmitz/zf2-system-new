<?php

namespace Admin\Entity;

use Base\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Plano
 *
 * @ORM\Table(name="plano", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Base\Entity\GlobalRepository")
 */
class Plano extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valor;

    /**
     * @var int
     *
     * @ORM\Column(name="cota", type="integer", nullable=false)
     */
    private $cota;

    /**
     * @var float
     *
     * @ORM\Column(name="percentual_indicacao", type="float", precision=10, scale=0, nullable=false)
     */
    private $percentualIndicacao;

    /**
     * @var int
     *
     * @ORM\Column(name="pontos", type="integer", nullable=false)
     */
    private $pontos;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_saque", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorSaque;
    
    /**
     * @var string
     *
     * @ORM\Column(name="hexadecimal", type="string", length=6, nullable=false)
     */
    private $hexadecimal;
    
    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Plano
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set valor
     *
     * @param string $valor
     *
     * @return Plano
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set cota
     *
     * @param int $cota
     *
     * @return Plano
     */
    public function setCota($cota)
    {
        $this->cota = $cota;

        return $this;
    }

    /**
     * Get cota
     *
     * @return int
     */
    public function getCota()
    {
        return $this->cota;
    }

    /**
     * Set percentualIndicacao
     *
     * @param float $percentualIndicacao
     *
     * @return Plano
     */
    public function setPercentualIndicacao($percentualIndicacao)
    {
        $this->percentualIndicacao = $percentualIndicacao;

        return $this;
    }

    /**
     * Get percentualIndicacao
     *
     * @return float
     */
    public function getPercentualIndicacao()
    {
        return $this->percentualIndicacao;
    }

    /**
     * Set pontos
     *
     * @param int $pontos
     *
     * @return Plano
     */
    public function setPontos($pontos)
    {
        $this->pontos = $pontos;

        return $this;
    }

    /**
     * Get pontos
     *
     * @return int
     */
    public function getPontos()
    {
        return $this->pontos;
    }

    /**
     * Set valorSaque
     *
     * @param string $valorSaque
     *
     * @return Plano
     */
    public function setValorSaque($valorSaque)
    {
        $this->valorSaque = $valorSaque;

        return $this;
    }

    /**
     * Get valorSaque
     *
     * @return string
     */
    public function getValorSaque()
    {
        return $this->valorSaque;
    }
    
    /**
     * Set hexadecimal
     *
     * @param string $hexadecimal
     *
     * @return Plano
     */
    public function setHexadecimal($hexadecimal)
    {
        $this->hexadecimal = $hexadecimal;

        return $this;
    }

    /**
     * Get hexadecimal
     *
     * @return string
     */
    public function getHexadecimal()
    {
        return $this->hexadecimal;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return Plano
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }
}
