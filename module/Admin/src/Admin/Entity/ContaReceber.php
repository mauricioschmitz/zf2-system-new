<?php

namespace Admin\Entity;

use Base\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * ContaReceber
 *
 * @ORM\Table(name="conta_receber", indexes={@ORM\Index(name="fk_conta_receber$conta$conta_id_idx", columns={"conta_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Admin\Entity\ContaReceberRepository")
 */
class ContaReceber extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="date", nullable=false)
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_previsto", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorPrevisto;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_recebido", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $valorRecebido;

    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \Admin\Entity\Conta
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Conta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="conta_id", referencedColumnName="id")
     * })
     */
    private $conta;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return ContaReceber
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return ContaReceber
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set valorPrevisto
     *
     * @param string $valorPrevisto
     *
     * @return ContaReceber
     */
    public function setValorPrevisto($valorPrevisto)
    {
        $this->valorPrevisto = $valorPrevisto;

        return $this;
    }

    /**
     * Get valorPrevisto
     *
     * @return string
     */
    public function getValorPrevisto()
    {
        return $this->valorPrevisto;
    }

    /**
     * Set valorRecebido
     *
     * @param string $valorRecebido
     *
     * @return ContaReceber
     */
    public function setValorRecebido($valorRecebido)
    {
        $this->valorRecebido = $valorRecebido;

        return $this;
    }

    /**
     * Get valorRecebido
     *
     * @return string
     */
    public function getValorRecebido()
    {
        return $this->valorRecebido;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return ContaReceber
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set conta
     *
     * @param \Admin\Entity\Conta $conta
     *
     * @return ContaReceber
     */
    public function setConta(\Admin\Entity\Conta $conta = null)
    {
        $this->conta = $conta;

        return $this;
    }

    /**
     * Get conta
     *
     * @return \Admin\Entity\Conta
     */
    public function getConta()
    {
        return $this->conta;
    }
}
