<?php

namespace Admin\Entity;

use Base\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Rastreamento
 *
 * @ORM\Table(name="rastreamento")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Admin\Entity\RastreamentoRepository")
 */
class Rastreamento extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=255, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="origem", type="text", length=65535, nullable=true)
     */
    private $origem;

    /**
     * @var string
     *
     * @ORM\Column(name="destino", type="text", length=65535, nullable=true)
     */
    private $destino;

    /**
     * @var string
     *
     * @ORM\Column(name="posicao", type="string", length=255, nullable=true)
     */
    private $posicao;

    /**
     * @var string
     *
     * @ORM\Column(name="situacao", type="string", length=255, nullable=false)
     */
    private $situacao;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=255, nullable=true)
     */
    private $descricao;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="json", type="text", nullable=true)
     */
    private $json;

    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var ArrayCollection Coleção de ocorrencia
     *
     * @ORM\OneToMany(targetEntity="Admin\Entity\RastreamentoOcorrencia", mappedBy="rastreamento")
     * @ORM\OrderBy({"data" = "ASC"})
     */
    private $ocorrencias;

    public function __construct(array $options = array()) {
        parent::__construct($options);
        $this->ocorrencias = new ArrayCollection();
    }

    public function getOcorrencias()
    {
        return $this->ocorrencias;
    }

    public function setOcorrencias(ArrayCollection $ocorrencias)
    {
        foreach ($ocorrencias as $ocorrencia) {
            $ocorrencia->setRastreamento($this);
        }
        $this->ocorrencias = $ocorrencias;
        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Rastreamento
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set origem
     *
     * @param string $origem
     *
     * @return Rastreamento
     */
    public function setOrigem($origem)
    {
        $this->origem = $origem;

        return $this;
    }

    /**
     * Get origem
     *
     * @return string
     */
    public function getOrigem()
    {
        return $this->origem;
    }

    /**
     * Set destino
     *
     * @param string $destino
     *
     * @return Rastreamento
     */
    public function setDestino($destino)
    {
        $this->destino = $destino;

        return $this;
    }

    /**
     * Get destino
     *
     * @return string
     */
    public function getDestino()
    {
        return $this->destino;
    }

    /**
     * Set posicao
     *
     * @param string $posicao
     *
     * @return Rastreamento
     */
    public function setPosicao($posicao)
    {
        $this->posicao = $posicao;

        return $this;
    }

    /**
     * Get posicao
     *
     * @return string
     */
    public function getPosicao()
    {
        return $this->posicao;
    }

    /**
     * Set situacao
     *
     * @param string $situacao
     *
     * @return Rastreamento
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;

        return $this;
    }

    /**
     * Get situacao
     *
     * @return string
     */
    public function getSituacao()
    {
        return $this->situacao;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return Rastreamento
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Rastreamento
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set json
     *
     * @param string $json
     *
     * @return Rastreamento
     */
    public function setJson($json)
    {
        $this->json = $json;

        return $this;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return Rastreamento
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get json
     *
     * @return string
     */
    public function getJson()
    {
        return $this->json;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }


    /**
     * Add ocorrencia
     *
     * @param \Admin\Entity\RastreamentoOcorrencia $ocorrencia
     *
     * @return Rastreamento
     */
    public function addOcorrencia(\Admin\Entity\RastreamentoOcorrencia $ocorrencia)
    {
        $this->ocorrencias[] = $ocorrencia;

        return $this;
    }

    /**
     * Remove ocorrencia
     *
     * @param \Admin\Entity\RastreamentoOcorrencia $ocorrencia
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeOcorrencia(\Admin\Entity\RastreamentoOcorrencia $ocorrencia)
    {
        return $this->ocorrencias->removeElement($ocorrencia);
    }
}
