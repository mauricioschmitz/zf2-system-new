<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use NeuroSYS\DoctrineDatatables\TableBuilder;
use NeuroSYS\DoctrineDatatables\Renderer\TwigRenderer;

class PessoajuridicaController extends AbstractController
{
    public function __construct() {
        $this->form = 'Admin\Form\PessoaJuridicaForm';
        $this->controller = 'pessoajuridica';
        $this->route = 'adm/default';
        $this->service = 'Admin\Service\PessoaJuridicaService';
        $this->entity = 'Admin\Entity\PessoaJuridica';
        $this->tituloTela = 'Pessoa Jurídica';
        $this->group_elements = array(
            'field' =>  'fones',
            'fields' => array(
                'telefone',
                'celular'
            )
        );
        $this->scripts_styles = array(
            array('file'=>'/libs/maskedInput/jquery.maskedinput.js', 'type'=>'headscript', 'function'=>'appendFile'),
            array('content'=>'Scripts.InitMasks();', 'type'=>'headscript', 'function'=>'appendScript'),
        );
    }
    
    public function indexAction()
    {
        $this->layout()->tituloTela = $this->tituloTela . ' | Lista';
        
        $this->getServiceLocator()->get('viewhelpermanager')->get('headScript')->appendFile($this->getRequest()->getBasePath().'/min?g=datatables-js');
        
        return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller));
    }
    
    public function listAction()
    {
        $loader   = new \Twig_Loader_String();
        $renderer = new TwigRenderer(new \Twig_Environment($loader));
        $params = $this->params()->fromQuery();
       
        $builder = new TableBuilder($this->getEm($this->entity), $params, null, $renderer);
        $builder
            ->from('Admin\Entity\PessoaJuridica', 'p')
            ->add('number', 'p.id', null, array(
                    'template' => '<div class="dataTablesMore">+</div>'
                )
            )
            ->add('text', 'p.nomeFantasia')
            ->add('text', 'p.razaoSocial')
            ->add('text', 'p.cnpj')
            ->add("text", "p.ativo", null, array(
                    'template'=> '{% if value %}<span class="label label-success" title="Ativo">Ativo</span>{% else %}<span class="label label-danger" title="Inativo">Inativo</span>{% endif %}'
                )
            )
            ->add("text", "p.ativo", null, array(
                'template' => ''
                . '<div class="dropdown">
                    <button class="btn btn-icon btn-rounded btn-primary waves-effect dropdown-toggle" type="button" id="dropdownMenu{{values.id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-cog"></i></button>                   
                    <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu{{values.id}}">
                        <li><a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'edit')) . '/{{values.id}}"> <i class="fa fa-pencil"></i> Editar</a></li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'activeset')) . '/{{values.id}}/{% if value %}0{% else %}1{% endif %}"> <i class="fa fa-{% if value %}remove{% else %}check{% endif %}"></i> {% if value %}Desativar{% else %}Ativar{% endif %}</a>
                        </li>
                    </ul>
                  </div>'
            ))
            ->end()
            ;
         
        $response = $builder->getTable()
            ->getResponseArray() // hydrate entity, defaults to array
            ;
        
        $result = new JsonModel($response);
        return $result;
    }
    
    public function validinsertAction()
    {
        $em = $this->getEm();
        $pessoa = $em->getRepository('Admin\Entity\PessoaJuridica')->findBy(array('cnpj'=>$this->params()->fromPost('cnpj', '')));
        if(!empty($pessoa)){
            $this->flashMessenger()->addErrorMessage('CNPJ Já cadastrado!');  
            $request = $this->getRequest();
            $request->getPost()->offsetUnset('cnpj');
        }
        return $this->forward()
            ->dispatch('Admin\Controller\Pessoajuridica', array('action'=>'insert'));
    }
    
    public function valideditAction()
    {
        $em = $this->getEm();
        $pessoas = $em->getRepository('Admin\Entity\PessoaJuridica')->findBy(array('cnpj'=>$this->params()->fromPost('cnpj')));
        
        if(!empty($pessoas)){
            foreach($pessoas as $pessoa){
                if($pessoa->getId() != $this->params()->fromRoute('id')){
                    $this->flashMessenger()->addErrorMessage('CNPJ Já cadastrado!');  
                    $request = $this->getRequest();
                    if($this->params()->fromPost('cnpj'))
                        $request->getPost()->offsetUnset('cnpj');
                }
            }
        }
        return $this->forward()
            ->dispatch('Admin\Controller\Pessoajuridica', array('action'=>'edit', 'id'=>$this->params()->fromRoute('id')));
    }
    
    
    public function aditionalParameters(){
        $retorno = array();
        $id = $this->params()->fromRoute('id', 0);
        if($id){
            $em = $this->getEm();
            $pessoaJuridica = $em->getRepository('Admin\Entity\PessoaJuridica')->findOneBy(array('id'=>$id));
            $retorno['pessoajuridica'] = $pessoaJuridica;
        }
        return $retorno;
    }
}
