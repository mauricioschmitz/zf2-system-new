<?php

namespace Admin\Controller;

use Admin\Controller\ImportXLSController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use NeuroSYS\DoctrineDatatables\TableBuilder;
use NeuroSYS\DoctrineDatatables\Renderer\TwigRenderer;

class ContapagarController extends ImportXLSController
{
    public function __construct() {
        $this->form = 'Admin\Form\ContaPagarForm';
        $this->controller = 'contapagar';
        $this->route = 'adm/default';
        $this->service = 'Admin\Service\ContaPagarService';
        $this->entity = 'Admin\Entity\ContaPagar';
        $this->external_objects  = array('conta'=>'Admin\Entity\Conta', 'categoria'=>'Admin\Entity\CategoriaConta');
        $this->data_elements = array('data');
        $this->tituloTela = 'Conta a Pagar';
        
        $this->parentEntity = 'Admin\Entity\Conta';
        $this->parentField = 'conta';
    }
    
    public function indexAction()
    {
        $this->layout()->tituloTela = $this->tituloTela . ' | Lista';
        $this->getServiceLocator()->get('viewhelpermanager')->get('headScript')->appendFile($this->getRequest()->getBasePath().'/min?g=datatables-js');
        
        $contas = $this->getEm()->getRepository('Admin\Entity\Conta')->getAtivos(array('data'=>'DESC'));
        
        if($id = $this->params()->fromQuery('conta')){
            $conta = $this->getEm()->getRepository('Admin\Entity\Conta')->find($id);
            $lista = $this->getEm()->getRepository('Admin\Entity\ContaPagar')->findBy(array('conta'=>$conta));
        }else{
            $lista = $this->getEm()->getRepository('Admin\Entity\ContaPagar')->findAll();
        }
        
        //Cálculos
        $totaisPagar = $this->getEm()->getRepository('Admin\Entity\ContaPagar')->getSum(isset($conta) ? $conta : null);
        $totaisReceber = $this->getEm()->getRepository('Admin\Entity\ContaReceber')->getSum(isset($conta) ? $conta : null);
        return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller, 'contas'=>$contas, 'lista'=>$lista, 'conta'=>isset($conta) ? $conta : null, 'totaisPago'=>$totaisPagar, 'totaisRecebido'=>$totaisReceber));
    }
    
    public function listAction()
    {
        $loader   = new \Twig_Loader_String();
        $renderer = new TwigRenderer(new \Twig_Environment($loader));
        $params = $this->params()->fromQuery();
        
        $filtros = array();
        if($conta = $this->params()->fromQuery('conta')){
            $filtros = array('conta'=>$this->getEm()->getRepository('Admin\Entity\Conta')->find($conta));
        }
        
        $builder = new TableBuilder($this->getEm($this->entity), $params, null, $renderer, $filtros);
        
        $builder
            ->from('Admin\Entity\ContaPagar', 'cp')
            ->join('cp.conta', 'c')
            ->join('cp.categoria', 'cat')
            ->add('number', 'cp.id', null, array(
                    'template' => '<div class="dataTablesMore">+</div>'
                )
            )
            ->add('text', 'c.titulo') 
            ->add('text', 'cat.titulo')
            ->add('text', 'cp.parcelaAtual, cp.qtdParcelas', null, array(
                    'template'=> '{{values.parcelaAtual}} / {{values.qtdParcelas}}'
                )) 
            ->add('text', 'cp.titulo') 
            ->add('date', 'cp.data', 'cp.data', array(
                    'template' => '{% if value %}{{ values.data|date("d/m/Y") }}{% endif %}'
                )
            )
            ->add('text', 'cp.valorPrevisto')  
            ->add('text', 'cp.valorPago')  
            ->add("text", "cp.ativo", null, array(
                    'template'=> '{% if value %}<span class="label label-success" title="Ativo">Ativo</span>{% else %}<span class="label label-danger" title="Inativo">Inativo</span>{% endif %}'
                )
            )
            ->add("text", "cp.ativo", null, array(
                'template' => ''
                . '<div class="dropdown">
                    <button class="btn btn-icon btn-rounded btn-primary waves-effect dropdown-toggle" type="button" id="dropdownMenu{{values.id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-cog"></i></button>                   
                    <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu{{values.id}}">
                        <li><a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'edit')) . '/{{values.id}}"> <i class="fa fa-pencil"></i> Editar</a></li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'activeset')) . '/{{values.id}}/{% if value %}0{% else %}1{% endif %}"> <i class="fa fa-{% if value %}remove{% else %}check{% endif %}"></i> {% if value %}Desativar{% else %}Ativar{% endif %}</a>
                        </li>
                    </ul>
                  </div>'
            ))
            ->end()
            ;
         
        $response = $builder->getTable()
            ->getResponseArray() // hydrate entity, defaults to array
            ;
        
        $result = new JsonModel($response);
        return $result;
    }
    
    public function aditionalParameters(){
        $retorno = array();
        $id = $this->params()->fromRoute('id', 0);
        if($id){
            $em = $this->getEm();
            $object = $em->getRepository('Admin\Entity\ContaPagar')->findOneBy(array('id'=>$id));
            $retorno['contaPagar'] = $object;
        }
        return $retorno;
    }
}
