<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use NeuroSYS\DoctrineDatatables\TableBuilder;
use NeuroSYS\DoctrineDatatables\Renderer\TwigRenderer;
use Admin\Entity\AfiliadoExtrato;

class AfiliadodistribuicaoController extends AbstractController
{
    public function __construct() {
        $this->controller = 'afiliadodistribuicao';
        $this->route = 'adm/default';
        $this->entity = 'Admin\Entity\AfiliadoDistribuicao';
        $this->service = 'Admin\Service\AfiliadoDistribuicao';
        $this->tituloTela = 'Pagamentos Realizados';
    }
    
    public function indexAction()
    {
        $em = $this->getEm();
        $this->layout()->tituloTela = $this->tituloTela . ' | Lista';
        
        $this->getServiceLocator()->get('viewhelpermanager')->get('headScript')->appendFile($this->getRequest()->getBasePath().'/min?g=datatables-js');
        
        return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller));
        
    }
    
    public function listAction()
    {
        $loader   = new \Twig_Loader_String();
        $renderer = new TwigRenderer(new \Twig_Environment($loader));
        $params = $this->params()->fromQuery();
        
        $builder = new TableBuilder($this->getEm($this->entity), $params, null, $renderer);
        
        $builder
            ->from('Admin\Entity\AfiliadoDistribuicao', 'd')
            ->add('number', 'd.id', null, array(
                    'template' => '<div class="dataTablesMore">+</div>'
                )
            )
            ->add('date', 'd.dataPagamento', 'd.dataPagamento', array(
                    'template' => '{% if values.dataPagamento %}{{ values.dataPagamento|date("d/m/Y") }}{% endif %}'
                )
            )
            ->add('text', 'd.descricao')
            ->add('text', 'd.valorArrecadado', null, array(
                    'template' => '{{ value|number_format(2, ",", ".") }}'
                )
            )
            ->add('text', 'd.valorCotas', null, array(
                    'template' => '{{ value|number_format(2, ",", ".") }}'
                )
            )
            ->add('text', 'd.valorComissao', null, array(
                    'template' => '{{ value|number_format(2, ",", ".") }}'
                )
            )
            ->add('text', 'd.valorSobra', null, array(
                    'template' => '{{ value|number_format(2, ",", ".") }}'
                )
            )
            ->end();
         
        $response = $builder->getTable()
            ->getResponseArray() // hydrate entity, defaults to array
            ;
        
        $result = new JsonModel($response);
        return $result;
        
    }
    
    public function aditionalParameters(){
        
    }
}
