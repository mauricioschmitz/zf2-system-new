<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;

class RenovacaoController extends AbstractController
{
    public function __construct() {
        $this->controller = 'renovacao';
        $this->form = 'Admin\Form\UpgradeForm';
        $this->route = 'adm/default';
        $this->entity = 'Admin\Entity\Afiliado';
        $this->service = 'Admin\Service\AfiliadoService';
        $this->tituloTela = 'Renovação';
    }
    
    public function indexAction()
    {
        $em = $this->getEm();
        $this->layout()->tituloTela = $this->tituloTela . ' | Lista';
        
        if(is_string($this->form))
            $form = new $this->form($em);
        else 
            $form = $this->form($em);
        
        $request = $this->getRequest();
        if($request->isPost()){
            $form->setData($request->getPost());
            
            if($form->isValid()){
                $plano = $em->getRepository('Admin\Entity\Plano')->find($this->params()->fromPost('plano_id'));
                $afiliado = $this->layout()->afiliado;
                $data['id'] = $afiliado->getId();
                $data['nome_plano'] = $plano->getTitulo();
                $data['valor_investimento'] = $plano->getValor();
                $data['cota'] = $plano->getCota();
                $data['percentual_indicacao'] = $plano->getPercentualIndicacao();
                $data['pontos'] = $plano->getPontos();
                $data['valor_saque'] = $plano->getValorSaque();
                $data['valor_sacado'] = 0;
                $data['inicio_vigencia'] = new \DateTime('0000-00-00');
                $data['fim_vigencia'] = new \DateTime('0000-00-00');
                $data['pago'] = 0;
                $data['ativo'] = 1;
                $service = $this->getServiceLocator()->get($this->service);
                if($service->save($data)){
                    
                    $config = $this->getServiceLocator()->get('config');
                    $config = $config['sendemail'];
                    $mail = $this->getServiceLocator()->get('Email');
                    $mail->sendMail($config['fromMail'], $config['fromName'], $afiliado->getUser()->getEmail(), $afiliado->getUser()->getName(), false, false, \Base\Util\Mail::$SOLICITARRENOVACAO['assunto'],  array('mensagem'=>\Base\Util\Mail::$SOLICITARRENOVACAO['mensagem'], 'variaveis'=>array('login'=>$afiliado->getUser()->getEmail(), 'link'=>$this->getEvent()->getRouter()->assemble(array(''), array('name'=>'adm', 'force_canonical'=>true)))), false, $em);
            
                    $this->flashMessenger()->addSuccessMessage('Solicitação enviada com sucesso!');
                    return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller, 'action'=>'informacoes', 'id'=>$afiliado->getId()));
                } else {
                    $this->flashMessenger()->addErrorMessage('Ocorreu um erro, tente novamente!');  
                }
            }
        }else{
            $em = $this->getEm();
            $planos = $em->getRepository('Admin\Entity\Plano')->findBy(array('ativo'=>1), array('pontos'=>'ASC'));

            return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller, 'form'=>$form, 'params'=>array('planos'=>$planos, 'planosselect'=>true)));
            
        }
    }
    
    public function informacoesAction(){
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $this->layout()->tituloTela = $this->tituloTela;
        $afiliado = $this->layout()->afiliado;
        
        $saldo = $em->getRepository('Admin\Entity\AfiliadoExtrato')->saldo($this->layout()->afiliado);
        
        $texto = $em->getRepository('Admin\Entity\Texto')->findOneBy(array('apelido'=>'contas_deposito'));
        return new ViewModel(array('afiliado'=>$afiliado, 'texto' => $texto, 'valor'=>$afiliado->getValorInvestimento()));
    }
    
    public function aditionalParameters(){
        
    }
}
