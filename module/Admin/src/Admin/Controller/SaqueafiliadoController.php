<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use NeuroSYS\DoctrineDatatables\TableBuilder;
use NeuroSYS\DoctrineDatatables\Renderer\TwigRenderer;
use Admin\Entity\AfiliadoExtrato;

class SaqueafiliadoController extends AbstractController
{
    public function __construct() {
        $this->form = 'Admin\Form\SaqueAfiliadoForm';
        $this->controller = 'saqueafiliado';
        $this->route = 'adm/default';
        $this->entity = 'Admin\Entity\SaqueAfiliado';
        $this->service = 'Admin\Service\SaqueAfiliadoService';
        $this->tituloTela = 'Saque';
        $this->default_external_objects = array('afiliado');
        $this->data_elements = array('data_pagamento');
        $this->success_insert_message = 'Saque solicitado com sucesso!';
        $this->error_insert_message = 'Ocorreu um erro na sua solicitação, tente novamento!';
        $this->invalid_insert_message = 'Ocorreu um erro na sua solicitação, tente novamento!';
        $this->aditional_insert_method = 'afterInsert';
        $this->aditional_edit_method = 'afterEdit';
    }
    
    public function indexAction()
    {
        $em = $this->getEm();
        $this->layout()->tituloTela = $this->tituloTela . ' | Lista';
        
        $this->getServiceLocator()->get('viewhelpermanager')->get('headScript')->appendFile($this->getRequest()->getBasePath().'/min?g=datatables-js');
        
        if(isset($this->layout()->afiliado)){
            $config = $em->getRepository('Admin\Entity\AfiliadoConfig')->findOneBy(array('ativo' => 1));
            
            $valorMaximo = ($this->layout()->afiliado->getValorInvestimento() * ($config->getPercentualRetorno() / 100));
            $valorSacado = $this->layout()->afiliado->getValorSacado();
            
            $saldo = $em->getRepository('Admin\Entity\AfiliadoExtrato')->saldo($this->layout()->afiliado);
            $valorMinimo = $this->layout()->afiliado->getValorSaque();
            
            
            
            return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller, 'saldo'=>$saldo, 'valorMinimo'=>$valorMinimo, 'valorMaximo'=>$valorMaximo, 'valorSacado'=>$valorSacado));
        }
        return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller));
        
        
    }
    
    public function listAction()
    {
        $loader   = new \Twig_Loader_String();
        $renderer = new TwigRenderer(new \Twig_Environment($loader));
        $params = $this->params()->fromQuery();
        
        if(isset($this->layout()->afiliado))
            $builder = new TableBuilder($this->getEm($this->entity), $params, null, $renderer, array('afiliado'=>$this->layout()->afiliado));
        else
            $builder = new TableBuilder($this->getEm($this->entity), $params, null, $renderer);
        
        $builder
            ->from('Admin\Entity\SaqueAfiliado', 's')
            ->join('s.afiliado', 'a')
            ->add('number', 's.id', null, array(
                    'template' => '<div class="dataTablesMore">+</div>'
                )
            );
        
        if(!isset($this->layout()->afiliado)){
            $builder->add('text', 'a.pessoaFisica', null, array(
                    'template'=> '{{ value.getNome() }}'
                ));
        }
        $builder
            ->add('date', 's.dataCriacao', 's.dataCriacao', array(
                    'template' => '{% if values.dataCriacao %}{{ values.dataCriacao|date("d/m/Y") }}{% endif %}'
                )
            )
            ->add('date', 's.dataPagamento', 's.dataPagamento', array(
                    'template' => '{% if values.dataPagamento %}{{ values.dataPagamento|date("d/m/Y") }}{% endif %}'
                )
            )
            ->add('text', 's.descricao')
            ->add('text', 's.valor', null, array(
                    'template' => '{{ value|number_format(2, ",", ".") }}'
                )
            )
            ->add("text", "s.situacao", null, array(
                    'template'=> '{% if value == 1 %}<span class="label label-success" title="Ativo">Depositado</span>{% elseif value == 0 %}<span class="label label-warning" title="Inativo">Aguardando depósito</span>{% elseif value == 2 %}<span class="label label-danger" title="Inativo">Recusado</span>{% endif %}'
                )
            )
            ;
        
        if(!isset($this->layout()->afiliado))
            $builder
            ->add("text", "s.situacao", null, array(
                'template' => ''
                . '{% if value != 1 %}
                    <div class="dropdown">
                    <button class="btn btn-icon btn-rounded btn-primary waves-effect dropdown-toggle" type="button" id="dropdownMenu{{values.id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-cog"></i></button>                   
                    <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu{{values.id}}">
                        <li><a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'edit')) . '/{{values.id}}"> <i class="fa fa-pencil"></i> Confirmar depósito</a></li>
                    </ul>
                  </div>
                  {% endif %}'
            ));
        
        $builder
            ->end();
         
        $response = $builder->getTable()
            ->getResponseArray(\NeuroSYS\DoctrineDatatables\Table::HYDRATE_ENTITY) // hydrate entity, defaults to array
            ;
        
        $result = new JsonModel($response);
        return $result;
        
    }
    
    public function afterInsert($data){
        $extrato = new AfiliadoExtrato(
            array(
                'afiliado' => $data['afiliado'],
                'tipo' => AfiliadoExtrato::$_tipo['saque'],
                'valor' => $data['valor'],
                'situacao' => 1,
                'descricao' => AfiliadoExtrato::$_descricao['saque'],
                'data_pagamento' => new \DateTime(date('Y-m-d'))
            )
        );
        $em = $this->getEm();
        $em->persist($extrato);
        $em->flush();
        
        $data['afiliado']->setValorSacado($data['afiliado']->getValorSacado()+$data['valor']);
        $em->persist($data['afiliado']);
        $em->flush();
                
        $data['entity']->setExtrato($extrato);
        $em->persist($data['entity']);
        $em->flush();
        
        $config = $this->getServiceLocator()->get('config');
        $config = $config['sendemail'];
        $mail = $this->getServiceLocator()->get('Email');
        $mail->sendMail($config['fromMail'], $config['fromName'], $data['afiliado']->getUser()->getEmail(), $data['afiliado']->getUser()->getName(), false, false, \Base\Util\Mail::$SOLICITARSAQUE['assunto'],  array('mensagem'=>\Base\Util\Mail::$SOLICITARSAQUE['mensagem'], 'variaveis'=>array('login'=>$data['afiliado']->getUser()->getEmail(), 'link'=>$this->getEvent()->getRouter()->assemble(array(''), array('name'=>'adm', 'force_canonical'=>true)))), false, $em);

    }

    public function afterEdit($data){
        $extrato = $data['entity']->getExtrato();
        $extrato->setSituacao(AfiliadoExtrato::$_situacao['paga']);
        $em = $this->getEm();
        $em->persist($extrato);
        $em->flush();
        
        $config = $this->getServiceLocator()->get('config');
        $config = $config['sendemail'];
        $mail = $this->getServiceLocator()->get('Email');
        $mail->sendMail($config['fromMail'], $config['fromName'], $data['afiliado']->getUser()->getEmail(), $data['afiliado']->getUser()->getName(), false, false, \Base\Util\Mail::$APROVARSAQUE['assunto'],  array('mensagem'=>\Base\Util\Mail::$APROVARSAQUE['mensagem'], 'variaveis'=>array('login'=>$data['afiliado']->getUser()->getEmail(), 'link'=>$this->getEvent()->getRouter()->assemble(array(''), array('name'=>'adm', 'force_canonical'=>true)))), false, $em);

    }

    public function aditionalParameters(){
        $id = $this->params()->fromRoute('id', 0);
        $em = $this->getEm();
        $config = $em->getRepository('Admin\Entity\AfiliadoConfig')->findOneBy(array('ativo' => 1));
        if(isset($this->layout()->afiliado)){
            $saldo = $em->getRepository('Admin\Entity\AfiliadoExtrato')->saldo($this->layout()->afiliado);
            $valorMinimo = $this->layout()->afiliado->getValorSaque();
            $afiliado = $this->layout()->afiliado;
            
            $valorMaximo = ($this->layout()->afiliado->getValorInvestimento() * ($config->getPercentualRetorno() / 100));
            $valorSacado = $this->layout()->afiliado->getValorSacado();
            
            return array('saldo'=>$saldo, 'valorMinimo'=>$valorMinimo, 'valorMaximo'=>$valorMaximo, 'valorSacado'=>$valorSacado, 'afiliado'=>$afiliado);
            
        }elseif($id){
            $saque = $em->getRepository('Admin\Entity\SaqueAfiliado')->find($id);
            $afiliado = $saque->getAfiliado();
            $saldo = $em->getRepository('Admin\Entity\AfiliadoExtrato')->saldo($afiliado);
            $valorMinimo = $afiliado->getValorSaque();
            
            $valorMaximo = ($afiliado->getValorInvestimento() * ($config->getPercentualRetorno() / 100));
            $valorSacado = $afiliado->getValorSacado();
            
            return array('saldo'=>$saldo, 'valorMinimo'=>$valorMinimo, 'valorMaximo'=>$valorMaximo, 'valorSacado'=>$valorSacado, 'afiliado'=>$afiliado);
        }
    }
}
