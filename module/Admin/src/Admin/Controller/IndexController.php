<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractController
{
    public function __construct() {
        $this->form = 'Admin\Form\Admin';
        $this->controller = 'Admin';
        $this->route = 'adm/default';
        $this->service = 'Admin\Service\AdminService';
        $this->entity = 'Admin\Entity\Admin';
        
        
    }
    public function indexAction()
    {
        $this->layout()->tituloTela = 'Dashboard';
        
        foreach($this->layout()->userinfo->getRoles() as $role){
            if($role->getRoleName() == 'Affiliate'){
                $em = $this->getEm();
                $afiliado = $em->getRepository('Admin\Entity\Afiliado')->findOneBy(array('user'=> $this->layout()->userinfo));
                if($afiliado->getPago() == 0){
                    return $this->redirect()->toRoute($this->route, array('controller'=>'confirmarpagamento', 'action'=>'index'));
                    
                }elseif($afiliado->getPago() == 2){
                    return new ViewModel(array('pagamento'=>'aguardando'));
                }
            }
        }
        
        return new ViewModel();
    }

    public function aditionalParameters(){
        return array();
    }
}
