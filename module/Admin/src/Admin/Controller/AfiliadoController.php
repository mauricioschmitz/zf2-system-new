<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use NeuroSYS\DoctrineDatatables\TableBuilder;
use NeuroSYS\DoctrineDatatables\Renderer\TwigRenderer;
use Admin\Entity\PlanoAfiliado;
use Admin\Entity\AfiliadoExtrato;
use Base\Util\Util;
use Admin\Entity\AfiliadoPagamento;

class AfiliadoController extends AbstractController {

    public function __construct() {
        $this->form = 'Affiliate\Form\AfiliadoPessoaFisicaForm';
        $this->controller = 'afiliado';
        $this->route = 'adm/default';
        $this->entity = 'Admin\Entity\Afiliado';
        $this->service = 'Affiliate\Service\AfiliadoService';
        $this->tituloTela = 'Afiliados';
    }

    public function indexAction() {
        $this->layout()->tituloTela = $this->tituloTela . ' | Lista';
        
        $this->getServiceLocator()->get('viewhelpermanager')->get('headScript')->appendFile($this->getRequest()->getBasePath() . '/min?g=datatables-js');
        return new ViewModel(array('route' => $this->route, 'controller' => $this->controller));
    }

    public function listAction() {
        $loader = new \Twig_Loader_String();
        $renderer = new TwigRenderer(new \Twig_Environment($loader));
        $params = $this->params()->fromQuery();

        $builder = new TableBuilder($this->getEm($this->entity), $params, null, $renderer);
        $builder
                ->from('Admin\Entity\Afiliado', 'a')
                ->join('a.pessoaFisica', 'pf')
                ->add('number', 'a.id', null, array(
                    'template' => '<div class="dataTablesMore">+</div>'
                        )
                )
                ->add('text', 'pf.nome')
                ->add("text", "a.id", "a.id", array(
                    'function' => '$saldo = $this->getTable()->getManager()->getRepository(\'Admin\Entity\AfiliadoExtrato\')->saldo($value); return \'<div>\'.number_format($saldo,2,\',\',\'.\').\'</div>\';'
                        )
                )
                ->add("text", "a.pago", null, array(
                    'template' => '{% if value == 0 %}<span class="label label-info" title="Aguardando confirmação">Aguardando confirmação</span>{% elseif value == 2 %}<span class="label label-warning" title="Aguardando aprovação">Aguardando aprovação</span>{% else %}<span class="label label-success" title="Pago">Pago</span>{% endif %}'
                        )
                )
                ->add("text", "a.ativo", null, array(
                    'template' => '{% if value %}<span class="label label-success" title="Ativo">Ativo</span>{% else %}<span class="label label-danger" title="Inativo">Inativo</span>{% endif %}'
                        )
                )
                ->add("text", "a.ativo", null, array(
                    'template' => ''
                    . '<div class="dropdown">
                    <button class="btn btn-icon btn-rounded btn-primary waves-effect dropdown-toggle" type="button" id="dropdownMenu{{values.id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-cog"></i></button>                   
                    <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu{{values.id}}">
                        <!--<li><a href="' . $this->url()->fromRoute($this->route, array('controller' => $this->controller, 'action' => 'edit')) . '/{{values.id}}"> <i class="fa fa-pencil"></i> Editar</a></li>-->
                        {% if values.pago == 0 or values.pago == 2 %}<li><a href="' . $this->url()->fromRoute($this->route, array('controller' => $this->controller)) . '/pagoset/{{values.id}}/1"> <i class="fa fa-check"></i> Aprovar pagamento</a></li>{% endif %}
                        <li><a href="' . $this->url()->fromRoute($this->route, array('controller' => $this->controller, 'action' => 'details')) . '/{{values.id}}"> <i class="fa fa-search"></i> Detalhes</a></li>
                        <li>
                            <a href="' . $this->url()->fromRoute($this->route, array('controller' => $this->controller, 'action' => 'activeset')) . '/{{values.id}}/{% if value %}0{% else %}1{% endif %}"> <i class="fa fa-{% if value %}remove{% else %}check{% endif %}"></i> {% if value %}Desativar{% else %}Ativar{% endif %}</a>
                        </li>
                    </ul>
                  </div>'
                ))
                ->end()
        ;


        $response = $builder->getTable()
                ->getResponseArray() // hydrate entity, defaults to array
        ;
        $result = new JsonModel($response);
        return $result;
    }
    
    public function detailsAction() {
        $id = $this->params()->fromRoute('id', 0);
        $em = $this->getEm();
        $afiliado = $em->getRepository('Admin\Entity\Afiliado')->find($id);
        
        $pessoaFisica = $afiliado->getPessoaFisica();
        
        $plano = $em->getRepository('Admin\Entity\Plano')->findOneBy(array('titulo'=>$afiliado->getNomePlano()));
        //Mapa
        $address = $pessoaFisica->getEndereco() . ',' . $pessoaFisica->getNumero() . ',' . $pessoaFisica->getBairro() . ',' . $pessoaFisica->getCidade() . ',' . $pessoaFisica->getUf() . ',' . $pessoaFisica->getCep();
        $geoLocalization = \Base\Util\Util::getGeoCode($address);
        $script = '$(function () {map(' . number_format($geoLocalization[0], 7, ".", "") . ',' . number_format($geoLocalization[1], 7, ".", "") . ', "#map1");});';
        $this->scripts_styles = array(
            array('file'=>'https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpXj82d8UpCi97wzo_nKXL7nYrd4G70', 'type'=>'headscript', 'function'=>'appendFile', 'external'=>true),
            array('file'=>'/libs/googlemaps/gmaps.js', 'type'=>'headscript', 'function'=>'appendFile'),
            array('file'=>'/js/maps.js', 'type'=>'headscript', 'function'=>'appendFile'),
            array('content'=>$script, 'type'=>'headscript', 'function'=>'appendScript')
        );
        
        $this->scriptsStyles();
                
        return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller, 'afiliado'=>$afiliado, 'plano'=>$plano));
    }
    
    public function pagosetAction() {
        $id = $this->params()->fromRoute('id', 0);
        $pago = $this->params()->fromRoute('ativo');

        $em = $this->getEm();
        
        $config = $this->getServiceLocator()->get('config');
        $config = $config['sendemail'];
                
        $hoje = date('Y-m-d');
        $qb = $em->createQueryBuilder();
        $qb->update($this->entity, 'e');
        $qb->set('e.pago', ':pago');
        $qb->set('e.inicioVigencia', ':inicioVigencia');
        $qb->set('e.fimVigencia', ':fimVigencia');
        $qb->setParameter('pago', $pago);
        $qb->setParameter('inicioVigencia', new \DateTime($hoje));
        $qb->setParameter('fimVigencia', new \DateTime(date('Y-m-d', strtotime($hoje . ' + 1 year'))));
        $qb->where("e.id = :id");
        $qb->setParameter('id', $id);
        $qb->getQuery()->execute();

        $afiliado = $em->getRepository('Admin\Entity\Afiliado')->find($id);
        //Soma os pontos
        $afiliado->setPontosPessoais($afiliado->getPontos());
        $em->persist($afiliado);
        $em->flush();
        
        $planoAfiliado = new PlanoAfiliado();

        $planoAfiliado->setTitulo($afiliado->getNomePlano());
        $planoAfiliado->setValor($afiliado->getValorInvestimento());
        $planoAfiliado->setCota($afiliado->getNomePlano());
        $planoAfiliado->setPercentualIndicacao($afiliado->getPercentualIndicacao());
        $planoAfiliado->setPontos($afiliado->getPontos());
        $planoAfiliado->setValorSaque($afiliado->getValorSaque());
        $planoAfiliado->setInicioVigencia($afiliado->getInicioVigencia());
        $planoAfiliado->setFimVigencia($afiliado->getFimVigencia());
        $planoAfiliado->setAfiliado($afiliado);
        $em->persist($planoAfiliado);
        $em->flush();

        if ($planoAfiliado->getId()) {
            //$url = $this->url()->fromRoute('adm/default', array('controller' => 'afiliado', 'action' => 'distribuirvalor'), array('force_canonical' => true));
            //Util::processarUrl($url . '?valor=' . $planoAfiliado->getValor() . '&id=' . $afiliado->getId());
            $afiliadoPagamento = new AfiliadoPagamento(array(
                'afiliado' => $afiliado,
                'descricao' => 'Novo Afiliado',
                'valor' => $afiliado->getValorInvestimento()
            ));
            $em->persist($afiliadoPagamento);
            $em->flush();
            
            if ($afiliado->getIndicacao()) {
                //Pega o afiliado indicador
                $afiliadoIndicador = $em->getRepository('Admin\Entity\Afiliado')->find($afiliado->getIndicacao());
                //Soma os Pontos
                $afiliadoIndicador->setPontosRede($afiliadoIndicador->getPontosRede()+$afiliado->getPontos());
                //Soma os pontos dele
                //
                //Pega o percentual que seu plano lhe paga por indicação
                $percentualIndicacao = $afiliadoIndicador->getPercentualIndicacao();
                //Calcula valor a pagar pela indicacao
                $valorPagarPorIndicacao = $afiliado->getValorInvestimento() * ($percentualIndicacao / 100);

                $afiliadoExtrato = new AfiliadoExtrato(array(
                    'afiliado'=>$afiliadoIndicador,
                    'tipo' => AfiliadoExtrato::$_tipo['comissão'],
                    'valor' => $valorPagarPorIndicacao,
                    'situacao' => AfiliadoExtrato::$_situacao['paga'],
                    'data_pagamento' => new \DateTime(date('Y-m-d')),
                    'descricao' => AfiliadoExtrato::$_descricao['comissão'] . $afiliado->getPessoaFisica()->getNome() . ' Plano - ' . $afiliado->getNomePlano()
                    )
                );
                $em->persist($afiliadoExtrato);
                $em->flush();
                
                $mail = $this->getServiceLocator()->get('Email');
                $mail->sendMail($config['fromMail'], $config['fromName'], $afiliadoIndicador->getUser()->getEmail(), $afiliadoIndicador->getUser()->getName(), false, false, \Base\Util\Mail::$APROVARCOMISSAO['assunto'],  array('mensagem'=>\Base\Util\Mail::$APROVARCOMISSAO['mensagem'], 'variaveis'=>array('nome'=>$afiliadoIndicador->getPessoaFisica()->getNome())), false, $em);

            }
            
            $mail = $this->getServiceLocator()->get('Email');
            $mail->sendMail($config['fromMail'], $config['fromName'], $afiliado->getUser()->getEmail(), $afiliado->getUser()->getName(), false, false, \Base\Util\Mail::$APROVARPAGAMENTO['assunto'],  array('mensagem'=>\Base\Util\Mail::$APROVARPAGAMENTO['mensagem'], 'variaveis'=>array('login'=>$afiliado->getUser()->getEmail(), 'link'=>$this->getEvent()->getRouter()->assemble(array(''), array('name'=>'adm', 'force_canonical'=>true)))), false, $em);

            $this->flashMessenger()->addSuccessMessage('Pagamento aprovado com sucesso!');
        } else {
            $this->flashMessenger()->addErrorMessage('Não foi possivel alterar o registro');
        }
        return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
    }
    
    public function contadepositoAction() {
        $this->form = 'Admin\Form\AfiliadoForm';
        
        $em = $this->getEm();

        if(is_string($this->form))
            $form = new $this->form($em);
        else 
            $form = $this->form($em);
        
        $request = $this->getRequest();
        
        $repository = $this->getEm()->getRepository($this->entity)->find($this->layout()->afiliado->getId());
        
        if($repository){
            $array = array();
            foreach($repository->toArray() as $key => $value){
                if($value instanceof \DateTime){
                    $array[$key] = $value->format ('d/m/Y H:i:s');
                }else {
                    $array[$key] = $value;
                }
            }
            $array['numeroBanco'] = $repository->getNumeroBanco();
            $form->setData($array);
            
            if($request->isPost()){
                $form->setData($request->getPost());
                
                if($form->isValid()){
                    $data = $request->getPost()->toArray();
                    $repository->setNumeroBanco($data['numeroBanco']);
                    $repository->setBanco($data['banco']);
                    $repository->setAgencia($data['agencia']);
                    $repository->setConta($data['conta']);
                    $repository->setTitular($data['titular']);
                    $repository->setObs($data['obs']);
                    $em->persist($repository);
                    $em->flush();
                    $this->flashMessenger()->addSuccessMessage('Dados bancários atualizados com sucesso!');
                    return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller, 'action'=>'contadeposito'));

                }else{
                    $this->flashMessenger()->addErrorMessage('Ocorreu um erro, tente novamente');
                }
            }
            
        }else{
            $this->flashMessenger()->addInfoMessage('Ocorreu um erro, tente novamentoe');
            return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller, 'action'=>'contadeposito'));
        }
        
        $this->scriptsStyles();
        
        return new ViewModel(array('form'=>$form, 'route'=>$this->route, 'controller'=>$this->controller , 'em' => $em, 'params'=>$this->aditionalParameters()));
    }

    public function aditionalParameters() {
        
    }

}
