<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use NeuroSYS\DoctrineDatatables\TableBuilder;
use NeuroSYS\DoctrineDatatables\Renderer\TwigRenderer;
use Admin\Entity\AfiliadoExtrato;

class MinhaequipeController extends AbstractController
{
    public function __construct() {
        $this->controller = 'minhaequipe';
        $this->route = 'adm/default';
        $this->tituloTela = 'Minha equipe';
    }
    
    public function indexAction()
    {
        $em = $this->getEm();
        $this->layout()->tituloTela = $this->tituloTela . ' | Lista';
        
        $indicados = $em->getRepository('Admin\Entity\Afiliado')->findIndicados($this->layout()->afiliado);
//        var_dump($indicados);die();
        return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller, 'indicados'=>$indicados));
    }

    public function aditionalParameters(){
    
    }
}
