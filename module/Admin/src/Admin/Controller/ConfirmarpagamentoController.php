<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use NeuroSYS\DoctrineDatatables\TableBuilder;
use NeuroSYS\DoctrineDatatables\Renderer\TwigRenderer;
use Base\Util\Mail;

class ConfirmarpagamentoController extends AbstractController
{
    public function __construct() {
        $this->form = 'Admin\Form\ConfirmarPagamentoForm';
        $this->controller = 'confirmarpagamento';
        $this->route = 'adm/default';
//        $this->service = 'Admin\Service\AfiliadoService';
        $this->entity = 'Admin\Entity\Afiliado';
        
        $this->tituloTela = 'Categoria';
    }
    
    public function indexAction() {
        foreach($this->layout()->userinfo->getRoles() as $role){
            if($role->getRoleName() == 'Affiliate'){
                $em = $this->getEm();
                $afiliado = $em->getRepository('Admin\Entity\Afiliado')->findOneBy(array('user'=> $this->layout()->userinfo));
                if($afiliado->getPago() == 2){
                    return $this->redirect()->toRoute($this->route, array('controller'=>'index', 'action'=>'index'));
                }
            }
        }
        
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $this->layout()->tituloTela = $this->tituloTela . ' | Adicionar';
        
        if(is_string($this->form))
            $form = new $this->form($em);
        else 
            $form = $this->form($em);
        
        $request = $this->getRequest();
        if($request->isPost()){
            $form->setData($request->getPost());
            
            foreach ($_FILES as $thisAttachment) {
                if($thisAttachment['type'] != 'image/jpeg'){
                    $this->flashMessenger()->addErrorMessage('Formato inválido de arquivo!');
                    return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller));
                }
            }
            if($form->isValid()){
                $mail = $this->getServiceLocator()->get('Email');
                $user = $this->layout()->userinfo;
                
                $config = $this->getServiceLocator()->get('config');
                $config = $config['sendemail'];
                
                if($mail->sendMail($user->getEmail(), $user->getName(), $config['toMail'], $config['toName'], false, false, Mail::$CONFIRMARPAGAMENTO['assunto'].'-'.$user->getName(),  array('mensagem'=>Mail::$CONFIRMARPAGAMENTO['mensagem'],'variaveis'=>array('nome'=>$user->getName(),'email'=>$user->getEmail())), $_FILES, $em) == '1'){
                    $afiliado->setPago(2);
                    $em->persist($afiliado);
                    $em->flush();
                    $this->flashMessenger()->addSuccessMessage('Comprovante enviado com sucesso!');
                    return $this->redirect()->toRoute($this->route, array('controller'=>'index', 'action'=>'index'));
                }else{
                    $this->flashMessenger()->addErrorMessage('Ocorreu um erro, tente novamente!');  
                }
                
            }else{
                $this->flashMessenger()->addErrorMessage('Dados inseridos inválidos');
            }
        }
        
        
        $this->scriptsStyles();
        
        return new ViewModel(array('form'=>$form, 'route'=>$this->route, 'controller'=>$this->controller ,'em'=>$em, 'params'=>$this->aditionalParameters()));
    }
    
    public function upgradeAction() {
        if($this->layout()->afiliado){
            $em = $this->getEm();
            $upgrade = $em->getRepository('Admin\Entity\UpgradeAfiliado')->findOneBy(array('afiliado'=> $this->layout()->afiliado, 'situacao' => 0));
            if(!$upgrade){
                return $this->redirect()->toRoute($this->route, array('controller'=>'index', 'action'=>'index'));
            }
        }
        
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $this->layout()->tituloTela = $this->tituloTela . ' | Adicionar';
        
        if(is_string($this->form))
            $form = new $this->form($em);
        else 
            $form = $this->form($em);
        
        $request = $this->getRequest();
        if($request->isPost()){
            $form->setData($request->getPost());
            
            foreach ($_FILES as $thisAttachment) {
                if($thisAttachment['type'] != 'image/jpeg'){
                    $this->flashMessenger()->addErrorMessage('Formato inválido de arquivo!');
                    return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller));
                }
            }
            if($form->isValid()){
                $mail = $this->getServiceLocator()->get('Email');
                $user = $this->layout()->userinfo;
                
                $config = $this->getServiceLocator()->get('config');
                $config = $config['sendemail'];
                
                if($mail->sendMail($user->getEmail(), $user->getName(), $config['toMail'], $config['toName'], false, false, Mail::$CONFIRMARPAGAMENTO['assunto'].' - UPGRADE - '.$user->getName(),  array('mensagem'=>Mail::$CONFIRMARPAGAMENTO['mensagem'],'variaveis'=>array('nome'=>$user->getName(),'email'=>$user->getEmail(), 'link'=>$this->getEvent()->getRouter()->assemble(array(''), array('name'=>'adm', 'force_canonical'=>true)))), $_FILES, $em) == '1'){
                    $upgrade->setSituacao(2);
                    $em->persist($upgrade);
                    $em->flush();
                    $this->flashMessenger()->addSuccessMessage('Comprovante upgrade enviado com sucesso!');
                    return $this->redirect()->toRoute($this->route, array('controller'=>'index', 'action'=>'index'));
                }else{
                    $this->flashMessenger()->addErrorMessage('Ocorreu um erro, tente novamente!');  
                }
                
            }else{
                $this->flashMessenger()->addErrorMessage('Dados inseridos inválidos');
            }
        }
        
        
        $this->scriptsStyles();
        
        return new ViewModel(array('form'=>$form, 'route'=>$this->route, 'controller'=>$this->controller ,'em'=>$em, 'params'=>$this->aditionalParameters()));
    }
    
    public function aditionalParameters(){
        return array();
    }
}
