<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use NeuroSYS\DoctrineDatatables\TableBuilder;
use NeuroSYS\DoctrineDatatables\Renderer\TwigRenderer;
use Admin\Entity\Noticia;
use Admin\Entity\ImagemNoticia;
use Base\Controller\FilesAbstractController;
use Doctrine\Common\Collections\Criteria;
use Facebook\Facebook;
class NoticiaController extends FilesAbstractController
{
    public function __construct() {
        $this->form = 'Admin\Form\NoticiaForm';
        $this->controller = 'noticia';
        $this->route = 'adm/default';
        $this->service = 'Admin\Service\NoticiaService';
        $this->entity = 'Admin\Entity\Noticia';
        $this->external_objects  = array('categoria'=>'Admin\Entity\Categoria');
        $this->tituloTela = 'Notícia';
        $this->data_elements = array('data_publicacao');
        
        $this->extensions = array('jpg', 'jpeg', 'png', 'pneg');
        $this->dir = 'noticias/';
        $this->marcadagua = array('watermark.png', 'watermark_400.png', 'watermark_220.png');
        $this->tamanhos = array(800,400,220);
        $this->fixed = array(160,110);
        $this->scripts_styles = array(
            array('file'=>'/libs/bootstrap-fileinput/js/fileinput.min.js', 'type'=>'headscript', 'function'=>'appendFile'),
            array('file'=>'/libs/bootstrap-fileinput/js/fileinput_locale_pt-BR.js', 'type'=>'headscript', 'function'=>'appendFile'),
            array('file'=>'/libs/bootstrap-fileinput/css/fileinput.css', 'type'=>'headlink', 'function'=>'appendStylesheet'),
            array('content'=>'$(document).ready(function(){'
                . '$(\'.summernote\').summernote({
                        lang: \'pt-BR\',
                        height: 200,
                        placeholder: \'\',
                        hint: {
                            mentions: [\'Publicidades%\'],
                            match: /\B%(\w*)$/,
                            search: function (keyword, callback) {
                                callback($.grep(this.mentions, function (item) {
                                    return item.indexOf(keyword) == 0;
                                }));
                            },
                            content: function (item) {
                              return \'%\' + item;
                            }
                        },
                        callbacks:{
                            onImageUpload: function(files) {
                                url = baseUrl+\'uploadeditor\';
                                Scripts.sendFileSummernote(files[0], url, $(this));
                            }
                        }

                    });
                });',
                'type'=>'headscript', 'function'=>'appendScript'),
        );
    }
    
    public function indexAction()
    {
        $this->layout()->tituloTela = $this->tituloTela . ' | Lista';
        $this->getServiceLocator()->get('viewhelpermanager')->get('headScript')->appendFile($this->getRequest()->getBasePath().'/min?g=datatables-js');
        return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller));
    }
    
    public function listAction()
    {
        $loader   = new \Twig_Loader_String();
        $renderer = new TwigRenderer(new \Twig_Environment($loader));
        $params = $this->params()->fromQuery();
       
        $builder = new TableBuilder($this->getEm($this->entity), $params, null, $renderer);
        $builder
            ->from('Admin\Entity\Noticia', 'n')
            ->join('n.categoria', 'c')
            ->add('number', 'n.id', null, array(
                    'template' => '<div></div>'
                )
            )
            ->add('text', 'c.titulo')
            ->add('text', 'n.titulo')
            ->add('date', 'n.data_publicacao', 'n.data_publicacao', array(
                    'template' => '{% if value %}{{ values.data_publicacao|date("d/m/Y H:i") }}{% endif %}'
                )
            )
            ->add('boolean', 'n.nivel')
            ->add("text", "n.ativo, n.status", null, array(
                    'template'=> '{% if value %}<span class="label label-success" title="Ativo">Ativo / {{ values.status }}</span>{% else %}<span class="label label-danger" title="Inativo">Inativo / {{ values.status }}</span>{% endif %}'
                )
            )
            ->add("text", "n.ativo", null, array(
                'template' => ''
                . '<div class="dropdown">
                    <button class="btn btn-icon btn-rounded btn-primary waves-effect dropdown-toggle" type="button" id="dropdownMenu{{values.id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-cog"></i></button>                   
                    <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu{{values.id}}">
                        <li><a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'facebookLogin')) . '/{{values.id}}"> <i class="fa fa-facebook"></i> Compartilhar</a></li>'
                . '     <li><a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'edit')) . '/{{values.id}}"> <i class="fa fa-pencil"></i> Editar</a></li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'activeset')) . '/{{values.id}}/{% if value %}0{% else %}1{% endif %}"> <i class="fa fa-{% if value %}remove{% else %}check{% endif %}"></i> {% if value %}Desativar{% else %}Ativar{% endif %}</a>
                        </li>
                    </ul>
                  </div>'
            ))
            ->end()
            ;
         
        $response = $builder->getTable()
            ->getResponseArray() // hydrate entity, defaults to array
            ;
        
        $result = new JsonModel($response);
        return $result;
    }
    
    public function insertAction() {
        $em = $this->getEm();
        
        $lista = $em->getRepository('Admin\Entity\Categoria')->findAll();
        $categoria = $lista[0];
        
        $noticia = new Noticia();
        $noticia->setCategoria($categoria);
        
        $em->persist($noticia);
        $em->flush();
        return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller, 'action'=>'edit', 'id'=>$noticia->getId()));
    }
    
    public function aditionalParameters(){
        $retorno = array();
        $id = $this->params()->fromRoute('id', 0);
        if($id){
            $em = $this->getEm();
            $noticia = $em->getRepository('Admin\Entity\Noticia')->findOneBy(array('id'=>$id));
            $retorno['noticia'] = $noticia;
        }
        $retorno['dir'] = $this->dir;
        return $retorno;
    }

    public function tratarArquivo($id, $arquivo) {
        $em = $this->getEm();
        $noticia = $em->getRepository('Admin\Entity\Noticia')->find($id);
        $imagemNoticia = new ImagemNoticia();
        $imagemNoticia->setNoticia($noticia);
        $imagemNoticia->setTitulo('');
        $imagemNoticia->setArquivo($arquivo['novoNome']);
        $imagemNoticia->setExportada(1);

        $em->persist($imagemNoticia);
        $em->flush();
        
        $key = $imagemNoticia->getId();
        $url = $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'removeImage'));

        
        $nomeArquivoMedi = $imagemNoticia->getArquivo();
        $nomeArquivoMedi = pathinfo($this->request->getBasePath().'/uploads/' . $this->dir .'/' . $nomeArquivoMedi, PATHINFO_FILENAME).'_400.'.pathinfo($this->request->getBasePath().'/uploads/' . $this->dir .'/' . $nomeArquivoMedi, PATHINFO_EXTENSION);
                
        return json_encode([
            'initialPreview' => [
                "<img style='height:160px' src='".$this->request->getBasePath()."/uploads/".$this->dir.$imagemNoticia->getArquivo()."' class='file-preview-image'><br><br>Autor<br><input type='text' onblur='salvaAutor(".$imagemNoticia->getId().", this.value)'><br><br><button type='button' class='btn btn-success btn-sm' data-clipboard-text='". $this->request->getBasePath().'/uploads/' . $this->dir .'' . $nomeArquivoMedi. "'><i class='fa fa-copy'></i></button>&nbsp;<button type='button' class='btn btn-info btn-sm btn-capa' data-image-id='".$key."' data-noticia-id='".$noticia->getId()."' data-urldestino='".$this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'changeorder'))."'><i class='fa fa-cog'></i></button>",
            ],
            'initialPreviewConfig' => [
                ['url' => $url, 'key' => $key],
            ],
            'append' => true // whether to append these configurations to initialPreview.
                             // if set to false it will overwrite initial preview
                             // if set to true it will append to initial preview
                             // if this propery not set or passed, it will default to true.
        ]);
    }

    public function salvaautorAction() {
        $em = $this->getEm();
        $id = $this->params()->fromPost('id', 0);
        $autor = $this->params()->fromPost('autor', '');

        $imagemNoticia = $em->getRepository('Admin\Entity\ImagemNoticia')->findOneBy(array('id'=>$id));
        $imagemNoticia->setTitulo($autor);
        $em->persist($imagemNoticia);
        $em->flush();
        die();

    }

    public function removeImageAction() {
        $em = $this->getEm();
        $id = $this->params()->fromPost('key', 0);
        $imagemNoticia = $em->getRepository('Admin\Entity\ImagemNoticia')->findOneBy(array('id'=>$id));
        @unlink(getcwd() . '/public/uploads/' . $this->dir .'/' .$imagemNoticia->getArquivo());
        
        foreach($this->tamanhos as $tamanho){
            $nomeArquivo = $imagemNoticia->getArquivo();
            $extension = pathinfo(getcwd() . 'uploads/' . $this->dir .'/' . $nomeArquivo, PATHINFO_EXTENSION);
            
            $nomeArquivo = getcwd() . '/public/uploads/' . $this->dir . pathinfo(getcwd() . '/uploads/' . $this->dir . $nomeArquivo, PATHINFO_FILENAME).'_'.$tamanho.'.'.$extension;
            @unlink($nomeArquivo);
        }
        
        $result = $this->forward()->dispatch('Admin\Controller\Imagemnoticia', array('action' => 'delete', 'id'=>$id));
        if($result){
            return new JsonModel(array(true));
        }else{
            return new JsonModel(array(false));
        }
        

        
        return $this->redirect()->toRoute($this->route, array('controller' => 'imagemnoticia', 'action'=>'delete', 'id'=>$id));
    }
    
    public function changeorderAction() {
        $noticiaid = $this->params()->fromPost('id', 0);
        $imageid = $this->params()->fromPost('imageid', 0);
        
        $qb = $this->getEm()->createQueryBuilder();
        $q = $qb->update('Admin\Entity\ImagemNoticia', 'i')
            ->set('i.ordem', 1)
            ->where('i.noticia = ?1')
            ->setParameter(1, $noticiaid)
            ->getQuery();
        
        $p = $q->execute();
        
        $q = $qb->update('Admin\Entity\ImagemNoticia', 'i')
            ->set('i.ordem', 0)
            ->where('i.id = ?1')
            ->setParameter(1, $imageid)
            ->getQuery();
        
        $p2 = $q->execute();
        
        
        
        return new JsonModel(array());
    }
    
    public function facebookLoginAction(){
        $config = $this->getServiceLocator()->get('config');
        $config = $config['facebook'];
        
//        require $config['vendor_dir'].'/facebook/php-sdk-v4/src/Facebook/Facebook.php';
        $fb = new Facebook(array(
            'app_id'  => $config['app_id'],
            'app_secret' => $config['app_secret'],
        ));
        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['email','manage_pages', 'publish_pages', 'publish_actions']; // optional
        $loginUrl = $helper->getLoginUrl('http://'.$_SERVER['HTTP_HOST'].'/adm/noticia/share/'.$this->params()->fromRoute('id'), $permissions);
        echo '<script>location.href ="'.$loginUrl.'"</script>';

    }
    
    public function shareAction(){
        $config = $this->getServiceLocator()->get('config');
        $config = $config['facebook'];
        
        $fb = new Facebook(array(
            'app_id'  => $config['app_id'],
            'app_secret' => $config['app_secret'],
        ));
        $helper = $fb->getRedirectLoginHelper();
        
        $accessToken = $helper->getAccessToken();
        
        if (isset($accessToken)) {
            $em = $this->getEm();
            $noticia = $em->getRepository('Admin\Entity\Noticia')->find($this->params()->fromRoute('id'));
            
            $linkNoticia = $this->url()->fromRoute('noticia/default', array('controller' => 'noticia', 'action' => 'detalhes', 'id' => $noticia->getId(), 'titulo' => parent::titleLink($noticia->getTitulo())));
            $linkData = [
                  'link' => (isset($_SERVER["HTTPS"]) ? 'https' : 'http').'://'.$_SERVER['HTTP_HOST'].$linkNoticia,
                  'message' => (isset($_SERVER["HTTPS"]) ? 'https' : 'http').'://'.$_SERVER['HTTP_HOST'].$linkNoticia
            ];
           
            foreach($config['groups_id'] as $group){
                try{
                    $response = $fb->post('/'.$group.'/feed', $linkData, $accessToken);
//                    $graphNode = $response->getGraphNode();
//                    $postId = $graphNode['id'];
//                    if(!$noticia->getFacebookPostId()){
//                        $noticia->setFacebookPostId($postId);
//                        $em->persist($noticia);
//                    }
                } catch (\Facebook\Exceptions\FacebookResponseException $ex) {
                    
                }
                
            }
            echo '<scritp>location.href="'.(isset($_SERVER["HTTPS"]) ? 'https' : 'http').'://'.$_SERVER['HTTP_HOST'].'/adm/noticia";</script>';
            $this->flashMessenger()->addSuccessMessage('Compartilhado com sucesso!');
        }else{
            $this->flashMessenger()->addErrorMessage('Ocorreu um erro ao compartilhar!');
        }
        
        return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller));
    }
    
    public static function nomeImagemMini($imagem){
        $ext_img = explode('.', $imagem);
        $ext = '.'.$ext_img[count($ext_img)-1];
        return str_replace($ext, '_Fixed'.$ext, $imagem);

    }
    
    public static function nomeImagemMiniDetalhes($imagem){
        $ext_img = explode('.', $imagem);
        $ext = '.'.$ext_img[count($ext_img)-1];
        return str_replace($ext, '_220'.$ext, $imagem);

    }
    
    
}
