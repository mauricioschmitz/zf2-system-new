<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;

class ImagemnoticiaController extends AbstractController
{
    public function __construct() {
        $this->controller = 'imagemnoticia';
        $this->route = 'adm/default';
        $this->service = 'Admin\Service\ImagemNoticiaService';
        $this->entity = 'Admin\Entity\ImagemNoticia';
        
        $this->tituloTela = 'Notícia';
    }
    
    public function aditionalParameters(){
        return array();
    }
}
