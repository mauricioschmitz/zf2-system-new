<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Login\Utility\UserPassword;

class PerfilController extends AbstractController
{
    public function __construct() {
        $this->form = 'Admin\Form\PerfilForm';
        $this->controller = 'perfil';
        $this->route = 'adm/default';
        $this->service = 'Admin\Service\PerfilService';
        $this->entity = 'Login\Entity\User';
        
        $this->scripts_styles = array(
            array('file'=>'/libs/maskedInput/jquery.maskedinput.js', 'type'=>'headscript', 'function'=>'appendFile'),
            array('content'=>'$(document).ready(function(){'
                . 'Scripts.InitMasks();'
                . '$(\'#formPerfil\').on(\'submit\', function(){
                        if($(\'#password\').val() != \'\' || $(\'#password_confirm\').val() != \'\' || $(\'#old_password\').val() != \'\'){
                            $(\'#password\').attr(\'required\',\'true\');
                            $(\'#password_confirm\').attr(\'required\',\'true\');
                            $(\'#old_password\').attr(\'required\',\'true\');
                        }else{
                            $(\'#password\').removeAttr(\'required\');
                            $(\'#password_confirm\').removeAttr(\'required\');
                            $(\'#old_password\').removeAttr(\'required\');
                        }
                        $(\'#formPerfil\').validate();
//                        return false;
                   });'
                . '});',
                'type'=>'headscript', 'function'=>'appendScript'),
        );
        $this->tituloTela = 'Perfil';
    }
    
    public function updateAction()
    {
        $this->layout()->tituloTela = $this->tituloTela;
        $em = $this->getEm();
        $userPassword = new UserPassword();
        if(is_string($this->form))
            $form = new $this->form($em);
        else 
            $form = $this->form($em);
        
        
        $form->getInputFilter()->remove('plano_id');
        $form->getInputFilter()->remove('approve');
            
        $request = $this->getRequest();
        
        $authenticationService = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
        $loggedUser = $authenticationService->getIdentity();
        $param = $loggedUser->getId();
        if(isset($this->layout()->afiliado)){
            $repository = $this->getEm()->getRepository('Admin\Entity\Afiliado')->find($this->layout()->afiliado->getId());
        }else{
            $form->getInputFilter()->remove('sexo');
            $form->getInputFilter()->remove('estado_civil');
            $repository = $this->getEm()->getRepository($this->entity)->find($param);
        }
        if($repository){
            $array = array();
            foreach($repository->toArray() as $key => $value){
                if($value instanceof \DateTime){
                    $array[$key] = $value->format ('d/m/Y H:i:s');
                }else {
                    $array[$key] = $value;
                }
            }
            if(isset($this->layout()->afiliado)){
                $arrayUser = array();
                foreach($array['user']->toArray() as $key => $value){
                    if($value instanceof \DateTime){
                        $arrayUser[$key] = $value->format ('d/m/Y H:i:s');
                    }else {
                        $arrayUser[$key] = $value;
                    }
                }
                $arrayPessoaFisica = array();
                foreach($array['pessoa_fisica']->toArray() as $key => $value){
                    if($value instanceof \DateTime){
                        $arrayPessoaFisica[$key] = $value->format ('d/m/Y H:i:s');
                    }else {
                        $arrayPessoaFisica[$key] = $value;
                    }
                }
                $form->setData($arrayUser);
                $form->setData($arrayPessoaFisica);
            }
            $form->setData($array);
            
            
            if($request->isPost()){
                $params = $request->getPost();
                $form->setData($params);
                $mudouSenha = false;
                if($params['password'] != '' || $params['old_password'] != '' || $params['password_confirm'] != ''){
                    $mudouSenha = true;
                    //valida se a old_password confere
                    $oUser = $em->getRepository('Login\Entity\User')->find($param);
                    if($userPassword->create($params['old_password']) != $oUser->getPassword()){
                        $this->flashMessenger()->addErrorMessage('Senha antiga não confere');
                        return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller, 'action'=>'update'));
                    }
                }else{
                    $form->getInputFilter()->remove('old_password');
                    $form->getInputFilter()->remove('password');
                    $form->getInputFilter()->remove('password_confirm');
                    
                }
                
                if($form->isValid()){
                    $data = $request->getPost()->toArray();
                    $service = $this->getServiceLocator()->get($this->service);
                    
                    //Se for afiliado
                    if(isset($this->layout()->afiliado)){
                        $dataPessoa = $data;
                        $dataPessoa['data_nascimento'] = new \DateTime(implode('-', array_reverse(explode('/',$dataPessoa['data_nascimento']))));
                        $dataPessoa['id'] = $this->layout()->afiliado->getPessoaFisica()->getId();
                        $servicePessoaFisica = $this->getServiceLocator()->get('Admin\Service\PessoaFisicaService');
                        if($_FILES['foto']['name']!= ""){
                            @unlink(getcwd().'/public/uploads/pessoaFisica/'.$this->layout()->afiliado->getPessoaFisica()->getFoto());
                            $File    = $this->params()->fromFiles('foto');
                            $dataPessoa = $servicePessoaFisica->foto($File,$dataPessoa);
                        }
                        
                        $servicePessoaFisica->save($dataPessoa);
                    }
                    
                    $data['id'] = (int)$param;
                    if(!$mudouSenha){
                        $data['password'] = $loggedUser->getPassword();
                    }else{
                        $data['password'] = $userPassword->create($data['password']);
                    }
                    if($service->save($data)){
                        $this->flashMessenger()->addSuccessMessage('Alterado com sucesso!');
                    } else {
                        $this->flashMessenger()->addErrorMessage('Ocorreu um erro!');  
                    }

                    return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller, 'action'=>'update'));

                }else{
                    $this->flashMessenger()->addErrorMessage('Dados inseridos inválidos');
                }
            }
            
        }else{
            $this->flashMessenger()->addInfoMessage('Registro não encontrado');
            return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller, 'action'=>'update'));
        }
        
        $this->scriptsStyles();
        
        return new ViewModel(array('form'=>$form, 'route'=>$this->route, 'controller'=>$this->controller,'em' => $em, 'params'=>$this->aditionalParameters()));
    }

    public function aditionalParameters(){
        $retorno = array();
        if(isset($this->layout()->afiliado)){
            $retorno['afiliado'] = $this->layout()->afiliado;
        }
        return $retorno;
    }
}
