<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;

class AfiliadoconfigController extends AbstractController
{
    public function __construct() {
        $this->form = 'Admin\Form\AfiliadoConfigForm';
        $this->controller = 'afiliadoconfig';
        $this->route = 'adm/default';
        $this->service = 'Admin\Service\AfiliadoConfigService';
        $this->entity = 'Admin\Entity\AfiliadoConfig';
        $this->tituloTela = 'Config';
    }
    
    public function indexAction()
    {
        
        $em = $this->getEm();
        $config = $em->getRepository('Admin\Entity\AfiliadoConfig')->findOneBy(array('ativo'=>1));
        return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller, 'action'=>'edit', 'id'=>$config->getId()));
    }
    
    public function aditionalParameters(){
        $retorno = array();
        $id = $this->params()->fromRoute('id', 0);
        if($id){
            $em = $this->getEm();
            $plano = $em->getRepository('Admin\Entity\Plano')->findOneBy(array('id'=>$id));
            $retorno['plano'] = $plano;
        }
        return $retorno;
    }
}
