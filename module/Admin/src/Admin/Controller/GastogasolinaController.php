<?php

namespace Admin\Controller;

use Admin\Controller\ImportXLSController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use NeuroSYS\DoctrineDatatables\TableBuilder;
use NeuroSYS\DoctrineDatatables\Renderer\TwigRenderer;

class GastogasolinaController extends ImportXLSController
{
    public function __construct() {
        $this->form = 'Admin\Form\GastoGasolinaForm';
        $this->controller = 'gastogasolina';
        $this->route = 'adm/default';
        $this->service = 'Admin\Service\GastoGasolinaService';
        $this->entity = 'Admin\Entity\GastoGasolina';
        $this->external_objects  = array('veiculo'=>'Admin\Entity\Veiculo');
        $this->money3casas_elements = array('preco_combustivel');
        $this->data_elements = array('data');
        $this->tituloTela = 'Gasto Gasolina';
        
        $this->parentEntity = 'Admin\Entity\Veiculo';
        $this->parentField = 'veiculo';
    }
    
    public function indexAction()
    {
        $this->layout()->tituloTela = $this->tituloTela . ' | Lista';
        $this->getServiceLocator()->get('viewhelpermanager')->get('headScript')->appendFile($this->getRequest()->getBasePath().'/min?g=datatables-js');
        
        $veiculos = $this->getEm()->getRepository('Admin\Entity\Veiculo')->findBy(array('ativo'=>1));
        $soma = null;
        if($id = $this->params()->fromQuery('veiculo')){
            $veiculo = $this->getEm()->getRepository('Admin\Entity\Veiculo')->find($id);
            $lista = $this->getEm()->getRepository('Admin\Entity\GastoGasolina')->findBy(array('veiculo'=>$veiculo));
            $somas = $this->getEm()->getRepository('Admin\Entity\GastoGasolina')->getSum($veiculo);
        }else{
            $lista = $this->getEm()->getRepository('Admin\Entity\GastoGasolina')->findAll();
        }
        return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller, 'veiculos'=>$veiculos, 'lista'=>$lista, 'somas'=>$somas));
    }
    
    public function listAction()
    {
        $loader   = new \Twig_Loader_String();
        $renderer = new TwigRenderer(new \Twig_Environment($loader));
        $params = $this->params()->fromQuery();
        
        $filtros = array('veiculo'=>$this->getEm()->getRepository('Admin\Entity\Veiculo')->findOneBy(array('ativo'=>1)));
        
        $builder = new TableBuilder($this->getEm($this->entity), $params, null, $renderer, $filtros);
        $builder
            ->from('Admin\Entity\GastoGasolina', 'g')
            ->join('g.veiculo', 'v')
            ->add('number', 'g.id', null, array(
                    'template' => '<div class="dataTablesMore">+</div>'
                )
            );
        
        $builder
            ->add('date', 'g.data', 'g.data', array(
                    'template' => '{% if value %}{{ values.data|date("d/m/Y") }}{% endif %}'
                )
            )
            ->add('text', 'g.km')    
            ->add('text', 'g.litros')  
            ->add('text', 'g.preco_combustivel')  
            ->add('text', 'g.preco_combustivel', 'g.litros', array(
                'template' => ''
                . '<div>{{values.preco_combustivel * values.litros}}</div>'
            ))  
            ->add('text', 'g.preco_combustivel')  
            ->add("text", "g.ativo", null, array(
                    'template'=> '{% if value %}<span class="label label-success" title="Ativo">Ativo</span>{% else %}<span class="label label-danger" title="Inativo">Inativo</span>{% endif %}'
                )
            )
            ->add("text", "g.ativo", null, array(
                'template' => ''
                . '<div class="dropdown">
                    <button class="btn btn-icon btn-rounded btn-primary waves-effect dropdown-toggle" type="button" id="dropdownMenu{{values.id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-cog"></i></button>                   
                    <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu{{values.id}}">
                        <li><a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'edit')) . '/{{values.id}}"> <i class="fa fa-pencil"></i> Editar</a></li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'activeset')) . '/{{values.id}}/{% if value %}0{% else %}1{% endif %}"> <i class="fa fa-{% if value %}remove{% else %}check{% endif %}"></i> {% if value %}Desativar{% else %}Ativar{% endif %}</a>
                        </li>
                    </ul>
                  </div>'
            ))
            ->end()
            ;
         
        $response = $builder->getTable()
            ->getResponseArray() // hydrate entity, defaults to array
            ;
        
        $result = new JsonModel($response);
        return $result;
    }
    
    public function aditionalParameters(){
        $retorno = array();
        $id = $this->params()->fromRoute('id', 0);
        if($id){
            $em = $this->getEm();
            $object = $em->getRepository('Admin\Entity\GastoGasolina')->findOneBy(array('id'=>$id));
            $retorno['gastoGasolina'] = $object;
        }
        return $retorno;
    }
}
