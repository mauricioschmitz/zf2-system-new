<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use NeuroSYS\DoctrineDatatables\TableBuilder;
use NeuroSYS\DoctrineDatatables\Renderer\TwigRenderer;

class ExtratocontaController extends AbstractController
{
    public function __construct() {
        $this->controller = 'extratoconta';
        $this->route = 'adm/default';
        $this->entity = 'Admin\Entity\AfiliadoExtrato';
        $this->service = 'Affiliate\Service\AfiliadoExtrato';
        $this->tituloTela = 'Extrato da conta';
    }
    
    public function indexAction()
    {
        $em = $this->getEm();
        $saldo = $em->getRepository('Admin\Entity\AfiliadoExtrato')->saldo($this->layout()->afiliado);
        $this->layout()->tituloTela = $this->tituloTela . ' | Lista';
        
        $this->getServiceLocator()->get('viewhelpermanager')->get('headScript')->appendFile($this->getRequest()->getBasePath().'/min?g=datatables-js');
        
        return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller, 'saldo'=>$saldo));
        
    }
    
    public function listAction()
    {
        $loader   = new \Twig_Loader_String();
        $renderer = new TwigRenderer(new \Twig_Environment($loader));
        $params = $this->params()->fromQuery();
        
        $builder = new TableBuilder($this->getEm($this->entity), $params, null, $renderer, array('afiliado'=>$this->layout()->afiliado, 'situacao'=>2));
        
        $builder
            ->from('Admin\Entity\AfiliadoExtrato', 'ae')
            ->add('number', 'ae.id', null, array(
                    'template' => '<div class="dataTablesMore">+</div>'
                )
            )
            ->add('date', 'ae.data_pagamento', 'ae.data_pagamento', array(
                    'template' => '{% if value %}{{ values.data_pagamento|date("d/m/Y") }}{% endif %}'
                )
            )
            ->add('text', 'ae.descricao')
            ->add('text', 'ae.valor', null, array(
                    'template' => '{{ value|number_format(2, ",", ".") }}'
                )
            )
            ->end();
         
        $response = $builder->getTable()
            ->getResponseArray() // hydrate entity, defaults to array
            ;
        
        $result = new JsonModel($response);
        return $result;
        
    }
    
    public function aditionalParameters(){
        
    }
}
