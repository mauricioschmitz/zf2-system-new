<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;
use Nette\Neon\Exception;
use Zend\Form\Element\DateTime;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use NeuroSYS\DoctrineDatatables\TableBuilder;
use NeuroSYS\DoctrineDatatables\Renderer\TwigRenderer;
use Base\Util\Google;
use Doctrine\Common\Collections\Criteria;
use Admin\Entity\RastreamentoOcorrencia;

class RastreamentoController extends AbstractController
{
    public function __construct() {
        $this->form = 'Admin\Form\RastreamentoForm';
        $this->controller = 'rastreamento';
        $this->route = 'adm/default';
        $this->service = 'Admin\Service\RastreamentoService';
        $this->serviceOcorrencia = 'Admin\Service\RastreamentoOcorrenciaService';
        $this->entity = 'Admin\Entity\Rastreamento';
        
        $this->tituloTela = 'Rastreamento';

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);

    }
    
    public function indexAction()
    {
        $this->layout()->tituloTela = $this->tituloTela . ' | Lista';
        $this->getServiceLocator()->get('viewhelpermanager')->get('headScript')->appendFile($this->getRequest()->getBasePath().'/min?g=datatables-js');
        
        return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller));
    }
    
    public function listAction()
    {
        $loader   = new \Twig_Loader_String();
        $renderer = new TwigRenderer(new \Twig_Environment($loader));
        $params = $this->params()->fromQuery();
        
        $builder = new TableBuilder($this->getEm($this->entity), $params, null, $renderer);
        $builder
            ->from('Admin\Entity\Rastreamento', 'r')
            ->add('number', 'r.id')
            ->add('text', 'r.codigo')
            ->add("text", "r.ativo", null, array(
                    'template'=> '{% if value %}<span class="label label-success" title="Ativo">Ativo</span>{% else %}<span class="label label-danger" title="Inativo">Inativo</span>{% endif %}'
                )
            )
            ->add("text", "r.ativo", null, array(
                'template' => ''
                . '<div class="dropdown">
                    <button class="btn btn-icon btn-rounded btn-primary waves-effect dropdown-toggle" type="button" id="dropdownMenu{{values.id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-cog"></i></button>                   
                    <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu{{values.id}}">
                        <li><a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'edit')) . '/{{values.id}}"> <i class="fa fa-pencil"></i> Editar</a></li>
                        <li><a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'details')) . '/{{values.id}}"> <i class="fa fa-info"></i> Detalhes</a></li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'activeset')) . '/{{values.id}}/{% if value %}0{% else %}1{% endif %}"> <i class="fa fa-{% if value %}remove{% else %}check{% endif %}"></i> {% if value %}Desativar{% else %}Ativar{% endif %}</a>
                        </li>
                    </ul>
                  </div>'
            ))
            ->end()
            ;
         
        $response = $builder->getTable()
            ->getResponseArray() // hydrate entity, defaults to array
            ;
        
        $result = new JsonModel($response);
        return $result;
    }

    public function loaddataAction(){
//        $json_url = "http://blackfriday.kabum.com.br/json/data.json";
//        $json = file_get_contents($json_url);
//        $data = json_decode($json, TRUE);
////        echo '<pre>';
//        echo 'Qtd: '. count($data['produtos']).'<br>';
//        foreach ($data['produtos'] as $produto){
////            var_dump($produto);die;
//
//            $str = 'Produto: '. $produto['produto'].'<br>';
//            $str .= 'Preço: '. $produto['vlr_oferta'].'<br>';
//            $str .= 'Preço original: '. $produto['vlr_normal'].'<br>';
//            $str .= '<img src="'.$produto['imagem'].'"><br>';
//
//            $str .= 'Data Ini: '. date('d/m/Y H:i:s', $produto['data_ini']).'<br>';
//            $str .= '<a href="http://www.kabum.com.br/cgi-local/site/produtos/descricao_blackfriday.cgi?codigo='.$produto['codigo'].'" target="_blank">Link</a>_______<br>';
//            echo $str;
//            if ((strpos(strtolower($produto['produto']), 'chromecast') !== false
//        || strpos(strtolower($produto['produto']), 'cadeirinha') !== false
//        || strpos(strtolower($produto['produto']), 'cooktop') !== false
//        || strpos(strtolower($produto['produto']), 'moto g') !== false
//        || strpos(strtolower($produto['produto']), 'fritadeira') !== false
//        || strpos(strtolower($produto['produto']), 'ventilador') !== false
////        || strpos(strtolower($produto['produto']), 'científica') !== false
////        || $produto['vlr_oferta'] <= 10
//        || strpos(strtolower($produto['produto']), 'ssd') !== false && $produto['vlr_oferta'] < 150)
//            //&& $produto['data_ini'] > strtotime(date('Y-m-d H:i:s'))
//        ){
//
//                $mail = $this->getServiceLocator()->get('Email');
//                $config = $this->getServiceLocator()->get('config');
//                $config = $config['sendemail'];
//
//                $htmlContent = $str;
//
//                $mail->sendMail($config['from'], 'MAuricio - Black Friday', 'contato@mauricioschmitz.com.br', 'Mauricio', false, false, 'Black Friday',  $htmlContent, null, $this->getEm(), $config);
//                die;
//            }
//        }
//        die;
        $em = $this->getEm();
        $rastreamentos = $em->getRepository('Admin\Entity\Rastreamento')->findNaoEntregue();
        $sendEmail = array();
        foreach($rastreamentos as $oRastreamento){
            // url encode the address
            $codigo = $oRastreamento->getCodigo();

            // google map geocode api url
            $url = "http://api.postmon.com.br/v1/rastreio/ect/{$codigo}";

            try{
                // get the json response
                $curl_handle=curl_init();
                curl_setopt($curl_handle, CURLOPT_URL,$url);
                curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
                curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
                $query = curl_exec($curl_handle);
                curl_close($curl_handle);

                // decode the json
                $resp_array = json_decode($query, true);

                $waypoints = array();
                //percorre a resposta
                if(!is_null($resp_array)){
                    foreach ($resp_array['historico'] as $key=>$ocorrencia){
                        if(is_null($oRastreamento->getOrigem()) || $key == 0){
                            $oRastreamento->setOrigem(@end(Google::getGeoCode($ocorrencia['local'])));
                        }
                        $oRastreamento->setPosicao(@end(Google::getGeoCode($ocorrencia['local'])));
                        $oRastreamento->setSituacao($ocorrencia['situacao']);

                        $em->persist($oRastreamento);
                        $em->flush();

                        $ocorrencia['data'] = \DateTime::createFromFormat('d/m/Y H:i', $ocorrencia['data']);
                        $ocorrencia['rastreamento'] = $oRastreamento;
                        $ocorrenciaExists = $em->getRepository('Admin\Entity\RastreamentoOcorrencia')->findOneOrEmpty($ocorrencia);

                        if(!empty($ocorrenciaExists)){
                            $ocorrencia['id'] = $ocorrenciaExists['id'];
                        }else{
                            if(!in_array($oRastreamento, $sendEmail) && $oRastreamento->getEmail())
                                array_push($sendEmail, $oRastreamento);
                        }

                        $service = $this->getServiceLocator()->get($this->serviceOcorrencia);
                        if($entity = $service->save($ocorrencia)){

                        }

                    }
                }

            }catch (\Exception $ex){

            }



        }
        if(!empty($sendEmail)){
            $this->sendEmail($sendEmail);
        }
        die();
    }

    public function sendEmail($rastreamentos){
        $em = $this->getEm();
        foreach($rastreamentos as $oRastreamento){
            $destino = (strtolower($oRastreamento->getSituacao()) == 'entrega efetuada' ? $oRastreamento->getDestino() : $oRastreamento->getPosicao());

            $ocorrencias = $oRastreamento->getOcorrencias();
            $origem = null;
            $waypoints = array();
            foreach ($ocorrencias as $oOcorrencia){
                if(is_null($origem)){
                    $origem = @end(Google::getGeoCode($oOcorrencia->getLocal()));
                }else{
                    if($oOcorrencia->getLocal() != $origem && $oOcorrencia->getLocal() != $destino && $oOcorrencia->getLocal() != $oRastreamento->getPosicao()){
                        array_push($waypoints, @end(Google::getGeoCode($oOcorrencia->getLocal())));
                    }
                }

            }

            $config = $this->getServiceLocator()->get('config');
            $config = $config['google'];
            $url = 'https://maps.googleapis.com/maps/api/directions/json?origin='.urlencode($origem).'&destination='.urlencode($destino).'&waypoints='.urlencode(implode('|',$waypoints)).'&key='.$config['api_key'];

            $json = file_get_contents($url);
            $oRastreamento->setJson($json);
            $em->persist($oRastreamento);
            $em->flush();

            $json = json_decode($json, true);

            $img = 'http://maps.googleapis.com/maps/api/staticmap?size=500x500&path=enc:'.$json['routes'][0]['overview_polyline']['points'];
//            $data = file_get_contents($img);

            $base64 = $img;//'data:image/image/png;base64,' . base64_encode($data);
            $link = 'https://www.google.com.br/maps/dir/'.urlencode($origem).'/'.urlencode(implode('/',$waypoints)).'/'.$destino;
            $mail = $this->getServiceLocator()->get('Email');
            $config = $this->getServiceLocator()->get('config');
            $config = $config['sendemail'];

            $htmlContent = '
            <div class="row">
                <div class="col-md-8">
                    <div class="hpanel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="showhide"><i class="fa fa-chevron-up m-b"></i></a>
                            </div>
                        <p style="font-size: 18px"><b>Mapa</b></p> 
                        </div>
                        <div class="panel-body m-t">
                                <h3>Código: <b>'.$oRastreamento->getCodigo().'</b></h3>
                        <div class="col-xs-12">
                            <img src="'.$base64.'">
                            <br>
                            <a href="'.$link.'">Ver mapa</a>
                        </div>
                        <div id="directions-panel" class="m-t"></div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        </div>
                        <p style="font-size: 18px"><b>Andamento</b></p>
                    </div>
                    <div class="panel-body list m-t">
                        <div class="list-item-container">';


            foreach ($oRastreamento->getOcorrencias()  as $oOcorrencia){
                $htmlContent .= '
                            <div class="list-item">
                                <div class="row">
                                    <div class="col-md-4">';
                $date = $oOcorrencia->getData();
                $htmlContent .= $date->format('d/m/Y').'<br>'.$date->format('H:i');
                $htmlContent .= ' 
                                    </div>
                                    <div class="col-md-8">
                                        <strong>'.$oOcorrencia->getSituacao().'</strong>
                                        <br>
                                        '.$oOcorrencia->getLocal().'<br>
                                        '.$oOcorrencia->getDetalhes().'<br>
                                    </div>
                                </div>
                            </div>';
            }
            $htmlContent .= ' 
                        </div>
                    </div>
                </div>
            </div>';
            $mail->sendMail($config['from'], 'Order Track', $oRastreamento->getEmail(), $oRastreamento->getDescricao(), false, false, 'Rastreamento',  $htmlContent, null, $this->getEm(), $config);

        }
    }

    public function detailsAction(){
        $em = $this->getEm();
        $id = $this->params()->fromRoute('id', 0);
        $oRastreamento = $em->getRepository('Admin\Entity\Rastreamento')->find($id);

        $destino = (strtolower($oRastreamento->getSituacao()) == 'entrega efetuada' ? $oRastreamento->getDestino() : $oRastreamento->getPosicao());

        $ocorrencias = $oRastreamento->getOcorrencias();
        $origem = null;
        $waypoints = array();
        foreach ($ocorrencias as $oOcorrencia){
            if(is_null($origem)){
                $origem = $oOcorrencia->getLocal();
            }else{
                if($oOcorrencia->getLocal() != $origem && $oOcorrencia->getLocal() != $destino && $oOcorrencia->getLocal() != $oRastreamento->getPosicao()){
                    array_push($waypoints, $oOcorrencia->getLocal());
                }
            }

        }
        $config = $this->getServiceLocator()->get('config');
        $config = $config['google'];
        $this->getServiceLocator()->get('viewhelpermanager')->get('headScript')->appendFile($this->getRequest()->getBasePath().'/js/maps.js');
        $this->getServiceLocator()->get('viewhelpermanager')->get('headScript')->appendFile('https://maps.googleapis.com/maps/api/js?key='.$config['api_key']);
        //https://maps.googleapis.com/maps/api/directions/json?origin=Boston,MA&destination=Concord,MA&waypoints=Charlestown,MA|Lexington,MA&key=AIzaSyAKXClpdO8uXsuz_0soA8WYmplRV0eBRpA
        return new ViewModel(array('oRastreamento'=>$oRastreamento, 'origem'=>$origem, 'destino'=>$destino, 'waypoints'=>$waypoints));
    }

    public function aditionalParameters(){
        return array();
    }
}
