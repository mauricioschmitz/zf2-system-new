<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use NeuroSYS\DoctrineDatatables\TableBuilder;
use NeuroSYS\DoctrineDatatables\Renderer\TwigRenderer;
use Admin\Entity\PlanoAfiliado;
use Admin\Entity\AfiliadoExtrato;
use Base\Util\Util;
use Admin\Entity\AfiliadoPagamento;
use Admin\Entity\AfiliadoDistribuicao;

class AfiliadopagamentoController extends AbstractController {

    public function __construct() {
        $this->form = 'Admin\Form\AfiliadoPagamentoForm';
        $this->controller = 'afiliadopagamento';
        $this->route = 'adm/default';
        $this->entity = 'Admin\Entity\AfiliadoPagamento';
        $this->service = 'Admin\Service\AfiliadoPagamentoService';
        $this->tituloTela = 'Pagamentos';
    }

    public function indexAction() {
        $this->layout()->tituloTela = $this->tituloTela . ' | Lista';
        $em = $this->getEm();
        
        if(is_string($this->form))
            $form = new $this->form($em);
        else 
            $form = $this->form($em);
        
        $config = $em->getRepository('Admin\Entity\AfiliadoConfig')->findOneBy(array('ativo' => 1));
        
        $resultDiasSemana = $this->diasSemana();
        $segunda = $resultDiasSemana['segunda'];
        $domingo = $resultDiasSemana['domingo'];
        $essaSegunda = $resultDiasSemana['essaSegunda'];
        
        $totalSemana = $em->getRepository('Admin\Entity\AfiliadoPagamento')->somaValor($segunda,$domingo);
        
        //Cria a lista de todos que devem receber
        $lista = $em->getRepository('Admin\Entity\Afiliado')->findByDistribuir($domingo);
        $totalCotas = 0;
        foreach ($lista as $key => $oAfiliado) {
            $totalCotas += $oAfiliado->getCota();
        }
        if($totalCotas == 0){
            $totalCotas++;
        }
        //Calcula o valor das cotas
        $valorDistribuido = $this->params()->fromQuery('valor', 0);
        if(!$valorDistribuido)
            $valorDistribuido = $totalSemana['valor'] * ($config->getPercentualDistribuicao() / 100); //Determina que serão divididos % do valor de acordo com o configurado
        
        $form->setData(array('valor'=>$valorDistribuido));
        $valorPorCota = $valorDistribuido / $totalCotas;
        $valorPorCota = round($valorPorCota, 2, PHP_ROUND_HALF_DOWN);
        
        $pagou = $em->getRepository('Admin\Entity\AfiliadoDistribuicao')->findBy(array('dataPagamento'=>$essaSegunda));
        
        $dados = array('totalSemana'=>$totalSemana['valor'], 'segunda'=>$segunda, 'domingo'=>$domingo, 'totalCotas'=>$totalCotas, 'valorCotas'=>$valorPorCota, 'valorDistribuido'=>$valorDistribuido, 'valor'=>$this->params()->fromQuery('valor', 0), 'pagou'=>$pagou);
        
        if(empty($pagou))
            $this->getServiceLocator()->get('viewhelpermanager')->get('headScript')->appendFile($this->getRequest()->getBasePath() . '/min?g=datatables-js');
        
        return new ViewModel(array('form'=>$form, 'route' => $this->route, 'controller' => $this->controller, 'dados'=>$dados));
    }
    
    public function listAction()
    {
        $loader   = new \Twig_Loader_String();
        $renderer = new TwigRenderer(new \Twig_Environment($loader));
        $params = $this->params()->fromQuery();
        
        $builder = new TableBuilder($this->getEm($this->entity), $params, null, $renderer, array('situacao'=>1));
        
        $builder
            ->from('Admin\Entity\AfiliadoExtrato', 'ae')
            ->join('ae.afiliado', 'a')
            ->add('number', 'ae.id', null, array(
                    'template' => '<div class="dataTablesMore">+</div>'
                )
            )
            ->add('text', 'a.pessoaFisica', null, array(
                    'template'=> '{{ value.getNome() }}'
                )
            )
            ->add('text', 'ae.tipo', null, array(
                    'template' => '{{ value|capitalize|replace({ "c": "C" }) }}'
                )
            )
            ->add('text', 'a.cota')
            ->add('text', 'ae.valor', null, array(
                    'template' => '{{ value|number_format(2, ",", ".") }}'
                )
            )
            ->end();
         
        $response = $builder->getTable()
            ->getResponseArray(\NeuroSYS\DoctrineDatatables\Table::HYDRATE_ENTITY) // hydrate entity, defaults to array
            ;
        
        $result = new JsonModel($response);
        return $result;
        
    }
    
    public function calcularAction()
    {
        $em = $this->getEm();
        
        $config = $em->getRepository('Admin\Entity\AfiliadoConfig')->findOneBy(array('ativo' => 1));
        
        $resultDiasSemana = $this->diasSemana();
        $segunda = $resultDiasSemana['segunda'];
        $domingo = $resultDiasSemana['domingo'];
        $essaSegunda = $resultDiasSemana['essaSegunda'];
        
        //Apaga os calculados
        $qb = $em->createQueryBuilder();
        $q = $qb->delete('Admin\Entity\AfiliadoExtrato', 'e')
                ->where('e.data_pagamento = ?1')
                ->setParameter(1, $essaSegunda)

                ->getQuery();
        $q->execute();
        
        $qb = $em->createQueryBuilder();
        $q = $qb->update('Admin\Entity\AfiliadoExtrato', 'e')
                ->set('e.situacao', AfiliadoExtrato::$_situacao['calculada'])
                ->where('e.tipo = ?1')
                ->andWhere('e.data_pagamento >= ?2')
                ->andWhere('e.data_pagamento <= ?3')
                ->setParameter(1, AfiliadoExtrato::$_tipo['comissão'])
                ->setParameter(2, $segunda)
                ->setParameter(3, $domingo)

                ->getQuery();
        $q->execute();
        
        $totalSemana = $em->getRepository('Admin\Entity\AfiliadoPagamento')->somaValor($segunda,$domingo);
        if(is_null($totalSemana['valor']))
            $totalSemana['valor'] = 0; 
        //Cria a lista de todos que devem receber
        $lista = $em->getRepository('Admin\Entity\Afiliado')->findByDistribuir($domingo);
        $listaCopy = $lista;
        $listaCopy2 = $lista;
        $totalCotas = 0;
        foreach ($lista as $key => $oAfiliado) {
            $totalCotas += $oAfiliado->getCota();
        }
        if($totalCotas == 0){
            $totalCotas++;
        }
        //Calcula o valor das cotas
        $valorDistribuido = $this->params()->fromPost('valor');
                
        $valorPorCota = $valorDistribuido / $totalCotas;
        $valorPorCota = round($valorPorCota, 2, PHP_ROUND_HALF_DOWN);
        
        $valorCotas = 0;
        foreach ($listaCopy as $oAfiliado){
            $afiliadoExtrato = new AfiliadoExtrato();
            $afiliadoExtrato->setAfiliado($oAfiliado);
            $afiliadoExtrato->setTipo(AfiliadoExtrato::$_tipo['cota']);
            $afiliadoExtrato->setValor($valorPorCota * $oAfiliado->getCota());
            $afiliadoExtrato->setSituacao(AfiliadoExtrato::$_situacao['calculada']);

            $afiliadoExtrato->setDescricao(AfiliadoExtrato::$_descricao['cota'] . ' ' . $segunda->format('d/m/Y') . ' e ' . $domingo->format('d/m/Y'));
            $afiliadoExtrato->setDataPagamento($essaSegunda);

            $em->persist($afiliadoExtrato);
            $em->flush();
            
            $valorCotas += $valorPorCota * $oAfiliado->getCota();
        }
        
        $pagar = $this->params()->fromQuery('pagar', false);
        if($pagar){
            $qb = $em->createQueryBuilder();
            $q = $qb->update('Admin\Entity\AfiliadoExtrato', 'e')
                    ->set('e.situacao', AfiliadoExtrato::$_situacao['paga'])
                    ->andWhere('e.data_pagamento >= ?1')
                    ->andWhere('e.data_pagamento <= ?2')
                    ->setParameter(1, $segunda)
                    ->setParameter(2, $essaSegunda)

                    ->getQuery();
            $q->execute();
            
            $comissao = $em->getRepository('Admin\Entity\AfiliadoExtrato')->somaComissao(array('de'=>$segunda,'ate'=>$domingo));
            if(is_null($comissao['comissao']))
                $comissao['comissao'] = 0;
            
            
            $sobra = $totalSemana['valor']-($valorCotas+$comissao['comissao']);
            $afiliadoDistribuicao = new AfiliadoDistribuicao(array(
                'valorArrecadado' => $totalSemana['valor'],
                'valorCotas' => $valorCotas,
                'valorComissao' => $comissao['comissao'],
                'valorSobra'=>$sobra,
                'dataPagamento'=>$essaSegunda,
                'descricao' => 'Pagamentos referente a '.$segunda->format('d/m/Y').' até '.$domingo->format('d/m/Y')
            ));
            $em->persist($afiliadoDistribuicao);
            $em->flush();
            
            //Seta quem chegou ao limite COMO FIM
            foreach($listaCopy2 as $oAfiliado){
                $valorMaximo = ($oAfiliado->getValorInvestimento() * ($config->getPercentualRetorno() / 100));
                $saldo = $em->getRepository('Admin\Entity\AfiliadoExtrato')->saldo($oAfiliado, true, false);
                $saldo += $oAfiliado->getValorSacado();
                if($saldo >= $valorMaximo){
                    $oAfiliado->setAtivo(0);
                    $em->persist($oAfiliado);
                    $em->flush();
                    
                    $qb = $em->createQueryBuilder();
                    $q = $qb->update('Admin\Entity\AfiliadoExtrato', 'e')
                            ->set('e.ativo', 0)
                            ->andWhere('e.afiliado = ?1')
                            ->setParameter(1, $oAfiliado)

                            ->getQuery();
                    $q->execute();
                }
            }
            
            $this->flashMessenger()->addSuccessMessage('Pamento realizado com sucesso!');
        }else{
            $this->flashMessenger()->addSuccessMessage('Cálculo finalizado');
        }
        return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller), array('query'=>array('valor'=>$valorDistribuido)));
    }
    
    public function diasSemana(){
        $diaDaSemanaAtual = date('N');
        $segunda = new \DateTime(date('Y-m-d 00:00:00', strtotime("-".($diaDaSemanaAtual+6)." days",strtotime(date('Y-m-d'))))); 
        $domingo = new \DateTime(date('Y-m-d 23:59:59', strtotime("-".($diaDaSemanaAtual)." days",strtotime(date('Y-m-d'))))); 
        $essaSegunda = new \DateTime(date('Y-m-d 00:00:00', strtotime("-".($diaDaSemanaAtual-1)." days",strtotime(date('Y-m-d'))))); 
        return array('segunda'=>$segunda, 'domingo'=>$domingo, 'essaSegunda'=>$essaSegunda);
    }
    public function aditionalParameters() {
        
    }

}
