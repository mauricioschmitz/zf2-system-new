<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use NeuroSYS\DoctrineDatatables\TableBuilder;
use NeuroSYS\DoctrineDatatables\Renderer\TwigRenderer;

class PlanoController extends AbstractController
{
    public function __construct() {
        $this->form = 'Admin\Form\PlanoForm';
        $this->controller = 'plano';
        $this->route = 'adm/default';
        $this->service = 'Admin\Service\PlanoService';
        $this->entity = 'Admin\Entity\Plano';
        $this->money_elements  = array('valor', 'valor_saque');
        $this->tituloTela = 'Plano';
    }
    
    public function indexAction()
    {
        $this->layout()->tituloTela = $this->tituloTela . ' | Lista';
        
        $this->getServiceLocator()->get('viewhelpermanager')->get('headScript')->appendFile($this->getRequest()->getBasePath().'/min?g=datatables-js');
        
        return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller));
    }
    
    public function listAction()
    {
        $loader   = new \Twig_Loader_String();
        $renderer = new TwigRenderer(new \Twig_Environment($loader));
        $params = $this->params()->fromQuery();
       
        $builder = new TableBuilder($this->getEm($this->entity), $params, null, $renderer);
        $builder
            ->from('Admin\Entity\Plano', 'p')
            ->add('number', 'p.id', null, array(
                    'template' => '<div class="dataTablesMore">+</div>'
                )
            )
            ->add('text', 'p.titulo')
            ->add('text', 'p.valor', null, array(
                    'template' => '{{ value|number_format(2, ",", ".") }}'
                )
            )
            ->add('text', 'p.cota')
            ->add('text', 'p.percentualIndicacao')
            ->add('text', 'p.pontos')
            ->add('text', 'p.valorSaque', null, array(
                    'template' => '{{ value|number_format(2, ",", ".") }}'
                )
            )
            ->add("text", "p.ativo", null, array(
                    'template'=> '{% if value %}<span class="label label-success" title="Ativo">Ativo</span>{% else %}<span class="label label-danger" title="Inativo">Inativo</span>{% endif %}'
                )
            )
            ->add("text", "p.ativo", null, array(
                'template' => ''
                . '<div class="dropdown">
                    <button class="btn btn-icon btn-rounded btn-primary waves-effect dropdown-toggle" type="button" id="dropdownMenu{{values.id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-cog"></i></button>                   
                    <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu{{values.id}}">
                        <li><a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'edit')) . '/{{values.id}}"> <i class="fa fa-pencil"></i> Editar</a></li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'activeset')) . '/{{values.id}}/{% if value %}0{% else %}1{% endif %}"> <i class="fa fa-{% if value %}remove{% else %}check{% endif %}"></i> {% if value %}Desativar{% else %}Ativar{% endif %}</a>
                        </li>
                    </ul>
                  </div>'
            ))
            ->end()
            ;
         
        $response = $builder->getTable()
            ->getResponseArray() // hydrate entity, defaults to array
            ;
        
        $result = new JsonModel($response);
        return $result;
    }
    
    public function aditionalParameters(){
        $retorno = array();
        $id = $this->params()->fromRoute('id', 0);
        if($id){
            $em = $this->getEm();
            $plano = $em->getRepository('Admin\Entity\Plano')->findOneBy(array('id'=>$id));
            $retorno['plano'] = $plano;
        }
        return $retorno;
    }
}
