<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;

abstract class ImportXLSController extends AbstractController
{
    protected $parentEntity = '';
    protected $parentField = '';
    
    
    public function importAction()
    {
        $request = $this->getRequest();
        if($request->isPost()){
            $data = $request->getPost()->toArray();
            foreach($data['item'] as $item){
                $object = $this->getEm()->getRepository($this->parentEntity)->find($item);
                $link = $object->getLink();
                
                if(!ini_set('default_socket_timeout', 15)) echo "<!-- unable to change socket timeout -->";

                if (($handle = fopen($link, "r")) !== FALSE) {
                    $spreadsheet_data = array();
                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        $spreadsheet_data[] = $data;
                    }
                    fclose($handle);
                }
                
                $service = $this->getServiceLocator()->get($this->service);
                $resultado = $service->importXLS($object,$spreadsheet_data);
                
                if($resultado){
                    $this->flashMessenger()->addSuccessMessage($this->success_insert_message);
                } else {
                    $this->flashMessenger()->addErrorMessage($this->error_insert_message);  
                }
            }
            return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller));
        }else{
            $lista = $this->getEm()->getRepository($this->parentEntity)->findBy(array('ativo'=>1));
            $result = new ViewModel(array('lista'=>$lista, 'route'=>$this->route, 'controller'=>$this->controller));
            $result->setTemplate('admin/partial/importxls.phtml');
            $result->setTerminal(true);

            return $result;
        }
        
    }
}
