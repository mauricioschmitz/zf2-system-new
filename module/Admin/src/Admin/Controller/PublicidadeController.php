<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use NeuroSYS\DoctrineDatatables\TableBuilder;
use NeuroSYS\DoctrineDatatables\Renderer\TwigRenderer;
use Admin\Form\BuscaEstatisticaForm;

class PublicidadeController extends AbstractController
{
    public function __construct() {
        $this->form = 'Admin\Form\PublicidadeForm';
        $this->controller = 'publicidade';
        $this->route = 'adm/default';
        $this->service = 'Admin\Service\PublicidadeService';
        $this->entity = 'Admin\Entity\Publicidade';
        $this->external_objects  = array('espaco_publicidade'=>'Admin\Entity\EspacoPublicidade');
        $this->file_elements  = array('publicidades' => 'arquivo');
        $this->money_elements  = array('valor');
        $this->tituloTela = 'Publicidade';
        $this->data_elements = array('data_publicacao','data_expiracao');
        $this->int_fields = array('sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat');
        
        $this->scripts_styles = array(
            array('file'=>'/libs/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css', 'type'=>'headlink', 'function'=>'appendStylesheet'),
            array('file'=>'/libs/chartjs/Chart.min.js', 'type'=>'headscript', 'function'=>'appendFile'),
        );
    }
    
    public function indexAction()
    {
        $this->layout()->tituloTela = $this->tituloTela . ' | Lista';
        
        $this->getServiceLocator()->get('viewhelpermanager')->get('headScript')->appendFile($this->getRequest()->getBasePath().'/min?g=datatables-js');
        
        return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller));
    }
    
    public function listAction()
    {
        $loader   = new \Twig_Loader_String();
        $renderer = new TwigRenderer(new \Twig_Environment($loader));
        $params = $this->params()->fromQuery();
       
        $builder = new TableBuilder($this->getEm($this->entity), $params, null, $renderer);
        $builder
            ->from('Admin\Entity\Publicidade', 'p')
            ->join('p.espaco_publicidade', 'e')

            ->add('number', 'p.id', null, array(
                    'template' => '<div class="dataTablesMore">+</div>'
                )
            )
            ->add('text', 'e.titulo')
            ->add('text', 'p.titulo')
            ->add('text', 'p.valor', null, array(
                    'template' => '{{ value|number_format(2, ",", ".") }}'
                )
            )
            ->add('date', 'p.data_publicacao', 'p.data_publicacao', array(
                    'template' => '{% if value %}{{ values.data_publicacao|date("d/m/Y") }}{% endif %}'
                )
            )
            ->add('date', 'p.data_expiracao', 'p.data_expiracao', array(
                    'template' => '{% if value %}{{ values.data_expiracao|date("d/m/Y") }}{% endif %}'
                )
            )
            ->add("text", "p.ativo", null, array(
                    'template'=> '{% if value %}<span class="label label-success" title="Ativo">Ativo</span>{% else %}<span class="label label-danger" title="Inativo">Inativo</span>{% endif %}'
                )
            )
            ->add("text", "p.ativo", null, array(
                'template' => ''
                . '<div class="dropdown">
                    <button class="btn btn-icon btn-rounded btn-primary waves-effect dropdown-toggle" type="button" id="dropdownMenu{{values.id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-cog"></i></button>                   
                    <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu{{values.id}}">
                        <li><a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'edit')) . '/{{values.id}}"> <i class="fa fa-pencil"></i> Editar</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'estatisticas')) . '/{{values.id}}"> <i class="pe-7s-graph1"></i> Estatísticas</a></li>
                        <li>
                            <a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'activeset')) . '/{{values.id}}/{% if value %}0{% else %}1{% endif %}"> <i class="fa fa-{% if value %}remove{% else %}check{% endif %}"></i> {% if value %}Desativar{% else %}Ativar{% endif %}</a>
                        </li>
                    </ul>
                  </div>'
            ))
            ->end()
            ;
         
        $response = $builder->getTable()
            ->getResponseArray() // hydrate entity, defaults to array
            ;
        
        $result = new JsonModel($response);
        return $result;
    }
    
    public function aditionalParameters(){
        $retorno = array();
        $id = $this->params()->fromRoute('id', 0);
        if($id){
            $em = $this->getEm();
            $publicidade = $em->getRepository('Admin\Entity\Publicidade')->findOneBy(array('id'=>$id));
            $retorno['publicidade'] = $publicidade;
        }
        return $retorno;
    }
    
    public function estatisticasAction()
    {
        if($this->params()->fromPost('inicio') != null && $this->params()->fromPost('fim') != null){
            $inicio = implode('-', array_reverse(explode('/',$this->params()->fromPost('inicio'))));
            $fim = implode('-', array_reverse(explode('/',$this->params()->fromPost('fim'))));
        }else{
            $inicio = date('Y-m-01');
            $fim = date('Y-m-t', strtotime($inicio));
        }
        $em = $this->getEm();
        $form = new BuscaEstatisticaForm($em);
        $form->setData(array('inicio'=>implode('/', array_reverse(explode('-',$inicio))), 'fim'=>implode('/', array_reverse(explode('-',$fim)))));
        $publicidade = $em->getRepository('Admin\Entity\Publicidade')->find($this->params()->fromRoute('id'));
        
        $repository = $em->getRepository('Admin\Entity\ClickPublicidade');
        
        $lista = $repository->findByPeriodo($publicidade, $inicio.' 00:00:00', $fim.' 23:59:59');
        
        $this->scriptsStyles();
        
        return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller, 'form'=>$form, 'publicidade'=>$publicidade, 'lista'=>$lista));
    }
}
