<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use NeuroSYS\DoctrineDatatables\TableBuilder;
use NeuroSYS\DoctrineDatatables\Renderer\TwigRenderer;

class TextoController extends AbstractController
{
    public function __construct() {
        $this->form = 'Admin\Form\TextoForm';
        $this->controller = 'texto';
        $this->route = 'adm/default';
        $this->service = 'Admin\Service\TextoService';
        $this->entity = 'Admin\Entity\Texto';
        
        $this->tituloTela = 'Textos';
        $this->scripts_styles = array(
            array('content'=>'$(document).ready(function(){'
                                . '$(\'.summernote\').summernote({
                                    lang: \'pt-BR\',
                                    height: 200,
                                    placeholder: \'\',
                                    callbacks:{
                                        onImageUpload: function(files) {
                                            url = baseUrl+\'uploadeditor\';
                                            Scripts.sendFileSummernote(files[0], url, $(this));
                                        }
                                    }

                                });
                            });',
                'type'=>'headscript', 'function'=>'appendScript'),
        );
    }
    
    public function indexAction()
    {
        $this->layout()->tituloTela = $this->tituloTela . ' | Lista';
        $this->getServiceLocator()->get('viewhelpermanager')->get('headScript')->appendFile($this->getRequest()->getBasePath().'/min?g=datatables-js');
        
        return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller));
    }
    
    public function listAction()
    {
        $loader   = new \Twig_Loader_String();
        $renderer = new TwigRenderer(new \Twig_Environment($loader));
        $params = $this->params()->fromQuery();
       
        $builder = new TableBuilder($this->getEm($this->entity), $params, null, $renderer);
        $builder
            ->from('Admin\Entity\Texto', 't')
            ->add('number', 't.id', null, array(
                    'template' => '<div></div>'
                )
            )
            ->add('text', 't.titulo')     
            ->add("text", "t.ativo", null, array(
                'template' => ''
                . '<div class="dropdown">
                    <button class="btn btn-icon btn-rounded btn-primary waves-effect dropdown-toggle" type="button" id="dropdownMenu{{values.id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-cog"></i></button>                   
                    <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu{{values.id}}">
                        <li><a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'edit')) . '/{{values.id}}"> <i class="fa fa-pencil"></i> Editar</a></li>
                    </ul>
                  </div>'
            ))
            ->end()
            ;
         
        $response = $builder->getTable()
            ->getResponseArray() // hydrate entity, defaults to array
            ;
        
        $result = new JsonModel($response);
        return $result;
    }

    public function aditionalParameters(){
        return array();
    }
}
