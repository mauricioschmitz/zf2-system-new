<?php

namespace Admin\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use NeuroSYS\DoctrineDatatables\TableBuilder;
use NeuroSYS\DoctrineDatatables\Renderer\TwigRenderer;
use Admin\Entity\UpgradeAfiliado;
use Admin\Entity\PlanoAfiliado;
use Admin\Entity\AfiliadoPagamento;
use Admin\Entity\AfiliadoExtrato;

class UpgradeController extends AbstractController
{
    public function __construct() {
        $this->controller = 'upgrade';
        $this->form = 'Admin\Form\UpgradeForm';
        $this->route = 'adm/default';
        $this->entity = 'Admin\Entity\UpgradeAfiliado';
        $this->service = 'Admin\Service\UpgradeAfiliadoService';
        $this->tituloTela = 'Upgrade';
    }
    
    public function indexAction()
    {
        $em = $this->getEm();
        $this->layout()->tituloTela = $this->tituloTela . ' | Lista';
        
        $config = $this->getServiceLocator()->get('config');
        $config = $config['sendemail'];
        
        if(is_string($this->form))
            $form = new $this->form($em);
        else 
            $form = $this->form($em);
        
        $request = $this->getRequest();
        if($request->isPost()){
            $form->setData($request->getPost());
            
            if($form->isValid()){
                $plano = $em->getRepository('Admin\Entity\Plano')->find($this->params()->fromPost('plano_id'));
                $upgrade = new UpgradeAfiliado($plano->toArray());
                $upgrade->setAfiliado($this->layout()->afiliado);
                $upgrade->setInicioVigencia(new \DateTime('0000-00-00'));
                $upgrade->setFimVigencia(new \DateTime('0000-00-00'));
//                var_dump($upgrade);die();
                $em->persist($upgrade);
                $em->flush();
                if($upgrade->getId()){
                    $mail = $this->getServiceLocator()->get('Email');
                    $mail->sendMail($config['fromMail'], $config['fromName'], $this->layout()->afiliado->getUser()->getEmail(), $this->layout()->afiliado->getUser()->getName(), false, false, \Base\Util\Mail::$SOLICITARUPGRADE['assunto'],  array('mensagem'=>\Base\Util\Mail::$SOLICITARUPGRADE['mensagem'], 'variaveis'=>array('login'=>$this->layout()->afiliado->getUser()->getEmail(), 'link'=>$this->getEvent()->getRouter()->assemble(array(''), array('name'=>'adm', 'force_canonical'=>true)))), false, $em);

                    $this->flashMessenger()->addSuccessMessage('Solicitação enviada com sucesso!');
                    return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller, 'action'=>'informacoes', 'id'=>$upgrade->getId()));
                } else {
                    $this->flashMessenger()->addErrorMessage('Ocorreu um erro, tente novamente!');  
                }
            }
        }else{
            $this->getServiceLocator()->get('viewhelpermanager')->get('headScript')->appendFile($this->getRequest()->getBasePath().'/min?g=datatables-js');
            
            if(isset($this->layout()->afiliado)){
                $em = $this->getEm();
                $planos = $em->getRepository('Admin\Entity\Plano')->findBy(array('ativo'=>1), array('pontos'=>'ASC'));

                return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller, 'form'=>$form, 'params'=>array('planos'=>$planos, 'planosselect'=>true, 'valoratual'=>$this->layout()->afiliado->getValorInvestimento())));
            }else{
                return new ViewModel(array('route'=>$this->route, 'controller'=>$this->controller, 'form'=>$form));
            }
        }
    }
    
    public function listAction()
    {
        $loader   = new \Twig_Loader_String();
        $renderer = new TwigRenderer(new \Twig_Environment($loader));
        $params = $this->params()->fromQuery();
        
        if(isset($this->layout()->afiliado))
            $filtros = array('afiliado'=>$this->layout()->afiliado);
        else
            $filtros = array('situacao' => array('func'=>'neq', 'valor'=>1));
            
        $builder = new TableBuilder($this->getEm($this->entity), $params, null, $renderer, $filtros);
        $builder
            ->from('Admin\Entity\UpgradeAfiliado', 'u')
            ->join('u.afiliado', 'a')
            ->add('number', 'u.id', null, array(
                    'template' => '<div class="dataTablesMore">+</div>'
                )
            );
        
        if(!isset($this->layout()->afiliado)){
            $builder->add('text', 'a.pessoaFisica', null, array(
                    'template'=> '{{ value.getNome() }}'
                ));
        }
        
        $builder
            ->add('text', 'u.titulo')
            ->add("text", "u.situacao", null, array(
                    'template'=> '{% if value == 1 %}<span class="label label-success" title="Confirmado">Confirmado</span>{% else %}<span class="label label-warning" title="Pendente">Pendente</span>{% endif %}'
                )
            )
            
            ->add('text', 'u.valor', null, array(
                    'template' => '{{ value|number_format(2, ",", ",") }}'
                )
            
            );
        if(isset($this->layout()->afiliado)){
            $builder
            ->add("text", "u.situacao", null, array(
                'template' => ''
                .  '{% if values.situacao == 0 %}
                    <div class="dropdown">
                        <button class="btn btn-icon btn-rounded btn-primary waves-effect dropdown-toggle" type="button" id="dropdownMenu{{values.id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-cog"></i></button>                   
                        <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu{{values.id}}">
                            <li><a href="' . $this->url()->fromRoute($this->route, array('controller'=>'confirmarpagamento', 'action'=>'upgrade')).'"> <i class="fa fa-check"></i> Confirmar pagamento</a></li>
                        </ul>
                    </div>
                    {% endif %}'
            ));
        
        }else {
            $builder
            ->add("text", "u.situacao", null, array(
                'template' => ''
                .  '{% if value == 0 or value == 2 %}
                    <div class="dropdown">
                        <button class="btn btn-icon btn-rounded btn-primary waves-effect dropdown-toggle" type="button" id="dropdownMenu{{values.id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-cog"></i></button>                   
                        <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu{{values.id}}">
                            <li><a href="' . $this->url()->fromRoute($this->route, array('controller'=>$this->controller, 'action'=>'ativar')).'/{{values.id}}"> <i class="fa fa-check"></i> Aprovar pagamento</a></li>
                        </ul>
                    </div>
                    {% endif %}'
            ));
        }
        $builder->end();
//        var_dump($builder->getTable()->getCountAllResults());die();
        $response = $builder->getTable()
            ->getResponseArray(\NeuroSYS\DoctrineDatatables\Table::HYDRATE_ENTITY) // hydrate entity, defaults to array
            ;
        
        $result = new JsonModel($response);
        return $result;
    }
    
    public function informacoesAction(){
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $this->layout()->tituloTela = $this->tituloTela;
        $afiliado = $this->layout()->afiliado;
        
        $novoPlano = $em->getRepository('Admin\Entity\UpgradeAfiliado')->find($this->params()->fromRoute('id', ''));
        
        $valorDiferenca = $novoPlano->getValor()-$afiliado->getValorInvestimento();
        
        $texto = $em->getRepository('Admin\Entity\Texto')->findOneBy(array('apelido'=>'contas_deposito'));
        return new ViewModel(array('afiliado'=>$afiliado, 'texto' => $texto, 'valorDiferenca'=>$valorDiferenca));
    }
    
    public function ativarAction(){
        $em = $this->getEm();
        
        $config = $this->getServiceLocator()->get('config');
        $config = $config['sendemail'];
                
        $upgrade = $em->getRepository('Admin\Entity\UpgradeAfiliado')->find($this->params()->fromRoute('id', ''));
        
        $inicioVigencia = new \DateTime(date('Y-m-d'));
        $fimVigencia =  new \DateTime(date('Y-m-d', strtotime("+1 year",strtotime(date('Y-m-d'))))); 
        
        $afiliado = $upgrade->getAfiliado();
        
        $valorAtual = $afiliado->getValorInvestimento();
        $novoValor = $upgrade->getValor();
        $diferenca = $novoValor-$valorAtual;
        
        $afiliado->setNomePlano($upgrade->getTitulo());
        $afiliado->setValorInvestimento($upgrade->getValor());
        $afiliado->setCota($upgrade->getCota());
        $afiliado->setPercentualIndicacao($upgrade->getPercentualIndicacao());
        $afiliado->setPontos($upgrade->getPontos());
        $afiliado->setPontosPessoais($upgrade->getPontos());
        $afiliado->setValorSaque($upgrade->getValorSaque());
        $afiliado->setInicioVigencia($inicioVigencia);
        $afiliado->setFimVigencia($fimVigencia);
        $em->persist($afiliado);
        $em->flush();
        
        $planoAfiliado = new PlanoAfiliado($upgrade->toArray());
        $em->persist($planoAfiliado);
        $em->flush();
        
        $upgrade->setSituacao(1);
        $upgrade->setInicioVigencia($inicioVigencia);
        $upgrade->setFimVigencia($fimVigencia);
        $service = $this->getServiceLocator()->get($this->service);
        
        if($service->save($upgrade->toArray())){
            
            $afiliadoPagamento = new AfiliadoPagamento(array(
                'afiliado' => $afiliado,
                'descricao' => 'Upgrade Afiliado',
                'valor' => $diferenca
            ));
            $em->persist($afiliadoPagamento);
            $em->flush();
            
            //Paga comissão
            if ($afiliado->getIndicacao()) {
                //Pega o afiliado indicador
                $afiliadoIndicador = $em->getRepository('Admin\Entity\Afiliado')->find($afiliado->getIndicacao());
                //Pega o percentual que seu plano lhe paga por indicação
                $percentualIndicacao = $afiliadoIndicador->getPercentualIndicacao();
                //Calcula valor a pagar pela indicacao
                $valorPagarPorIndicacao = $diferenca * ($percentualIndicacao / 100);

                $afiliadoExtrato = new AfiliadoExtrato(array(
                    'afiliado'=>$afiliadoIndicador,
                    'tipo' => AfiliadoExtrato::$_tipo['comissão'],
                    'valor' => $valorPagarPorIndicacao,
                    'situacao' => AfiliadoExtrato::$_situacao['paga'],
                    'data_pagamento' => new \DateTime(date('Y-m-d')),
                    'descricao' => AfiliadoExtrato::$_descricao['comissão'] .' Upgrade -'. $afiliado->getPessoaFisica()->getNome() . ' Plano - ' . $afiliado->getNomePlano()
                    )
                );
                $em->persist($afiliadoExtrato);
                $em->flush();
                
                $mail = $this->getServiceLocator()->get('Email');
                $mail->sendMail($config['fromMail'], $config['fromName'], $afiliadoIndicador->getUser()->getEmail(), $afiliadoIndicador->getUser()->getName(), false, false, \Base\Util\Mail::$APROVARCOMISSAO['assunto'],  array('mensagem'=>\Base\Util\Mail::$APROVARCOMISSAO['mensagem'], 'variaveis'=>array('nome'=>$afiliadoIndicador->getPessoaFisica()->getNome())), false, $em);
            }
            
            $mail = $this->getServiceLocator()->get('Email');
            $mail->sendMail($config['fromMail'], $config['fromName'], $afiliado->getUser()->getEmail(), $afiliado->getUser()->getName(), false, false, \Base\Util\Mail::$APROVARPAGAMENTOUPGRADE['assunto'],  array('mensagem'=>\Base\Util\Mail::$APROVARPAGAMENTOUPGRADE['mensagem'], 'variaveis'=>array('login'=>$afiliado->getUser()->getEmail(), 'link'=>$this->getEvent()->getRouter()->assemble(array(''), array('name'=>'adm', 'force_canonical'=>true)))), false, $em);

            $this->flashMessenger()->addSuccessMessage('Upgrade aplicado com sucesso!');
        } else {
            $this->flashMessenger()->addErrorMessage('Ocorreu um erro!');  
        }

        return $this->redirect()->toRoute($this->route, array('controller'=>$this->controller));
    }
    
    public function aditionalParameters(){
        
    }
}
