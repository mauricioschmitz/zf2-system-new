<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Jornal;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Admin\Service\CategoriaService;
use Jornal\View\Helper\CategoriaHelper;
use Jornal\View\Helper\PublicidadeHelper;
use Jornal\View\Helper\NovoPublicidadeHelper;
use Jornal\View\Helper\PopupHelper;
use Jornal\View\Helper\NoticiaHelper;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        
        $eventManager->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) {
            $controller      = $e->getTarget();
            $controllerClass = get_class($controller);
            $moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
            $config          = $e->getApplication()->getServiceManager()->get('config');
            if (isset($config['module_layouts'][$moduleNamespace])) {
                $controller->layout($config['module_layouts'][$moduleNamespace]);
            }
        }, 100);
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        
        $sm = $e->getApplication()->getServiceManager();
 
        $sm->get('viewhelpermanager')->setFactory('CategoriaHelper', function ($sm) use ($e) {
            return new CategoriaHelper($e, $sm);
        });
        $sm->get('viewhelpermanager')->setFactory('PublicidadeHelper', function ($sm) use ($e) {
            return new PublicidadeHelper($e, $sm);
        });
        $sm->get('viewhelpermanager')->setFactory('NovoPublicidadeHelper', function ($sm) use ($e) {
            return new NovoPublicidadeHelper($e, $sm);
        });
        $sm->get('viewhelpermanager')->setFactory('PopupHelper', function ($sm) use ($e) {
            return new PopupHelper($e, $sm);
        });
        $sm->get('viewhelpermanager')->setFactory('NoticiaHelper', function ($sm) use ($e) {
            return new NoticiaHelper($e, $sm);
        });
        
        $config = $e->getApplication()->getServiceManager()->get('config');
        $layout = $e->getViewModel();
        $layout->GA_ID = $config['analytics']['GA_ID'];

        $em = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($em);
        $em->attach('route', array($this, 'doHttpsRedirect'));
    }
    
    public function doHttpRedirect(MvcEvent $e){
        $sm = $e->getApplication()->getServiceManager();
        $uri = $e->getRequest()->getUri();
        $scheme = $uri->getScheme();
        if ($scheme != 'http'){
            $uri->setScheme('http');
            $response=$e->getResponse();
            $response->getHeaders()->addHeaderLine('Location', $uri);
            $response->setStatusCode(302);
            $response->sendHeaders();
            return $response;
        }
    }

    public function doHttpsRedirect(MvcEvent $e){
        $sm = $e->getApplication()->getServiceManager();
        $uri = $e->getRequest()->getUri();
        $scheme = $uri->getScheme();
        if ($scheme != 'https'){
            $uri->setScheme('https');
            $response=$e->getResponse();
            $response->getHeaders()->addHeaderLine('Location', $uri);
            $response->setStatusCode(302);
            $response->sendHeaders();
            return $response;
        }
    }
    
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig(){
        
        return array(
            'factories' => array(
                'Admin\Service\CategoriaService' => function($em){
                    return new CategoriaService($em->get('Doctrine\ORM\EntityManager'));
                },
            ),
        );
    }
    
    public function getViewHelperConfig(){
        return array(
            'factories' => array(
                // a chave do array aqui é o nome pelo qual você
                // chamará o seu view helper no script da view
                'categoriaHelper' => function($sm, $e) {
                    $locator = $sm->getServiceLocator();
                    $service = $e->getApplication()->getServiceManager();
                    // $sm é o gerenciador de view helpers (view
                    // helper manager), então nós precisamos
                    // coletar o gerenciador de serviços principal
                    return new CategoriaHelper($e, $sm);
                },
                'publicidadeHelper' => function($sm, $e) {
                    $locator = $sm->getServiceLocator();
                    $service = $e->getApplication()->getServiceManager();
                    return new PublicidadeHelper($e, $sm);
                },
                'novoPublicidadeHelper' => function($sm, $e) {
                    $locator = $sm->getServiceLocator();
                    $service = $e->getApplication()->getServiceManager();
                    return new PublicidadeHelper($e, $sm);
                },
                'popupHelper' => function($sm, $e) {
                    $locator = $sm->getServiceLocator();
                    $service = $e->getApplication()->getServiceManager();
                    return new PopupHelper($e, $sm);
                },
                'noticiaHelper' => function($sm, $e) {
                    $locator = $sm->getServiceLocator();
                    $service = $e->getApplication()->getServiceManager();
                    return new NoticiaHelper($e, $sm);
                },
            ),
        );
    }
}
