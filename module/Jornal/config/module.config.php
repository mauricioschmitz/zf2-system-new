<?php
namespace Jornal;

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Jornal\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
                
            ),
            'imagemnoticia' =>array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/imagemnoticia',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Jornal\Controller',
                        'controller'    => 'Index',
                        'action'        => 'imagemnoticia',
                    )
                )
            ),
            'newsletter' =>array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/newsletter',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Jornal\Controller',
                        'controller'    => 'Newsletter',
                        'action'        => 'insert',
                    )
                )
            ),
            'noticia' =>array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/n',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Jornal\Controller',
                        'controller'    => 'Noticia',
                        'action'        => 'detalhe',
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:id[/:titulo]]',
                            'constraints' => array(
                                'id'         => '\d+',
//                                'titulo'     => '[0-9][a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    )
                ),
            ),
            'geraminis' =>array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/geraminis',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Jornal\Controller',
                        'controller'    => 'Noticia',
                        'action'        => 'geraminis',
                    )
                )
            ),
            'categoria' =>array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/c',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Jornal\Controller',
                        'controller'    => 'Noticia',
                        'action'        => 'listbycategoria',
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:id[/:titulo[/:page]]]',
                            'constraints' => array(
                                'id'         => '\d+',
                                'titulo'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'page'         => '\d+',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    )
                ),
            ),
            'publicidade' =>array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/publicidade',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Jornal\Controller',
                        'controller'    => 'Publicidade',
                        'action'        => 'index',
                    )
                )
            ),
            'contato' =>array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/contato',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Jornal\Controller',
                        'controller'    => 'Contato',
                        'action'        => 'index',
                    )
                )
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Jornal\Controller\Index' => 'Jornal\Controller\IndexController',
            'Jornal\Controller\Noticia' => 'Jornal\Controller\NoticiaController',
            'Jornal\Controller\Publicidade' => 'Jornal\Controller\PublicidadeController',
            'Jornal\Controller\Newsletter' => 'Jornal\Controller\NewsletterController',
            'Jornal\Controller\Contato' => 'Jornal\Controller\ContatoController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/jornal.phtml',
            'jornal/index/index' =>    __DIR__ . '/../view/jornal/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
            'partial/share'      => __DIR__ . '/../view/jornal/partial/share.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'TpMinify' => array(
        'serveOptions' => array(
            'minApp' => array(
                'groups' => array(
                    'jornal-css' => array(
                        getcwd() . '/public/libs/bootstrap/dist/css/bootstrap.min.css',
                        getcwd() . '/public/jornal/css/style.css',
                    ),
                    'jornal-js' => array(
                        getcwd() . '/public/libs/jquery/dist/jquery.min.js',
                        getcwd() . '/public/libs/bootstrap/dist/js/bootstrap.min.js',
                        getcwd() . '/public/js/google_analytics.js'
                    )
                )
            )
        ),
    ),
    'sendemail' => array(
        'destinatario' => 'gerente.alternativo@gmail.com.br',
    ),
    'analytics' => array(
        'GA_ID' => 'UA-70851580-1',
    ),
    'facebook' => array(
        'app_id' => '835833123227168',
        'app_secret' => '33630772f6e350ad5b4cceb424b75ff9',
        'default_graph_version' => 'v2.2',
        'fileUpload' => false, // optional
        'allowSignedRequest' => false, // optional, but should be set to false for non-canvas apps
        'groups_id' => array(
            'me'
//            '1433670350277690', //Portal do Cidadao
//            '100003907988999',//Flávio Pefil
//            '431069560427849',// Flavio Empresario
//            '666462990164028',//Vale Alternativo
//            '196034167453149', //Testes
//            '168206043365835' //StoreMais
        ),
        'vendor_dir' => __DIR__.'/../../../vendor/'
    ),
);
