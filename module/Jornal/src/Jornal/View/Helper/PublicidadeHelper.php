<?php
namespace Jornal\View\Helper;
use Jornal\View\Helper\AbstractJornalHelper;
use Admin\Entity\VisualizacaoPublicidade;
use Zend\Crypt\BlockCipher;

class PublicidadeHelper extends AbstractJornalHelper{
    
    protected  $exibidos = array();

    /**
     * Invoke Helper
     * @return boolean
     */
    public function __invoke($espaco = '', $url = '',$aClass = '') {
        $em = $this->getEntityManager();
        $result = null;
        $espacoPublicidade = $this->getEntityManager()->getRepository('Admin\Entity\EspacoPublicidade')->findOneBy(array('tamanho'=>$espaco));
        
        $tamanhos = explode('x', $espaco);
        
        if(!is_null($espacoPublicidade))
            $result = $this->getEntityManager()->getRepository('Admin\Entity\Publicidade')->findPublicidade($this->exibidos, $espacoPublicidade->getId());
        
        if(!is_null($result)){
            array_push($this->exibidos, $result->getId());
            
//            //Grava a visualização
//            $visualizacaoPublicidade = new VisualizacaoPublicidade();
//            $visualizacaoPublicidade->setPublicidade($result);
//            $visualizacaoPublicidade->setIp($_SERVER["REMOTE_ADDR"]);
//            $visualizacaoPublicidade->setDescricao("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
//            $em->persist($visualizacaoPublicidade);
//            $em->flush();
            
            $blockCipher = BlockCipher::factory('mcrypt', array(
                'algo' => 'blowfish',
                'mode' => 'cfb',
                'hash' => 'sha512'
            ));
            $blockCipher->setKey('encryption_id');
            $id = $blockCipher->encrypt($result->getId());
            $em->getConnection()->close();
            return '<a '.($result->getLink() ? 'href="'.$this->getView()->url('publicidade',array('controller'=>'publicidade', 'action'=>'index')).'?id='.$result->getId().'&link='.  urlencode($result->getLink()).'&o='.urlencode("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]").'"' : '').' target="_blank" class="'.$aClass.'"><img src="'.$url.$result->getArquivo().'" style="max-width:'.$tamanhos[0].'; max-height:'.$tamanhos[1].';"></a>';
            
        }
        
        $em->getConnection()->close();
        return '<a href="'.$this->getView()->url('contato').'" class="'.$aClass.'"><img src="'.$url.$espaco.'.jpg" data-no-retina="true"></a>';
       
    }
    
    /**
     * Invoke Helper With Cache
     * @return boolean
     */
//    public function __invoke() {
//        //Neste metodo você pode fazer o que quiser por ex: buscar um valor e retornar
//        $cache   = $this->getServiceManager()->get('cache');
//        $key    = 'categorias-menu';
//        $result = $cache->getItem($key, $success);
//        if (!$success) {
//            $result = $this->getEntityManager()->getRepository('Admin\Entity\Categoria')->findBy(array('ativo'=>1),array('cor'=>'ASC'));
//            $cache->setItem($key, $result);
//        }
//        return $result;
//       
//    }
 
    
}
?>