<?php
namespace Jornal\View\Helper;
use Jornal\View\Helper\AbstractJornalHelper;
use Admin\Entity\VisualizacaoPublicidade;
use Zend\Crypt\BlockCipher;

class NovoPublicidadeHelper extends AbstractJornalHelper{
    
    protected $exibidos = array();
    protected $adsensesExibidos = array();
    protected $adsenses = array("4828157222", "9870776820", "3824243225", "5300976422", "6777709625", "8254442826", "9731176023", "2207909224","3684642424", "5161375629");
    /**
     * Invoke Helper
     * @return string
     */
    public function __invoke($espaco = '', $url = '',$aClass = '', $qtd=3, $sort='RAND()') {
        if($qtd == -1) {
            $retorno = $this->recursive($espaco, $url, $aClass);
            return $retorno;
        }else {
            $count = 0;
            $em = $this->getEntityManager();
            $result = null;
            $espacoPublicidade = $this->getEntityManager()->getRepository('Admin\Entity\EspacoPublicidade')->findOneBy(array('tamanho' => $espaco));

            $tamanhos = explode('x', $espaco);

            for ($i = 1; $i <= $qtd; $i++) {
                if (!is_null($espacoPublicidade))
                    $result = $this->getEntityManager()->getRepository('Admin\Entity\Publicidade')->findPublicidade($this->exibidos, $espacoPublicidade->getId(), $sort);

                if (!is_null($result)) {
                    array_push($this->exibidos, $result->getId());

//                    $blockCipher = BlockCipher::factory('mcrypt', array(
//                        'algo' => 'blowfish',
//                        'mode' => 'cfb',
//                        'hash' => 'sha512'
//                    ));
//                    $blockCipher->setKey('encryption_id');
//                    $id = $blockCipher->encrypt($result->getId());
                    $id = $result->getId();
                    $em->getConnection()->close();
                    ${'item' . $i} = '<a ' . ($result->getLink() ? 'href="' . $this->getView()->url('home') . '?link_publicidade=true&id=' . $result->getId() . '&link=' . urlencode($result->getLink()) . '&o=' . urlencode("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") . '"' : '') . ' target="_blank" class="' . $aClass . '"><img src="' . $url . $result->getArquivo() . '" style="max-width:' . $tamanhos[0] . '; max-height:' . $tamanhos[1] . ';"></a>';
                    $count++;
                } else {
                    //${'item' . $i} = '<a href="' . $this->getView()->url('contato') . '" class="' . $aClass . '"><img src="' . $url . $espaco . '.jpg" data-no-retina="true"></a>';
                }
            }


//            $em->getConnection()->close();
            if (!$count) {
                $adsenses = $this->adsenses;
                return '<div class="item active">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <ins class="adsbygoogle"
                             style="display:block; width: ' . $tamanhos[0] . 'px;height:' . $tamanhos[1] . 'px"
                             data-ad-client="ca-pub-3532645017061750"
                             data-ad-slot="' . $adsenses[rand(0, 9)] . '"
                             data-ad-format="auto"></ins>
                        <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>';


            }
            $retorno = '';
            for ($i = 1; $i <= $qtd; $i++) {
                if(isset(${'item' . $i}))
                    $retorno .= '<div class="item ' . ($i == 1 ? 'active' : '') . '">' . ${'item' . $i} . '</div>';
            }

            return $retorno;
        }
    }

    /**
     * @return string
     */
    public function recursive($espaco = '', $url = '',$aClass = '', $retorno= '')
    {
        $em = $this->getEntityManager();
        $fecha = false;

        $result = null;
        $espacoPublicidade = $this->getEntityManager()->getRepository('Admin\Entity\EspacoPublicidade')->findOneBy(array('tamanho'=>$espaco));
        $tamanhos = explode('x', $espaco);

        if(!is_null($espacoPublicidade))
            $result = $this->getEntityManager()->getRepository('Admin\Entity\Publicidade')->findPublicidade($this->exibidos, $espacoPublicidade->getId());
        if(!is_null($result)) {
            array_push($this->exibidos, $result->getId());
//            return '';

//            $blockCipher = BlockCipher::factory('mcrypt', array(
//                'algo' => 'blowfish',
//                'mode' => 'cfb',
//                'hash' => 'sha512'
//            ));
//            $blockCipher->setKey('encryption_id');

            $id = $result->getId();
            $retorno .= '<div><a ' . ($result->getLink() ? 'href="' . $this->getView()->url('home') . '?link_publicidade=true&id=' . $result->getId() . '&link=' . urlencode($result->getLink()) . '&o=' . urlencode("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") . '"' : '') . ' target="_blank" class="' . $aClass . '"><img src="' . $url . $result->getArquivo() . '" style="max-width:' . $tamanhos[0] . '; max-height:' . $tamanhos[1] . ';"></a><br><br></div>';
        }else{
            //$retorno .= '<div><a href="'.$this->getView()->url('contato').'" class="'.$aClass.'"><img src="'.$url.$espaco.'.jpg" data-no-retina="true"></a><br><br></div>';
            $fecha = true;
        }
        if($fecha){
            echo $retorno;
            return $retorno;
        }else {
            $this->recursive($espaco, $url, $aClass, $retorno);
        }
    }
    /**
     * @return string
     */
    public function recursive2($espaco = '', $url = '',$aClass = '', $retorno= '') {

        $em = $this->getEntityManager();
        $fecha = false;
        $qtd = 0;

        $result = null;
        $espacoPublicidade = $this->getEntityManager()->getRepository('Admin\Entity\EspacoPublicidade')->findOneBy(array('tamanho'=>$espaco));
        $tamanhos = explode('x', $espaco);

        if(!is_null($espacoPublicidade))
            $result = $this->getEntityManager()->getRepository('Admin\Entity\Publicidade')->findPublicidade($this->exibidos, $espacoPublicidade->getId());
        if(!is_null($result)){
            array_push($this->exibidos, $result->getId());

            $blockCipher = BlockCipher::factory('mcrypt', array(
                'algo' => 'blowfish',
                'mode' => 'cfb',
                'hash' => 'sha512'
            ));
            $blockCipher->setKey('encryption_id');
            $id = $blockCipher->encrypt($result->getId());
//            $em->getConnection()->close();
            ${'item1'} = '<a '.($result->getLink() ? 'href="'.$this->getView()->url('home').'?link_publicidade=true&id='.$result->getId().'&link='.  urlencode($result->getLink()).'&o='.urlencode("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]").'"' : '').' target="_blank" class="'.$aClass.'"><img src="'.$url.$result->getArquivo().'" style="width:'.$tamanhos[0].'; height:'.$tamanhos[1].';"></a>';
            $qtd++;
        }else{

            ${'item1'} = '<a href="'.$this->getView()->url('contato').'" class="'.$aClass.'"><img src="'.$url.$espaco.'.jpg" data-no-retina="true"></a>';
            $qtd++;

            $fecha = true;
            $qtd++;
            $minimo = 6;
            $itens = $qtd;
//            if($qtd < $minimo){
//                for ($j = $itens; $j <= $minimo; $j++){
//                    ${'item'.$j} = '<div class="item active">
//                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
//                        <ins class="adsbygoogle"
//                             style="display:block; width: ' . $tamanhos[0] . 'px;height:' . $tamanhos[1] . 'px"
//                             data-ad-client="ca-pub-3532645017061750"
//                             data-ad-slot="' . $adsenses[rand(0, 9)] . '"
//                             data-ad-format="auto"></ins>
//                        <script>
//                                    (adsbygoogle = window.adsbygoogle || []).push({});
//                        </script>
//                    </div>';
//                    $qtd++;
//                }
//            }
        }

//        $em->getConnection()->close();
        for($i = 1; $i < $qtd; $i++){
            $retorno .= '<div class="item '.($i==1?'active':'').'" style="width:317px; height: 280px;">'.${'item'.$i}.'</div>';
        }
        if($fecha){
            return $retorno;
        }else {
            $this->recursive($espaco, $url, $aClass, $retorno);
        }
    }

}
?>