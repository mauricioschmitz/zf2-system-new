<?php
namespace Jornal\View\Helper;
use Jornal\View\Helper\AbstractJornalHelper;

class CategoriaHelper extends AbstractJornalHelper{
    
    /**
     * Invoke Helper
     * @return array of Admin\Entity\Categoria
     */
    public function __invoke() {
        $result = $this->getEntityManager()->getRepository('Admin\Entity\Categoria')->findBy(array('ativo'=>1),array('cor'=>'ASC'));
        $this->getEntityManager()->getConnection()->close();
        return $result;
       
    }
    
    /**
     * Invoke Helper With Cache
     * @return array of Admin\Entity\Categoria
     */
//    public function __invoke() {
//        //Neste metodo você pode fazer o que quiser por ex: buscar um valor e retornar
//        $cache   = $this->getServiceManager()->get('cache');
//        $key    = 'categorias-menu';
//        $result = $cache->getItem($key, $success);
//        if (!$success) {
//            $result = $this->getEntityManager()->getRepository('Admin\Entity\Categoria')->findBy(array('ativo'=>1),array('cor'=>'ASC'));
//            $cache->setItem($key, $result);
//        }
//        return $result;
//       
//    }
 
    
}
?>