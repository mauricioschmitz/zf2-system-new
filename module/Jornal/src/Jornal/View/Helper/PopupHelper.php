<?php
namespace Jornal\View\Helper;
use Jornal\View\Helper\AbstractJornalHelper;
use Admin\Entity\VisualizacaoPublicidade;
use Zend\Crypt\BlockCipher;

class PopupHelper extends AbstractJornalHelper{
    
    protected  $exibidos = array();

    /**
     * Invoke Helper
     * @return boolean
     */
    public function __invoke($espaco = '', $url = '',$aClass = '', $qtd=1) {
        if(!isset($_COOKIE['popupValeAlternativo'])) {


            $em = $this->getEntityManager();
            $result = null;
            $espacoPublicidade = $this->getEntityManager()->getRepository('Admin\Entity\EspacoPublicidade')->findOneBy(array('tamanho' => $espaco));
            $em->getConnection()->close();
            $tamanhos = explode('x', $espaco);

            if (!is_null($espacoPublicidade))
                $result = $this->getEntityManager()->getRepository('Admin\Entity\Publicidade')->findPublicidade($this->exibidos, $espacoPublicidade->getId());
//
            if (!is_null($result)) {
                array_push($this->exibidos, $result->getId());
                $id = $result->getId();
                $em->getConnection()->close();
                $conteudo = '<script type="text/javascript">
                                $("#fundoTransparente").fadeIn();
                                function fecharPop() {
                                    var minutes = 5;
                                    var name = "popupValeAlternativo";
                                    var value = "true";
                                    var date = new Date();
                                    date.setTime(date.getTime()+(minutes * 60 * 1000));
                                    var expires = "; expires="+date.toGMTString();
                                    document.cookie = name+"="+value+expires+"; path=/";
                                       
                                    $("#fundoTransparente").fadeOut();
                                    $("#popup").fadeOut();
                                }
                            </script>
                            <div id="popup" style="display: block;">
                                <div class="centraliza">
                                    <div class="imagem">
                                        <div class="fechar">
                                            <a href="javascript:fecharPop();">
                                                <img src="' . $this->getView()->basePath() . '/jornal/images/fechar.png">
                                            </a>
                                        </div>
                                        <div class="link" ' . ($result->getLink() ? 'onclick="window.open(\'' . $this->getView()->url('home') . '?link_publicidade=true&id=' . $result->getId() . '&link=' . urlencode($result->getLink()) . '&o=' . urlencode("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") . '\');"' : '') . '></div>
                                    </div>
                                </div>
                            </div>
                            <style>

                                #popup { display:none; height:100%; z-index:20; width:100%; position:fixed; }
                                #popup .centraliza { position:fixed; width:100%;height: 100%; }
                                #popup .centraliza .imagem { ' . ($result->getLink() ? 'cursor: pointer;' : '') . ' margin:200px 10%;position: relative; background-image:url(' . $url . $result->getArquivo() . '); width:584px; height:404px; background-repeat: no-repeat }
                                #popup .centraliza .imagem .link { width:584px; height:404px; }
                                #popup .centraliza .imagem .fechar { width:35px; margin:-15px -15px 0 0; float:right; height:34px; position: absolute; right: 0px; z-index: 999999999}
                                @media (max-width: 768px) {
                                    #popup .centraliza .imagem{
                                        background-size: 100% auto; width:90%; height: 100%;
                                        margin: 100px auto;
                                    }
                                    
                                    #popup .centraliza .imagem .link { width:90%; height:100%; }
                                }
                            </style>';

//
            }else{
                $conteudo = '';
            }
        }else{
            $conteudo = '';
        }

        return $conteudo;
    }
    

    public function aa(){

    }
    
}
?>