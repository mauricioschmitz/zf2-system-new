<?php

namespace Jornal\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Element;
use Admin\Form\Filter\CategoriaFilter;
/**
 * Description of CategoriaForm
 *
 * @author mauricioschmitz
 */
class ContatoForm extends AbstractForm {

    public function __construct() {
        parent::__construct(null);

        $this->setInputFilter(new CategoriaFilter());
        //Input nome
        $nome = new Element\Text('titulo');
        $nome->setLabel('Título')
                ->setAttributes(array(
                    'maxlenght' => 255,
                    'class' => 'md-input'
        ));
        $this->add($nome);

        //Input cor
        $cor = new Element\Radio('cor');
        $cor->setLabel('Cor')
                ->setOptions(
                array(
                    'value_options' => array(
                        '0' => '0',
                        '1' => '1',
                        '2' => '2',
                        '3' => '3',
                        '4' => '4',
                        '5' => '5',
                        '6' => '6',
                        '7' => '7',
                        '8' => '8',
                        '9' => '9',
                        '10' => '10'
                    )
                )
        );
        $this->add($cor);

        //Input destaque
        $destaque = new Element\Checkbox('destaque');
        $destaque->setLabel('Destaque')
                ->setAttributes(array(
                    'class' => 'has-value'
                    
        ))
        ->setCheckedValue(1);

        $this->add($destaque);

        //Botão submit
        $button = new Element\Button('submit');
        $button->setLabel('Salvar')
                ->setAttributes(array(
                    'type' => 'submit',
                    'class' => 'btn tbn-default'
        ));

        $this->add($button);
    }

   
}
