<?php

namespace Jornal\Controller;

use Zend\Mvc\Controller\AbstractActionController;

abstract class AbstractJornalController extends AbstractActionController
{
    protected $em;
    protected $tituloTela;
    protected $route;
    protected $controller;
    
    protected $scripts_styles = array();
    
    abstract public function __construct();
    
    public function imagemnoticiaAction(){
        $request = $this->getRequest();
        $imagem = base64_decode($this->params()->fromQuery('imagem', ''));
        
        $w = $this->params()->fromQuery('w', 0);
        $h = $this->params()->fromQuery('h', 0);

        $extension = pathinfo($imagem, PATHINFO_EXTENSION);        
        if($extension == 'png' || $extension == 'pneg')
            $renderImage = imagecreatefrompng($imagem);
        else
            $renderImage = imagecreatefromjpeg($imagem);
        
        if($w && $h){
            $dst_x = 0;   // X-coordinate of destination point
            $dst_y = 0;   // Y-coordinate of destination point
            $src_x = 0;   // Crop Start X position in original image
            $src_y = 0;   // Crop Srart Y position in original image
            $dst_w = $w; // Thumb width
            $dst_h = $h; // Thumb height
            $src_w = $w; // Crop end X position in original image
            $src_h = $h; // Crop end Y position in original image

            // Creating an image with true colors having thumb dimensions (to merge with the original image)
            $dst_image = imagecreatetruecolor($dst_w, $dst_h);
            // Cropping
            imagecopyresampled($dst_image, $renderImage, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
            $renderImage = $dst_image;
        }
        // Saving
        imagejpeg($renderImage);
        
    }
    
    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEm(){
        if($this->em == null){
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
//        if (!$this->em->isOpen()) {
//            $this->em = $this->em->create(
//              $this->em->getConnection(), $this->em->getConfiguration());
//        }
         
        return $this->em;
    }
        
    /**
     * @return String
     */
    public function getTituloTela(){
        return $this->tituloTela;
    }
    
    /**
     * @return String
     */
    public function arrayToJsString(array $array){
        $retorno = '[';
        $aux = '';
        foreach($array as $element){
            $retorno .= $aux."'".$element."'";
            $aux = ',';
        }
        $retorno .= ']';
        return $retorno;
    }
    
    /**
     * @return String
     */
    public static function titleLink($str){
        $str = strtolower(utf8_decode($str)); $i=1;
        $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
        $str = preg_replace("/([^a-z0-9])/",'-',utf8_encode($str));
        while($i>0) $str = str_replace('--','-',$str,$i);
        if (substr($str, -1) == '-') $str = substr($str, 0, -1);
        return $str;
    }
    
    public function scriptsStyles(){
        if(count($this->scripts_styles) > 0){
            foreach($this->scripts_styles as $script_style){
                if(isset($script_style['file']))
                    $this->getServiceLocator()->get('viewhelpermanager')->get($script_style['type'])->{$script_style['function']}($this->request->getBasePath() . $script_style['file']);
                else if(isset($script_style['content']))    
                    $this->getServiceLocator()->get('viewhelpermanager')->get($script_style['type'])->{$script_style['function']}($script_style['content']);
            }
        }
    }
    
}
