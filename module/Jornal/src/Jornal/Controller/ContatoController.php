<?php

namespace Jornal\Controller;

use Jornal\Controller\AbstractJornalController;
use Zend\View\Model\ViewModel;
use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail as SendmailTransport;

class ContatoController extends AbstractJornalController {

    public function __construct() {
        
    }

    public function indexAction() {
        $this->layout()->headTitle = 'Contato';
        $this->layout()->current_menu = 'contato';
        $request = $this->getRequest();
        
        if($request->isPost()){
            $dados = $request->getPost()->toArray();
            
            if($dados['cf_email'] == '' ||  $dados['cf_name'] == '' || strlen($dados['cf_message']) < 20){
                echo '0';
                die();
            }
            $config = $this->getServiceLocator()->get('config');
            $config = $config['sendemail'];

            
            $message = new Message();
            $message->setEncoding("UTF-8");
            $message->addFrom($dados['cf_email'], $dados['cf_name'])
                    ->addTo($config['destinatario'])
                    ->setSubject($dados['cf_subject']);
            $message->setBody($dados['cf_message']);
            
//            $message->addCc("")
//                    ->addBcc("");
            
            $message->addReplyTo($dados['cf_email'], $dados['cf_name']);
            
            $transport = new SendmailTransport();
            try{
                $transport->send($message);
                $retorno = '1';
            } catch (Exception $ex) {
                $retorno = '0';
            }
            
            echo $retorno;
            die();
        }
        $this->layout()->description = 'Vale Alternativo - Contato';
        $this->layout()->dateTime = date('Y-m-d H:i:s');
        return new ViewModel();
    }

}
