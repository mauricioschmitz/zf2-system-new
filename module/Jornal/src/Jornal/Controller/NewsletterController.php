<?php

namespace Jornal\Controller;

use Jornal\Controller\AbstractJornalController;
use Zend\View\Model\JsonModel;
use Admin\Entity\Newsletter;

class NewsletterController extends AbstractJornalController {
    
    public function __construct() {
        $this->route = 'newsletter';
        $this->controller = 'newsletter';
    }

    public function indexAction() {
        
    }

    public function insertAction() {
        $em = $this->getEm();
        $repository = $em->getRepository('Admin\Entity\Newsletter');
        $email = $repository->findOneBy(array('email'=>$this->params()->fromPost('newsletter-email')));
        
        if(!empty($email) && !is_null($email->getEmail())){
            $retorno = '0';
        }else{
        
            $newsletter = new Newsletter();
            $newsletter->setEmail($this->params()->fromPost('newsletter-email'));

            $em->persist($newsletter);
            $em->flush();
            if($newsletter->getId())
                $retorno = '1';
            else
                $retorno = '0';
        }
        $em->getConnection()->close();
        return new JsonModel(array($retorno));
    }
    
   
}
