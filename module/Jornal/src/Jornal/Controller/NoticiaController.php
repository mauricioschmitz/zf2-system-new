<?php

namespace Jornal\Controller;

use Jornal\Controller\AbstractJornalController;
use Zend\View\Model\ViewModel;
use Admin\Entity\VisualizacaoNoticia;

class NoticiaController extends AbstractJornalController {
    
    public function __construct() {
        $this->route = 'noticia';
        $this->controller = 'noticia';
    }

    public function indexAction() {
        
    }

    public function detalheAction() {
        $this->scripts_styles = array(
            array('file'=>'/libs/jackbox/jackbox-packed.min.js', 'type'=>'headscript', 'function'=>'appendFile'),
            array('file'=>'/libs/jackbox/jackbox.min.css', 'type'=>'headlink', 'function'=>'appendStylesheet'),
            array('content'=>'// jackbox
(function ($) {
    "use strict";

    $(function () {
        if ($(".jackbox[data-group]").length) {
            jQuery(".jackbox[data-group]").jackBox("init", {
                showInfoByDefault: false,
                preloadGraphics: false,
                fullscreenScalesContent: true,
                autoPlayVideo: false,
                flashVideoFirst: false,
                defaultVideoWidth: 960,
                defaultVideoHeight: 540,
                baseName: ".jackbox",
                className: ".jackbox",
                useThumbs: true,
                thumbsStartHidden: false,
                thumbnailWidth: 75,
                thumbnailHeight: 50,
                useThumbTooltips: false,
                showPageScrollbar: false,
                useKeyboardControls: true
            });
        }
    });
})(jQuery);        
    ', 'type'=>'headscript', 'function'=>'appendScript')
        );
        
        $em = $this->getEm();
        $repository = $em->getRepository('Admin\Entity\Noticia');
        
        $noticia = $repository->findOneBy(array('id'=>$this->params()->fromRoute('id', 0)));
        array_push($repository->exibidos,$noticia->getId());
        if(!$noticia || $noticia->getStatus() == 'suspenso' || !$noticia->getStatus()){
            return $this->redirect()->toRoute('home');
        }
        
        //Grava Visualizacao da noticia
//        $visNoticia = new VisualizacaoNoticia();
//        $visNoticia->setNoticia($noticia);
//        $visNoticia->setIp($_SERVER["REMOTE_ADDR"]);
//        $visNoticia->setDescricao("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
//        $em->persist($visNoticia);
//        $em->flush();
        
        $this->layout()->headTitle = $noticia->getTitulo();
        $this->layout()->current_menu = $noticia->getCategoria()->getCor();
        
        $vejaTambem = $repository->findByNivel('',$noticia->getCategoria(), 4);
        $ultimas = $repository->findByNivel('',null, 4);
        
        $noticias['vejaTambem'] = $vejaTambem;
        $noticias['ultimas'] = $ultimas;
        
        //Seta as metas para o Facebook
        $this->layout()->description = ($noticia->getChamada() != '' ? $noticia->getChamada() : $this->limitaStr(strip_tags(str_replace('%Publicidades%', '', $noticia->getDescricao())), 200));
        $this->layout()->categoriaArquivo = $noticia->getCategoria()->getTitulo();
        $this->layout()->dateTime = $noticia->getDataPublicacao()->format('Y-m-d H:i:s');
        $this->layout()->tags = $noticia->getTag();
        if(!$noticia->getImagens()->isEmpty()){
            $this->layout()->image = $noticia->getImagens()->first()->getArquivo();
        }
        
        $config = $this->getServiceLocator()->get('config');
        $config = $config['facebook'];
        
        $detalhes = explode('%Publicidades%', $noticia->getDescricao());
        
        $this->scriptsStyles();
        $em->getConnection()->close();
        return new ViewModel(array('noticia' => $noticia, 'noticias' => $noticias, 'config'=>$config, 'detalhes'=>$detalhes));
    }

    public function listbycategoriaAction(){
        $em = $this->getEm();
        $repository = $em->getRepository('Admin\Entity\Categoria');
        $categoria = $repository->find($this->params()->fromRoute('id', 0));
        
        $this->layout()->headTitle = $categoria->getTitulo();
        $this->layout()->current_menu = $categoria->getCor();
        
        $repository = $em->getRepository('Admin\Entity\Noticia');
        $page = $this->params()->fromRoute('page')-1;
        if($page < 0) $page = 0;
        $limit = 20;
        $noticias = $repository->getPagedNoticia($categoria,$page,$limit);
        
        $ultimas = $repository->findByNivel('',null, 4);
        
        $outrasNoticias['ultimas'] = $ultimas;
        
        if(!$noticias)
            return $this->redirect()->toRoute('home');
        
        $total = count($noticias);
        $qtdPg = ceil($total/$limit);
        
        $this->layout()->description = 'Vale Alternativo - '.$categoria->getTitulo();
        $this->layout()->dateTime = date('Y-m-d H:i:s');
        $em->getConnection()->close();
        return new ViewModel(array('categoria'=>$categoria, 'noticias'=>$noticias, 'page'=>$page, 'total'=>$total, 'qtdPg'=>$qtdPg, 'outrasNoticias' => $outrasNoticias));
    }
    
    function limitaStr($str, $limit){
        if (strlen($str)>$limit){
            $str = substr($str,0,$limit);
            $ultChr = strrpos($str,' ');
            $str = substr($str,0,$ultChr).'...';
        }
        return $str;
    }
    
    public function geraminisAction(){
        $em = $this->getEm();
        $todas = $em->getRepository('Admin\Entity\ImagemNoticia')->findBy(array('exportada'=>0));
        $qtd = count($todas);
        $imagemNoticia = $em->getRepository('Admin\Entity\ImagemNoticia')->findOneBy(array('exportada'=>0), array('id'=>'desc'));
        
        $folder = getcwd() . '/public/uploads/noticias/';
        $imagem = $imagemNoticia->getArquivo();
        $w = 160;
        $h = 110;

        $extension = pathinfo($imagem, PATHINFO_EXTENSION);  
        $nomeThumb = str_replace('.'.$extension, '_Fixed.'.$extension, $imagem);
        
        if($extension == 'png' || $extension == 'pneg')
            $renderImage = imagecreatefrompng($folder.$this->nomeImagemMini($imagem));
        else
            $renderImage = imagecreatefromjpeg($folder.$this->nomeImagemMini($imagem));
        
        if($w && $h){
            $dst_x = 0;   // X-coordinate of destination point
            $dst_y = 0;   // Y-coordinate of destination point
            $src_x = 0;   // Crop Start X position in original image
            $src_y = 0;   // Crop Srart Y position in original image
            $dst_w = $w; // Thumb width
            $dst_h = $h; // Thumb height
            $src_w = $w; // Crop end X position in original image
            $src_h = $h; // Crop end Y position in original image

            // Creating an image with true colors having thumb dimensions (to merge with the original image)
            $dst_image = imagecreatetruecolor($dst_w, $dst_h);
            // Cropping
            imagecopyresampled($dst_image, $renderImage, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
            $renderImage = $dst_image;
        }
        // Saving
        if(imagejpeg($renderImage, $folder.$nomeThumb, 100)){
            $imagemNoticia->setExportada(1);
            $em->persist($imagemNoticia);
            $em->flush();
//            $nomeApagar = str_replace('.'.$extension, '_220.'.$extension, $imagem);
//            @unlink($folder.$nomeApagar);
        }
        
        echo 'Faltam: '.$qtd;
        echo '<script>setTimeout(function(){window.location.href = window.location},500);</script>';
        die();
    }
    
    public function nomeImagemMini($imagem){
        $ext_img = explode('.', $imagem);
        $ext = '.'.$ext_img[count($ext_img)-1];
        return str_replace($ext, '_220'.$ext, $imagem);

    }
}
