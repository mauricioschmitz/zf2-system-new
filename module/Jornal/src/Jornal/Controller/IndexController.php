<?php

namespace Jornal\Controller;

use Admin\Entity\ClickPublicidade;
use Jornal\Controller\AbstractJornalController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractJornalController {

    public function __construct() {
        $this->scripts_styles = array(
            array('file'=>'/libs/owl-carousel/owl.carousel.min.js', 'type'=>'headscript', 'function'=>'appendFile'),
            array('file'=>'/libs/owl-carousel/owl.carousel.css', 'type'=>'headlink', 'function'=>'appendStylesheet'),
            array('content'=>'$("#owl-demo-5").owlCarousel({
	      items : 5,
	      navSpeed: 800,
	      nav : true,
	      navText:false,
	      loop:true,
	      autoplay: true,
	      autoplaySpeed: 800,
	        responsive:{
		        0:{
		            items:1
		        },
		        481:{
		            items:3
		        },
		        992:{
		            items:5
		        }
		    }
	  	});', 'type'=>'headscript', 'function'=>'appendScript')
        );
    }

    public function indexAction() {
        if(isset($_GET['link_publicidade']) && $_GET['link_publicidade'] == 'true'){
            ini_set('display_errors', 1);
            $id = $this->params()->fromQuery('id');

            $em = $this->getEm();
            $result = $em->getRepository('Admin\Entity\Publicidade')->findOneBy(array('id'=>$this->params()->fromQuery('id')));
            if(!is_null($result)){
                //Grava o click
                $clickPublicidade = new ClickPublicidade();
                $clickPublicidade->setPublicidade($result);
                $clickPublicidade->setIp($_SERVER["REMOTE_ADDR"]);
                $clickPublicidade->setDescricao(urldecode($this->params()->fromQuery('origem')));
                $em->persist($clickPublicidade);
                $em->flush();
                $em->getConnection()->close();
                return $this->redirect()->toUrl(urldecode($this->params()->fromQuery('link')));

            }else{
                $em->getConnection()->close();
                return $this->redirect()->toRoute('home');
            }
            die;
        }

        $this->layout()->headTitle = 'Home';
        $this->layout()->current_menu = 'capa';

        $em = $this->getEm();
        $em->getConnection()->close();
        $repository = $em->getRepository('Admin\Entity\Noticia');
        $banners = $repository->findByNivel('A', null, 5);
        $destaques = $repository->findByNivel('B',null, 8);
        
        $ultimas = $repository->findByNivel('',null, 6);
        $categoriaRepository = $em->getRepository('Admin\Entity\Categoria');
        $categorias = $categoriaRepository->getHomeDestaques();

        $categoriasDestaque = array();
        foreach($categorias as $i=>$categoria){
            $categoriasDestaque[$i]['categoria'] = $categoria;
            $categoriasDestaque[$i]['noticias'] = $repository->findByNivel('',$categoria, 6);
        }

//        $centro = $repository->findByNivel('C',null, 4);
        
        $noticias['banners'] = $banners;
        $noticias['destaques'] = $destaques;
        $noticias['categoriasDestaque'] = $categoriasDestaque;
        $noticias['ultimas'] = $ultimas;
//        $noticias['centro'] = $centro;
        
//        $noticia = $noticias['banners'][0];
        $this->layout()->description = 'Vale Alternativo';
        $this->layout()->dateTime = date('Y-m-d H:i:s');
        
        $this->scriptsStyles();
        $em->getConnection()->close();
        return new ViewModel(array('noticias' => $noticias));
    }

}
