<?php

namespace Jornal\Controller;

use Jornal\Controller\AbstractJornalController;
use Admin\Entity\ClickPublicidade;
use Zend\Crypt\BlockCipher;        


class PublicidadeController extends AbstractJornalController {

    public function __construct() {
        
    }

    public function indexAction() {
        
//        $blockCipher = BlockCipher::factory('mcrypt', array(
//            'algo' => 'blowfish',
//            'mode' => 'cfb',
//            'hash' => 'sha512'
//        ));
//        $blockCipher->setKey('encryption_id');
//        $id = $blockCipher->decrypt($this->params()->fromQuery('id'));
        $id = $this->params()->fromQuery('id');

        $em = $this->getEm();
        $result = $em->getRepository('Admin\Entity\Publicidade')->findOneBy(array('id'=>$this->params()->fromQuery('id')));
        if(!is_null($result)){
            //Grava o click
            $clickPublicidade = new ClickPublicidade();
            $clickPublicidade->setPublicidade($result);
            $clickPublicidade->setIp($_SERVER["REMOTE_ADDR"]);
            $clickPublicidade->setDescricao(urldecode($this->params()->fromQuery('origem')));
            $em->persist($clickPublicidade);
            $em->flush();
            $em->getConnection()->close();
            return $this->redirect()->toUrl(urldecode($this->params()->fromQuery('link')));
            
        }else{
            $em->getConnection()->close();
            return $this->redirect()->toRoute('home');
        }
        
    }

}
