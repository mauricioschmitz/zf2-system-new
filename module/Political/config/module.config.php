<?php
namespace Political;

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Political\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
                
            ),
            'about' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/about',
                    'defaults' => array(
                        'controller' => 'Political\Controller\Index',
                        'action'     => 'about',
                    ),
                ),
                
            ),
            'work' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/work',
                    'defaults' => array(
                        'controller' => 'Political\Controller\Index',
                        'action'     => 'work',
                    ),
                ),
                
            ),
            'videos' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/videos',
                    'defaults' => array(
                        'controller' => 'Political\Controller\Index',
                        'action'     => 'videos',
                    ),
                ),
                
            ),
            'projetos' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/projetos',
                    'defaults' => array(
                        'controller' => 'Political\Controller\Index',
                        'action'     => 'projetos',
                    ),
                ),
                
            ),
            'depoimentos' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/depoimentos',
                    'defaults' => array(
                        'controller' => 'Political\Controller\Index',
                        'action'     => 'depoimentos',
                    ),
                ),
                
            ),
            'contato' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/contato',
                    'defaults' => array(
                        'controller' => 'Political\Controller\Index',
                        'action'     => 'contato',
                    ),
                ),
                
            ),
            'download' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/download',
                    'defaults' => array(
                        'controller' => 'Political\Controller\Index',
                        'action'     => 'download',
                    ),
                ),
                
            ),
            'newsletter' =>array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/newsletter',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Political\Controller',
                        'controller'    => 'Newsletter',
                        'action'        => 'insert',
                    )
                )
            ),
            'noticia' =>array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/post',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Political\Controller',
                        'controller'    => 'Noticia',
                        'action'        => 'detalhe',
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:id[/:titulo]]',
                            'constraints' => array(
                                'id'         => '\d+',
//                                'titulo'     => '[0-9][a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    )
                ),
            ),
//            'geraminis' =>array(
//                'type'    => 'Literal',
//                'options' => array(
//                    'route'    => '/geraminis',
//                    'defaults' => array(
//                        '__NAMESPACE__' => 'Jornal\Controller',
//                        'controller'    => 'Noticia',
//                        'action'        => 'geraminis',
//                    )
//                )
//            ),
            'categoria' =>array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/c',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Political\Controller',
                        'controller'    => 'Noticia',
                        'action'        => 'listbycategoria',
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:id[/:titulo[/:page]]]',
                            'constraints' => array(
                                'id'         => '\d+',
                                'titulo'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'page'         => '\d+',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    )
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Political\Controller\Index' => 'Political\Controller\IndexController',
            'Political\Controller\Noticia' => 'Political\Controller\NoticiaController',
            'Political\Controller\Newsletter' => 'Political\Controller\NewsletterController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/political.phtml',
            'political/index/index' =>    __DIR__ . '/../view/political/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
            'partial/sidebar'      => __DIR__ . '/../view/political/partial/sidebar.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'TpMinify' => array(
        'serveOptions' => array(
            'minApp' => array(
                'groups' => array(
                    'political-css' => array(
//                        getcwd() . '/public/jornal/css/style.css',
                    ),
                    'political-js' => array(
//                        getcwd() . '/public/js/google_analytics.js'
                    )
                )
            )
        ),
    ),
    'sendemail' => array(
        'destinatario' => 'contato@mauricioschmitz.com.br',
    ),
    'analytics' => array(
        'GA_ID' => '',
    ),
    'facebook' => array(
        'app_id' => '',
        'app_secret' => '',
        'default_graph_version' => 'v2.2',
        'fileUpload' => false, // optional
        'allowSignedRequest' => false, // optional, but should be set to false for non-canvas apps
        'groups_id' => array(
            'me'
//            '196034167453149', //Testes
//            '168206043365835' //StoreMais
        ),
        'vendor_dir' => __DIR__.'/../../../vendor/'
    ),
);
