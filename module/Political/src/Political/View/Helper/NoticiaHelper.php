<?php
namespace Jornal\View\Helper;
use Jornal\View\Helper\AbstractJornalHelper;
use Admin\Entity\VisualizacaoNoticia;

class NoticiaHelper extends AbstractJornalHelper{
    
    protected  $exibidos = array();

    /**
     * Invoke Helper
     * @return boolean
     */
    public function __invoke($espaco = '') {
        $em = $this->getEntityManager();
        $result = null;
        $espacoPublicidade = $this->getEntityManager()->getRepository('Admin\Entity\EspacoPublicidade')->findOneBy(array('tamanho'=>$espaco));
        
        if(!is_null($espacoPublicidade))
            $result = $this->getEntityManager()->getRepository('Admin\Entity\Publicidade')->findPublicidade($this->exibidos, $espacoPublicidade->getId());
        
        if(!is_null($result)){
            array_push($this->exibidos, $result->getId());
            
            //Grava a visualização
            $visualizacaoNoticia = new VisualizacaoNoticia();
            $visualizacaoNoticia->setNoticia($result);
            $visualizacaoNoticia->setIp($_SERVER["REMOTE_ADDR"]);
            $em->persist($visualizacaoNoticia);
            $em->flush();
            
            return $result->getArquivo();
        }
        
        return $espaco.'.jpg';
       
    }
    
    /**
     * Invoke Helper With Cache
     * @return boolean
     */
//    public function __invoke() {
//        //Neste metodo você pode fazer o que quiser por ex: buscar um valor e retornar
//        $cache   = $this->getServiceManager()->get('cache');
//        $key    = 'categorias-menu';
//        $result = $cache->getItem($key, $success);
//        if (!$success) {
//            $result = $this->getEntityManager()->getRepository('Admin\Entity\Categoria')->findBy(array('ativo'=>1),array('cor'=>'ASC'));
//            $cache->setItem($key, $result);
//        }
//        return $result;
//       
//    }
 
    
}
?>