<?php
namespace Political\View\Helper;
use Political\View\Helper\AbstractPoliticalHelper;

class CategoriaHelper extends AbstractPoliticalHelper{
    
    /**
     * Invoke Helper
     * @return array of Admin\Entity\Categoria
     */
    public function __invoke() {
        $result = $this->getEntityManager()->getRepository('Admin\Entity\Categoria')->findBy(array('ativo'=>1),array('cor'=>'ASC'));
        $this->getEntityManager()->getConnection()->close();
        return $result;
       
    }
    
}
?>