<?php

namespace Political\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PoliticalConfig
 *
 * @ORM\Table(name="political_config")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Base\Entity\GlobalRepository")
 */
class PoliticalConfig
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="partido", type="string", length=50, nullable=false)
     */
    private $partido;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=50, nullable=false)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="cor1", type="string", length=45, nullable=true)
     */
    private $cor1;

    /**
     * @var string
     *
     * @ORM\Column(name="cor2", type="string", length=45, nullable=true)
     */
    private $cor2;

    /**
     * @var string
     *
     * @ORM\Column(name="cor3", type="string", length=45, nullable=true)
     */
    private $cor3;

    /**
     * @var string
     *
     * @ORM\Column(name="cor4", type="string", length=45, nullable=true)
     */
    private $cor4;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", length=255, nullable=true)
     */
    private $twitter;

    /**
     * @var string
     *
     * @ORM\Column(name="linkedin", type="string", length=255, nullable=true)
     */
    private $linkedin;

    /**
     * @var string
     *
     * @ORM\Column(name="instagram", type="string", length=255, nullable=true)
     */
    private $instagram;

    /**
     * @var string
     *
     * @ORM\Column(name="youtube", type="string", length=255, nullable=true)
     */
    private $youtube;

    /**
     * @var string
     *
     * @ORM\Column(name="skype", type="string", length=255, nullable=true)
     */
    private $skype;

    /**
     * @var int
     *
     * @ORM\Column(name="ativo", type="integer", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=false)
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_modificacao", type="datetime", nullable=false)
     */
    private $dataModificacao = 'CURRENT_TIMESTAMP';



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return PoliticalConfig
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set partido
     *
     * @param string $partido
     *
     * @return PoliticalConfig
     */
    public function setPartido($partido)
    {
        $this->partido = $partido;

        return $this;
    }

    /**
     * Get partido
     *
     * @return string
     */
    public function getPartido()
    {
        return $this->partido;
    }

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return PoliticalConfig
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set cor1
     *
     * @param string $cor1
     *
     * @return PoliticalConfig
     */
    public function setCor1($cor1)
    {
        $this->cor1 = $cor1;

        return $this;
    }

    /**
     * Get cor1
     *
     * @return string
     */
    public function getCor1()
    {
        return $this->cor1;
    }

    /**
     * Set cor2
     *
     * @param string $cor2
     *
     * @return PoliticalConfig
     */
    public function setCor2($cor2)
    {
        $this->cor2 = $cor2;

        return $this;
    }

    /**
     * Get cor2
     *
     * @return string
     */
    public function getCor2()
    {
        return $this->cor2;
    }

    /**
     * Set cor3
     *
     * @param string $cor3
     *
     * @return PoliticalConfig
     */
    public function setCor3($cor3)
    {
        $this->cor3 = $cor3;

        return $this;
    }

    /**
     * Get cor3
     *
     * @return string
     */
    public function getCor3()
    {
        return $this->cor3;
    }

    /**
     * Set cor4
     *
     * @param string $cor4
     *
     * @return PoliticalConfig
     */
    public function setCor4($cor4)
    {
        $this->cor4 = $cor4;

        return $this;
    }

    /**
     * Get cor4
     *
     * @return string
     */
    public function getCor4()
    {
        return $this->cor4;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     *
     * @return PoliticalConfig
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     *
     * @return PoliticalConfig
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set linkedin
     *
     * @param string $linkedin
     *
     * @return PoliticalConfig
     */
    public function setLinkedin($linkedin)
    {
        $this->linkedin = $linkedin;

        return $this;
    }

    /**
     * Get linkedin
     *
     * @return string
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }

    /**
     * Set instagram
     *
     * @param string $instagram
     *
     * @return PoliticalConfig
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;

        return $this;
    }

    /**
     * Get instagram
     *
     * @return string
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    /**
     * Set youtube
     *
     * @param string $youtube
     *
     * @return PoliticalConfig
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;

        return $this;
    }

    /**
     * Get youtube
     *
     * @return string
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * Set skype
     *
     * @param string $skype
     *
     * @return PoliticalConfig
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;

        return $this;
    }

    /**
     * Get skype
     *
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Set ativo
     *
     * @param int $ativo
     *
     * @return PoliticalConfig
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set dataCriacao
     *
     * @param \DateTime $dataCriacao
     *
     * @return PoliticalConfig
     */
    public function setDataCriacao($dataCriacao)
    {
        $this->dataCriacao = $dataCriacao;

        return $this;
    }

    /**
     * Get dataCriacao
     *
     * @return \DateTime
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }

    /**
     * Set dataModificacao
     *
     * @param \DateTime $dataModificacao
     *
     * @return PoliticalConfig
     */
    public function setDataModificacao($dataModificacao)
    {
        $this->dataModificacao = $dataModificacao;

        return $this;
    }

    /**
     * Get dataModificacao
     *
     * @return \DateTime
     */
    public function getDataModificacao()
    {
        return $this->dataModificacao;
    }
}
