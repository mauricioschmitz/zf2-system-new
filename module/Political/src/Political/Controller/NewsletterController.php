<?php

namespace Political\Controller;

use Political\Controller\AbstractPoliticalController;
use Zend\View\Model\JsonModel;
use Admin\Entity\Newsletter;

class NewsletterController extends AbstractPoliticalController {
    
    public function __construct() {
        $this->route = 'newsletter';
        $this->controller = 'newsletter';
    }

    public function indexAction() {
        
    }

    public function insertAction() {
        $em = $this->getEm();
        $repository = $em->getRepository('Admin\Entity\Newsletter');
        $email = $repository->findOneBy(array('email'=>$this->params()->fromPost('email1')));
        
        if(!preg_match('/^[a-z0-9&\'\.\-_\+]+@[a-z0-9\-]+\.([a-z0-9\-]+\.)*+[a-z]{2}/is', $this->params()->fromPost('email1'))){
            $error = "Informe um e-mail válido!";
            
        }elseif(!empty($email) && !is_null($email->getEmail())){
            $error = "E-mail já cadstrado!";
            
        }else{
        
            $newsletter = new Newsletter();
            $newsletter->setEmail($this->params()->fromPost('email1'));

            $em->persist($newsletter);
            $em->flush();
            if($newsletter->getId())
                $error = "E-mail cadastrado com sucesso!";
            else
                $error = "Ocorreu um erro, tente novamente";
        }
        $em->getConnection()->close();
        echo $error;die();
    }
    
   
}
