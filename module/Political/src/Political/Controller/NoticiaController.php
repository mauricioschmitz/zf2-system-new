<?php

namespace Political\Controller;

use Political\Controller\AbstractPoliticalController;
use Zend\View\Model\ViewModel;
use Admin\Entity\VisualizacaoNoticia;

class NoticiaController extends AbstractPoliticalController {
    
    public function __construct() {
        $this->route = 'noticia';
        $this->controller = 'noticia';
    }

    public function indexAction() {
        
    }

    public function detalheAction() {
        $this->scripts_styles = array(
            array('file'=>'/libs/jackbox/jackbox-packed.min.js', 'type'=>'headscript', 'function'=>'appendFile'),
            array('file'=>'/libs/jackbox/jackbox.min.css', 'type'=>'headlink', 'function'=>'appendStylesheet'),
            array('content'=>'// jackbox
(function ($) {
    "use strict";

    $(function () {
        if ($(".jackbox[data-group]").length) {
            jQuery(".jackbox[data-group]").jackBox("init", {
                showInfoByDefault: false,
                preloadGraphics: false,
                fullscreenScalesContent: true,
                autoPlayVideo: false,
                flashVideoFirst: false,
                defaultVideoWidth: 960,
                defaultVideoHeight: 540,
                baseName: ".jackbox",
                className: ".jackbox",
                useThumbs: true,
                thumbsStartHidden: false,
                thumbnailWidth: 75,
                thumbnailHeight: 50,
                useThumbTooltips: false,
                showPageScrollbar: false,
                useKeyboardControls: true
            });
        }
    });
})(jQuery);        
    ', 'type'=>'headscript', 'function'=>'appendScript')
        );
        
        $em = $this->getEm();
        $repository = $em->getRepository('Admin\Entity\Noticia');
        
        $post = $repository->findOneBy(array('id'=>$this->params()->fromRoute('id', 0)));
        array_push($repository->exibidos,$post->getId());
        if(!$post){
            return $this->redirect()->toRoute('home');
        }
        
        //Grava Visualizacao da noticia
        $visNoticia = new VisualizacaoNoticia();
        $visNoticia->setNoticia($post);
        $visNoticia->setIp($_SERVER["REMOTE_ADDR"]);
        $visNoticia->setDescricao("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
        $em->persist($visNoticia);
        $em->flush();
        
        $this->layout()->headTitle = $post->getTitulo();
        $this->layout()->current_menu = $post->getCategoria()->getCor();
        
        //Seta as metas para o Facebook
        $this->layout()->description = ($post->getChamada() != '' ? $post->getChamada() : $this->limitaStr(strip_tags(str_replace('%Publicidades%', '', $post->getDescricao())), 200));
        $this->layout()->categoriaArquivo = $post->getCategoria()->getTitulo();
        $this->layout()->dateTime = $post->getDataPublicacao()->format('Y-m-d H:i:s');
        $this->layout()->tags = $post->getTag();
        if(!$post->getImagens()->isEmpty()){
            $this->layout()->image = $post->getImagens()->first()->getArquivo();
        }
        
        $config = $this->getServiceLocator()->get('config');
        $config = $config['facebook'];
        
        $detalhes = explode('%Publicidades%', $post->getDescricao());
        
        $this->scriptsStyles();
        $em->getConnection()->close();
        return new ViewModel(array('post' => $post, 'config'=>$config, 'detalhes'=>$detalhes));
    }
    
    public function listbycategoriaAction(){
        $em = $this->getEm();
        $repository = $em->getRepository('Admin\Entity\Categoria');
        $categoria = $repository->find($this->params()->fromRoute('id', 0));
        
        $this->layout()->headTitle = $categoria->getTitulo();
        $this->layout()->current_menu = $categoria->getCor();
        
        $repository = $em->getRepository('Admin\Entity\Noticia');
        $page = $this->params()->fromRoute('page')-1;
        if($page < 0) $page = 0;
        $limit = 10;
        $posts = $repository->getPagedNoticia($categoria,$page,$limit);
        
        if(!$posts)
            return $this->redirect()->toRoute('home');
        
        $total = count($posts);
        $qtdPg = ceil($total/$limit);
        
        $this->layout()->description = 'Flávio Molinari - PSDB 45XXX - '.$categoria->getTitulo();
        $this->layout()->dateTime = date('Y-m-d H:i:s');
        $em->getConnection()->close();
        return new ViewModel(array('categoria'=>$categoria, 'posts'=>$posts, 'page'=>$page, 'total'=>$total, 'qtdPg'=>$qtdPg));
    }
    
    function limitaStr($str, $limit){
        if (strlen($str)>$limit){
            $str = substr($str,0,$limit);
            $ultChr = strrpos($str,' ');
            $str = substr($str,0,$ultChr).'...';
        }
        return $str;
    }
    
    public function geraminisAction(){
        $em = $this->getEm();
        $todas = $em->getRepository('Admin\Entity\ImagemNoticia')->findBy(array('exportada'=>0));
        $qtd = count($todas);
        $imagemNoticia = $em->getRepository('Admin\Entity\ImagemNoticia')->findOneBy(array('exportada'=>0), array('id'=>'desc'));
        
        $folder = getcwd() . '/public/uploads/noticias/';
        $imagem = $imagemNoticia->getArquivo();
        $w = 160;
        $h = 110;

        $extension = pathinfo($imagem, PATHINFO_EXTENSION);  
        $nomeThumb = str_replace('.'.$extension, '_Fixed.'.$extension, $imagem);
        
        if($extension == 'png' || $extension == 'pneg')
            $renderImage = imagecreatefrompng($folder.$this->nomeImagemMini($imagem));
        else
            $renderImage = imagecreatefromjpeg($folder.$this->nomeImagemMini($imagem));
        
        if($w && $h){
            $dst_x = 0;   // X-coordinate of destination point
            $dst_y = 0;   // Y-coordinate of destination point
            $src_x = 0;   // Crop Start X position in original image
            $src_y = 0;   // Crop Srart Y position in original image
            $dst_w = $w; // Thumb width
            $dst_h = $h; // Thumb height
            $src_w = $w; // Crop end X position in original image
            $src_h = $h; // Crop end Y position in original image

            // Creating an image with true colors having thumb dimensions (to merge with the original image)
            $dst_image = imagecreatetruecolor($dst_w, $dst_h);
            // Cropping
            imagecopyresampled($dst_image, $renderImage, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
            $renderImage = $dst_image;
        }
        // Saving
        if(imagejpeg($renderImage, $folder.$nomeThumb, 100)){
            $imagemNoticia->setExportada(1);
            $em->persist($imagemNoticia);
            $em->flush();
//            $nomeApagar = str_replace('.'.$extension, '_220.'.$extension, $imagem);
//            @unlink($folder.$nomeApagar);
        }
        
        echo 'Faltam: '.$qtd;
        echo '<script>setTimeout(function(){window.location.href = window.location},500);</script>';
        die();
    }
    
    public function nomeImagemMini($imagem){
        $ext_img = explode('.', $imagem);
        $ext = '.'.$ext_img[count($ext_img)-1];
        return str_replace($ext, '_220'.$ext, $imagem);

    }
}
