<?php

namespace Political\Controller;

use Political\Controller\AbstractJornalController;
use Zend\View\Model\ViewModel;
use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail as SendmailTransport;


class IndexController extends AbstractPoliticalController {

    public function __construct() {
        $this->scripts_styles = array(
        );
        $this->downloadFiles = array('musica.mp3', 'jingle.mp3');
        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');
    }

    public function indexAction() {
        $em = $this->getEm();
        $em->getConnection()->close();
        $repository = $em->getRepository('Admin\Entity\Noticia');
        
        $videoCat = $em->getRepository('Admin\Entity\Categoria')->findOneBy(array('apelido'=>'videos'));
        $postsCat = $em->getRepository('Admin\Entity\Categoria')->findOneBy(array('apelido'=>'noticias'));
        
        $posts = $repository->findByNivel('A', $postsCat, 3);
        $video = $em->getRepository('Admin\Entity\Texto')->findOneBy(array('apelido'=>'video_capa'));
        
        
        return new ViewModel(array('posts'=>$posts, 'video'=>$video, 'postsCat'=>$postsCat));
    }

    public function aboutAction() {
        $em = $this->getEm();

        $texto = $em->getRepository('Admin\Entity\Texto')->findOneBy(array('apelido' => 'sobre'));
        return new ViewModel(array('texto' => $texto));
    }

    public function workAction() {
        $em = $this->getEm();

        $texto = $em->getRepository('Admin\Entity\Texto')->findOneBy(array('apelido' => 'work'));
        return new ViewModel(array('texto' => $texto));
    }
    
    public function videosAction() {
        $em = $this->getEm();

        $texto = $em->getRepository('Admin\Entity\Texto')->findOneBy(array('apelido' => 'videos'));
        return new ViewModel(array('texto' => $texto));
    }
    
    public function projetosAction() {
        $em = $this->getEm();
        
        $texto = $em->getRepository('Admin\Entity\Texto')->findOneBy(array('apelido' => 'projetos'));
        return new ViewModel(array('texto' => $texto));
    }
    
    public function depoimentosAction() {
        $em = $this->getEm();
        
        $texto = $em->getRepository('Admin\Entity\Texto')->findOneBy(array('apelido' => 'depoimentos'));
        return new ViewModel(array('texto' => $texto));
    }

    public function contatoAction() {
        $this->layout()->headTitle = 'Contato';
        $this->layout()->current_menu = 'contato';
        $request = $this->getRequest();

        if ($request->isPost()) {
            $dados = $request->getPost()->toArray();
            $error = array();
            if (strlen($dados['cf_name']) < 2) {
                $error['name'] = "Por favor, informe seu nome!";
            }

            if (!preg_match('/^[a-z0-9&\'\.\-_\+]+@[a-z0-9\-]+\.([a-z0-9\-]+\.)*+[a-z]{2}/is', $dados['cf_email'])) {
                $error['email'] = "Por favor, informe um e-mail válido";
            }

            if (strlen($dados['cf_message']) < 3) {
                $error['comments'] = "Por favor, digite uma mensagem.";
            }

            if (strlen($dados['cf_subject']) < 2) {
                $error['subject'] = "Por favor, informe um assunto.";
            }

            if (!$error) {
                $config = $this->getServiceLocator()->get('config');
                $config = $config['sendemail'];

                $message = new Message();
                $message->setEncoding("UTF-8");
                $message->addFrom($dados['cf_email'], $dados['cf_name'])
                        ->addTo($config['destinatario'])
                        ->setSubject($dados['cf_subject']);
                $message->setBody($dados['cf_message']);

//            $message->addCc("")
//                    ->addBcc("");

                $message->addReplyTo($dados['cf_email'], $dados['cf_name']);

                $transport = new SendmailTransport();
                try {
                    $transport->send($message);
                    echo "<li class='success'> Olá, " . $dados['cf_name'] . ". Recebemos seu contato, assim que possível estaremos respondendo! </li>";
                } catch (Exception $ex) {
                    echo "<li class='error'> Olá, " . $dados['cf_name'] . ". Ocorreu um erro, por favor tente novamente! </li>";
                }
            } else {

                $response = (isset($error['name'])) ? "<li class='error'>" . $error['name'] . "</li> \n" : null;
                $response .= (isset($error['email'])) ? "<li class='error'>" . $error['email'] . "</li> \n" : null;
                $response .= (isset($error['subject'])) ? "<li class='error'>" . $error['subject'] . "</li> \n" : null;
                $response .= (isset($error['comments'])) ? "<li class='error'>" . $error['comments'] . "</li>" : null;

                echo $response;
            } # end if there was an error sending
            die();
        }
        $this->layout()->description = 'Flávio Molinari - PSDB 45XXX - Contato';
        $this->layout()->dateTime = date('Y-m-d H:i:s');
        return new ViewModel();
    }
    
    
}
