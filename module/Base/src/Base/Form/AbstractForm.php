<?php

namespace Base\Form;

use Zend\Form\Form;
use Zend\Form\Element;
/**
 * Description of AbstractForm
 *
 * @author mauricioschmitz
 */
abstract class AbstractForm extends Form {

    public function __construct(){
        parent::__construct();
        
        $this->setAttribute('method', 'POST');
        
        $token = new Element\Csrf($this->getToken());
        $this->add($token);
    }
    
    public function getToken(){
        return 'token_'. get_class($this);
    }
    
    /*
     * $cep, $uf, $cidade, $endereco, $numero, $bairro, $complemento 
     */
    public function addEnderecoElement($names, $labels){
        //Input cep
        $this->add(array(
            'name' => $names[0],
            'type' => 'text',
            'attributes' => array(
                'id' => $names[0],
                'required' => 'true',
                'class' => 'form-control cep',
                'autofocus' => 'false',
                'title' => 'Informe o '.$labels[0],
            ),
            'options' => array(
                'label' =>$labels[0],
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input uf
        $this->add(array(
            'name' => $names[1],
            'type' => 'text',
            'attributes' => array(
                'id' => $names[1],
                'required' => 'true',
                'class' => 'form-control uf readonly',
                'autofocus' => 'false',
//                'readonly' => 'readonly',
                'title' => 'Informe o '.$labels[1],
            ),
            'options' => array(
                'label' =>$labels[1],
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input cidade
        $this->add(array(
            'name' => $names[2],
            'type' => 'text',
            'attributes' => array(
                'id' => $names[2],
                'required' => 'true',
                'class' => 'form-control cidade readonly',
                'autofocus' => 'false',
//                'readonly' => 'readonly',
                'title' => 'Informe o '.$labels[2],
            ),
            'options' => array(
                'label' =>$labels[2],
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input endereco
        $this->add(array(
            'name' => $names[3],
            'type' => 'text',
            'attributes' => array(
                'id' => $names[3],
                'required' => 'true',
                'class' => 'form-control endereco',
                'autofocus' => 'false',
                'title' => 'Informe o '.$labels[3],
            ),
            'options' => array(
                'label' =>$labels[3],
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input numero
        $this->add(array(
            'name' => $names[4],
            'type' => 'text',
            'attributes' => array(
                'id' => $names[4],
                'required' => 'true',
                'class' => 'form-control numero',
                'autofocus' => 'false',
                'title' => 'Informe o '.$labels[4],
            ),
            'options' => array(
                'label' =>$labels[4],
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input bairro
        $this->add(array(
            'name' => $names[5],
            'type' => 'text',
            'attributes' => array(
                'id' => $names[5],
                'class' => 'form-control bairro',
                'autofocus' => 'false',
                'title' => 'Informe o '.$labels[5],
            ),
            'options' => array(
                'label' =>$labels[5],
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
        
        //Input complemento
        $this->add(array(
            'name' => $names[6],
            'type' => 'text',
            'attributes' => array(
                'id' => $names[6],
                'class' => 'form-control complemento',
                'autofocus' => 'false',
                'title' => 'Informe o '.$labels[6],
            ),
            'options' => array(
                'label' =>$labels[6],
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
            )
        ));
    }
}
