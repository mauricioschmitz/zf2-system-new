<?php
namespace Base\View\Helper;

use Base\View\Helper\AbstractBaseHelper;

class SetupHelper extends AbstractBaseHelper{
    
    /**
     * Invoke Helper
     * @return Base\Entity\Setup
     */
    public function __invoke() {
        $result = $this->getEntityManager()->getRepository('Base\Entity\Setup')->findOneBy(array('active'=>1));
        return $result;
       
    }
    
    /**
     * Invoke Helper With Cache
     * @return array of Admin\Entity\Categoria
     */
//    public function __invoke() {
//        //Neste metodo você pode fazer o que quiser por ex: buscar um valor e retornar
//        $cache   = $this->getServiceManager()->get('cache');
//        $key    = 'categorias-menu';
//        $result = $cache->getItem($key, $success);
//        if (!$success) {
//            $result = $this->getEntityManager()->getRepository('Admin\Entity\Categoria')->findBy(array('ativo'=>1),array('cor'=>'ASC'));
//            $cache->setItem($key, $result);
//        }
//        return $result;
//       
//    }
 
    
}
?>