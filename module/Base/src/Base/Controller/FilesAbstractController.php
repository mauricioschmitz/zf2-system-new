<?php

namespace Base\Controller;

/**
 * Description of FilesAbstractController
 *
 * @author mauricioschmitz
 */

use Zend\View\Model\JsonModel;

abstract class FilesAbstractController extends AbstractController{
    
    protected $extensions = array();
    protected $tamanhos = array(1024);
    protected $dir = '';
    protected $marcadagua = array();
    
    protected $fixed = array();

    /**
     * Abstract method
     */
    abstract public function tratarArquivo($id, $arquivo);
    
    /**
     * @return array of files
     */
    public function imageUploadAction(){
        $marcadaagua = $this->params()->fromQuery('marcadagua');
        
        $folder = getcwd() . '/public/uploads/' . $this->dir;
        $retorno = 'false';
        if($_FILES['files']['name']!= ""){
            $ext = explode('.',$_FILES['files']['name']);
            $qt = count($ext);
            $ext = $ext[$qt-1];
            if (!in_array(strtolower($ext),$this->extensions)) {
                $retorno = "invalido";
                
            }else{
                $name = $_FILES['files']['name'];
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $nomeOriginal = pathinfo($name, PATHINFO_FILENAME);
                $novoNome = (md5($name . '_' . time()) . '.' . $extension);
                $novoNomeWithoutExtension = (md5($name . '_' . time()));
                
                $medida = getimagesize($_FILES['files']['tmp_name']);
                $thumbsX = array();
                $thumbsY = array();
                
                if($medida[0]>$medida[1]) {
                    if($this->tamanhos[0] > $medida[0]){
                        $x = $medida[0];
                        if($this->tamanhos[0] > $medida[1]){
                            $y = $medida[1];
                        }
                    }else{
                        $x = $this->tamanhos[0];
                        $y = $medida[1]/($medida[0]/$this->tamanhos[0]);
                    }
                    
                    $tamanhos = $this->tamanhos;
                    for($i = 1; $i < count($tamanhos); $i++){
                        array_push($thumbsX, $tamanhos[$i]);
                        array_push($thumbsY, $medida[1]/($medida[0]/$tamanhos[$i]));
                    }
                }else{
                    if($this->tamanhos[0]  > $medida[1]){
                        $y = $medida[1];
                        if($this->tamanhos[0]  > $medida[0]){
                            $x = $medida[0];
                        }
                    }else{
                        $y = $this->tamanhos[0] ;
                        $x = $medida[0]/($medida[1]/$this->tamanhos[0] );
                    }

                    $tamanhos = $this->tamanhos;
                    for($i = 1; $i < count($tamanhos); $i++){
                        array_push($thumbsY, $tamanhos[$i]);
                        array_push($thumbsX, $medida[0]/($medida[1]/$tamanhos[$i]));
                    }
                }
                
                $watermark = $this->getServiceLocator()->get('WaterMark');
                
                if(strtolower($extension) == 'jpg' || strtolower($extension) == 'jpeg'){
                    $watermark_options = array(
                        'halign' => 1,
                        'valign' => 1,
                        'hshift' => -10,
                        'vshift' => -10,
                        'type' => IMAGETYPE_JPEG,
                        'jpeg-quality' => 80,
                    );
                    //cria imagem temporarioa
                    $imagem = imagecreatefromjpeg($_FILES['files']['tmp_name']);

                    //imagem
                    $final = imagecreatetruecolor($x, $y);
                    imagecopyresampled($final, $imagem,0, 0, 0, 0, $x, $y, $medida[0], $medida[1]);
                    
                    if($this->marcadagua[0] != '' && $marcadaagua){
                        $watermark_options['watermark'] = $folder . $this->marcadagua[0];
                        // Save watermarked image to file
                        $final = $watermark->output($final, $folder .$novoNome, $watermark_options);
                    }else{
                        imagejpeg($final, $folder .$novoNome, 80);
                        imagedestroy($final);
                    }
                    
                    $arquivo = array('nomeOriginal'=>$nomeOriginal, 'novoNome' => $novoNome);
                    for($i = 0; $i < count($thumbsX); $i++){
                        $nomeThumb = $novoNomeWithoutExtension.'_'.$this->tamanhos[($i+1)].'.'.$extension;
                        $thumb = imagecreatetruecolor($thumbsX[$i], $thumbsY[$i]);
                        imagecopyresampled($thumb, $imagem,0, 0, 0, 0, $thumbsX[$i], $thumbsY[$i], $medida[0], $medida[1]);
                        
                        
                        if($this->marcadagua[$i+1] != '' && $marcadaagua){
                            $watermark_options['watermark'] = $folder . $this->marcadagua[$i+1];
                            // Save watermarked image to file
                            $thumb = $watermark->output($thumb, $folder .$nomeThumb, $watermark_options);
                        }else{
                            imagejpeg($thumb, $folder .$nomeThumb, 100);
                            imagedestroy($thumb);
                        }
                    }
                    
                    //Cria a imagem cortada em tamanho fixo
                    if(!empty($this->fixed) && isset($this->fixed[0]) && isset($this->fixed[1])){
                        $nomeFixed = $novoNomeWithoutExtension.'_Fixed.'.$extension;
                        $w = $this->fixed[0];
                        $h = $this->fixed[1];
                        
                        $renderImage = imagecreatefromjpeg($folder .$nomeThumb);

                        $dst_x = 0;   // X-coordinate of destination point
                        $dst_y = 0;   // Y-coordinate of destination point
                        $src_x = 0;   // Crop Start X position in original image
                        $src_y = 0;   // Crop Srart Y position in original image
                        $dst_w = $w; // Thumb width
                        $dst_h = $h; // Thumb height
                        $src_w = $w; // Crop end X position in original image
                        $src_h = $h; // Crop end Y position in original image

                        // Creating an image with true colors having thumb dimensions (to merge with the original image)
                        $dst_image = imagecreatetruecolor($dst_w, $dst_h);
                        // Cropping
                        imagecopyresampled($dst_image, $renderImage, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
                        $renderImage = $dst_image;

                        // Saving
                        imagejpeg($renderImage, $folder .$nomeFixed, 100);
                    }
                }elseif(strtolower ($extension) == 'png' || strtolower ($extension) == 'pneg'){
                    $watermark_options = array(
                        'halign' => 1,
                        'valign' => 1,
                        'hshift' => -10,
                        'vshift' => -10,
                        'type' => IMAGETYPE_PNG,
                        'jpeg-quality' => 80,
                    );
                    $imagem = imagecreatefrompng($_FILES['files']['tmp_name']);
                    
                    $isTrueColor = imageistruecolor($imagem);

                    if($isTrueColor){ // Verifica se é truecolor
                        $final  = imagecreatetruecolor( $x, $y );
                        imagealphablending($final, false);
                        imagesavealpha($final  , true );

                    }else{
                        $final  = imagecreate( $x, $y );
                        imagealphablending( $final, false );
                        $transparent = imagecolorallocatealpha( $final, 0, 0, 0, 127 );
                        imagefill( $final, 0, 0, $transparent );
                        imagesavealpha( $final,true );
                        imagealphablending( $final, true );
                    }
                    
                    imagecopyresampled($final, $imagem, 0, 0, 0, 0, $x, $y, $medida[0], $medida[1]);
                    if($this->marcadagua[0] != '' && $marcadaagua){
                        $watermark_options['watermark'] = $folder . $this->marcadagua[0];
                        // Save watermarked image to file
                        $final = $watermark->output($final, $folder .$novoNome, $watermark_options);
                    }else{
                        imagecopyresampled($final, $imagem, 0, 0, 0, 0, $thumbsX[$i], $thumbsY[$i], $medida[0], $medida[1]);
                        imagepng($final, $folder.$novoNome);
                    }
                    
                    $arquivo = array('nomeOriginal'=>$nomeOriginal, 'novoNome' => $novoNome);
                    
                    for($i = 0; $i < count($thumbsX); $i++){
                        $nomeThumb = $novoNomeWithoutExtension.'_'.$this->tamanhos[($i+1)].'.'.$extension;
                        
                        if($isTrueColor){ // Verifica se é truecolor
                            $thumb  = imagecreatetruecolor( $thumbsX[$i], $thumbsY[$i] );
                            imagealphablending($thumb, false);
                            imagesavealpha($thumb  , true );

                        }else{
                            $thumb  = imagecreate( $thumbsX[$i], $thumbsY[$i] );
                            imagealphablending( $thumb, false );
                            $transparent = imagecolorallocatealpha( $thumb, 0, 0, 0, 127 );
                            imagefill( $thumb, 0, 0, $transparent );
                            imagesavealpha( $thumb,true );
                            imagealphablending( $thumb, true );
                        }
                        imagecopyresampled($thumb, $imagem, 0, 0, 0, 0, $thumbsX[$i], $thumbsY[$i], $medida[0], $medida[1]);
                        if($this->marcadagua[$i+1] != '' && $marcadaagua){
                            $watermark_options['watermark'] = $folder . $this->marcadagua[$i+1];
                            // Save watermarked image to file
                            $thumb = $watermark->output($thumb, $folder .$nomeThumb, $watermark_options);
                        }else{
                            
                            imagepng($thumb, $folder.$nomeThumb);
                        }

                    }
                    
                    //Cria a imagem cortada em tamanho fixo
                    if(!empty($this->fixed) && isset($this->fixed[0]) && isset($this->fixed[1])){
                        $nomeFixed = $novoNomeWithoutExtension.'_Fixed.'.$extension;
                        $w = $this->fixed[0];
                        $h = $this->fixed[1];
                        
                        $renderImage = imagecreatefrompng($imagem);
                        
                        $dst_x = 0;   // X-coordinate of destination point
                        $dst_y = 0;   // Y-coordinate of destination point
                        $src_x = 0;   // Crop Start X position in original image
                        $src_y = 0;   // Crop Srart Y position in original image
                        $dst_w = $w; // Thumb width
                        $dst_h = $h; // Thumb height
                        $src_w = $w; // Crop end X position in original image
                        $src_h = $h; // Crop end Y position in original image

                        // Creating an image with true colors having thumb dimensions (to merge with the original image)
                        $dst_image = imagecreatetruecolor($dst_w, $dst_h);
                        // Cropping
                        imagecopyresampled($dst_image, $renderImage, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
                        $renderImage = $dst_image;

                        // Saving
                        imagejpeg($renderImage, $folder .$nomeFixed, 100);
                    }
                }else{
                    $retorno = 'invalido';
                }
                $retorno = $this->tratarArquivo($param = $this->params()->fromRoute('id', 0),$arquivo);
            }
        }
        echo $retorno;
        die();
    }

}
