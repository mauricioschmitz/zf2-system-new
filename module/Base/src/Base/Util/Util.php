<?php

namespace Base\Util;

class Util {

    public static function calculaIdade($dataNascimento) {
        $data_nasc = explode('/', $dataNascimento);

        $data = date('d/m/Y');

        $data = explode('/', $data);

        $anos = $data[2] - $data_nasc[2];

        if ($data_nasc[1] > $data[1])
            return $anos - 1;

        if ($data_nasc[1] == $data[1])
            if ($data_nasc[0] <= $data[0]) {
                return $anos;
            } else {
                return $anos - 1;
            }

        if ($data_nasc[1] < $data[1])
            return $anos;
    }
    
    public static function processarUrl($url) {
        $ch = curl_init($url);
        curl_setopt_array($ch, array(
                        CURLOPT_HEADER => 0,
                        CURLOPT_RETURNTRANSFER =>true,
                        CURLOPT_NOSIGNAL => 1, //to timeout immediately if the value is < 1000 ms
                        CURLOPT_TIMEOUT_MS => 1000, //The maximum number of mseconds to allow cURL functions to execute
                        CURLOPT_VERBOSE => 1,
                        CURLOPT_HEADER => 1
        ));

        $out = curl_exec($ch);
        curl_close($ch);
        return true;
       
    }
    
    /**
     * Função para retornar geolocalizacao
     *
     * @param string $address
     */
    public static function getGeoCode($address){
 
        // url encode the address
        $address = urlencode($address);

        // google map geocode api url
        $url = "http://maps.google.com/maps/api/geocode/json?address={$address}";

        // get the json response
        $resp_json = file_get_contents($url);

        // decode the json
        $resp = json_decode($resp_json, true);
        // response status will be 'OK', if able to geocode given address 
        if($resp['status']=='OK'){

            // get the important data
            $lati = $resp['results'][0]['geometry']['location']['lat'];
            $longi = $resp['results'][0]['geometry']['location']['lng'];
            $formatted_address = $resp['results'][0]['formatted_address'];

            // verify if data is complete
            if($lati && $longi && $formatted_address){

                // put the data in the array
                $data_arr = array();            

                array_push(
                    $data_arr, 
                        $lati, 
                        $longi, 
                        $formatted_address
                    );

                return $data_arr;

            }else{
                return false;
            }

        }else{
            return false;
        }
    }
}
