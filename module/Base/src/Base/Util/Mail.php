<?php

namespace Base\Util;

use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Mime;
use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\View\Model\ViewModel;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

class Mail {

    public static $CONFIRMARCADASTRO = array(
        'assunto' => 'Confirmação de cadastro',
        'mensagem' => '<p>Obrigado pelo seu cadastro, não esqueça de realizar a confirmação do pagamento!</p>
        <div style="margin:10px; background: #fff padding:10px">  <p>Seus dados de acesso são:</p>
          <p><a href="%link%"><b>Clique aqui</b></a> para acessar sua conta</p>
          <p><b>Login:</b> %login%</p>
          <p><b>Senha:</b> <span style="font-size:10px">(a mesma usada no cadastro)</span></p>
        </div>'
    );
    public static $CONFIRMARPAGAMENTO = array(
        'assunto' => 'Confirmação de pagamento',
        'mensagem' => "
        Confirmação de pagamento enviada por email!\n
        Um afiliado enviou uma confimação de depósito/transferência:\n
        Nome:  %nome%\n
        E-mail: %email%\n
        "
    );
    public static $APROVARPAGAMENTO = array(
        'assunto' => 'Pagamento aprovado',
        'mensagem' => '<p>Seu pagamento foi aprovado!</p>
        <div style="margin:10px; background: #fff padding:10px">  <p>Seu novo plano está ativo:</p>
          <p><a href="%link%"><b>Clique aqui</b></a> para acessar sua conta</p>
          <p><b>Login:</b> %login%</p>
          <p><b>Senha:</b> <span style="font-size:10px">(a mesma usada no cadastro)</span></p>
        </div>'
    );
    public static $SOLICITARUPGRADE = array(
        'assunto' => 'Solicitação de upgrade',
        'mensagem' => '<p>Recebemos sua solicitação de upgrade, realize a confirmação do pagamento para ativar seu novo plano!</p>
        <div style="margin:10px; background: #fff padding:10px">
          <p><a href="%link%"><b>Clique aqui</b></a> para acessar sua conta</p>
          <p><b>Login:</b> %login%</p>
          <p><b>Senha:</b> <span style="font-size:10px">(a mesma usada no cadastro)</span></p>
        </div>'
    );
    public static $APROVARPAGAMENTOUPGRADE = array(
        'assunto' => 'Pagamento aprovado (UPGRADE)',
        'mensagem' => '<p>Seu pagamento referente ao upgrade de sua conta foi aprovado!</p>
        <div style="margin:10px; background: #fff padding:10px">  <p>Seu novo plano está ativo:</p>
          <p><a href="%link%"><b>Clique aqui</b></a> para acessar sua conta</p>
          <p><b>Login:</b> %login%</p>
          <p><b>Senha:</b> <span style="font-size:10px">(a mesma usada no cadastro)</span></p>
        </div>'
    );
    
    public static $APROVARCOMISSAO = array(
        'assunto' => 'Comissão por indicação',
        'mensagem' => '<p>Seu indicado ingressou para o grupo!</p>
        <div style="margin:10px; background: #fff padding:10px">  <p>Sua comissão já está disponível em seu extrato:</p>
          <p>Dados do Indicado</p>
          <p><b>Nome:</b> %nome%</p>
        </div>'
    );
    
    public static $SOLICITARRENOVACAO = array(
        'assunto' => 'Solicitação de renovação',
        'mensagem' => '<p>Recebemos sua solicitação de renovação!</p>
        <div style="margin:10px; background: #fff padding:10px">  <p>Seu novo plano está aguardando confirmação de pagamento, não esqueça de nos enviar o comprovante de pagamento o mesmo pode ser enviado pelo seu escritório:</p>
          <p><a href="%link%"><b>Clique aqui</b></a> para acessar sua conta</p>
          <p><b>Login:</b> %login%</p>
          <p><b>Senha:</b> <span style="font-size:10px">(a mesma usada no cadastro)</span></p>
        </div>'
    );
    
    public static $SOLICITARSAQUE = array(
        'assunto' => 'Solicitação de saque',
        'mensagem' => '<p>Recebemos sua solicitação de saque, aguarde a confirmação!</p>
        <div style="margin:10px; background: #fff padding:10px">
          <p><a href="%link%"><b>Clique aqui</b></a> para acessar sua conta</p>
          <p><b>Login:</b> %login%</p>
          <p><b>Senha:</b> <span style="font-size:10px">(a mesma usada no cadastro)</span></p>
        </div>'
    );
    public static $APROVARSAQUE = array(
        'assunto' => 'Solicitação de saque APROVADA',
        'mensagem' => '<p>Seu saque foi aprovado, para mais detalhes confira seu extrato em sua conta!</p>
        <div style="margin:10px; background: #fff padding:10px">
          <p><a href="%link%"><b>Clique aqui</b></a> para acessar sua conta</p>
          <p><b>Login:</b> %login%</p>
          <p><b>Senha:</b> <span style="font-size:10px">(a mesma usada no cadastro)</span></p>
        </div>'
    );
    public function sendMail($fromMail = false, $fromName = false, $toMail = false, $toName = false, $cc = false, $cco = false, $subject = false, $messageContent = false, $attachments = false, $em=false, $config=false) {
        $setup = $em->getRepository('Base\Entity\Setup')->findOneBy(array('active'=>1));
        $view = new \Zend\View\Renderer\PhpRenderer();
        $resolver = new \Zend\View\Resolver\TemplateMapResolver();
        $resolver->setMap(array(
            'mailTemplate' => __DIR__ . '/../../../view/mails/email_template.phtml'
        ));
        $view->setResolver($resolver);

        $viewModel = new ViewModel();
        $viewModel->setTemplate('mailTemplate')->setVariables(array(
            'setup' => $setup,
            'nome' => $toName,
            'conteudo' => $this->trataMensagem($messageContent),
        ));

        $htmTemplate    = new \Zend\Mime\Part($view->render($viewModel));
        $htmTemplate->type = 'text/html';
        
        // then add them to a MIME message
        $mimeMessage = new MimeMessage();

        if ($attachments) {
            $text = new MimePart($this->trataMensagem($messageContent));
            $text->charset = 'utf-8';
            $text->type = "text/html; charset=UTF-8";

            $content = new MimeMessage();
            $content->addPart($text);

            $contentPart = new MimePart($content->generateMessage());
            $contentPart->type = "multipart/alternative;\n boundary=\"" .
                    $content->getMime()->boundary() . '"';

            $mimeMessage->addPart($contentPart);
//              // Add each attachment
            foreach ($attachments as $thisAttachment) {
                $attachment = new MimePart(file_get_contents($thisAttachment['tmp_name']));
                $attachment->filename = $thisAttachment['name'];
                $attachment->type = $thisAttachment['type'];
                $attachment->encoding = Mime::ENCODING_BASE64;
                $attachment->disposition = Mime::DISPOSITION_ATTACHMENT;
                $mimeMessage->addPart($attachment);
            }
            
        } else {
            $mimeMessage->addPart($htmTemplate);
        }

        // and finally we create the actual email
        $message = new Message();

        $message->addFrom($fromMail, $fromName)
                ->addTo($toMail, $toName)
                ->setSubject($subject);


        if ($cc) {
            foreach ($cc as $nome => $email) {
                $message->addCc($email, $nome);
            }
        }

        if ($cco) {
            foreach ($cc as $nome => $email) {
                $message->addBcc($email, $nome);
            }
        }

        $message->addReplyTo($fromMail, $fromName);
        $message->setBody($mimeMessage);
        if($config && isset($config['mail_auth'])){
            $transport = new SmtpTransport();
            $mail_auth = $config['mail_auth'];
            $options   = new SmtpOptions($mail_auth);
            $transport->setOptions($options);

        }else{
            $transport = new SendmailTransport();
        }
        try {

            $transport->send($message);
            return '1';
        } catch (Exception $ex) {
            return '0';
        }
    }

    private function trataMensagem($mensagem){
        if(is_array($mensagem)){
            foreach ($mensagem['variaveis'] as $key => $variavel) {
                $mensagem['mensagem'] = str_replace('%' . $key . '%', $variavel, $mensagem['mensagem']);
            }
            $mensagem = $mensagem['mensagem'];
        }
        return $mensagem;
    }
}
