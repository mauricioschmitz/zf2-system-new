<?php
namespace Login\Form;

use Base\Form\AbstractForm;
use Login\Form\Filter\RecoverFilter;
use Zend\Form\Element\Captcha,
    Zend\Captcha\Image as CaptchaImage;

class RecoverForm extends AbstractForm
{

    public function __construct()
    {
        parent::__construct();
        
        $this->setInputFilter(new RecoverFilter());
        $this->setAttribute('id', 'loginForm');
        $this->setAttribute('class', 'validate');
        $this->add(array(
            'name' => 'login',
            'type' => 'text',
            'attributes' => array(
                'id' => 'login',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'true',
                'title' => 'Informe seu e-mail/usuário'
            ),
            'options' => array(
                'label' =>'E-mail/Usuário',
                'label_attributes' => array(
                    'class'  => 'control-label'
                ),
            )
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Enviar',
                'class' => 'btn btn-success btn-block'
            ),
            'options' => array(
                'label' =>'Enviar',
                
            )
        ));
    }
}