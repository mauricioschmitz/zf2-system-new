<?php
namespace Login\Form;

use Base\Form\AbstractForm;
use Login\Form\Filter\LoginFilter;
use Zend\Form\Element\Captcha,
    Zend\Captcha\Image as CaptchaImage;

class LoginForm extends AbstractForm
{

    public function __construct($urlcaptcha = null)
    {
        parent::__construct();
        
        $this->setInputFilter(new LoginFilter());
        $this->setAttribute('id', 'loginForm');
        $this->setAttribute('class', 'validate');
        $this->add(array(
            'name' => 'login',
            'type' => 'text',
            'attributes' => array(
                'id' => 'login',
                'required' => 'true',
                'class' => 'form-control',
                'autofocus' => 'true',
                'title' => 'Informe seu e-mail/usuário'
            ),
            'options' => array(
                'label' =>'E-mail/Usuário',
                'label_attributes' => array(
                    'class'  => 'control-label'
                ),
            )
        ));
        
        $this->add(array(
            'name' => 'password',
            'type' => 'password',
            'attributes' => array(
                'id' => 'password',
                'required' => 'true',
                'class' => 'form-control',
                'placeholder' => '*****',
                'autofocus' => 'false',
                'title' => 'Informe sua senha'
            ),
            'options' => array(
                'label' =>'Password',
                'label_attributes' => array(
                    'class'  => 'control-label'
                ),
            )
        ));
        
        //Input destaque
        $this->add(array(
            'name' => 'rememberme',
            'type' => 'checkbox',
            'attributes' => array(
                'value' => 1,
                'class' =>'i-checks',
                'checked' =>'checked'
            ),
            'options' => array(
                'label' =>'Lembrar-me',
                'label_attributes' => array(
                    'class'  => 'col-sm-2 control-label'
                ),
                'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0
            )
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Entrar',
                'class' => 'btn btn-success btn-block'
            ),
            'options' => array(
                'label' =>'Entrar',
                
            )
        ));
        
        if(!is_null($urlcaptcha)){
            $dirdata = getcwd() . '/public/data';
            //pass captcha image options
            $captchaImage = new CaptchaImage(  array(
                    'font' => $dirdata . '/fonts/arial.ttf',
                    'width' => 250,
                    'height' => 100,
                    'dotNoiseLevel' => 40,
                    'lineNoiseLevel' => 3)
            );
            $captchaImage->setImgDir($dirdata.'/captcha');
            $captchaImage->setImgUrl($urlcaptcha);

            //add captcha element...
            $this->add(array(
                'type' => 'Zend\Form\Element\Captcha',
                'name' => 'captcha',
                'options' => array(
                    'label' => 'Informe o texto abaixo',
                    'captcha' => $captchaImage,
                ),
            ));
        }
    }
}