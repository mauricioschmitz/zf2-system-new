<?php
namespace Login\Form\Filter;

use Zend\InputFilter\InputFilter;
/**
 * Description of NewPasswordFilter
 *
 * @author mauricioschmitz
 */
class NewPasswordFilter extends InputFilter {
    
    public function __construct() {
        $isEmpty = \Zend\Validator\NotEmpty::IS_EMPTY;
       
        $array = array('password'=>'Senha');
        foreach ($array as $key=>$value){
            $this->add(array(
                'name' => $key,
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'StripTags'
                    ),
                    array(
                        'name' => 'StringTrim'
                    )
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                $isEmpty => ($key == 'cnpj' ? 'Informe um '. $value.' válido' : $value.' não deve estar vazio')
                            )
                        ),
                        'break_chain_on_failure' => true
                    )
                )
            ));
        }
    }
}
