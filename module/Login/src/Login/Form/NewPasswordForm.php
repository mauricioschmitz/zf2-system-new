<?php
namespace Login\Form;

use Base\Form\AbstractForm;
use Login\Form\Filter\NewPasswordFilter;
use Zend\Form\Element\Captcha,
    Zend\Captcha\Image as CaptchaImage;

class NewPasswordForm extends AbstractForm
{

    public function __construct()
    {
        parent::__construct();
        
        $this->setInputFilter(new NewPasswordFilter());
        $this->setAttribute('id', 'loginForm');
        $this->setAttribute('class', 'validate');
        $this->add(array(
            'name' => 'password',
            'type' => 'password',
            'attributes' => array(
                'id' => 'password',
                'required' => 'true',
                'class' => 'form-control',
                'placeholder' => '*****',
                'autofocus' => 'false',
                'title' => 'Informe sua senha'
            ),
            'options' => array(
                'label' =>'Senha',
                'label_attributes' => array(
                    'class'  => 'control-label'
                ),
            )
        ));
        
        $this->add(array(
            'name' => 'password_confirm',
            'type' => 'password',
            'attributes' => array(
                'id' => 'password_confirm',
                'required' => 'true',
                'class' => 'form-control',
                'placeholder' => '*****',
                'data-rule-equalTo' => '#password',
                'title' => 'Repita sua senha'
            ),
            'options' => array(
                'label' =>'Repita a senha',
                'label_attributes' => array(
                    'class'  => 'control-label'
                ),
            ),
            'validators' => array(
                array(
                    'name' => 'Identical',
                    'options' => array(
                        'token' => 'password', // name of first password field
                    ),
                ),
            ),
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Enviar',
                'class' => 'btn btn-success btn-block'
            ),
            'options' => array(
                'label' =>'Enviar',
                
            )
        ));
    }
}