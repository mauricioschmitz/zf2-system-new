<?php

namespace Login\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Login\Utility\UserPassword;
use Zend\Session\Container;
use Zend\Session\SessionManager;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
class IndexController extends AbstractController
{
    public function __construct() {
        $this->form = 'Login\Form\LoginForm';
        $this->controller = 'Login';
        $this->route = 'login';
        $this->service = 'Login\Service\LoginService';
        $this->entity = 'Login\Entity\Login';
    }
    public function indexAction()
    {
        $request = $this->getRequest();
        $data = $request->getPost();

        $login = $data['login'];
        $user = $this->_getUserDetails($login);
        $em = $this->getEm();

        $capctha = !is_null($user) && $user->getAttempts() >= 3 ? true : false;

        if(is_string($this->form))
            $form = new $this->form(($capctha ? $this->getRequest()->getBaseUrl().'/data/captcha/' : null));
        else 
            $form = $this->form;
        
        $errors = null;
        if($request->isPost()){
            
            $form->setData($data);
            
            if ($form->isValid()) {
                $data = $form->getData();
                
                $userPassword = new UserPassword();
                $encyptPass = $userPassword->create($data['password']);
                
                $authService = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');

                $adapter = $authService->getAdapter();
                if (strpos($login, '@') !== false) {
                    // use email identity property
                    $adapter->getOptions()->setIdentityProperty('email');
                } else {
                    // use username identity property
                    $adapter->getOptions()->setIdentityProperty('username');
                }

                $adapter->setIdentityValue($data['login']);
                $adapter->setCredentialValue($encyptPass);
                $authResult = $authService->authenticate();
                
                if ($authResult->isValid()) {
                    $this->flashMessenger()->addSuccessMessage('Logado com successo');
                    //Remember
                    if($data['rememberme']){
                        $sessionManager = new SessionManager();
                        $sessionManager->rememberMe(2419200);
                    }
                    //Zera as tentativas
                    $user->setAttempts(0);
                    $em->persist($user);
                    $em->flush();
                    
                    if(isset($_SESSION['URL_REDIRECT'])){
                        $this->redirect()->toUrl($_SESSION['URL_REDIRECT']);
                        unset($_SESSION['URL_REDIRECT']);
                    }
                    return $this->redirect()->toRoute('adm');
                } else {
                    if(is_object($user)) {
                        $user->setAttempts($user->getAttempts() + 1);
                        $em->persist($user);
                        $em->flush();

                    }
                    $this->flashMessenger()->addErrorMessage('Credenciais inválidas');
                }
            } else {
                $errors = $form->getMessages();
                foreach($errors as $key=>$messages){
                    $message = '';
                    if(!is_numeric($key))
                        $message .= '<b>' . $key . '</b>: ';
                    
                    foreach($messages as $message){
                        $message .=  $message . '<br>';
                    }
                    if($message != '')
                        $this->flashMessenger()->addErrorMessage($message);
                }
            }
        }

        return new ViewModel(array('form'=>$form, 'captcha'=>$capctha));
       
    }
    
    public function recoverAction()
    {
        $this->form = 'Login\Form\RecoverForm';
        if(is_string($this->form))
            $form = new $this->form();
        else 
            $form = $this->form;
        
        $request = $this->getRequest();
        $data = $request->getPost();
        
        if($request->isPost()){
            
            $form->setData($data);
            
            if ($form->isValid()) {
                $data = $form->getData();
                
                $login = $data['login'];
                $user = $this->_getUserDetails($login);
                if(is_object($user)){
                    $em = $this->getEm();
                    $userPassword = new UserPassword();
                    $token = $userPassword->create(time());
                    
                    $user->setToken($token);
                    $em->persist($user);
                    $em->flush();
                    
                    $setup = $em->getRepository('Base\Entity\Setup')->findOneBy(array('active'=>1));
                    $url = $this->url()->fromRoute('newpassword', array(), array('query' => array('id'=>$token), 'force_canonical' => true));
                    $mensagem  = ''
                            . '<p>Para alterar sua senha clique no link abaixo:</p>'
                            . '<a href="'.$url.'">'.$url.'</a>'
                            . '<p>Caso não tenha feito essa solicitação, recomendamos atenção, pois alguém solicitou por você.</p>';
                    
                    $mail = new \Base\Util\Mail();
                    $envio = $mail->sendMail('naoresponder@'.$request->getUri()->getHost(), $setup->getSiteName(), $user->getEmail(), $user->getName(), false, false, 'Recuperar Senha', $mensagem, false, $em, $this->getServiceLocator()->get('config'));
                    if($envio) {
                        $this->flashMessenger()->addSuccessMessage('Você receberá um e-mail para recuperar sua senha');
                    }else{
                        $this->flashMessenger()->addErrorMessage('Ocorreu um erro com o envio do email');
                    }

                    return $this->redirect()->toRoute('login');
                }else{
                    $this->flashMessenger()->addErrorMessage('Usuário não encontrado!');
                }
            }else{
                $this->flashMessenger()->addErrorMessage('Usuário não encontrado!');
            }
        }
        
        return new ViewModel(array('form'=>$form));

    }
    
    public function newpasswordAction(){
        $this->form = 'Login\Form\NewPasswordForm';
        if(is_string($this->form))
            $form = new $this->form();
        else 
            $form = $this->form;
        
        $user = $this->getEm()->getRepository('Login\Entity\User')->findOneBy(array('token'=>$this->params()->fromQuery('id')));
        if(!is_object($user)){
            $this->flashMessenger()->addErrorMessage('Link inválido!');
            return $this->redirect()->toRoute('login');
        }
        
        $request = $this->getRequest();
        $data = $request->getPost();
        if($request->isPost()){
            
            $form->setData($data);
            
            if ($form->isValid()) {
                $em = $this->getEm();
                $userPassword = new UserPassword();
                $pass = $userPassword->create($data['password']);
                
                try{
                    $user->setPassword($pass);
                    $user->setToken('');
                    $em->persist($user);
                    $em->flush();
                    $this->flashMessenger()->addSuccessMessage('Sua senha foi alterada com sucesso!');
                    return $this->redirect()->toRoute('login');
                } catch (Exception $ex) {
                    $this->flashMessenger()->addErrorMessage('Ocorreu um erro ao tentar recuperar sua senha, tente novamente!');
                }
                

            }else{
                $this->flashMessenger()->addErrorMessage('Ocorreu um erro ao tentar recuperar sua senha, tente novamente!');
            }
        }
        
        return new ViewModel(array('form'=>$form));
    }
   
    public function logoutAction(){
        $authService = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
        
        $session = new Container('User');
        $session->getManager()->destroy();
        
        $authService->clearIdentity();
        
        $this->flashMessenger()->addSuccessMessage('Logout realizado com successo');
        
        $this->redirect()->toRoute($this->route);
    }
    
    private function _getUserDetails($login)
    {
        $em = $this->getEm();
        if (strpos($login, '@') !== false) {
            $user = $em->getRepository('Login\Entity\User')->findOneBy(array('email'=>$login));
        }else{
            $user = $em->getRepository('Login\Entity\User')->findOneBy(array('username'=>$login));
        }
        return $user;
    }

    public function aditionalParameters(){
        return array();
    }
}
