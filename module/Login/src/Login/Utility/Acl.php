<?php
namespace Login\Utility;

use Zend\Permissions\Acl\Acl as ZendAcl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Acl extends ZendAcl implements ServiceLocatorAwareInterface
{

    const DEFAULT_ROLE = 'guest';

    protected $_roleTableObject;

    protected $serviceLocator;

    protected $roles;

    protected $permissions;

    protected $resources;

    protected $rolePermission;

    protected $commonPermission;
    
    //acoes
    public static $LER = array("index", "list");
    public static $EXCLUIR = array("activeset", "delete");
    public static $EDITAR = array("edit");
    public static $ESCREVER = array("insert");
    //rotinas
    //JORNAL
    public static $PUBLICIDADE = 'Admin\Controller\Publicidade';
    public static $NOTICIA = 'Admin\Controller\Noticia';
    public static $CATEGORIA = 'Admin\Controller\Categoria';
    
    //AFILIADO ADMIN
    public static $PLANO = 'Admin\Controller\Plano';
    public static $AFILIADO = 'Admin\Controller\Afiliado';
    public static $AFILIADOCONFIG = 'Admin\Controller\Afiliadoconfig';
    public static $AFILIADOPAGAMENTO = 'Admin\Controller\Afiliadopagamento';
    public static $AFILIADODISTRIBUICAO = 'Admin\Controller\Afiliadodistribuicao';
    
    //AFILIADO
    public static $UPGRADE = 'Admin\Controller\Upgrade';
    public static $MINHAEQUIPE = 'Admin\Controller\Minhaequipe';
    public static $EXTRATOCONTA = 'Admin\Controller\Extratoconta';
    public static $SAQUE = 'Admin\Controller\Saqueafiliado';
    public static $RENOVACAO = 'Admin\Controller\Renovacao';
    
    //GENERICOS
    public static $PESSOAFISICA = 'Admin\Controller\Pessoafisica';
    public static $PESSOAJURIDICA = 'Admin\Controller\Pessoajuridica';
    public static $TEXTO = 'Admin\Controller\Texto';
    
    //FINANCEIRO
    public static $GASTOGASOLINA = 'Admin\Controller\Gastogasolina';
    public static $VEICULO = 'Admin\Controller\Veiculo';
    public static $CONTA = 'Admin\Controller\Conta';
    public static $CONTA_PAGAR = 'Admin\Controller\Contapagar';
    public static $CONTA_RECEBER = 'Admin\Controller\Contareceber';
    public static $CATEGORIA_CONTA = 'Admin\Controller\Categoriaconta';

    //OUTROS
    public static $RASTREAMENTO = 'Admin\Controller\Rastreamento';
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        
        return $this;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function initAcl()
    {
        $this->roles = $this->_getAllRoles();
        $this->resources = $this->_getAllResources();
        $this->rolePermission = $this->_getRolePermissions();
        // we are not putting these resource & permission in table bcz it is
        // common to all user
        $this->commonPermission = array(
            'Login\Controller\Index' => array(
                'logout',
                'index'                
            )
        );
        $this->_addRoles()
            ->_addResources()
            ->_addRoleResources();
    }
    
    public function autorizado($roles, $resource, $permission){
        $return = true;
        foreach($permission as $perm){
            if(!$this->isAccessAllowed($roles, $resource, $perm))
                $return = false;
        }
        return $return;
    }
    
    public function isAccessAllowed($roles, $resource, $permission)
    {
        if (! $this->hasResource($resource)) {
            return false;
        }
        foreach($roles as $role){
            if($role->getRoleName() == 'Master' || $this->isAllowed($role->getRoleName(), $resource, $permission)){
                return true;
            }
        }
        return false;
    }

    protected function _addRoles()
    {
        $this->addRole(new Role(self::DEFAULT_ROLE));
        
        if (! empty($this->roles)) {
            foreach ($this->roles as $role) {
                $roleName = $role['role_name'];
                if (! $this->hasRole($roleName)) {
                    $this->addRole(new Role($roleName), self::DEFAULT_ROLE);
                }
            }
        }
        return $this;
    }

    protected function _addResources()
    {
        if (! empty($this->resources)) {
            foreach ($this->resources as $resource) {
                if (! $this->hasResource($resource['resource_name'])) {
                    $this->addResource(new Resource($resource['resource_name']));
                }
            }
        }
        
        // add common resources
        if (! empty($this->commonPermission)) {
            foreach ($this->commonPermission as $resource => $permissions) {
                if (! $this->hasResource($resource)) {
                    $this->addResource(new Resource($resource));
                }
            }
        }
        
        return $this;
    }

    protected function _addRoleResources()
    {
        // allow common resource/permission to guest user
        if (! empty($this->commonPermission)) {
            foreach ($this->commonPermission as $resource => $permissions) {
                foreach ($permissions as $permission) {
                    $this->allow(self::DEFAULT_ROLE, $resource, $permission);
                }
            }
        }
        
        if (! empty($this->rolePermission)) {
            foreach ($this->rolePermission as $rolePermissions) {
                $this->allow($rolePermissions['roleName'], $rolePermissions['resourceName'], $rolePermissions['permissionName']);
            }
        }
        
        return $this;
    }

    protected function _getAllRoles()
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $lista = $em->getRepository('Login\Entity\Role')->findBy(array('active'=>1));
        $retorno = array();
        foreach($lista as $role){
            array_push($retorno, $role->toArray());
        }
        return $retorno;
    }

    protected function _getAllResources()
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $lista = $em->getRepository('Login\Entity\Resource')->findBy(array('active'=>1));
        $retorno = array();
        foreach($lista as $resource){
            array_push($retorno, $resource->toArray());
        }
        return $retorno;
    }

    protected function _getRolePermissions()
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        return $em->getRepository('Login\Entity\RolePermission')->getRolePermissions();
    }
    
    private function debugAcl($role, $resource, $permission)
    {
        echo 'Role:-' . $role . '==>' . $resource . '\\' . $permission . '<br/>';
    }
}
