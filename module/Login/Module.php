<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Login;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Login\Entity\WhitelistRepository;
use Zend\Authentication\AuthenticationService;
use Login\Utility\Acl;
use Base\Util\Mail;
use Zend\View\Helper\FlashMessenger;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $eventManager->attach(MvcEvent::EVENT_DISPATCH, array(
            $this,
            'beforeDispatch'
        ), 100);
        
    }
    
    function beforeDispatch(MvcEvent $event)
    {
        $request = $event->getRequest();
        $response = $event->getResponse();
        $target = $event->getTarget();
        $eventManager = $event->getApplication()->getEventManager();
        
        
        $serviceManager = $event->getApplication()->getServiceManager();
        $authenticationService = $serviceManager->get('Zend\Authentication\AuthenticationService');
        $acl = $serviceManager->get('Acl');       
        
                
        $whiteList = $this->_getAllWhitelist($event);
        $controller = $event->getRouteMatch()->getParam('controller');
        $action = $event->getRouteMatch()->getParam('action');
        $requestedResourse = $controller . "-" . $action;
        if ($authenticationService->hasIdentity()) {
            if(in_array($requestedResourse, $whiteList)){

            }else if ($controller == 'Login\Controller\Index') {
                $flashMessenger = new FlashMessenger;
                $flashMessenger->addWarningMessage('Você já está logado!');
                $url = $event->getRequest()->getBaseUrl().'/adm';
                $response->setHeaders($response->getHeaders()
                    ->addHeaderLine('Location', $url));
                $response->setStatusCode(302);
                $response->sendHeaders();
                
            } else {
                
                $loggedUser = $authenticationService->getIdentity();
                
                $userRoles = $loggedUser->getRoles();

                $acl->initAcl();
                $event->getViewModel()->acl = $acl;
                $status = $acl->isAccessAllowed($userRoles, $controller, $action);
                if (! $status) {
                    $url = $event->getRouter()->assemble(array(), array('name' => 'permissiondenied'));
                    $response->setHeaders($response->getHeaders()
                        ->addHeaderLine('Location', $url));
                    $response->setStatusCode(302);
                }
            }
        } else {
            if ($controller != 'Login\Controller\Index' && !in_array($requestedResourse, $whiteList)) {
                $urlRedirect = $request->getUri()->getScheme().'://'.$request->getUri()->getHost().$request->getRequestUri();        
                $_SESSION['URL_REDIRECT'] = $urlRedirect;
        
                $flashMessenger = new FlashMessenger;
                $flashMessenger->addWarningMessage('Você não está logado!');
                $url = $event->getRequest()->getBaseUrl().'/login';
                $response->setHeaders($response->getHeaders()
                    ->addHeaderLine('Location', $url));
                $response->setStatusCode(302);
                $response->sendHeaders();
            }else{
                $acl->initAcl();
                $event->getViewModel()->acl = $acl;
            }
            
        }
    }
    
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig(){
        return array(
            'factories' => array(
                'Zend\Authentication\AuthenticationService' => function($serviceManager) {
                    return $serviceManager->get('doctrine.authenticationservice.orm_default');
                },
                'Acl' => function ($serviceManager)
                {
                    return new Acl();
                },
                'Email' => function ($serviceManager)
                {
                    return new Mail();
                },
            ),
        );
    }
    
    protected function _getAllWhitelist($event){
        $serviceManager = $event->getApplication()->getServiceManager();
        $em = $serviceManager->get('Doctrine\ORM\EntityManager');
        $lista = $em->getRepository('Login\Entity\Whitelist')->findBy(array('active'=>1));
        $retorno = array();
        foreach($lista as $whitelist){
            array_push($retorno, $whitelist->getWhiteListName());
        }
        
        return $retorno;
    }
}
