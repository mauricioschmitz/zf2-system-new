<?php

namespace Blog\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;
use Base\Util\Mail;

class IndexController extends AbstractController
{
    public function __construct() {
        $this->form = 'Blog\Form\Portfolio';
        $this->controller = 'Blog';
        $this->route = 'blog/default';
        $this->service = 'Blog\Service\BlogService';
        $this->entity = 'Blog\Entity\Blog';
    }
    public function indexAction()
    {
        return new ViewModel();
    }
    
    public function aditionalParameters() {

    }
    
    public function contatoAction(){
        $request = $this->getRequest();
        
        if($request->isPost()){
            
            $mail = $this->getServiceLocator()->get('Email');
            $htmlContent = '<div>';
            $fields = array('name' => 'Name', 'surname' => 'Surname', 'phone' => 'Phone', 'email' => 'Email', 'message' => 'Message');
            $required = array('name','surname', 'email', 'message');
            $okMessage = 'Formulário de contato enviado com sucesso. Obrigado, eu vou responder para você em breve!';
            $errorMessage = 'Ocorreu um erro, tente novamente!';
            $post = $request->getPost()->toArray();
            foreach ($post as $key => $value) {
                if(in_array($fields[$key],$required) && $value == ''){
                    echo json_encode(array('type' => 'error', 'message' => $errorMessage));
                    die();
                }
                if (isset($fields[$key])) {
                    $htmlContent .= "<div><b>".$fields[$key]."</b>:".$value."</div>";
                }
            }
            $htmlContent .= '</div>';
            if($mail->sendMail($post['email'], $post['name'], 'contato@mauricioschmitz.com.br', 'Mauricio Schmitz', false, false, 'Mensagem enviada pelo site',  $htmlContent, $_FILES, $this->getEm())){
                $responseArray = array('type' => 'success', 'message' => $okMessage);
                
            }else{
                $responseArray = array('type' => 'error', 'message' => $errorMessage);
            }
            
            echo json_encode($responseArray);
            die();
        }
    }
}
