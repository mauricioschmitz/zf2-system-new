$(function () {
    var paraTag = $('input#submit').parent('p');
    $(paraTag).children('input').remove();
    $(paraTag).append('<input type="button" name="submit" id="submit" value="Enviar mensagem" class="buttoncontact" />');

    $('#main input#submit').click(function () {
        $('#main').append('<img src="'+baseUrl + '/political/images/loader.gif" alt="" />');

        var name = $('input#cf_name').val();
        var email = $('input#cf_email').val();
        var subject = $('input#cf_subject').val();
        var comments = $('textarea#cf_message').val();

        $.ajax({
            type: 'post',
            url: baseUrl + '/contato',
            data: 'cf_name=' + name + '&cf_email=' + email + '&cf_subject=' + subject + '&cf_message=' + comments,
            success: function (results) {
                $('#main img').fadeOut(1000);
                $('ul#response').html(results);
            }
        });
    });
});
		