// newsletter
(function ($) {
    "use strict";

    $(function () {
        // Sticky and Go-top

        (function ($, window) {

            function Temp(el, options) {
                this.el = $(el);
                this.init(options);
            }

            Temp.DEFAULTS = {
                sticky: true
            }

            Temp.prototype = {
                init: function (options) {
                    var base = this;
                    base.window = $(window);
                    base.options = $.extend({}, Temp.DEFAULTS, options);
                    base.menuWrap = $('.nav-content');
                    base.goTop = $('<button class="go-to-top" id="go-to-top"></button>').appendTo(base.el);

                    // Sticky
                    if (base.options.sticky) {
                        base.sticky.stickySet.call(base, base.window);
                    }

                    // Scroll Event
                    base.window.on('scroll', function (e) {
                        if (base.options.sticky) {
                            base.sticky.stickyInit.call(base, e.currentTarget);
                        }
                        base.gotoTop.scrollHandler.call(base, e.currentTarget);
                    });

                    // Click Handler Button GotoTop
                    base.gotoTop.clickHandler(base);
                },
                sticky: {
                    stickySet: function () {
                        var menuWrap = this.menuWrap, offset;
                        if (menuWrap.length) {
                            offset = menuWrap.offset().top;
                            $.data(menuWrap, 'data', {
                                offset: offset,
                                height: menuWrap.outerHeight(true)
                            });
                            this.spacer = $('<div/>', {'class': 'spacer'}).insertBefore(menuWrap);
                        }
                    },
                    stickyInit: function (win) {
                        var base = this, data;
                        if (base.menuWrap.length) {
                            data = $.data(base.menuWrap, 'data');
                            base.sticky.stickyAction(data, win, base);
                        }
                    },
                    stickyAction: function (data, win, base) {
                        var scrollTop = $(win).scrollTop();
                        if (scrollTop > data.offset) {
                            base.spacer.css({height: data.height});
                            if (!base.menuWrap.hasClass('sticky')) {
                                base.menuWrap.addClass('sticky');
                            }
                        } else {
                            base.spacer.css({height: 'auto'});
                            if (base.menuWrap.hasClass('sticky')) {
                                base.menuWrap.removeClass('sticky');
                            }
                        }
                    }
                },
                gotoTop: {
                    scrollHandler: function (win) {
                        $(win).scrollTop() > 200 ?
                                this.goTop.addClass('go-top-visible') :
                                this.goTop.removeClass('go-top-visible');
                    },
                    clickHandler: function (self) {
                        self.goTop.on('click', function (e) {
                            e.preventDefault();
                            $('html, body').animate({scrollTop: 0}, 800);
                        });
                    }
                }
            }

            /* Temp Plugin
             * ================================== */

            $.fn.Temp = function (option) {
                return this.each(function () {
                    var $this = $(this), data = $this.data('Temp'),
                            options = typeof option == 'object' && option;
                    if (!data) {
                        $this.data('Temp', new Temp(this, options));
                    }
                });
            }

            $('body').Temp({
                sticky: true
            });

        })(jQuery, window);

        var subscribe = $('[id^="newsletter"]');
        subscribe.append('<div class="message_container_subscribe"></div>');
        var message = $('.message_container_subscribe'), text;

        subscribe.on('submit', function (e) {
            var self = $(this);

            if (self.find('input[type="email"]').val() == '') {
                text = "Por favor informe seu e-mail!";
                message.html('<div class="alert_box warning"><p>' + text + '</p></div>')
                        .slideDown()
                        .delay(4000)
                        .slideUp(function () {
                            $(this).html("");
                        });

            } else {
                self.find('span.error').hide();
                $.ajax({
                    type: "POST",
                    url: baseUrl + "/newsletter",
                    data: self.serialize(),
                    success: function (data) {
                        if (data == '1') {
                            text = "Seu e-mail foi enviado com sucesso!";
                            message.html('<div class="alert_box success"><p>' + text + '</p></div>')
                                    .slideDown()
                                    .delay(4000)
                                    .slideUp(function () {
                                        $(this).html("");
                                    })
                                    .prevAll('input[type="email"]').val("");
                        } else {
                            text = "E-mail inválido ou já cadastrado!";
                            message.html('<div class="alert_box error"></i><p>' + text + '</p></div>')
                                    .slideDown()
                                    .delay(4000)
                                    .slideUp(function () {
                                        $(this).html("");
                                    });
                        }
                    }
                });
            }
            e.preventDefault();
        });

        var cf = $('#contactform');
        cf.append('<div class="message_container"></div>');

        cf.on("submit", function (event) {

            var self = $(this), text;
            self.find('#spin').removeClass('hidden');
            var request = $.ajax({
                url: baseUrl + "/contato",
                type: "post",
                data: self.serialize()
            });

            request.then(function (data) {
                self.find('#spin').addClass('hidden');
                if (data == "1") {

                    text = "Seu e-mail foi enviado com sucesso!";

                    cf.find('input:not([type="submit"]),textarea').val('');

                    $('.message_container').html('<div class="alert_box success"><i class="fa fa-smile-o"></i><p>' + text + '</p></div>')
                            .delay(150)
                            .slideDown(300)
                            .delay(4000)
                            .slideUp(300, function () {
                                $(this).html("");
                            });

                } else {
                    if (cf.find('textarea').val().length < 20) {
                        text = "A memsagem deve conter pelo menos 20 caracteres!"
                    }
                    if (cf.find('input').val() == "") {
                        text = "Campos obrigatórios devem ser preenchidos!";
                    }
                    $('.message_container').html('<div class="alert_box error"><i class="fa fa-exclamation-triangle"></i><p>' + text + '</p></div>')
                            .delay(150)
                            .slideDown(300)
                            .delay(4000)
                            .slideUp(300, function () {
                                $(this).html("");
                            });
                }
            }, function () {
                $('.message_container').html('<div class="alert_box error"><i class="fa fa-exclamation-triangle"></i><p>Ocorreu um erro ao enviar, tente novamente!</p></div>')
                        .delay(150)
                        .slideDown(300)
                        .delay(4000)
                        .slideUp(300, function () {
                            $(this).html("");
                        });
            });


            event.preventDefault();
        });
    });

    $('.fb-share, .tw-share').click(function (e) {
        e.preventDefault();
        window.open($(this).attr('href'), 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
        return false;
    });

    var sliderNav = $('.slick-class');

    var maxItems = 15;
    if(sliderNav.children('div').length < maxItems) {
        maxItems = sliderNav.children('div').length;
    }
    $('.slick-class').slick({
        infinite: true,
        slidesToShow: maxItems-1,
        slidesToScroll: 1,
        vertical: true,
        autoplay: true,
        autoplaySpeed: 3000
    });
})(jQuery);