/***
 * Javascript para loading in tela
 */

var Scripts = [];

Scripts.toastrAlerts = function () {
    // Toastr options
    if (typeof toastr !== 'undefined') {
        toastr.options = {
            "debug": false,
            "newestOnTop": false,
            "positionClass": "toast-top-center",
            "closeButton": true,
            "toastClass": "animated fadeInDown",
        };
    }

};

Scripts.startValidate = function () {
    $("form.validate").each(function (index) {
        Scripts.validar($(this));
    });
};

Scripts.validar = function (target){
    $(target).validate({
        ignore: ":not(:visible)",
        highlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";
            $(element).closest('.input-group, .form-group, .control-group').removeClass('has-success').addClass('has-error');
            $(element).closest('.input-group, .form-group, .control-group').addClass('required');
            //$(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";
            $(element).closest('.input-group, .form-group, .control-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.input-group, .form-group, .control-group').removeClass('required');
            //$(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block input-error',
        errorPlacement: function (error, element) {
            var id_attr = "#" + $(element).attr("id");
            if (element.length) {
                /*
                 ** Verifica se o pai do elemento possui
                 ** a classe input-group, adicionando o elemento fora da div
                 ** de span e input
                 */
                if ($(id_attr).parent().parent().parent().hasClass("input-group")) {
                    error.insertAfter(element.parent().parent());
                } else if ($(id_attr).parent().parent().hasClass("input-group")) {
                    error.insertAfter(element.parent().parent());
                } else if ($(id_attr).parent().hasClass("input-group")) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            } else {
                /*
                 ** Verifica se o pai do elemento possui
                 ** a classe input-group, adicionando o elemento fora da div
                 ** de span e input
                 */
                if ($(id_attr).parent().hasClass("input-group")) {
                    error.insertAfter(element.parent);
                } else {
                    error.insertAfter(element());
                }
            }
//            $( element )
//                .closest( "form" )
//                .find( "label[for='" + element.attr( "id" ) + "']" )
//                .append( error );
        }
    });
};


/*
 * Função de envio de arquivos do Summernote
 */
Scripts.sendFileSummernote = function (file, url, editor) {
    var data = new FormData();
    data.append("file", file);
    $('div.progress-editor').fadeIn();
    $.ajax({
        data: data,
        type: "POST",
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        xhr: function () {
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload)
                myXhr.upload.addEventListener('progress', Scripts.progressHandling, false);
            return myXhr;
        },
        success: function (objFile) {
            editor.summernote('insertImage', objFile.resposta);
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
};

/*
 * Galeria do editor
 */
Scripts.galeriaEditor = function () {
    Scripts.showLoading();
    $.ajax({
        method: "GET",
        url: baseUrl + 'galeriaeditor',
        data: ''
    })
            .done(function (msg) {
                $('.galeriaEditor').html(msg);
                $('.galeriaEditor').fadeIn();
                Scripts.hideLoading();
            });
};


/*
 * Progress bar
 */
Scripts.progressHandling = function (e) {
    if (e.lengthComputable) {
        c = e.loaded;
        t = e.total;

        per = (c * 100) / t;


        $('div.my-progress').css({'width': per + '%'});
        $('div.my-progress').html(per + '%');
        // reset progress on complete
        if (e.loaded == e.total) {
            setTimeout(function () {
                $('div.progress-editor').fadeOut();
            }, 3000);
        }
    }
};

Scripts.uploadMultiple = function (url, extensions, previews, previewsConfig) {
    $("#files").fileinput({
        language: "pt-BR",
        uploadUrl: url,
        allowedFileExtensions: extensions,
        uploadAsync: true,
        uploadExtraData: function (previewId, index) {
            if ($(".kv-upload-progress div.progress-bar").attr("aria-valuenow") == 100) {
                //window.location.reload();
                $('button[type="submit"]').removeClass('disabled').attr({'data-toggle': '', 'title': ''});
                $('button[type="submit"]').tooltip('destroy');
            }

            Scripts.imagemCapa();
            return {key: index};
        },
        initialPreview: previews,
        initialPreviewConfig: previewsConfig,
        overwriteInitial: false
    });

    $('#files').on('fileselect', function (event, numFiles, label) {
        $('button[type="submit"]').addClass('disabled').attr({'data-toggle': 'tooltip', 'title': 'Existem imagens não enviadas'});
        $('button[type="submit"]').tooltip();

    });
};

/**
 * Função de loading
 */
Scripts.showLoading = function () {

    $('#loading').show();
    $('#blockLoading').show();

};

Scripts.hideLoading = function () {

    $('#loading').hide();
    $('#blockLoading').hide();

};

Scripts.InitMasks = function () {
    $(".moeda").maskMoney({allowZero: true});
    $(".cep").mask("99999-999");
    $(".cpf").mask("999.999.999-99");
    $(".cnpj").mask("99.999.999/9999-99");
    $("input.hora").mask("99:99:99");
    $("input.hora-min").mask("99:99");
    $("input.data").mask("99/99/9999");
    $("input.data-hora").mask("99/99/9999 99:99");
    $('.datetime').mask('99/99/9999 99:99');
    $('.date').mask('99/99/9999');
    $(".telefone").keyup(function () {
        Scripts.mascara(document.getElementById($(this).attr('id')), Scripts.mtel);
    });
    $(".telefone").prop('maxlength', 15);

    $(".celular").keyup(function () {
        Scripts.mascara(document.getElementById($(this).attr('id')), Scripts.mtel);
    });
    $(".celular").prop('maxlength', 15);

    $('.cep').change(function () {
        Scripts.BuscaCep($(this).val());
    });
};

Scripts.BuscaCep = function (cep) {
    $.ajax({
        data: null,
        type: "GET",
        url: 'https://api.postmon.com.br/v1/cep/' + cep,
        cache: false,
        contentType: false,
        processData: false,
        xhr: function () {
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload)
                myXhr.upload.addEventListener('progress', Scripts.progressHandling, false);
            return myXhr;
        },
        success: function (data) {
            $('.uf').val(data.estado);
            $('.cidade').val(data.cidade);
            $('.endereco').val(data.logradouro);
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
};

Scripts.mascara = function (o, f) {
    v_obj = o;
    v_fun = f;
    setTimeout(Scripts.execmascara(), 1);
};

Scripts.execmascara = function () {
    v_obj.value = v_fun(v_obj.value);
};

Scripts.mtel = function (v) {
    v = v.replace(/\D/g, ""); //Remove tudo o que não é dígito
    v = v.replace(/^(\d{2})(\d)/g, "($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
    v = v.replace(/(\d)(\d{4})$/, "$1-$2"); //Coloca hífen entre o quarto e o quinto dígitos
    return v;
};

/**
 * Função de Ajax Modal
 */
Scripts.ajaxModal = function () {
    $('.ajaxModal').unbind('click');
    $('.ajaxModal').on('click', function () {

        target = $(this).attr('data-target');
        $('.modal-content').html('<div class="color-line"></div>');
        $('.modal-content').hide();
        Scripts.showLoading();

        url = $(this).attr('data-href');
        var modalClass = $(this).attr('big-modal');

        if (modalClass)
            $('.modal-dialog').addClass('modal-lg');
        else
            $('.modal-dialog').removeClass('modal-lg');

        var urlAppend = $(this).attr('url-append');
        var urlAppendRedirector = $(this).attr('url-append-redirector');
        if (urlAppend) {
            url += urlAppend;
        }
        if (urlAppendRedirector) {
            url += '?urlRedirector=' + urlAppendRedirector;
        }
        $.ajax({
            method: "GET",
            url: url,
            data: ''
        })
                .done(function (msg) {
                    $(target + ' .modal-content').append(msg);
                    $(target + ' .modal-content').show();
                    Scripts.hideLoading();
                });
    });
};

$(document).ready(function () {
    Scripts.toastrAlerts();
    Scripts.startValidate();
    Scripts.ajaxModal();

    var clipboard = new Clipboard('.btn');

    clipboard.on('success', function (e) {
        //showTooltip(e.trigger, 'Copiado!');
    });

    $('.datetime').datetimepicker({
        locale: 'pt-BR'
    });

    $('.date').datepicker({
        language: 'pt-BR',
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('body').append('<div id="loading" style="display:none;"><img src="' + baseUrl + '/images/ajax-loader.gif"></div>');
    $('body').append('<div id="blockLoading" style="display:none;"></div>');

    $('.fb-share, .tw-share').click(function (e) {
        e.preventDefault();
        window.open($(this).attr('href'), 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
        return false;
    });
});